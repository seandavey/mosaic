package work;

import java.time.LocalDate;

import db.DBConnection;
import db.Rows;
import util.Text;
import web.HTMLWriter;

class State {
	int period;
	int year;
	double balance;
	double given;
	double net;
	double received;
	Work work;
	double worked;

	//--------------------------------------------------------------------------

	State(LocalDate date, int people_id, Work work, DBConnection db) {
		this.work = work;
		int this_period = date.getMonthValue();
		if (work.m_quarterly)
			this_period = (this_period - 1) / 3 + 1;
		int this_year = date.getYear();
		double required_hours = work.requiredHours(people_id, db);
		Rows rows = Work.getRowsForBalances(people_id, work.m_quarterly, db);
		while (rows.next()) {
			year = rows.getInt(1);
			period = rows.getInt(2);
			if (year > this_year || year == this_year && period > this_period)
				break;
			worked = rows.getDouble(3);
			given = rows.getDouble(4);
			received = rows.getDouble(5);
			net = work.net(worked, given, received, required_hours);
			balance = work.balance(balance + net);
		}
		rows.close();
		while (year <= this_year && (year < this_year || period < this_period)) {
			net = worked = given = received = 0;
			balance = work.balance(balance);
			period++;
			if (period == (work.m_quarterly ? 5 : 13)) {
				year++;
				period = 1;
			}
		}
	}

	//--------------------------------------------------------------------------

	void
	write(HTMLWriter w) {
		w.write("Your current work bank balance is ")
			.write(balance)
			.space()
			.write(Text.pluralize("hour", balance))
			.write(". This ")
			.write(work.m_quarterly ? "quarter" : "month")
			.write(" you have worked ")
			.write(worked)
			.space()
			.write(Text.pluralize("hour", worked));
		if (work.m_required_hours_per_period > 0)
			w.write(" (")
				.write(work.m_required_hours_per_period)
				.write(" required)");
		w.write(", given ")
			.write(given)
			.space()
			.write(Text.pluralize("hour", given))
			.write(", and received ")
			.write(received)
			.space()
			.write(Text.pluralize("hour", received))
			.write(", for a net of ")
			.write(net)
			.space()
			.write(Text.pluralize("hour", net))
			.write(".");
	}

	//--------------------------------------------------------------------------

	void
	writeGiftButton(HTMLWriter w) {
		w.ui.buttonOnClick("Give some hours as a gift", "new Dialog({cancel:true,ok:true,on_complete:function(){app.page_menu_select('Work Hours')},title:'Give Hours',url:context+'/Work/gift_form?balance=" + balance + "'}).open()", false);
	}
}
