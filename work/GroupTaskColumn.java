package work;

import java.util.Map;

import app.Request;
import db.Form;
import db.Rows;
import db.RowsSelect;
import db.Select;
import db.View;
import db.View.Mode;
import db.column.ColumnBase;
import web.HTMLWriter;

class GroupTaskColumn extends ColumnBase<GroupTaskColumn> {
	private boolean m_track_hours_by_group_not_task;

	//--------------------------------------------------------------------------

	public
	GroupTaskColumn(String name, boolean track_hours_by_group_not_task) {
		super(name);
		m_track_hours_by_group_not_task = track_hours_by_group_not_task;
		m_display_name = "task";
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getOrderBy() {
		return "(SELECT name FROM groups WHERE groups.id=(SELECT groups_id FROM work_tasks WHERE work_tasks.id=work_tasks_id)),(SELECT task FROM work_tasks WHERE work_tasks.id=work_tasks_id)";
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getOrderByDesc() {
		return "(SELECT name FROM groups WHERE groups.id=(SELECT groups_id FROM work_tasks WHERE work_tasks.id=work_tasks_id)) DESC,(SELECT task FROM work_tasks WHERE work_tasks.id=work_tasks_id) DESC";
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, boolean inline, Request r) {
		HTMLWriter w = r.w;
		w.setId("group_select");
		RowsSelect rows_select;
		if (m_track_hours_by_group_not_task)
			rows_select = new RowsSelect("work_tasks_id", new Select("work_tasks.id id,name").distinct().from("work_tasks").joinOn("groups", "groups.id=groups_id").where("work_tasks.active").orderBy("name"), "name", "id", r);
		else {
			rows_select = new RowsSelect(null, new Select("id,name").from("groups").where("id IN (SELECT DISTINCT groups_id FROM work_tasks WHERE active)").orderBy("name"), "name", "id", r);
			if (rows_select.size() == 0) {
				new RowsSelect("work_tasks_id", new Select("id,task").from("work_tasks").where("active").orderBy("task"), "task", "id", r)
					.setAllowNoSelection(true)
					.setInline(true)
					.write(r.w);
				return;
			}
		}
		int work_tasks_id = 0;
		if (v.getMode() == Mode.EDIT_FORM) {
			work_tasks_id = v.data.getInt("work_tasks_id");
			if (m_track_hours_by_group_not_task)
				rows_select.setSelectedOption(null, Integer.toString(work_tasks_id));
			else
				rows_select.setSelectedOption(null, r.db.lookupString(new Select("groups_id").from("work_tasks").whereIdEquals(work_tasks_id)));
		}
		rows_select.setAllowNoSelection(true)
			.setInline(true)
			.write(r.w);
		if (!m_track_hours_by_group_not_task) {
			w.nbsp();
			w.setId("group_tasks");
			w.ui.select(m_name, (String[])null, null, null, true);
			Rows tasks = new Rows(new db.Select("id,task,groups_id").from("work_tasks").where("active").orderBy("groups_id,task"), r.db);
			w.write("<script>_.$('#group_select').tasks = {");
			int current_group_id = 0;
			boolean first_task = true;
			while (tasks.next()) {
				String task = tasks.getString(2);
				if (task == null || task.length() == 0)
					continue;
				int group_id = tasks.getInt(3);
				if (group_id != current_group_id) {
					if (current_group_id != 0)
						w.write("],");
					w.write(group_id).write(":[");
					current_group_id = group_id;
					first_task = true;
				}
				if (first_task)
					first_task = false;
				else
					w.write(",");
				w.write("[").write(tasks.getString(1)).write(",").jsString(task).write("]");
			}
			tasks.close();
			w.write("""
					]}
				_.$('#group_select').set_tasks = function(id) {
					var g = _.$('#group_select')
					var s = _.$('#group_tasks')
					_.dom.empty(s)
					var i = g.options[g.selectedIndex].value
					if (!i)
						i = 0
					var t = g.tasks[i]
					if (t) {
						s.append($option())
						for (var i=0; i<t.length; i++) {
							var e = $option({value:t[i][0]}, t[i][1])
							if (id == t[i][0])
								e.selected = 'selected'
							s.append(e)
						}
					}
				};
				_.$('#group_select').addEventListener(\"change\", _.$('#group_select').set_tasks)
				_.$('#group_select').set_tasks(""").write(work_tasks_id).write(")</script>");
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		if (v.data.getInt("link") != 0) {
			r.w.write(v.data.getDouble("hours") > 0 ? "gift received" : "gift given");
			return true;
		}
		int task_id = v.data.getInt("work_tasks_id");
		if (task_id != 0) {
			Object[] row = r.db.readRowObjects(new Select("task,groups_id").from("work_tasks").whereIdEquals(task_id));
			if (row[1] != null) {
				r.w.write(r.db.lookupString(new Select("name").from("groups").whereIdEquals((int)row[1])));
				if (row[0] != null)
					r.w.write(": ");
			}
			if (row[0] != null)
				r.w.write(row[0]);
		}
		return true;
	}
}
