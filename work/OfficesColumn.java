package work;

import java.util.Map;

import app.Request;
import db.Rows;
import db.Select;
import db.View;
import db.column.ColumnBase;
import ui.Table;
import web.HTMLWriter;

public class OfficesColumn extends ColumnBase<OfficesColumn> {
	public
	OfficesColumn() {
		super("offices");
		m_is_computed = true;
		m_is_sortable = false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String, Object> data, Request r) {
		HTMLWriter w = r.w;
		Table table = w.ui.table().addClass("table-borderless").addClass("table-sm").addClass("table-striped");
		Select query = new Select("office,start_date").from("offices_history").joinOn("offices", "offices.id=offices_id").whereEquals("person", v.data.getInt("id")).andWhere("end_date IS NULL").orderBy("1");
		Rows rows = new Rows(query, r.db);
		while (rows.next()) {
			table.tr().td(rows.getString(1)).td();
			w.writeDate(rows.getDate(2));
		}
		rows.close();
		table.close();
		return true;
	}
}
