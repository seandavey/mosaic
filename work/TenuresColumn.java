package work;

import java.time.LocalDate;
import java.util.Map;

import app.Request;
import db.Rows;
import db.Select;
import db.View;
import db.View.Mode;
import db.column.ColumnBase;
import ui.Table;
import web.HTMLWriter;

public class TenuresColumn extends ColumnBase<TenuresColumn> {
	private boolean	m_show_num_people;
	private String	m_table;

	//--------------------------------------------------------------------------

	public
	TenuresColumn(String table) {
		super("tenures");
		m_table = table;
		m_is_computed = true;
		m_is_sortable = false;
	}

	//--------------------------------------------------------------------------

	public final TenuresColumn
	setShowNumPeople(boolean show_num_people) {
		m_show_num_people = show_num_people;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String, Object> data, Request r) {
		HTMLWriter w = r.w;
		Table table = w.ui.table().addClass("table-borderless").addClass("table-sm").addClass("table-striped");
		Select query = new Select("start_date,end_date,first,last").from(m_table + "_history").joinOn("people", "people.id=person").whereEquals(m_table + "_id", v.getID()).orderBy("1,3,4");
		if (v.getMode() == Mode.LIST || v.getMode() == Mode.READ_ONLY_LIST)
			query.andWhere("end_date IS NULL");
		Rows rows = new Rows(query, r.db);
		while (rows.next()) {
			LocalDate start_date = rows.getDate(1);
			LocalDate end_date = rows.getDate(2);
			table.tr().td();
			w.write(rows.getString(3)).space().write(rows.getString(4));
			table.td();
			w.writeDate(start_date);
			if (start_date != null || end_date != null)
				w.write(" - ");
			w.writeDate(end_date);
		}
		if (m_show_num_people) {
			int num_people = v.data.getInt("num_people_needed");
			if (num_people > 0)
				table.tr().td(rows.getRowCount() + " of " + num_people + " people needed");
		}
		rows.close();
		table.close();
		return true;
	}
}
