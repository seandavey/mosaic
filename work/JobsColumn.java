package work;

import java.util.Map;

import app.Request;
import db.Rows;
import db.Select;
import db.View;
import db.column.ColumnBase;
import ui.Table;
import web.HTMLWriter;

public class JobsColumn extends ColumnBase<JobsColumn> {
	public
	JobsColumn() {
		super("jobs");
		m_is_computed = true;
		m_is_sortable = false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String, Object> data, Request r) {
		HTMLWriter w = r.w;
		Table table = w.ui.table().addClass("table-borderless").addClass("table-sm").addClass("table-striped");
		Select query = new Select("job,start_date,(SELECT(name) FROM groups WHERE groups.id=groups_id) AS g").from("jobs_history").joinOn("jobs", "jobs.id=jobs_id").whereEquals("person", v.data.getInt("id")).andWhere("end_date IS NULL").orderBy("3,1");
		Rows rows = new Rows(query, r.db);
		while (rows.next()) {
			table.tr().td(rows.getString(3)).td(rows.getString(1)).td();
			w.writeDate(rows.getDate(2));
		}
		rows.close();
		table.close();
		return true;
	}
}
