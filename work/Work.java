package work;

import java.sql.Types;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

import app.ClassTask;
import app.Person;
import app.Request;
import app.Roles;
import app.Site;
import app.SiteModule;
import bootstrap5.ButtonNav;
import bootstrap5.ButtonNav.Panel;
import db.AnnualReport;
import db.DBConnection;
import db.Filter;
import db.NameValuePairs;
import db.OneToMany;
import db.OrderBy;
import db.Rows;
import db.RowsSelect;
import db.SQL;
import db.Select;
import db.ViewDef;
import db.access.AccessPolicy;
import db.access.Or;
import db.access.RecordOwnerAccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.column.NumberColumn;
import db.column.PersonColumn;
import db.column.TextAreaColumn;
import db.feature.Feature.Location;
import db.feature.MonthFilter;
import db.feature.QuarterFilter;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Dividers;
import db.section.Tabs;
import mail.Mail;
import mosaic.MailTemplate;
import pages.Page;
import ui.Form;
import ui.FormBase;
import ui.NavBar;
import ui.Option;
import ui.Table;
import util.Text;
import util.Time;
import web.HTMLWriter;

public class Work extends SiteModule {
	@JSONField
	private String	m_balance_column_label = "balance";
	@JSONField
	boolean			m_can_set_for_individuals;
	@JSONField(choices = { "Offices", "Ongoing Jobs", "One Time Tasks", "Work Hours", "Work Tasks" }, after = "Item to display when page is opened")
	String			m_default_item = "Work Hours";
	@JSONField
	private boolean	m_group_people_by_homes;
	@JSONField
	private boolean	m_group_report_sections = true;
	@JSONField
	boolean			m_hours_may_be_gifted;
	@JSONField
	double			m_max_bankable_hours;
	@JSONField(choices = { "Actions", "Offices", "Ongoing Jobs", "One Time Tasks", "Reports", "Work Hours", "Work Tasks" })
	String[]		m_menu = { "Actions", "Offices", "Ongoing Jobs", "One Time Tasks", "Reports", "Work Hours", "Work Tasks" };
	@JSONField
	boolean			m_quarterly;
	@JSONField(fields = {"m_can_set_for_individuals"})
	double			m_required_hours_per_period;
	@JSONField(label="email statements automatically",fields={"m_statement_date"})
	boolean			m_send_statements;
	@JSONField
	boolean			m_show_balance = true;
	@JSONField
	boolean			m_show_everyone_in_people_reports;
	@JSONField
	boolean			m_show_result_column;
	@JSONField(after="of each month",label="on day")
	private int		m_statement_date = 10;
	@JSONField(label="from",type="mail list")
	private String	m_statement_from;
	@JSONField(label="Intro")
	private String	m_statement_intro = "Hello,<br />  this automatically generated email is your monthly community work statement.";
	@JSONField(fields= {"m_statement_from","m_send_statements","m_statement_intro"})
	boolean			m_support_email_statements	= true;
	@JSONField
	private boolean m_track_hours_by_group_not_task;

	private static LookupColumn s_group_column = new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setFilter(new Filter() {
		@Override
		public boolean
		accept(Rows rows, Request r) {
			return rows.getBoolean("active");
		}
	});
	private static String[] s_months = new String[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
	private static String[]	s_ordinals = { "1st", "2nd", "3rd", "4th" };

	// --------------------------------------------------------------------------

	public
	Work() {
		m_description = "Tracking of community tasks and member contributions by date and number of hours";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Work", this, true) {
			@Override
			public void
			writeContent(Request r) {
				if (m_show_balance)
					r.w.js("app.subscribe('work_hours',function(){net.replace('#balance',context+'/Work/balance')});");
				if (r.getPathSegment(1) != null)
					return;
				if (m_default_item != null)
					switch(m_default_item) {
					case "Offices":
						writeOffices(r);
						return;
					case "Ongoing Jobs":
						writeOngoingJobs(r);
						return;
					case "One Time Tasks":
						writeOneTimeTasks(r);
						return;
					case "Work Hours":
						writeWorkHours(r);
						return;
					case "Work Tasks":
						writeWorkTasks(r);
					}
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("jobs")
			.add(new JDBCColumn("job", Types.VARCHAR))
			.add(new JDBCColumn("description", Types.VARCHAR))
			.add(new JDBCColumn("groups_id", Types.INTEGER))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("num_people_needed", Types.INTEGER));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("jobs_history")
			.add(new JDBCColumn("person", "people"))
			.add(new JDBCColumn("start_date", Types.DATE))
			.add(new JDBCColumn("end_date", Types.DATE))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("jobs"));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("offices")
				.add(new JDBCColumn("office", Types.VARCHAR))
				.add(new JDBCColumn("description", Types.VARCHAR))
				.add(new JDBCColumn("notes", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("offices_history")
			.add(new JDBCColumn("person", "people"))
			.add(new JDBCColumn("start_date", Types.DATE))
			.add(new JDBCColumn("end_date", Types.DATE))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("offices"));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("tasks")
			.add(new JDBCColumn("description", Types.VARCHAR))
			.add(new JDBCColumn("groups_id", Types.INTEGER))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("num_people_needed", Types.VARCHAR))
			.add(new JDBCColumn("workers", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("work_tasks")
			.add(new JDBCColumn("active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("groups_id", Types.INTEGER))
			.add(new JDBCColumn("task", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("work_hours")
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("hours", Types.DOUBLE))
			.add(new JDBCColumn("link", Types.INTEGER))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"))
			.add(new JDBCColumn("work_tasks"));
		JDBCTable.adjustTable(table_def, true, false, db);
		db.createIndex("work_hours", "date");
	}

	// --------------------------------------------------------------------------

	double
	balance(double balance) {
		if (m_max_bankable_hours > 0)
			return Math.min(balance, m_max_bankable_hours);
		return balance;
	}

	// --------------------------------------------------------------------------

	@ClassTask({"people_id"})
	public String
	buildStatement(int people_id, LocalDate date, DBConnection db) {
		Person person = new Person(people_id, db);
		MailTemplate content = new MailTemplate();
		content.header(person.getName() + " Work Statement");

		if (m_statement_intro != null) {
			content.append(HTMLWriter.breakNewlines(m_statement_intro));
			content.append("<br /><br />");
		}
		content.append("<h3>").append(Time.formatter.getMonthNameLong(date)).append(" ").append(date.getYear()).append("</h3>");
		content.append("<table class=\"table\"><thead><tr><th style=\"border:1px solid;padding:5px;\">Date</th>");
		if (!m_track_hours_by_group_not_task)
			content.append("<th style=\"border:1px solid;padding:5px;\">Task</th>");
		content.append("<th style=\"border:1px solid;padding:5px;\">Hours</th></tr></thead>");
		double total = 0;
		Select query = new Select(m_track_hours_by_group_not_task ? "date,hours" : "date,task,hours").from("work_hours").where("_owner_=" + people_id).andWhere("date>='" + SQL.firstDayOfMonth(date) + "' AND date<='" + SQL.lastDayOfMonth(date) + "'").orderBy("date");
		if (!m_track_hours_by_group_not_task)
			query.joinOn("work_tasks",  "work_tasks_id=work_tasks.id");
		try (Rows rows = new Rows(query, db)) {
			while (rows.next()) {
				content.append("<tr><td style=\"border:1px solid;padding:5px;\">");
				content.append(rows.getString("date"));
				if (!m_track_hours_by_group_not_task) {
					content.append("</td><td style=\"border:1px solid;padding:5px;\">");
					content.append(rows.getString("task"));
				}
				double hours = rows.getDouble("hours");
				total += hours;
				content.append("</td><td style=\"border:1px solid;padding:5px;text-align:right\">");
				content.append(Text.two_digits.format(hours));
				content.append("</td></tr>");
			}
		}
		content.append("<tr><td colspan=\"").append(m_track_hours_by_group_not_task ? 1 : 2).append("\" style=\"border:1px solid;padding:5px;\">Total</td><td style=\"border:1px solid;padding:5px;text-align:right\">").append(Text.two_digits.format(total)).append("</td></tr>");
		content.append("</table><br><table class=\"table\"><tr><td>Hours worked by the whole community</td><td style=\"padding-left:10px;text-align:right\">");
		ArrayList<Double> hours = new ArrayList<>();
		total = 0;
		try (Rows rows = new Rows(new Select("SUM(hours)").from("work_hours").where("date>='" + SQL.firstDayOfMonth(date) + "' AND date<='" + SQL.lastDayOfMonth(date) + "'").groupBy("_owner_"), db)) {
			while (rows.next()) {
				double h = rows.getDouble(1);
				hours.add(h);
				total += h;
			}
		}
		content.append(Text.two_digits.format(total));
		content.append("</td></tr><tr><td>Number of people recording hours</td><td style=\"padding-left:10px;text-align:right\">");
		content.append(hours.size());
		if (hours.size() > 0) {
			content.append("</td></tr><tr><td>Average hours per person recording hours</td><td style=\"padding-left:10px;text-align:right\">");
			content.append(Text.two_digits.format(total / hours.size()));
			double median;
			Collections.sort(hours);
			if (hours.size() % 2 == 0)
				median = (hours.get(hours.size() / 2) + hours.get(hours.size() / 2 - 1)) / 2;
			else
				median = hours.get(hours.size() / 2);
			content.append("</td></tr><tr><td>Median hours</td><td style=\"padding-left:10px;text-align:right\">");
			content.append(Text.two_digits.format(median));
		}
		content.append("</td></tr></table><br><br>");

		content.append("<h3>Year to Date</h3><table class=\"table\"><tr><td>Total hours worked by you</td><td style=\"padding-left:10px;text-align:right\">");
		date = date.minusMonths(1);
		hours = new ArrayList<>();
		total = 0;
		double person_total = 0;
		try (Rows rows = new Rows(new Select("_owner_,SUM(hours)").from("work_hours").where("date>='" + date.getYear() + "-01-01' AND date<='" + date.getYear() + "-12-31'").groupBy("_owner_"), db)) {
			while (rows.next()) {
				double h = rows.getDouble(2);
				hours.add(h);
				total += h;
				if (rows.getInt(1) == people_id)
					person_total = h;
			}
		}
		content.append(Text.two_digits.format(person_total));
		content.append("</td></tr><tr><td>Hours worked by the whole community</td><td style=\"padding-left:10px;text-align:right\">");
		content.append(Text.two_digits.format(total));
		content.append("</td></tr><tr><td>Number of people recording hours</td><td style=\"padding-left:10px;text-align:right\">");
		content.append(hours.size());
		if (hours.size() > 0) {
			content.append("</td></tr><tr><td>Average hours per person recording hours</td><td style=\"padding-left:10px;text-align:right\">");
			content.append(Text.two_digits.format(total / hours.size()));
			double median;
			Collections.sort(hours);
			if (hours.size() % 2 == 0)
				median = (hours.get(hours.size() / 2) + hours.get(hours.size() / 2 - 1)) / 2;
			else
				median = hours.get(hours.size() / 2);
			content.append("</td></tr><tr><td>Median hours</td><td style=\"padding-left:10px;text-align:right\">");
			content.append(Text.two_digits.format(median));
		}
		content.append("</td></tr></table>");
		content.append("</body></html>");
		return content.toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		switch(path_segments[0]) {
		case "Annual Report by Group":
			AnnualReport report = new AnnualReport(new Select("(SELECT name FROM groups WHERE groups.id=work_tasks.groups_id),work_tasks.id,task,date,hours").from("work_tasks").join("work_tasks", "work_hours"), "Task", 2, m_track_hours_by_group_not_task ? 1 : 3, 5)
				.setDetail("date,(SELECT first || ' ' || last FROM people WHERE people.id=work_hours._owner_) AS person,hours", "work_hours", "work_tasks_id");
			if (!m_track_hours_by_group_not_task)
				report.setGroup("group", 1);
			if (m_quarterly)
				report.setQuarterly(true);
			report.write(path_segments[0], r);
			return true;
		case "Annual Report by Person":
			Select select = new Select("first || ' ' || last,_owner_,date,hours").from("people");
			if (m_group_people_by_homes) {
				select.insertColumns("number").joinOn("households", "people.households_id=households.id").joinOn("homes", "households.homes_id=homes.id");
				report = new AnnualReport(select, "Person", 3, 2, 5)
					.setGroup("home", 1);
			} else
				report = new AnnualReport(select, "Person", 2, 1, 4);
			if (m_quarterly)
				report.setQuarterly(true);
			if (m_show_everyone_in_people_reports) {
				report.setJoinWithDates("work_hours", "people.id=work_hours._owner_", true);
				select.where("people.active");
			} else
				select.joinOn("work_hours", "people.id=work_hours._owner_");
			report.setDetail("date,(SELECT task FROM work_tasks WHERE work_tasks.id=work_tasks_id) AS task,hours", "work_hours", "_owner_")
				.write(path_segments[0], r);
			return true;
		case "balances":
			writeHistory(r);
			return true;
		case "History by Person":
			writeHistoryByPerson(r);
			return true;
		case "hours by date":
			Site.site.newView("work_hours", r).writeComponent();
			if (m_hours_may_be_gifted)
				new State(Time.newDate(), r.getUser().getId(), this, r.db).writeGiftButton(r.w);
			return true;
		case "jobs by job":
			Site.site.newView("jobs", r).writeComponent();
			return true;
		case "jobs by person":
			Site.site.newView("jobs by person", r).writeComponent();
			return true;
		case "Offices":
			writeOffices(r);
			return true;
		case "offices by office":
			Site.site.newView("offices", r).writeComponent();
			return true;
		case "offices by person":
			Site.site.newView("offices by person", r).writeComponent();
			return true;
		case "One Time Tasks":
			writeOneTimeTasks(r);
			return true;
		case "Ongoing Jobs":
			writeOngoingJobs(r);
			return true;
		case "Report by Group":
			Site.site.newView("work_hours by group", r).writeComponent();
			return true;
		case "Report by Person":
			Site.site.newView("work_hours by person", r).writeComponent();
			return true;
		case "Work Hours":
			writeWorkHours(r);
			return true;
		case "Work Tasks":
			writeWorkTasks(r);
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch (segment_one) {
		case "balance":
			writeBalance(r);
			return true;
		case "gift_form":
			writeGiftForm(r);
			return true;
		case "gift_max":
			State state = new State(r.getDate("date"), r.getInt("people_id", 0), this, r.db);
			r.response.add("balance", state.balance)
				.ok();
			return true;
		case "required hours":
			writeRequiredHours(r.getInt("people_id", 0), r);
			return true;
		case "SendView Statements":
			r.w.h3("Send/View Statements");
			writeSendViewStatements(r);
			return true;
		case "Set Required Work Hours":
			writeRequiredHoursConfig(r);
			return true;
		case "View Statements":
			LocalDate date = LocalDate.of(r.getInt("y", 0), r.getInt("m", 0), 1);
			String[] ids = r.getParameter("ids").split(",");
			for (int i=0; i<ids.length; i++) {
				if (i > 0)
					r.w.hr();
				r.w.write(buildStatement(Integer.parseInt(ids[i]), date, r.db));
			}
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch(segment_one) {
		case "gift hours":
			NameValuePairs nvp = new NameValuePairs();
			int from_id = r.getInt("from", 0);
			if (from_id == 0)
				from_id = r.getUser().getId();
			int to_id = r.getInt("to", 0);
			double hours = r.getDouble("hours", 0);
			LocalDate date_given = r.getDate("date_given");
			if (date_given == null)
				date_given = Time.newDate();
			nvp.setDate("date", date_given)
				.set("hours", 0.0 - hours)
				.set("_owner_", from_id);
			from_id = r.db.insert("work_hours", nvp).id;
			nvp.setDate("date", r.getDate("date_received"))
				.set("hours", hours)
				.set("link", from_id)
				.set("_owner_", to_id);
			to_id = r.db.insert("work_hours", nvp).id;
			r.db.update("work_hours", "link=" + to_id, from_id);
			return true;
		case "required hours":
			Person person = new Person(r.getInt("people_id", 0), r.db);
			if ("default".equals(r.getParameter("hours"))) {
				person.deleteData("required hours", r.db);
				return true;
			}
			double h = r.getDouble("hours", -1);
			if (h != -1)
				person.setData("required hours", h, r.db);
			return true;
		case "Send Statements":
			String[] ids = r.getParameter("ids").split(",");
			for (String id : ids)
				sendStatement(Integer.parseInt(id), r.db);
			return true;
		}
		return false;

	}

	// --------------------------------------------------------------------------

	@Override
	public void
	everyNight(LocalDateTime now, DBConnection db) {
		int day_of_month = now.getDayOfMonth();
		if (m_send_statements && day_of_month == m_statement_date)
			sendStatements(db);
	}

	// --------------------------------------------------------------------------

	static Rows
	getRowsForBalances(int people_id, boolean quarterly, DBConnection db) {
		return new Rows(new Select("extract(year from date),extract(" + (quarterly ? "quarter" : "month") + " from date),SUM(CASE WHEN link IS NULL THEN hours END),-SUM(CASE WHEN link IS NOT NULL AND hours < 0 THEN hours END),SUM(CASE WHEN link IS NOT NULL AND hours > 0 THEN hours END)").from("work_hours").whereEquals("_owner_", people_id).groupBy("extract(year from date),extract(" + (quarterly ? "quarter" : "month") + " from date)").orderBy("1,2"), db);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		Roles.add("work", "can edit various lists and access Actions on Work page with this role");
	}

	//--------------------------------------------------------------------------

	double
	net(double worked, double given, double received, double required_hours) {
		return worked - required_hours - given + received;
	}

	//--------------------------------------------------------------------------

	private Table
	newHistoryTab(ui.Tabs tabs, Table table, int year, Request r) {
		if (table != null)
			table.close();
		tabs.pane(Integer.toString(year));
		table = r.w.ui.table().addDefaultClasses();
		table.th(m_quarterly ? "quarter" : "month").th("worked").th("gave").th("received").th("net hours");
		if (m_show_result_column)
			table.th("result");
		table.th(m_balance_column_label);
		return table;
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("jobs"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("work").add().delete().edit().view())
				.setDefaultOrderBy("groups_id;job")
				.setColumnNamesForm("job", "description", "num_people_needed", "groups_id", "notes")
				.setColumnNamesTable("groups_id", "job", "tenures")
				.setColumn(new TextAreaColumn("description").setIsRichText(true, false))
				.setColumn(s_group_column)
				.setColumn(new TenuresColumn("jobs").setDisplayName("workers").setShowNumPeople(true))
				.addRelationshipDef(new OneToMany("jobs_history"));
		if (name.equals("jobs by person"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy())
				.setBaseFilter("active AND user_name!='admin'")
				.setDefaultOrderBy("first,last")
				.setRecordName("person", true)
				.setFrom("people")
				.setColumnNamesTable("first", "last", "jobs")
				.setColumn(new JobsColumn());
		if (name.equals("jobs_history"))
			return new ViewDef(name)
				.setAccessPolicy(new Or(new RoleAccessPolicy("work").add().delete().edit(), new RecordOwnerAccessPolicy().setColumn("person").add().delete().edit()))
				.setAddButtonText("Sign Up")
				.setDefaultOrderBy("start_date,end_date")
				.setRecordName("Tenure", true)
				.setShowAddButtonOnOneList(true)
				.setColumnNamesTableAndForm(new String[] { "person", "start_date", "end_date", "notes" })
				.setColumn(new LookupColumn("person", "people", "first,last", "active", "first,last").setDefaultToUserId());
		if (name.equals("offices"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("work").add().delete().edit().view())
				.setDefaultOrderBy("office")
				.setColumnNamesTable("office", "tenures")
				.setColumn(new TextAreaColumn("description").setIsRichText(true, false))
				.setColumn(new TenuresColumn("offices"))
				.addRelationshipDef(new OneToMany("offices_history"));
		if (name.equals("offices by person"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy())
				.setBaseFilter("active AND user_name!='admin'")
				.setDefaultOrderBy("first,last")
				.setRecordName("person", true)
				.setFrom("people")
				.setColumnNamesTable("first", "last", "offices")
				.setColumn(new OfficesColumn());
		if (name.equals("offices_history"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("work").add().delete().edit().view())
				.setDefaultOrderBy("start_date,end_date")
				.setRecordName("Tenure", true)
				.setColumnNamesTableAndForm(new String[] { "person", "start_date", "end_date", "notes" })
				.setColumn(new LookupColumn("person", "people", "first,last", "active", "first,last").setDefaultToUserId());
		if (name.equals("tasks"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete().edit())
				.setDefaultOrderBy("groups_id;description")
				.setRecordName("Task", true)
				.setColumnNamesForm(new String[] { "description", "groups_id", "num_people_needed", "workers", "notes", "_owner_" })
				.setColumnNamesTable(new String[] { "groups_id", "description", "num_people_needed", "workers", "notes", "_owner_" })
				.setColumn(s_group_column)
				.setColumn(new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("posted by").setIsReadOnly(true));
		if (name.equals("work_hours"))
			return new ViewDef(name) {
					@Override
					public String
					beforeDelete(String where, Request r) {
						r.db.delete("work_hours", r.db.lookupInt(new Select("link").from("work_hours").whereIdEquals(where.substring(where.indexOf('=') + 1)), 0), false);
						return null;
					}
					@Override
					public String
					beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
						NameValuePairs nvp2 = new NameValuePairs();
						nvp2.set("hours", -nvp.getDouble("hours", 0));
						r.db.update("work_hours", nvp2, r.db.lookupInt(new Select("link").from("work_hours").whereIdEquals(id), 0));
						return null;
					}
				}
				.setAccessPolicy(new Or(new RecordOwnerAccessPolicy() {
					@Override
					protected boolean
					userOwnsRecord(Rows data, Request r) {
						return super.userOwnsRecord(data, r) && (data.getDouble("hours") < 0 || data.getInt("link") == 0);
					}
					@Override
					protected boolean
					userOwnsRecord(String from, int id, Request r) {
						Object[] row = r.db.readRowObjects(new Select("hours,link").from("work_hours").whereIdEquals(id));
						return super.userOwnsRecord(from, id, r) && ((Double)row[0] < 0 || row[1] == null || (Integer)row[1] == 0);
					}
				}.add().delete().edit(), new RoleAccessPolicy("work").add().delete().edit()))
				.addFeature(new MonthFilter("work_hours", Location.TOP_LEFT))
				.addFeature(new YearFilter("work_hours", Location.TOP_LEFT))
				.setDefaultOrderBy("date")
				.setRecordName("Entry", true)
				.setColumnNamesTableAndForm(new String[] { "work_tasks_id", "date", "hours", "_owner_", "notes" })
				.setColumn(new Column("date").setDefaultToDateNow().setIsRequired(true))
				.setColumn(new NumberColumn("hours").setIsRequired(true).setMin("0.25").setStep("0.25"))
				.setColumn(new LookupColumn("_owner_", "people", "first,last", "active", "first,last").setDefaultToUserId().setDisplayName("person"))
				.setColumn(new GroupTaskColumn("work_tasks_id", m_track_hours_by_group_not_task));
		if (name.equals("work_hours by group")) {
			ViewDef view_def = new ViewDef(name)
				.addFeature(m_quarterly ? new QuarterFilter("work_hours", Location.TOP_LEFT) : new MonthFilter("work_hours", Location.TOP_LEFT))
				.addFeature(new YearFilter("work_hours", Location.TOP_LEFT))
				.setAccessPolicy(new AccessPolicy())
				.setAllowSorting(false)
				.setFrom("work_tasks")
				.setPrintButtonLocation(Location.TOP_RIGHT)
				.setRecordName("Time", true)
				.setSelectFrom("work_tasks JOIN work_hours ON work_hours.work_tasks_id=work_tasks.id")
				.setShowHead(false)
				.setColumn(new LookupColumn("groups_id", "groups", "name", "active", "name").setAllowNoSelection(true).setDisplayName("group"))
				.setColumn(new Column("hours").setTotal(true))
				.setColumn(new LookupColumn("_owner_", "people", "first,last", "active", "first,last").setDefaultToUserId().setDisplayName("person"));
			if (m_group_report_sections)
				view_def.addSection(new Dividers("groups_id", new OrderBy("groups_id")))
					.setDefaultOrderBy("date,task")
					.setColumnNamesTable(new String[] { "task", "date", "hours", "_owner_", "notes" });
			else
				view_def.setDefaultOrderBy("groups_id,date,task")
					.setColumnNamesTable(new String[] { "groups_id", "task", "date", "hours", "_owner_", "notes" });
			return view_def;
		}
		if (name.equals("work_hours by person"))
			return new ViewDef(name)
				.addFeature(m_quarterly ? new QuarterFilter("work_hours", Location.TOP_LEFT) : new MonthFilter("work_hours", Location.TOP_LEFT))
				.addFeature(new YearFilter("work_hours", Location.TOP_LEFT))
				.addSection(new Dividers("_owner_", new OrderBy("_owner_")))
				.setAccessPolicy(new AccessPolicy())
				.setAllowSorting(false)
				.setDefaultOrderBy("date")
				.setFrom("work_hours")
				.setPrintButtonLocation(Location.TOP_RIGHT)
				.setRecordName("Time", true)
				.setSelectFrom("work_hours JOIN work_tasks ON work_hours.work_tasks_id=work_tasks.id")
				.setShowHead(false)
				.setColumnNamesTable(new String[] { "date", "hours", "groups_id", "task", "notes" })
				.setColumn(new LookupColumn("groups_id", "groups", "name", "active", "name").setAllowNoSelection(true).setDisplayName("group"))
				.setColumn(new Column("hours").setTotal(true))
				.setColumn(new LookupColumn("_owner_", "people", "first,last", "active", "first,last").setDefaultToUserId().setDisplayName("person"))
				.setColumn(new GroupTaskColumn("work_tasks_id", m_track_hours_by_group_not_task));
		if (name.equals("work_hours person"))
			return new ViewDef(name)
				.addFeature(new YearFilter("work_hours", Location.TOP_LEFT))
				.addSection(new Tabs("date").setFormatMonthNameShort(true))
				.setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit())
				.setAllowSorting(false)
				.setFrom("work_hours")
				.setRecordName("Time", true)
				.setColumnNamesForm(new String[] { "work_tasks_id", "date", "hours", "notes", "_owner_" })
				.setColumnNamesTable(new String[] { "date", "work_tasks_id", "hours" })
				.setColumn(new Column("date").setDefaultToDateNow())
				.setColumn(new LookupColumn("_owner_", "people", "first,last", "active", "first,last").setDefaultToUserId().setDisplayName("person"))
				.setColumn(new GroupTaskColumn("work_tasks_id", m_track_hours_by_group_not_task));
		if (name.equals("work_tasks"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("work").add().delete().edit())
				.setDefaultOrderBy("groups_id NULLS FIRST,task")
				.setRecordName("Task", true)
				.setColumnNamesTableAndForm(m_track_hours_by_group_not_task ? new String[] { "groups_id", "active" } : new String[] { "groups_id", "task", "active" })
				.setColumn(new LookupColumn("groups_id", "groups", "name", "active", "name").setAllowNoSelection(true).setDisplayName("group"));
		return null;
	}

	//--------------------------------------------------------------------------

	double
	requiredHours(int people_id, DBConnection db) {
		double required_hours = m_required_hours_per_period;
		if (m_can_set_for_individuals) {
			Person person = new Person(people_id, db);
			required_hours = person.getDataDouble("required hours", required_hours);
		}
		return required_hours;
	}

	// --------------------------------------------------------------------------

	@ClassTask({"household_id"})
	public void
	sendStatement(int people_id, DBConnection db) {
		String email = db.lookupString(new Select("email").from("people").whereIdEquals(people_id));
		LocalDate date = Time.newDate().minusMonths(1);
		new Mail("Monthly Work Statement", buildStatement(people_id, date, db))
			.from(m_statement_from)
			.to(email)
			.send(false);
	}

	// --------------------------------------------------------------------------

	@ClassTask
	public void
	sendStatements(DBConnection db) {
		Rows rows = new Rows(new Select("id").from("people").where("active AND email IS NOT NULL"), db);
		while (rows.next())
			sendStatement(rows.getInt(1), db);
		rows.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeBalance(Request r) {
		new State(Time.newDate(), r.getUser().getId(), this, r.db).write(r.w);
	}

	//--------------------------------------------------------------------------

	private void
	writeGiftForm(Request r) {
		LocalDate today = Time.newDate();
		HTMLWriter w = r.w;
		w.write("<form id=\"gift_form\" action=\"").write(Site.context).write("/Work/gift hours\" method=\"post\">");
		Table table = w.ui.table()
			.addStyle("border-collapse", "separate")
			.addStyle("border-spacing", "10px")
			.td("give").td();
		boolean user_is_admin = r.userIsAdmin();
		String balance = Text.two_digits_max.format(r.getDouble("balance", 0));
		w.setAttribute("required", "required").numberInput("hours", null, user_is_admin ? null : balance, null, null, true).write(" hours (max: <span id=\"gift_max_span\">").write(balance).write("</span>)</td></tr>");
		if (user_is_admin) {
			table.tr().td("from").td();
			new RowsSelect("from", new Select("id,first,last").from("people").where("active").orderBy("lower(first),lower(last)"), "first,last", "id", r).setSelectedOption(null, Integer.toString(r.getUser().getId())).write(r.w);
		}
		table.tr().td("to").td();
		new RowsSelect("to", new Select("id,first,last").from("people").where("active").orderBy("lower(first),lower(last)"), "first,last", "id", r).write(r.w);
		table.tr().td("date given").td();
		w.write("<div style=\"display:inline-block\">").dateInput("date_given", today, "get_gift_max", false, true).write("</div>");
		table.tr().td("date received").td();
		w.write("<div style=\"display:inline-block\">").dateInput("date_received", today).write("</div>");
		table.close();
		w.write("</form>")
			.js("function get_gift_max(){" +
					"var f=_.$('#gift_form');" +
					"net.get_json(context+'/Work/gift_max?date='+f.date_given.value+'&people_id='+" + (user_is_admin ? "f.from.options[f.from.selectedIndex].value" : r.getUser().getId()) + "," +
						"function(o){" +
							"let b=o.balance.toFixed(2);" +
							"f.hours.max=b;" +
							"_.$('#gift_max_span').innerText=b" +
						"},true)" +
				"}");
	}

	//--------------------------------------------------------------------------

	private void
	writeHistory(Request r) {
		r.w.write("<p>");
		int people_id = r.getInt("people_id", r.getUser().getId());
		if (people_id == -1)
			r.setSessionFlag("work show all", true);
		boolean show_all = r.testSessionFlag("work show all");
		Select query = new Select("id,first,last").from("people").where("user_name!='admin'").orderBy("LOWER(first),LOWER(last)");
		if (!show_all)
			query.where("active");
		ui.Select s = new RowsSelect(null, query, "first,last", "id", r)
			.setInline(true)
			.setOnChange("_.$('#button_nav').button_nav.refresh('/Work/balances?people_id='+this.options[this.selectedIndex].value)")
			.setSelectedOption(null, Integer.toString(people_id));
		if (!show_all)
			s.addLastOption(new Option("show inactive people", "-1"));
		s.write(r.w);
		r.w.write("</p>");
		Rows rows = getRowsForBalances(people_id, m_quarterly, r.db);
		if (!rows.next()) {
			rows.close();
			r.w.write("no hours recorded");
			return;
		}
		int this_period = Time.newDate().getMonthValue();
		if (m_quarterly)
			this_period = (this_period - 1) / 3 + 1;
		int this_year = Time.newDate().getYear();
		DecimalFormat df = new DecimalFormat("0.##");
		ui.Tabs tabs = r.w.ui.tabs(null).open();
		int year = rows.getInt(1);
		Table table = newHistoryTab(tabs, null, year, r);
		double balance = 0;
		int period = rows.getInt(2);
		double required_hours = requiredHours(people_id, r.db);
		do {
			while (year < rows.getInt(1)) {
				while (period <= (m_quarterly ? 4 : 12)) {
					balance = writeHistoryRow(balance, 0, 0, 0, period, year, this_period, this_year, df, table, required_hours);
					period++;
				}
				year++;
				period = 1;
				table = newHistoryTab(tabs, table, year, r);
			}
			while (period < rows.getInt(2)) {
				balance = writeHistoryRow(balance, 0, 0, 0, period, year, this_period, this_year, df, table, required_hours);
				period++;
			}
			balance = writeHistoryRow(balance, rows.getDouble(3), rows.getDouble(4), rows.getDouble(5), period, year, this_period, this_year, df, table, required_hours);
			period++;
			if (period == (m_quarterly ? 5 : 13)) {
				year++;
				period = 1;
				table = newHistoryTab(tabs, table, year, r);
			}
		} while (rows.next());
		rows.close();
		while (year <= this_year && (year < this_year || period <= this_period)) {
			balance = writeHistoryRow(balance, 0, 0, 0, period, year, this_period, this_year, df, table, required_hours);
			period++;
			if (period == (m_quarterly ? 5 : 13)) {
				year++;
				period = 1;
				table = newHistoryTab(tabs, table, year, r);
			}
		}
		table.close();
		tabs.setStartTab(Integer.toString(year));
		tabs.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeHistoryByPerson(Request r) {
		HTMLWriter w = r.w;
		int earliest_year = r.db.lookupInt("extract(year from min(date))", "work_hours", null, 0);
		int latest_year = r.db.lookupInt("extract(year from max(date))", "work_hours", null, 0);
		int year = Math.max(Math.min(r.getInt("year", Time.newDate().getYear()), latest_year), earliest_year);
		int period = r.getInt("period", m_quarterly ? Time.newDate().getMonthValue() / 4 + 1 : Time.newDate().getMonthValue());
		w.write("<p>");
		if (m_quarterly) {
			w.setAttribute("onchange", "net.replace('#main',context+'/api/Work/History by Person?period='+this.options[this.selectedIndex].value+'&year=" + year + "')")
				.ui.selectOpen(null, true);
			for (int i=1; i<=4; i++) {
				w.write("<option");
				if (i == period)
					w.write(" selected=\"selected\"");
				w.write(" value=\"").write(i).write("\">").write(s_ordinals[i - 1]).write(" quarter</option>");
			}
			w.tagClose();
		} else {
			w.setAttribute("onchange", "net.replace('#main',context+'/api/Work/History by Person?period='+this.options[this.selectedIndex].value+'&year=" + year + "')")
				.ui.selectOpen(null, true);
			for (int i=1; i<=12; i++) {
				w.write("<option");
				if (i == period)
					w.write(" selected=\"selected\"");
				w.write(" value=\"").write(i).write("\">").write(s_months[i - 1]).write("</option>");
			}
			w.tagClose();
		}
		w.nbsp()
			.setAttribute("onchange", "net.replace('#main',context+'/api/Work/History by Person?year='+this.options[this.selectedIndex].text+'&period=" + period + "')")
			.ui.selectOpen(null, true);
		for (int i=earliest_year; i<=latest_year; i++) {
			w.write("<option");
			if (i == year)
				w.write(" selected=\"selected\"");
			w.write(">").write(i).write("</option>");
		}
		w.tagClose();
		w.addStyle("float:right")
			.setAttribute("title", "open print view in new tab")
			.ui.buttonIconOnClick("printer", "_.open_print_window(_.$('#main'))");
		w.write("</p>");

		Table table = r.w.ui.table().addDefaultClasses();
		if (m_group_people_by_homes)
			table.th("home");
		table.th("name").th("starting bank balance").th("worked").th("gave").th("received").th("net hours");
		if (m_show_result_column)
			table.th("result");
		table.th("ending bank balance");
		Select query = new Select("people.id,first,last").from("people").where("people.active");
		if (m_group_people_by_homes)
			query.insertColumns("number").joinOn("households", "people.households_id=households.id").joinOn("homes", "households.homes_id=homes.id").orderBy("number,first,last");
		else
			query.orderBy("first,last");
		Rows people = new Rows(query, r.db);
		while (people.next()) {
			table.tr();
			if (m_group_people_by_homes)
				table.td(people.getString(1));
			table.td(people.getString("first") + " " + people.getString("last"));
			int people_id = people.getInt("id");
			Rows rows = getRowsForBalances(people_id, m_quarterly, r.db);
			if (!rows.next()) {
				rows.close();
				continue;
			}
			DecimalFormat df = new DecimalFormat("0.##");
			double balance = 0;
			int y = rows.getInt(1);
			int p = rows.getInt(2);
			if (y > year || y == year && p > period) {
				rows.close();
				continue;
			}
			double required_hours = requiredHours(people_id, r.db);
			boolean written = false;
			do {
				while (y < rows.getInt(1) || p < rows.getInt(2)) {
					balance = balance(balance + net(0, 0, 0, required_hours));
					p++;
					if (p > (m_quarterly ? 4 : 12)) {
						y++;
						p = 1;
					}
				}
				if (y == year && p == period) {
					writeHistoryRow(balance, rows.getDouble(3), rows.getDouble(4), rows.getDouble(5), df, table, required_hours);
					written = true;
					break;
				}
				balance = balance(balance + net(rows.getDouble(3), rows.getDouble(4), rows.getDouble(5), required_hours));
				p++;
				if (p > (m_quarterly ? 4 : 12)) {
					y++;
					p = 1;
				}
			} while (rows.next());
			if (!written) {
				while (y < year || p < period) {
					balance = balance(balance + net(0, 0, 0, required_hours));
					p++;
					if (p > (m_quarterly ? 4 : 12)) {
						y++;
						p = 1;
					}
				}
				writeHistoryRow(balance, 0, 0, 0, df, table, required_hours);
			}
			rows.close();
		}
		people.close();
		table.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeHistoryRow(double balance, double worked, double given, double received, DecimalFormat df, Table table, double required_hours) {
		table.td(df.format(balance));
		table.td(df.format(worked));
		table.td(df.format(given));
		table.td(df.format(received));
		double net = net(worked, given, received, required_hours);
		table.td(df.format(net));
		if (m_show_result_column)
			table.td(df.format(balance + net));
		table.td(df.format(balance(balance + net)));
	}

	//--------------------------------------------------------------------------

	private double
	writeHistoryRow(double balance, double worked, double given, double received, int period, int year, int this_period, int this_year, DecimalFormat df, Table table, double required_hours) {
		table.tr().td(m_quarterly ? "Quarter " + period : Time.formatter.getMonthNameLong(period));
		table.td(df.format(worked));
		table.td(df.format(given));
		table.td(df.format(received));
		double net = net(worked, given, received, required_hours);
		table.td(df.format(net));
		if (m_show_result_column)
			table.td(df.format(balance + net));
		balance = balance(balance + net);
		table.td(year > this_year || year == this_year && period >= this_period ? "" : df.format(balance));
		return balance;
	}

	//--------------------------------------------------------------------------

	private void
	writeOffices(Request r) {
		ButtonNav bn = new ButtonNav("button_nav", 3, r);
		bn.add(new Panel("By Office", "person-badge", apiURL("offices by office")));
		Site.site.newView("offices", r).writeComponent();
		bn.add(new Panel("By Person", "person", apiURL("offices by person")));
		bn.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeOneTimeTasks(Request r) {
		Site.site.newView("tasks", r).writeComponent();
	}

	//--------------------------------------------------------------------------

	final void
	writeOngoingJobs(Request r) {
		ButtonNav bn = new ButtonNav("button_nav", 3, r);
		bn.add(new Panel("By Job", "tools", apiURL("jobs by job")));
		Site.site.newView("jobs", r).writeComponent();
		bn.add(new Panel("By Person", "person", apiURL("jobs by person")));
		bn.close();
	}

	// --------------------------------------------------------------------------

	@Override
	public NavBar
	writePageMenu(Request r) {
		NavBar nav_bar = navBar("main", 1, r).setBrand(getName(), null).setActiveItem(m_default_item).open();
		for (String item : m_menu)
			if ("Actions".equals(item)) {
				boolean user_has_work_role = r.userHasRole("work");
				if ((m_can_set_for_individuals || m_support_email_statements) && user_has_work_role) {
					nav_bar.dropdownOpen("Actions", false);
					if (m_support_email_statements)
						nav_bar.item("Send/View Statements", "SendView Statements");
					if (m_can_set_for_individuals)
						nav_bar.item("Set Required Work Hours");
					nav_bar.dropdownClose();
				}
			} else if ("Reports".equals(item))
				nav_bar.dropdownOpen("Reports", false)
				.item("Annual Report by Group", apiURL("Annual Report by Group"))
				.item("Annual Report by Person", apiURL("Annual Report by Person"))
				.item((m_quarterly ? "Quarterly" : "Monthly") + " History by Person", apiURL("History by Person"))
				.item((m_quarterly ? "Quarterly" : "Monthly") + " Report by Group", apiURL("Report by Group"))
				.item((m_quarterly ? "Quarterly" : "Monthly") + " Report by Person", apiURL("Report by Person"))
				.dropdownClose();
			else
				nav_bar.item(item, apiURL(item));
		nav_bar.close();
		return nav_bar;
	}

	//--------------------------------------------------------------------------

	private boolean
	writeRequiredHours(int people_id, Request r) {
		Person person = new Person(r.getInt("people_id", 0), r.db);
		double required_hours = person.getDataDouble("required hours", -1);
		DecimalFormat df = new DecimalFormat("0.##");
		if (required_hours == -1) {
			r.w.write("uses default value ")
				.write(df.format(m_required_hours_per_period))
				.write(" hours");
			return false;
		}
		r.w.write(df.format(required_hours))
			.write(" hours");
		return true;
	}

	//--------------------------------------------------------------------------

	private void
	writeRequiredHoursConfig(Request r) {
		HTMLWriter w = r.w;
		w.h3("Required " + (m_quarterly ? "Quarterly" : "Monthly") + " Work Hours");
		w.js("function get_hours(){var s=_.$('#people_select');net.replace(s.nextElementSibling,context+'/Work/required hours?people_id='+s.options[s.selectedIndex].value,{on_complete:function(){s.nextElementSibling.nextElementSibling.nextElementSibling.style.visibility=s.nextElementSibling.innerText.startsWith('uses')?'hidden':''}})}");
		int people_id = r.getInt("people_id", r.getUser().getId());
		w.setId("people_select");
		new RowsSelect(null, new Select("id,first,last").from("people").where("active").orderBy("first,last"), "first,last", "id", r).setOnChange("get_hours()").setSelectedOption(null, Integer.toString(people_id)).write(r.w);
		w.space()
			.tagOpen("span");
		boolean set = writeRequiredHours(people_id, r);
		w.tagClose()
			.space()
			.ui.buttonOnClick("Change", "var s=_.$('#people_select');Dialog.prompt('Number of hours required',function(t){net.post(context+'/Work/required hours','people_id='+s.options[s.selectedIndex].value+'&hours='+t,function(){get_hours()})})")
			.space();
		if (!set)
			w.addStyle("visibility:hidden");
		w.ui.buttonOnClick("Use Default", "var s=_.$('#people_select');net.post(context+'/Work/required hours','people_id='+s.options[s.selectedIndex].value+'&hours=default',function(){get_hours()})");
	}

	// --------------------------------------------------------------------------

	private void
	writeSendViewStatements(Request r) {
		HTMLWriter w = r.w;
		Form f = new Form(null, null, w).setButtonsLocation(FormBase.Location.NONE);
		f.rowOpen(null);
		w.ui.buttonOnClick(Text.all, "this.parentNode.querySelectorAll('input[type=checkbox]').forEach(function(e){e.checked=true})")
			.space()
			.ui.buttonOnClick(Text.none, "this.parentNode.querySelectorAll('input[type=checkbox]').forEach(function(e){e.checked=false})");
		w.write("<div style=\"display:flex;flex-wrap:wrap\">");
		try (Rows rows = new Rows(new Select("id,first,last").from("people").where("active AND email IS NOT NULL").orderBy("first,last"), r.db)) {
			while (rows.next()) {
				w.addStyle("width:200px")
					.tagOpen("span");
				w.ui.checkbox("h" + rows.getInt(1), Text.join(" ", rows.getString(2), rows.getString(3)), null, false, false, false)
					.tagClose();
			}
		}
		f.rowOpen(null);
		w.br();
		String[] min_max = r.db.readRow(new Select("EXTRACT(year FROM MIN(date)),EXTRACT(year FROM MAX(date))").from("work_hours"));
		LocalDate now = Time.newDate();
		w.setId("month").ui.monthSelect(now.getMonthValue())
			.space()
			.setId("year").ui.yearSelect(Integer.parseInt(min_max[0]), Integer.parseInt(min_max[1]), now.getYear(), false);
		f.rowOpen(null);
		w.js("""
				function send_view(b,f) {
					let ids=[]
					b.form.querySelectorAll('input[type=checkbox]').forEach(function(e){if(e.checked)ids.push(e.name.substring(1))})
					if(ids.length==0){
						Dialog.alert(null,'Please select one or more people')
						return
					}
					f(ids, b.form.elements.month.value, b.form.elements.year.value)
				}""");
		w.ui.buttonOnClick(Text.send, "send_view(this,function(ids,m,y){net.post(context+'/Work/Send Statements','ids='+ids.join(',')+'&m='+m+'&y='+y,function(){Dialog.alert(null,'statements sent')})})")
			.space()
			.ui.buttonOnClick(Text.view, "send_view(this,function(ids,m,y){new Dialog({url:context+'/Work/View Statements?ids='+ids.join(',')+'&m='+m+'&y='+y}).show()})");
		f.close();
	}

	//--------------------------------------------------------------------------

	final void
	writeWorkHours(Request r) {
		if (m_show_balance) {
			r.w.write("<div class=\"alert alert-secondary\" id=\"balance\">");
			writeBalance(r);
			r.w.write("</div>");
		}
		ButtonNav bn = new ButtonNav("button_nav", 3, r);
		bn.add(new Panel("By Job", "calendar", apiURL("hours by date")));
		bn.add(new Panel("History", "card-list", apiURL("balances")));
		bn.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeWorkTasks(Request r) {
		Site.site.newView("work_tasks", r).writeComponent();
	}
}
