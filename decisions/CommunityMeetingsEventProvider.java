package decisions;

import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.PriorityQueue;

import app.Request;
import app.Roles;
import calendar.Event;
import calendar.MosaicEventProvider;
import db.DBConnection;
import db.Rows;
import db.Select;
import db.ViewDef;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.TextAreaColumn;
import db.feature.Feature.Location;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import mosaic.Jobs;
import util.Text;
import web.JS;

public class CommunityMeetingsEventProvider extends MosaicEventProvider {
	@JSONField
	protected String m_extra_text;

	//--------------------------------------------------------------------------

	public
	CommunityMeetingsEventProvider(String name) {
		super(name);
		m_events_table = "community_meetings";
		m_events_have_start_time = true;
		m_jobs = new Jobs().setJobs("facilitator");
		m_show_on_menu = true;
		setAccessPolicy(new RoleAccessPolicy("community meetings calendar").add().delete().edit().view());
		setColor("#FFDC00");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addAllEvents(ArrayList<Event> events, Request r) {
		Rows rows = new Rows(new Select("date,start_time,end_time,agenda,id").from(m_events_table).where("date>=current_date"), r.db);
		while (rows.next()) {
			int id = rows.getInt(5);
			events.add(new Event(this, rows.getDate(1), eventText(rows.getString(4) == null ? 0 : id), id).setStartTime(rows.getTime(2)).setEndTime(rows.getTime(3)));
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addCalendarsEvents(PriorityQueue<Event> events, LocalDate from, LocalDate to, Request r) {
		Select query = new Select("date,start_time,end_time,agenda,id").from(m_events_table).where("date>='" + from.toString() + "'");
		if (to != null)
			query.andWhere("date<'" + to.toString() + "'");
		Rows rows = new Rows(query, r.db);
		while (rows.next()) {
			int id = rows.getInt(5);
			events.add(new Event(this, rows.getDate(1), eventText(rows.getString(4) == null ? 0 : id), id).setStartTime(rows.getTime(2)).setEndTime(rows.getTime(3)));
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable(m_events_table)
			.add(new JDBCColumn("agenda", Types.VARCHAR))
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("end_time", Types.TIME))
			.add(new JDBCColumn("start_time", Types.TIME));
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
		adjustReminders(db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected int
	countEvents(int target_max, LocalDate date, Request r) {
		return r.db.countRows(m_events_table, "date>='" + date.toString() + "'");
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if ("agenda".equals(r.getPathSegment(2))) {
			r.w.writeWithLinks(r.db.lookupString(new Select("agenda").from(m_events_table).whereIdEquals(r.getPathSegmentInt(1))));
			return true;
		}
		return super.doGet(full_page, r);
	}

	// --------------------------------------------------------------------------

	private String
	eventText(int id) {
		String title = Text.singularize(m_name);
		StringBuilder s = new StringBuilder(title);
		if (id != 0)
			s.append("<br/><a href=\"#\" onclick=\"new Dialog({ok:true,title:'" + title + "',url:" + JS.string(getRelativeURL(Integer.toString(id), "agenda").toString()) + "}).open()\">Agenda</a>");
		if (m_extra_text != null)
			s.append("<p>").append(m_extra_text).append("</p>");
		return s.toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		Roles.add("community meetings calendar", "can edit the Community Meetings calendar with this role");
	}

	//--------------------------------------------------------------------------

	@Override
	protected Event
	loadEvent(Rows rows) {
		return new Event(this, rows.getDate("date"), eventText(rows.getString("agenda") == null ? 0 : rows.getInt("id")), rows.getInt("id")).setStartTime(rows.getTime("start_time")).setEndTime(rows.getTime("end_time"));
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals(m_name) || name.equals(m_events_table)) {
			ArrayList<String> column_names_form = new ArrayList<>();
			column_names_form.add("date");
			column_names_form.add("start_time");
			column_names_form.add("end_time");
			m_jobs.addJobs(column_names_form);
			column_names_form.add("agenda");
			ArrayList<String> column_names_table = new ArrayList<>();
			column_names_table.add("date");
			column_names_table.add("start_time");
			column_names_table.add("end_time");
			m_jobs.addJobs(column_names_table);
			ViewDef view_def = new ViewDef(name)
				.addFeature(new YearFilter(name, Location.TOP_LEFT))
				.setAccessPolicy(new RoleAccessPolicy("decisions").add().delete().edit())
				.setDefaultOrderBy("date")
				.setFrom(m_events_table)
				.setRecordName(Text.singularize(m_name), true)
				.setColumnNamesForm(column_names_form)
				.setColumnNamesTable(column_names_table)
				.setColumn(new TextAreaColumn("agenda").setIsRichText(true, false))
				.setColumn(new Column("start_time").setDefaultToPrevious())
				.setColumn(new Column("end_time").setDefaultToPrevious());
			m_jobs.setColumns(view_def);
			return view_def;
		}
		return super._newViewDef(name);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, boolean hide_active, Request r) {
		super.writeSettingsForm(new String[] { "m_active", "m_color", "m_display_item_labels", "m_display_items", "m_events_have_start_time", "m_ical_items", "m_display_name", "m_support_reminders", "m_tooltip_item_labels", "m_tooltip_items" }, in_dialog, hide_active, r);
	}
}
