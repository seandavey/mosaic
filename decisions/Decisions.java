package decisions;

import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import app.ClassTask;
import app.Person;
import app.Request;
import app.Roles;
import app.Site;
import attachments.Attachments;
import db.Action;
import db.DBConnection;
import db.Form;
import db.NameValuePairs;
import db.OneTable;
import db.OneToMany;
import db.OrderBy;
import db.Reorderable;
import db.Rows;
import db.SQL;
import db.Select;
import db.View;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.Or;
import db.access.RecordOwnerAccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.column.OptionsColumn;
import db.feature.Feature.Location;
import db.feature.LookupFilter;
import db.feature.TSVectorSearch;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.jdbc.TSVector;
import db.object.JSONField;
import db.section.Dividers;
import groups.GroupAccessPolicy;
import mail.Mail;
import mosaic.Jobs;
import mosaic.People;
import pages.Page;
import social.Comments;
import social.NewsProvider;
import ui.NavBar;
import ui.Select.Type;
import ui.Table;
import util.Text;
import util.Time;
import web.HTMLWriter;

public class Decisions extends NewsProvider {
	@JSONField
	protected String							m_agenda_head = "The agenda for the meeting is to address the following items in order as time allows:";
	private final Attachments					m_attachments = new Attachments("decisions");
	protected CommunityMeetingsEventProvider	m_ep;
	@JSONField
	private String								m_no_agenda_text = "There is no agenda for this meeting.";
	protected OneTable							m_one_table;
	private Page								m_page;
	@JSONField
	private String								m_parking_lot_name = "Parking Lot";
	@JSONField(fields={"m_send_prioritize_email"})
	protected boolean							m_proposals_need_approval;
	@JSONField
	private String								m_proposals_pane_head;
	@JSONField(type="role")
	private String								m_proposals_role;
	@JSONField(admin_only=true)
	protected String[]							m_proposal_types;
	@JSONField(label="days before meeting")
	private int									m_send_agenda_email_days_before_meeting = 3;
	@JSONField(fields={"m_send_agenda_email_days_before_meeting", "m_no_agenda_text"})
	boolean										m_send_agenda_email;
	@JSONField(label="send emails to",type="mail list")
	String										m_send_announcements_to;
	@JSONField(fields={"m_send_prioritize_email_days_before_meeting"})
	boolean										m_send_prioritize_email;
	@JSONField(label="days before meeting")
	private int									m_send_prioritize_email_days_before_meeting = 5;
	private OptionsColumn						m_status_column;
	@JSONField(fields={"m_agenda_head","m_send_agenda_email"})
	boolean										m_support_agendas;
	@JSONField(fields={"m_proposals_need_approval","m_proposals_pane_head","m_proposals_role"})
	boolean										m_support_proposals;
	@JSONField
	boolean										m_support_voting;
	@JSONField
	boolean										m_system_is_opt_in;
	private final Column						m_type_column = new Column("type").setViewRole("administrator");
	private Column								m_vote_totals_column;

	//--------------------------------------------------------------------------

	public
	Decisions(CommunityMeetingsEventProvider event_provider) {
		super("decisions", "decisions");
		m_ep = event_provider;
		m_description = "System for adding proposals, creating agendas by the community, online votes, recording decisions, and " + Text.singularize(event_provider.getName()) + " schedules";
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	addPages(DBConnection db) {
		m_page = new Page("Decisions", this, true){
			@Override
			public void
			writeContent(Request r) {
				r.w.write("<div id=\"decisions\">");
				writePageContent(r);
				r.w.write("</div>");
				int id = r.getInt("pending", 0);
				if (id != 0 || "proposals".equals(r.getPathSegment(1)))
					r.w.js("app.page_menu_select('Proposals')");
				if (id != 0)
					r.w.js("setTimeout(function(){_.$('tr[data-id=\"" + id + "\"]').lastChild.firstChild.click()},1000)");
				if ("Votes".equals(r.getParameter("v")))
					r.w.js("app.page_menu_select('Votes')");
				else {
					int v = r.getInt("v", 0);
					if (v != 0)
						r.w.js("window.addEventListener('load',function(){new Dialog({url:context+'/Views/decisions/component?db_key_value=" + v + "&db_mode=READ_ONLY_FORM',title:'Decision',overflow:'auto'})})");
				}
			}
			@Override
			public NavBar
			writeMenu(Request r) {
				if (m_one_table != null)
					m_one_table.setOneMode(r);
				return writePageMenu(r);
			}
		};
		Site.pages.add(m_page, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("decisions")
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("text", Types.VARCHAR))
			.add(new JDBCColumn("status", Types.VARCHAR))
			.add(new JDBCColumn("type", Types.VARCHAR).setDefaultValue("Regular"))
			.add(new JDBCColumn("_owner_", "people").setOnDeleteSetNull(true))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP))
			.add(new TSVector());
		if (m_one_table != null)
			m_one_table.adjust(table_def);
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);

		table_def = new JDBCTable("decisions_agree")
			.add(new JDBCColumn("decisions"))
			.add(new JDBCColumn("people"));
		JDBCTable.adjustTable(table_def, true, false, db);

		table_def = new JDBCTable("decisions_rank")
			.add(new JDBCColumn("decisions_id", Types.INTEGER))
			.add(new JDBCColumn("people_id", Types.INTEGER))
			.add(new JDBCColumn("_order_", Types.INTEGER));
		JDBCTable.adjustTable(table_def, true, false, db);

		table_def = new JDBCTable("votes")
			.add(new JDBCColumn("decisions"))
			.add(new JDBCColumn("vote", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	private static int[]
	countVotes(int decisions_id, DBConnection db) {
		Rows counts = new Rows(new Select("vote,COUNT(*)").from("votes").whereEquals("decisions_id", decisions_id).groupBy("vote").orderBy("vote DESC"), db);
		int[] votes = new int[3];
		while (counts.next()) {
			String vote = counts.getString(1);
			int count = counts.getInt(2);
			if (vote.equals("yes"))
				votes[0] = count;
			else if (vote.equals("no"))
				votes[1] = count;
			else
				votes[2] = count;
		}
		counts.close();
		return votes;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		switch (path_segments[0]) {
		case "Decisions":
			ViewState.setBaseFilter("decisions", getFilter("status='passed'", r), r);
			writeComponent("decisions", r);
			return true;
		case "Meetings":
			Site.site.newView(m_ep.m_events_table, r).writeComponent();
			return true;
		case "Old Proposals":
			ViewState.setBaseFilter("old proposals", getFilter("status='completed (no decision)' OR status='did not pass' OR status='dropped' OR status='withdrawn'", r), r);
			Site.site.newView("old proposals", r).writeComponent();
			return true;
		case "Opted In Members":
			writeOptedInMembersPane(r);
			return true;
		case "Proposals":
			writeProposalsPane(r);
			return true;
		case "Votes":
			writeVotesPane(r);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	doAPIPost(String[] path_segments, Request r) {
		int decision_id = Integer.parseInt(path_segments[0]);
		String vote = path_segments[2];
		if (vote != null) {
			NameValuePairs nvp = new NameValuePairs();
			nvp.set("decisions_id", decision_id);
			boolean owner_vote = r.db.lookupString(new Select("status").from("decisions").whereIdEquals(decision_id)).equals("owner vote");
			if (owner_vote)
				nvp.set("_owner_", ((mosaic.Person)r.getUser()).getHousehold(r.db).getId());
			else
				nvp.set("_owner_", r.getUser().getId());
			String where = nvp.getWhereString("votes", r.db);
			nvp.set("vote", vote);
			r.db.updateOrInsert("votes", nvp, where);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		int decisions_id = r.getPathSegmentInt(1);
		if (decisions_id == 0)
			return false;
		int people_id = r.getUser().getId();
		String segment_two = r.getPathSegment(2);
		if (segment_two == null) {
			if (r.getBoolean("agree")) {
				int id = r.db.lookupInt("id", "decisions_agree", "decisions_id=" + decisions_id + " AND people_id=" + people_id, -1);
				if (id == -1) {
					r.db.insert("decisions_agree", "decisions_id,people_id", decisions_id + "," + people_id);
					if (r.db.countRows("decisions_agree", "decisions_id=" + decisions_id) >= getQuorum(r.db))
						r.db.update("decisions", "status='in process'", decisions_id);
				}
			} else {
				r.db.delete("decisions_agree", "decisions_id=" + decisions_id + " AND people_id=" + people_id, false);
				if (r.db.countRows("decisions_agree", "decisions_id=" + decisions_id) < getQuorum(r.db))
					r.db.update("decisions", "status='pending'", decisions_id);
			}
			return true;
		} else if (segment_two.equals("close vote")) {
			int[] votes = countVotes(decisions_id, r.db);
			if (r.db.lookupString(new Select("status").from("decisions").whereIdEquals(decisions_id)).equals("owner vote")) {
				r.db.update("decisions", "type='owner vote'", decisions_id);
				int owner_households = r.db.countRows(new Select("COUNT(*)").from("households join people on people.households_id=households.id").where("participant AND people.active AND owner").groupBy("households.id"));
				if (votes[0] + votes[1] >= owner_households * 0.25 && votes[0] >= votes[1] * 2)
					r.db.update("decisions", "status='passed'", decisions_id);
				else
					r.db.update("decisions", "status='did not pass'", decisions_id);
			} else if (votes[0] + votes[1] >= getQuorum(r.db) && votes[0] >= votes[1] * 2)
				r.db.update("decisions", "status='passed'", decisions_id);
			else
				r.db.update("decisions", "status='did not pass'", decisions_id);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	everyNight(LocalDateTime now, DBConnection db) {
		if (m_support_agendas && m_send_agenda_email) {
			LocalDate days_from_now = now.toLocalDate().plusDays(m_send_agenda_email_days_before_meeting);
			try (Rows rows = new Rows(new Select("id,start_time,end_time").from(m_ep.m_events_table).where("date='" + days_from_now.toString() + "'"), db)) {
				if (rows.next())
					sendAgendaEmail(rows.getInt(1), days_from_now, rows.getTime(2), rows.getTime(3), db);
			}
		}
		if (m_support_proposals && m_proposals_need_approval && m_send_prioritize_email && db.exists(m_ep.m_events_table, "date='" + now.toLocalDate().plusDays(m_send_prioritize_email_days_before_meeting).toString() + "'"))
			sendPrioritizeEmail(db);
	}

	//--------------------------------------------------------------------------

	protected String
	getAgenda(Request r, DBConnection db) {
		Map<String,Integer> map = new HashMap<>();
		Select query = new Select("title,text,_owner_").from("decisions").where("status='in process'").orderBy("_timestamp_");
		if (m_proposal_types != null && m_proposal_types.length > 1)
			query.andWhere("type='Regular'");
		if (m_one_table != null && r != null)
			m_one_table.adjust(query, r);
		List<String[]> proposals = db.readRows(query);
		int num_proposals = proposals.size();
		if (num_proposals == 0)
			return null;

		query = new Select("title,people_id").from("decisions").join("decisions", "decisions_rank").where("status='in process'").orderBy("people_id,_order_ NULLS LAST,date");
		if (m_proposal_types != null && m_proposal_types.length > 1)
			query.andWhere("type='Regular'");
		Rows rows = new Rows(query, db);
		for (String[] proposal : proposals)
			map.put(proposal[0], 0);
		int previous_people_id = 0;
		int row = 0;
		while (rows.next()) {
			int people_id = rows.getInt(2);
			if (people_id != previous_people_id) {
				row = 0;
				previous_people_id = people_id;
			}
			String title = rows.getString(1);
			map.put(title, map.get(title) + proposals.size() - row);
			row++;
		}
		rows.close();
		TreeMap<Integer,List<String>> ordered = new TreeMap<>();
		for (String title : map.keySet()) {
			Integer i = map.get(title);
			if (i > 0) {
				if (ordered.get(i) == null)
					ordered.put(i, new ArrayList<>());
				ordered.get(i).add(title);
			}
		}
		StringBuilder agenda = new StringBuilder();
		agenda.append(m_agenda_head).append("<ol>");
		for (Integer i : ordered.descendingKeySet()) {
			List<String> titles = ordered.get(i);
			for (String title : titles)
				agenda.append("<li>").append(title).append("</li>");
		}
		for (String[] proposal : proposals)
			if (map.get(proposal[0]) == 0)
				agenda.append("<li>").append(proposal[0]).append("</li>");
		agenda.append("</ol>");
		for (Integer i : ordered.descendingKeySet()) {
			List<String> titles = ordered.get(i);
			for (String title : titles)
				for (String[] proposal : proposals)
					if (proposal[0].equals(title)) {
						agenda.append("<hr /><h4>")
							.append(title)
							.append("</h4><div style=\"font-size:smaller\">Submitted by ")
							.append(Site.site.lookupName(Integer.parseInt(proposal[2]), db))
							.append("</div>")
							.append(proposal[1]);
						break;
					}
		}
		for (String[] proposal : proposals)
			if (map.get(proposal[0]) == 0)
				agenda.append("<hr /><h4>")
					.append(proposal[0])
					.append("</h4><div style=\"font-size:smaller\">Submitted by ")
					.append(Site.site.lookupName(Integer.parseInt(proposal[2]), db))
					.append("</div>")
					.append(proposal[1]);
		agenda.append("<hr />");
		return agenda.toString();
	}

	//--------------------------------------------------------------------------

	private String
	getFilter(String filter, Request r) {
		if (m_one_table == null)
			return filter;
		return m_one_table.getFilter(filter, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getItemAction(int id, DBConnection db) {
		String status = db.lookupString(new Select("status").from("decisions").whereIdEquals(id));
		return "posted a " + getType(status);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getItemURL(int item_id, Request r) {
		return Site.site.getAbsoluteURL("Decisions").set("v", item_id).toString();
	}

	//--------------------------------------------------------------------------

	private static int
	getNumParticipants(DBConnection db) {
		return db.countRows("people", "active AND participant");
	}

	//--------------------------------------------------------------------------

	static int
	getQuorum(DBConnection db) {
		return (int)Math.ceil(getNumParticipants(db) / 4.0);
	}

	//--------------------------------------------------------------------------

	private static String
	getType(String status) {
		return switch(status) {
		case "pending" -> "Proposal";
		case "online vote" -> "Online Vote";
		case "owner vote" -> "Owner Vote";
		default -> "Decision";
		};
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		setStatusColumn();
		m_vote_totals_column = new Column("vote_totals"){
			@Override
			public boolean
			showOnForm(View.Mode mode, Rows data, Request r) {
				return r.db.exists(new Select().from("votes").whereEquals("decisions_id", data.getInt("id")));
			}
			@Override
			public boolean
			writeValue(View v, Map<String,Object> data, Request r) {
				Rows rows = new Rows(new Select("vote,COUNT(*)").from("votes").andWhere("votes.decisions_id=" + v.data.getString("id")).groupBy("vote").orderBy("vote DESC"), r.db);
				boolean first = true;
				while (rows.next()) {
					String vote = rows.getString(1);
					int num_votes = rows.getInt(2);
					if (first)
						first = false;
					else
						r.w.write(", ");
					r.w.write(vote).write(": ").write(num_votes);
				}
				rows.close();
				if (first)
					return false;
				return true;
			}
		}.setIsComputed(true);
		m_attachments.init(db);
		Roles.add("decisions", "can edit the various decision lists and votes with this role");
		Roles.add("proposals", "can be used to limit who can add proposals and votes via the \"proposals role\" setting for the Decisions module");
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("group_decisions") || name.equals("group_old proposals") || name.equals("group_parking lot") || name.equals("group_pending list") || name.equals("group_tabled proposals") || name.equals("group_votes")) {
			ViewDef view_def = _newViewDef(name.substring(6))
				.setName(name)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDefaultToSessionAttribute().setDisplayName("Group").setIsHidden(true).setViewRole("admin"));
			if (name.equals("group_decisions"))
				view_def.removeSection(Dividers.class)
					.setAccessPolicy(new GroupAccessPolicy(0).add().delete().edit());
			return view_def;
		}
		if (name.equals("add proposal")) {
			ViewDef view_def = new DecisionsViewDef(name, m_attachments) {
					@Override
					public void
					afterInsert(int id, NameValuePairs nvp, Request r) {
						if (m_send_announcements_to != null)
							if ((m_proposal_types == null || m_proposal_types.length <= 1 || "Regular".equals(nvp.getString("type"))) && "pending".equals(nvp.getString("status")))
								sendNewProposalEmail(id, nvp.getString("title"), r);
						super.afterInsert(id, nvp, r);
						((NewsProvider)Site.site.getModule("Decisions")).afterInsert(id, nvp, r);
					}
				}
				.setAccessPolicy(m_proposals_role != null ? new RoleAccessPolicy(m_proposals_role).add() : null)
				.setAddButtonText("Add New Proposal")
				.setOnSuccess("net.replace('#decisions_pane')")
				.setRecordName("Proposal", true)
				.setColumnNamesForm(m_proposal_types == null || m_proposal_types.length <= 1 ? new String[] { "title", "text", "status", "_owner_" } : new String[] { "type", "title", "text", "status", "_owner_" })
				.setColumn(new Column("status").setDefaultValue(m_proposals_need_approval ? "pending" : "in process").setIsHidden(true));
			if (m_proposal_types != null && m_proposal_types.length > 1)
				view_def.setColumn(new OptionsColumn("type", m_proposal_types).setHelpText("note: CI proposals are only for the January meeting"));
			if (m_one_table != null)
				m_one_table.adjust(view_def).setIsHidden(true);
			return view_def;
		}
		if (name.equals("add vote"))
			return new DecisionsViewDef(name, m_attachments) {
					@Override
					public void
					afterInsert(int id, NameValuePairs nvp, Request r) {
						if (m_one_table == null || m_one_table.isOneMode(r))
							sendVoteEmail(nvp.getString("title"), "owner vote".equals(nvp.getString("status")), r);
						super.afterInsert(id, nvp, r);
					}
				}
				.setAccessPolicy(m_proposals_role != null ? new RoleAccessPolicy(m_proposals_role).add() : null)
				.setAddButtonText("Add New Vote")
				.setRecordName("Vote", true)
				.setColumnNamesForm("status", "title", "text", "_owner_")
				.setColumn(new OptionsColumn("status", "online vote", "owner vote") {
						@Override
						public boolean
						isHidden(Request r) {
							return m_one_table != null && !m_one_table.isOneMode(r);
						}
					}.setDefaultValue("online vote").setDisplayName("type").setType(Type.RADIO));
		if (name.equals("ci proposals"))
			return new DecisionsViewDef(name, m_attachments)
				.setAccessPolicy(new Or(new RoleAccessPolicy("decisions").add().delete().edit().view(), m_proposals_role != null ? new RoleAccessPolicy(m_proposals_role).add().delete().edit().view() : new RecordOwnerAccessPolicy().add().delete().edit().view()))
				.setDefaultOrderBy("_timestamp_")
				.setRecordName("Proposal", true)
				.setColumnNamesForm(new String[] { "type", "title", "text", "status", "date", "_owner_", "_timestamp_" })
				.setColumnNamesTable(new String[] { "title" })
				.setColumn(new Column("date").setViewRole("decisions"))
				.setColumn(m_status_column)
				.setColumn(m_type_column);
		if (name.equals("decisions")) {
			ViewDef view_def = addHooks(new DecisionsViewDef(name, m_attachments)
				.addFeature(new TSVectorSearch(null, Location.TOP_RIGHT))
				.addFeature(new LookupFilter("name", "groups", "active AND NOT private", "View", Location.TOP_LEFT).setNoneLabel("Community Decisions"))
				.addSection(new Dividers("groups_id", new OrderBy("groups_id", false, true)).setCollapseRows(true).setNullValueLabel(null))
				.setAccessPolicy(new RoleAccessPolicy("decisions").add().delete().edit().view())
				.setDefaultOrderBy("lower(title)")
				.setRecordName("Decision", true)
				.setTSVectorColumns("title", "text")
				.setColumn(m_status_column));
			ArrayList<String> column_names = new ArrayList<>();
			column_names.add("title");
			column_names.add("date");
			addColumnNames(column_names);
			view_def.setColumnNamesTable(column_names);
			column_names.clear();
			if (m_proposal_types != null && m_proposal_types.length > 1)
				column_names.add("type");
			column_names.add("title");
			column_names.add("text");
			column_names.add("status");
			column_names.add("date");
			addColumnNames(column_names);
			if (m_support_voting)
				column_names.add("vote_totals");
			view_def.setColumnNamesForm(column_names);
			view_def.setColumn(new Column("date").setQuickFilter(new YearFilter("decisions", Location.TOP_LEFT)));
			if (m_support_voting)
				view_def
					.setColumn(m_vote_totals_column)
					.addRelationshipDef(new OneToMany("votes"));
			if (m_one_table != null)
				m_one_table.adjust(view_def);
			return view_def;
		}
		if (name.equals("decisions_attachments"))
			return m_attachments._newViewDef(name);
		if (name.equals("old proposals")) {
			ViewDef view_def = new DecisionsViewDef(name, m_attachments).setAccessPolicy(new RoleAccessPolicy("decisions").delete().edit().view())
				.setDefaultOrderBy("date DESC NULLS LAST,title")
				.setRecordName("Proposal", true)
				.setColumnNamesForm(m_support_voting ? new String[] { "title", "text", "status", "date", "_owner_", "_timestamp_", "vote_totals" } : new String[] { "title", "text", "status", "date", "_owner_", "_timestamp_" })
				.setColumnNamesTable(new String[] { "title", "date", "status" })
				.setColumn(m_status_column);
			if (m_one_table != null)
				m_one_table.adjust(view_def);
			if (m_support_voting)
				view_def
					.setColumn(m_vote_totals_column)
					.addRelationshipDef(new OneToMany("votes"));
			Comments.addFormHook(view_def);
			return view_def;
		}
		if (name.equals("parking lot")) {
			ViewDef view_def = new DecisionsViewDef(name, m_attachments) {
					@Override
					public Reorderable
					getReorderable(Request r) {
						if (userIsParticipant(r))
							return super.getReorderable(r);
						return null;
					}
				}
				.setAccessPolicy(new RoleAccessPolicy("decisions").delete().edit().view())
				.setAllowSorting(false)
				.setRecordName("Proposal", true)
				.setReorderable(new Reorderable("decisions_rank", "decisions_id").setInstructions("(click and drag to prioritize the proposals, highest priority at the top)"))
				.setDefaultOrderBy("_order_,_timestamp_") // must be after setReorderable
				.setSelectFrom("decisions LEFT JOIN decisions_rank ON decisions_rank.decisions_id=decisions.id AND people_id=$(@user id)")
				.setColumnNamesForm(new String[] { "title", "text", "status", "date", "_owner_", "_timestamp_" })
				.setColumnNamesTable(new String[] { "title", "_timestamp_" })
				.setColumn(new Column("date").setViewRole("decisions"))
				.setColumn(m_status_column);
			if (m_one_table != null)
				m_one_table.adjust(view_def);
			Comments.addFormHook(view_def);
			return view_def;
		}
		if (name.equals("pending list")) {
			ViewDef view_def = new DecisionsViewDef(name, m_attachments)
				.setAccessPolicy(new RoleAccessPolicy("decisions").delete().edit().view())
				.setAllowSorting(false)
				.setDefaultOrderBy("title")
				.setRecordName("Proposal", true)
				.setColumnNamesForm(new String[] { "type", "title", "_owner_", "text", "agree", "others", "status" })
				.setColumnNamesTable(new String[] { "title", "_owner_", "_timestamp_", "agree" })
				.setColumn(new Column("agree") {
					private void
					writeInputValue(View v, Request r) {
						Person user = r.getUser();
						if (user == null)
							return;
						String decisions_id = v.data.getString("id");
						int num_agree = r.db.countRows("decisions_agree", "decisions_id=" + decisions_id);
						HTMLWriter w = r.w;
						int id = r.db.lookupInt("id", "decisions_agree", "decisions_id=" + decisions_id + " AND people_id=" + user.getId(), -1);
						if (id != -1) {
							if (v.getMode() == View.Mode.EDIT_FORM || v.getMode() == View.Mode.READ_ONLY_FORM || num_agree == 1)
								w.write("you've agreed ");
							else
								w.write(num_agree).write(" people have agreed, including you ");
							w.ui.buttonOnClick("Don't agree", "net.post(context+'/Decisions/" + decisions_id + "','agree=false',function(){app.replace('pending list')})");
						} else {
							if (userIsParticipant(r))
								w.ui.buttonOnClick("Agree", "net.post(context+'/Decisions/" + decisions_id + "','agree=true',function(){app.replace('pending list')})");
							if (v.getMode() != View.Mode.EDIT_FORM && v.getMode() != View.Mode.READ_ONLY_FORM) {
								w.write(" (").write(num_agree).write(" other");
								if (num_agree == 1)
									w.write(" has");
								else
									w.write("s have");
								w.write(" agreed)");
							}
						}
					}
					@Override
					public void
					writeInput(View v, Form f, boolean inline, Request r) {
						writeInputValue(v, r);
					}
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						writeInputValue(v, r);
						return true;
					}
				}.setDisplayName("agree to discuss"))
				.setColumn(m_status_column)
				.setColumn(new Column("others") {
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						List<String> names = r.db.readValues(new Select("first||' '||last").from("decisions_agree JOIN people ON people.id=decisions_agree.people_id").where("decisions_id=" + v.data.getString("id") + " AND people_id!=" + r.getUser().getId()).orderBy("first,last"));
						for (int i=0; i<names.size(); i++) {
							if (i > 0)
								r.w.write(", ");
							r.w.write(names.get(i));
						}
						return names.size() > 0;
					}
				}.setDisplayName("others who've agreed").setIsComputed(true))
				.setColumn(m_type_column)
				.addAction(new Action("send announcement", false, false) {
					@Override
					public void
					perform(String id, Request r) {
						sendNewProposalEmail(Integer.parseInt("id"), r.db.lookupString(new Select("title").from("decisions").whereIdEquals(Integer.parseInt("id"))), r);
					}
					@Override
					protected void
					writeLink(String view_def_name, String id, Request r) {
						if (r.userIsAdministrator())
							super.writeLink(view_def_name, id, r);
					}
				});
			if (m_one_table != null)
				m_one_table.adjust(view_def);
			Comments.addFormHook(view_def);
			return view_def;
		}
		if (name.equals("tabled proposals")) {
			ViewDef view_def = new DecisionsViewDef(name, m_attachments)
				.setAccessPolicy(new RoleAccessPolicy("decisions").delete().edit().view())
				.setRecordName("Proposal", true)
				.setColumnNamesForm(new String[] { "title", "text", "status" })
				.setColumnNamesTable(new String[] { "title", "_timestamp_" })
				.setColumn(m_status_column);
			if (m_one_table != null)
				m_one_table.adjust(view_def);
			Comments.addFormHook(view_def);
			return view_def;
		}
		if (name.equals("votes")) {
			ViewDef view_def = new ViewDef(name)
				.setAccessPolicy(new AccessPolicy())
				.setRecordName("Vote", true)
//				.setShowColumnHeads(false)
				.setColumnNamesTable(new String[] { "_owner_", "vote" })
				.setColumn(new Column("_owner_"){
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						if ("owner vote".equals(r.db.lookupString(new Select("type").from("decisions").whereIdEquals(v.data.getInt("decisions_id")))))
							r.w.write(r.db.lookupString(new Select("name").from("households").whereIdEquals(v.data.getInt("_owner_"))));
						else
							r.w.write(Site.site.lookupName(v.data.getInt("_owner_"), r.db));
						return true;
					}
				});
			if (m_one_table != null)
				m_one_table.adjust(view_def);
			return view_def;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	@ClassTask({"id","date","start time","end time"})
	private void
	sendAgendaEmail(int id, LocalDate date, LocalTime start, LocalTime end, DBConnection db) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("h:mm a");
		StringBuilder message = new StringBuilder("The next ").append(Text.singularize(m_ep.getName())).append(" is scheduled for ")
			.append(date.getMonthValue()).append('/').append(date.getDayOfMonth()).append(' ').append(dtf.format(start)).append('-').append(dtf.format(end)).append(". ");
		Jobs jobs = m_ep.getJobs();
		if (jobs != null) {
			Rows rows = jobs.selectJobs(m_ep.m_events_table, id, db);
			int[] ids = jobs.getIDs(rows);
			String[] labels = jobs.getLabels();
			for (int i=0; i<ids.length; i++)
				if (ids[i] != 0)
					message.append("The ")
						.append(labels[i])
						.append(" for the meeting will be ")
						.append(Site.site.lookupName(ids[i], db))
						.append(". ");
		}
		int facilitator = db.lookupInt(new Select("facilitator").from(m_ep.m_events_table).whereIdEquals(id), 0);
		if (facilitator != 0)
			message.append("The facilitator for the meeting will be ")
				.append(Site.site.lookupName(facilitator, db))
				.append(". ");
		String agenda = getAgenda(null, db);
		if (agenda == null)
			agenda = m_no_agenda_text ;
		else if (m_system_is_opt_in) {
			int num = getNumParticipants(db);
			message.append(num);
			message.append(" people have opted in to the system so ");
			message.append(Math.round(num * 0.25));
			message.append(" are needed for a quorum. ");
		}
		db.update(m_ep.m_events_table, "agenda=" + SQL.string(agenda), id);
		message.append(agenda);

		new Mail(Text.singularize(m_ep.getName()), message.toString())
			.from(m_send_announcements_to)
			.to(m_send_announcements_to)
			.send();
	}

	//--------------------------------------------------------------------------

	void
	sendNewProposalEmail(int id, String title, Request r) {
		String content = "A new proposal with the title \"" + title + "\" has been posted. Click <a href=\"" + Site.site.getAbsoluteURL("Decisions").set("pending", id) + "\">here</a> to view the proposal.";
		if (m_proposals_need_approval)
			content += " After " + getQuorum(r.db) +
				" community members who have opted in have clicked the \"agree\" button, the proposal will be automatically moved to the Parking Lot where it will be included on future Community Meeting agendas." +
				" If you don't see the \"agree\" button then that means either enough people have already agreed and the proposal is in the Parking Lot or that you have not opted in to the decision making system.";
		new Mail("New Proposal", content)
			.from(m_send_announcements_to)
			.to(m_send_announcements_to)
			.send();
	}

	//--------------------------------------------------------------------------

	public Decisions
	setOneTable(OneTable one_table) {
		m_one_table = one_table;
		return this;
	}

	//--------------------------------------------------------------------------

	private void
	sendPrioritizeEmail(DBConnection db) {
		boolean parking_lot = db.countRows("decisions", "status='in process'") > 1;
		int pending = db.countRows("decisions", "status='pending' AND type='Regular'");
		if (parking_lot || pending > 0) {
			String content = "This is a reminder that in two days the agenda will be set for the next community meeting.";
			if (pending > 0)
				if (pending == 1)
					content += " There is one proposal that is pending.";
				else
					content += " There are " + Integer.toString(pending) + " proposals that are pending.";
			content += " Please go to the <a href=\"" + Site.site.getAbsoluteURL(m_page.getName()) + "/proposals\">decisions page</a> if you would like to";
			if (parking_lot) {
				content += " prioritize the proposals in the Parking Lot";
				if (pending > 0)
					content += " and/or";
			}
			if (pending > 0)
				content += " check the list of pending proposals to see which ones you agree should come to a meeting";
			content += ".";
			List<String> emails = db.readValues(new Select("email").from("people").where("email IS NOT NULL AND participant AND active"));
			new Mail("Community Meeting Reminder", content)
				.to(emails)
				.send();
		}
	}

	//--------------------------------------------------------------------------

	void
	sendVoteEmail(String title, boolean owner, Request r) {
		String where = "email IS NOT NULL AND active";
		if (owner)
			where += " AND owner";
		else
			where += " AND participant";
		List<String> emails = r.db.readValues(new Select("email").from("people").where(where));
		new Mail(owner ? "Owner Vote" : "Online Vote", "A new vote with the title \"" + title + "\" has been posted. Click <a href=\"" + Site.site.getAbsoluteURL(m_page.getName()).set("v", "Votes") + "\">here</a> to cast a vote for your household.")
			.to(emails)
			.send();
	}

	//--------------------------------------------------------------------------

	private void
	setStatusColumn() {
		ArrayList<String> options = new ArrayList<>();
		options.add("completed (no decision)");
		options.add("did not pass");
		options.add("dropped");
		if (m_support_proposals)
			options.add("in process");
		if (m_support_voting) {
			options.add("online vote");
			options.add("owner vote");
		}
		options.add("passed");
		if (m_support_proposals && m_proposals_need_approval)
			options.add("pending");
		options.add("tabled");
		options.add("withdrawn");
		m_status_column = new OptionsColumn("status", options).setDefaultValue(m_support_proposals ? "in process" : "passed").setEditRole("decisions").setViewRole("decisions");
	}

	//--------------------------------------------------------------------------

	public boolean
	systemIsOptIn() {
		return m_system_is_opt_in;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request r) {
		boolean system_is_opt_in = m_system_is_opt_in;
		super.updateSettings(r);
		setStatusColumn();
		if (system_is_opt_in != m_system_is_opt_in) {
			People people = (People)Site.site.getModule("People");
			if (m_system_is_opt_in)
				people.addJDBCColumn(new JDBCColumn("participant", Types.BOOLEAN));
			else
				people.removeJDBCColumn("participant");
			people.adjustTables(r.db);
		}
	}

	//--------------------------------------------------------------------------

	static boolean
	userIsOwner(Request r) {
		return r.db.lookupBoolean(new Select("owner").from("people").whereIdEquals(r.getUser().getId()));
	}

	//--------------------------------------------------------------------------

	boolean
	userIsParticipant(Request r) {
		return !r.userIsGuest() && (!m_system_is_opt_in || r.db.lookupBoolean(new Select("participant").from("people").whereIdEquals(r.getUser().getId())));
	}

	//--------------------------------------------------------------------------

	private void
	writeComponent(String name, Request r) {
		String current_page = (String)r.getSessionAttribute("current page");
		if (current_page != null && current_page.startsWith("Group ")) {
			name = "group_" + name;
			ViewState.setBaseFilter(name, "groups_id=" + current_page.substring(6), r);
			ViewState.setQuicksearch(name, null, null, r);
		}
		Site.site.newView(name, r).writeComponent();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, boolean for_mail, HTMLWriter w, DBConnection db) {
		String[] v = db.readRow(new Select("title,status").from("decisions").whereIdEquals(item_id));
		if (for_mail)
			w.a(v[0], Site.site.getAbsoluteURL("Decisions").set("v", item_id).toString());
		else
			w.write("<a onclick=\"new Dialog({ok:{text:'done',click:function(){Dialog.top().close()}},max_width:'doc',title:'View " + getType(v[1]) + "',url:context+'/Views/decisions?db_key_value=")
				.write(item_id).write("&amp;db_mode=READ_ONLY_FORM'});return false;\" href=\"#\">")
				.write(v[0])
				.write("</a>");
		if (!for_mail)
			m_attachments.write(item_id, w, db);
	}

	//--------------------------------------------------------------------------

	void
	writeOptedInMembersPane(Request r) {
		HTMLWriter w = r.w;
		int user_id = r.getUser().getId();
		if (m_system_is_opt_in && !r.userIsGuest() && !userIsParticipant(r)) {
			LocalDate birthday = r.db.lookupDate(new Select("birthday").from("people").whereIdEquals(user_id));
			if (birthday == null)
				r.w.write("You are not currently opted in. Please go to the Settings page, enter your birthday, and click Save.");
			else if (birthday.until(Time.newDate(), ChronoUnit.YEARS) >= 18)
				r.w.ui.buttonOnClick("Opt In", "net.post(context+'/Views/person settings/update','db_key_value=" + user_id + "&participant=true',function(){net.replace('#decisions_pane')})");
			else
				r.w.write("You must be at least 18 to opt in.");
		}
		w.write("<div><table><tr><td style=\"padding-right:50px;vertical-align: top;\">");
		Rows rows = new Rows(new Select("id,first,last").from("people").where("participant AND active").orderBy("first,last"), r.db);
		while (rows.next()) {
			w.write(rows.getString(2)).write(' ').write(rows.getString(3));
			if (m_system_is_opt_in && user_id == rows.getInt(1))
				w.write(' ').ui.buttonOnClick("Opt Out", "net.post(context+'/Views/person settings/update','db_key_value=" + user_id + "&participant=false',function(){net.replace('#decisions_pane')})");
			w.br();
		}
		rows.close();
		w.write("</td><td style=\"vertical-align: top;\"><h4 style=\"margin-top:0\">")
			.write("Meeting Votes</h4>People: ").write(getNumParticipants(r.db)).br()
			.write("Households: ").write(r.db.countRows(new Select("households_id").distinct().from("people").where("participant AND active").orderBy("households_id"))).br()
			.write("Homes: ").write(r.db.countRows(new Select("number").distinct().from("people join households on households.id=people.households_id join homes on homes.id=households.homes_id").where("participant AND people.active"))).br()
			.write("Quorum: ").write(getQuorum(r.db)).br();
		int owner_households = r.db.countRows(new Select("COUNT(*)").from("households join people on people.households_id=households.id").where("participant AND people.active AND owner").groupBy("households.id"));
		w.write("<h4>Owner Votes</h4>Households: ").write(owner_households).br()
			.write("Quorum: ").write((int)(owner_households * 0.25)).br()
			.write("</td></tr></table></div>");
	}

	//--------------------------------------------------------------------------

	public void
	writePageContent(Request r) {
		if (!m_support_proposals && !m_system_is_opt_in) {
			writeComponent("decisions", r);
			return;
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	writePageMenu(Request r) {
		if (!m_support_proposals && !m_system_is_opt_in)
			return null;
		String page = (String)r.getSessionAttribute("current page");
		NavBar nav_bar = navBar("decisions_pane", page.startsWith("Group ") ? 4 : 2, r).open();
		Map<String,Object> items = new TreeMap<>();
		if (m_support_proposals)
			items.put("Proposals", null);
		items.put("Decisions", null);
		if (m_support_proposals && r.db.exists("decisions", getFilter("status='completed (no decision)' OR status='did not pass' OR status='dropped' OR status='withdrawn'", r)))
			items.put("Old Proposals", null);
		if (m_system_is_opt_in && (m_one_table == null || m_one_table.isOneMode(r)))
			items.put("Opted In Members", null);
		if (m_support_voting)
			items.put("Votes", null);
		nav_bar.items(items)
			.close();
		return nav_bar;
	}

	//--------------------------------------------------------------------------

	private void
	writeProposalsPane(Request r) {
		HTMLWriter w = r.w;
		if (m_proposals_pane_head != null) {
			w.write("<div class=\"alert alert-secondary\">");
			writeText(m_proposals_pane_head, r.db, w);
			w.write("</div>");
		}
		if (!r.userIsGuest())
			w.ui.buttonOnClick("Submit new proposal", "new Dialog({cancel:true,ok:{text:'add'},owner:_.c(this),title:'Submit Proposal',url:context+'/Views/add proposal/component?db_mode=ADD_FORM'});return false;");
		if (m_proposals_need_approval) {
			String filter = getFilter(m_proposal_types == null || m_proposal_types.length <= 1 ? "status='pending'" : "status='pending' AND type='Regular'", r);
			if (r.db.exists("decisions", filter)) {
				w.h3("Pending List");
				ViewState.setBaseFilter("pending list", filter, r);
				Site.site.newView("pending list", r).writeComponent();
				int id = r.getInt("pending", 0);
				if (id != 0)
					w.js("_.table.dialog_view(this,'pending list','/coho/Views/pending list/component?db_mode=READ_ONLY_FORM&db_key_value=" + id + "');");
			}
		}
		String filter = getFilter("status='in process'", r);
		if (r.db.exists("decisions", filter)) {
			w.h3(m_parking_lot_name);
			ViewState.setBaseFilter("parking lot", filter, r);
			Site.site.newView("parking lot", r).writeComponent();
		}
		filter = getFilter("status='pending' AND type='Capital Improvement'", r);
		if (r.db.exists("decisions", filter)) {
			w.h3("Capital Improvement Proposals");
			ViewState.setBaseFilter("ci proposals", filter, r);
			Site.site.newView("ci proposals", r).writeComponent();
		}
		filter = getFilter("status='tabled'", r);
		if (r.db.exists("decisions", filter)) {
			w.h3("Tabled Proposals");
			ViewState.setBaseFilter("tabled proposals", filter, r);
			Site.site.newView("tabled proposals", r).writeComponent();
		}
		if (m_support_agendas) {
			String agenda = getAgenda(r, r.db);
			if (agenda == null)
				agenda = m_no_agenda_text ;
			w.write("<br /><div class=\"alert alert-secondary\">")
				.h3("Next Agenda")
				.writeWithLinks(agenda)
				.write("</div>");
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeText(String text, DBConnection db, HTMLWriter w) {
		int start = 0;
		int index = text.indexOf("$(");
		while (index != -1) {
			w.write(text.substring(start, index));
			index += 2;
			int end = text.indexOf(')', index);
			String var = text.substring(index, end);
			if (var.equals("quorum"))
				w.write(getQuorum(db));
			start = end + 1;
			index = text.indexOf("$(", start);
		}
		w.write(text.substring(start));
	}

	//--------------------------------------------------------------------------

	void
	writeVotesPane(Request r) {
		HTMLWriter w = r.w;
		if (r.userHasRole("decisions"))
			w.ui.buttonOnClick("Add New Vote", "new Dialog({cancel:true,ok:{text:'add'},reload_page:true,title:'Add New Vote',url:context+'/Views/add vote/component?db_mode=ADD_FORM'});return false;");
		boolean user_is_owner = userIsOwner(r);
		boolean user_is_participant = userIsParticipant(r);
		Rows rows = new Rows(new Select("*").from("decisions").where(getFilter("status='online vote' OR status='owner vote'", r)).orderBy("title"), r.db);
		while (rows.next()) {
			int decision_id = rows.getInt("id");
			boolean owner_vote = rows.getString("status").equals("owner vote");
			w.hr();
			w.h2(rows.getString("title"));
			if (r.userHasRole("decisions"))
				w.ui.buttonOnClick("Close Vote", "net.post(context+'/Decisions/" + decision_id + "/close vote',null,function(){document.location=document.location})");
			w.tagOpen("p");
			w.write(rows.getString("text"));
			m_attachments.write(decision_id, w, r.db);
			w.tagClose();
			if (owner_vote && user_is_owner || !owner_vote && user_is_participant) {
				String vote = r.db.lookupString(new Select("vote").from("votes").where("decisions_id=" + decision_id + " AND _owner_=" + (owner_vote ? ((mosaic.Person)r.getUser()).getHousehold(r.db).getId() : r.getUser().getId())));
				if (vote != null) {
					if (owner_vote)
						w.write("Your household has voted with the vote <b>");
					else
						w.write("You have voted with the vote <b>");
					w.write(vote).write("</b>. You may change your vote if you want.<br /><br />");
				}
				w.write("Vote: ");
				w.ui.buttonOnClick("Yes", "net.post(context+'" + apiURL(decision_id + "/votes/yes") + "',null,function(){net.replace('#decisions_pane')})");
				w.space();
				w.ui.buttonOnClick("No", "net.post(context+'" + apiURL(decision_id + "/votes/no") + "',null,function(){net.replace('#decisions_pane')})");
				w.space();
				w.ui.buttonOnClick("Abstain", "net.post(context+'" + apiURL(decision_id + "/votes/abstain") + "',null,function(){net.replace('#decisions_pane')})");
			}
			w.h4("Votes");
			int[] votes = null;
			List<String[]> voters = r.db.readRows(owner_vote ? new Select("name,vote").from("votes JOIN households ON households.id=votes._owner_").where("decisions_id=" + decision_id).orderBy("name") : new Select("first,last,vote").from("votes JOIN people ON people.id=votes._owner_").where("decisions_id=" + decision_id).orderBy("first,last"));
			if (voters.size() == 0)
				w.write("no votes yet");
			else {
				Table table = w.ui.table().borderSpacing(5);
				for (String[] row : voters) {
					table.tr();
					if (owner_vote)
						table.td(row[0]).td(row[1]);
					else
						table.td(row[0] + " " + row[1]).td(row[2]);
				}
				table.close();
				w.h4("Totals");
				votes = countVotes(decision_id, r.db);
				w.write("yes: ").write(votes[0]).br().write("no: ").write(votes[1]).br().write("abstain: ").write(votes[2]);
				if (votes[0] / 2 >= votes[1])
					w.br().write("Currently at least 2/3 of yes or no votes are <b>yes</b> votes");
				else
					w.br().write("Currently less than 2/3 of yes or no votes are <b>yes</b> votes");
			}
			w.h4("Info");
			if (owner_vote) {
				int owner_households = r.db.countRows(new Select("COUNT(*)").from("households join people on people.households_id=households.id").where("people.active AND owner").groupBy("households.id"));
				w.write("owner households: ").write(owner_households).br().write("quorum: ").write((int)Math.ceil(owner_households / 4.0));
			} else
				w.write("participants: ").write(getNumParticipants(r.db)).br().write("quorum: ").write(getQuorum(r.db));
		}
		rows.close();
	}
}
