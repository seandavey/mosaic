package decisions;

import java.util.Map;

import app.Request;
import attachments.Attachments;
import attachments.ViewDefWithAttachments;
import db.NameValuePairs;
import db.OneToMany;
import db.column.Column;
import db.column.PersonColumn;
import db.column.TextAreaColumn;

public class DecisionsViewDef extends ViewDefWithAttachments {
	public
	DecisionsViewDef(String name, Attachments attachments) {
		super(name, attachments);
		setFrom("decisions");
		setColumn(new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("posted by").setIsReadOnly(true));
		setColumn(new Column("_timestamp_").setDisplayName("submitted"));
		setColumn(new TextAreaColumn("text").setIsRichText(true, false));
		setColumn(new Column("title").setIsRequired(true));
		addRelationshipDef(new OneToMany("decisions_attachments"));
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeInsert(NameValuePairs nvp, Request r) {
		nvp.set("tsvector", r.getParameter("title") + "\n" + r.getParameter("text"));
		return super.beforeInsert(nvp, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		nvp.set("tsvector", r.getParameter("title") + "\n" + r.getParameter("text"));
		return super.beforeUpdate(id, nvp, previous_values, r);
	}
}
