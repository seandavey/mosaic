package calendar;

import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import accounting.Accounting;
import app.Request;
import app.Site;
import app.Stringify;
import db.AnnualReport;
import db.DBConnection;
import db.Form;
import db.NameValuePairs;
import db.OneToMany;
import db.OrderBy;
import db.Rows;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.access.AccessPolicy;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.CurrencyColumn;
import db.column.DateColumn;
import db.column.LookupColumn;
import db.column.PersonColumn;
import db.column.TimeColumn;
import db.feature.Feature.Location;
import db.feature.MonthFilter;
import db.feature.QuarterFilter;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Dividers;
import households.Household;
import util.Array;
import util.Time;

@Stringify
public class GuestRoomEventProvider extends MosaicEventProvider {
	@JSONField
	int					m_max_reservation_length;
	@JSONField(label = "max nights per year")
	int					m_nights_per_year;
	@JSONField(label = "email to send reservation requests to")
	String				m_reservation_email;
	@JSONField
	String				m_reserved_by_label = "reserved by";
	@JSONField
	private boolean		m_show_event_owner = true;
//	@JSONField
//	private boolean		m_show_event_owner_on_mouse_over;
	@JSONField(admin_only = true)
	private boolean		m_support_high_season;
	@JSONField
	boolean				m_use_guest_field;

	//--------------------------------------------------------------------------

	public
	GuestRoomEventProvider(String name) {
		super(name);
		setAccessPolicy(new RecordOwnerAccessPolicy().setCanDeletePastRecords(false).setCanEditPastRecords(false).add().delete().edit());
		m_display_items = new String[] { "location", "time", "notes", "posted by" };
		m_end_date_label = "last night";
		m_events_can_repeat = true;
		m_fee_description = "Guest Room Fee";
		m_show_on_menu = true;
		m_start_date_label = "first night";
		m_waiting_list_message = "There is another reservation that overlaps with this one. Reservations that were made later will be on a waiting list.";
	}

	//--------------------------------------------------------------------------
	// TODO: change to work with multiple (waiting list) reservations

	@Override
	protected void
	addInvoices(int id, DBConnection db) {
		if (!m_charge_for_use || m_invoice_account_id == 0)
			return;
		String columns = "id,date,end_date,_owner_";
		if (m_rates == null)
			columns += ",rate";
		if (m_rate_categories != null && m_rate_categories.length > 1)
			columns += ",rate_category";
		Select query = new Select(columns).from(m_events_table);
		if (id != 0)
			query.where("id=" + id + " AND end_date <='" + Time.newDate().minusDays(1).toString() + "'");
		else
			query.where("end_date <='" + Time.newDate().minusDays(1).toString() + "' AND invoiced IS NOT TRUE");
		Rows rows = new Rows(query, db);
		while (rows.next()) {
			LocalDate last_night = rows.getDate(3);
			int household_id = db.lookupInt(new Select("households_id").from("people").whereIdEquals(rows.getInt(4)), 0);
			int event_id = rows.getInt(1);
			int num_nights = countNights(new int[] { event_id }, rows.getDate(2), last_night, db);
			Accounting.addInvoice(m_invoice_account_id, household_id, last_night, m_fee_description, num_nights * getRate(rows), event_id, db);
		}
		db.update(m_events_table, "invoiced=TRUE", query.getWhere());
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	addReports() {
		super.addReports();
		m_reports.add("household");
		if ("home/unit".equals(m_household_or_home))
			m_reports.add("home/unit");
		m_reports.sort(null);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	addJDBCColumns(JDBCTable table_def) {
		if (m_use_guest_field)
			table_def.add(new JDBCColumn("guest_name", Types.VARCHAR));
		super.addJDBCColumns(table_def);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	appendReminderItem(String item, String label, Reminder reminder, StringBuilder sb, DBConnection db) {
		if ("guest".equals(item))
			appendReminderItem(item, label, ((GuestRoomEvent)reminder.getEvent()).m_guest, sb);
		else
			super.appendReminderItem(item, label, reminder, sb, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	appendReminderWhat(Reminder reminder, StringBuilder message, DBConnection db) {
		if (m_use_guest_field)
			appendReminderItem("guest", "Guest", reminder, message, db);
		else
			appendReminderItem("posted by", m_reserved_by_label, reminder, message, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected String
	buildReminderDate(Event event, LocalDate date, boolean last_event) {
		StringBuilder sb = new StringBuilder();
		String start_date = Time.formatter.getDateLong(date);
		String end_date = Time.formatter.getDateLong(event.getEndDate());
		sb.append(last_event ? end_date : start_date);

		LocalTime start_time = event.getStartTime();
		if (last_event) {
			if (start_time != null) {
				LocalTime end_time = event.getEndTime();
				if (end_time != null)
					sb.append(" at ").append(Time.formatter.getTime(end_time, true));
			}
			return sb.toString();
		}

		if (start_time != null) {
			LocalTime end_time = event.getEndTime();
			if (end_time != null)
				sb.append(" ").append(Time.formatter.getTime(start_time, true)).append(" to ").append(end_date).append(" ").append(Time.formatter.getTime(end_time, true));
			else
				sb.append(" at ").append(Time.formatter.getTime(start_time, true));
		} else
			sb.append(" to ").append(end_date);
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	final int
	countNights(int[] ids, LocalDate from, LocalDate to, DBConnection db) {
		PriorityQueue<Event> events = new PriorityQueue<>();
		ArrayList<Event> repeating_events = new ArrayList<>();
		Rows rows = new Rows(new Select("*").from(m_events_table).where("date<='" + to.toString() + "' AND end_date>='" + from.toString() + "'"), db);
		while (rows.next()) {
			Event event = loadEvent(rows);
			if (m_events_have_start_time)
				event.setEndDate(event.getEndDate().minusDays(1));
			repeating_events.add(event);
		}
		rows.close();
		while (!from.isAfter(to)) {
			addRepeatingEvents(events, repeating_events, from);
			from = from.plusDays(1);
		}
		setOverlaps(events);
		int num_days = 0;
		for (Event event: events)
			if (Array.indexOf(ids, event.getID()) != -1 && !event.isOverlapping())
				++num_days;
		return num_days;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	endDateAfterStart() {
		return m_events_have_start_time;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getDayCSSClass(LocalDate date, LocalDate today) {
		if (ChronoUnit.DAYS.between(date, today) == 0)
			return "today";
		if (m_support_high_season) {
			int d = date.getDayOfMonth();
			int m = date.getMonthValue();
			if (m == 1 && d <= 3)
				return "peak";
			if (m == 11 && d >= 24)
				return "peak";
			if (m == 12 && d >= 21)
				return "peak";
		}
		return "day";
	}

	//--------------------------------------------------------------------------

	@Override
	protected String
	getEventEditJS(Event event, LocalDate date, Request r) {
		if (r.getUser() == null || !r.userIsAdministrator() && m_invoice_account_id != 0 && ((GuestRoomEvent)event).m_invoiced && !r.userIsAdmin() && !r.userHasRole("calendar editor") && !(m_role != null && r.userHasRole(m_role)))
			return null;
		return super.getEventEditJS(event, date, r);
	}

	//--------------------------------------------------------------------------

	@Override
	protected String[]
	getFieldOptions(String field_name) {
		if ("m_reminder_subject_items".equals(field_name)) {
			if (m_use_guest_field)
				return getFieldOptions(new String[] { "category", "date", "event", "guest", "location" });
			return getFieldOptions(new String[] { "category", "date", "event", "location", "posted by" });
		}
		if (m_use_guest_field)
			return getFieldOptions(new String[] { "guest", "location", "notes", "posted by", "time" });
		return getFieldOptions(new String[] { "location", "notes", "posted by", "time" });
	}

	//--------------------------------------------------------------------------

	@Override
	protected Event
	loadEvent(Rows rows) {
		return new GuestRoomEvent(this, rows);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals(m_name)) {
			ViewDef view_def = new EventViewDef(this, m_attachments) {
					@Override
					public void
					afterUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
						((GuestRoomEventProvider)m_ep).redoInvoices(id, r.db);
						super.afterUpdate(id, nvp, previous_values, r);
					}
					@Override
					public String
					getAddButtonOnClick(String url) {
						if (m_reservation_email != null)
							return "window.location='mailto:" + m_reservation_email + "'";
						return super.getAddButtonOnClick(url);
					}
					@Override
					public String
					validate(int id, Request r) {
						if (m_max_reservation_length > 0)
							if (ChronoUnit.DAYS.between(r.getDate("date"), r.getDate("end_date")) > m_max_reservation_length - 1)
								return "You may reserve a maximum of " + m_max_reservation_length + " nights";
						return super.validate(id, r);
					}
				}
				.setAccessPolicy(m_access_policy)
				.setFrom(m_events_table)
				.setRecordName("Reservation", true);
			ArrayList<String> column_names_form = new ArrayList<>();
			if (m_nights_per_year > 0) {
				column_names_form.add("usage");
				view_def.setColumn(new Column("usage") {
					private String
					getIDs(int household_id, Request r) {
						LocalDate date = r.getDate("date");
						int year = date.getYear();
						return r.db.lookupString(
								new Select("string_agg(distinct id::varchar, ',')")
									.from(m_events_table)
									.where("date<='" + year + "-12-31' AND end_date>='" + year + "-1-1'")
									.andWhere("household".equals(m_household_or_home) ?
										"_owner_ IN(SELECT id FROM people WHERE households_id=" + household_id + ")" :
										"_owner_ IN(SELECT id FROM people WHERE households_id IN(SELECT id FROM households WHERE homes_id=" + r.db.lookupInt(new Select("homes_id").from("households").whereIdEquals(household_id), 0) + "))"));
					}
					@Override
					public boolean
					showOnForm(View.Mode mode, Rows data, Request r) {
						if (!super.showOnForm(mode, data, r))
							return false;
						mosaic.Person user = (mosaic.Person)r.getUser();
						if (user == null)
							return false;
						Household household = user.getHousehold(r.db);
						if (household == null)
							return false;
						return getIDs(household.getId(), r) != null;
					}
					@Override
					public void
					writeInput(View v, Form f, boolean inline, Request r) {
						mosaic.Person user = (mosaic.Person)r.getUser();
						if (user == null)
							return;
						Household household = user.getHousehold(r.db);
						if (household == null)
							return;
						LocalDate date = r.getDate("date");
						int year = date.getYear();
						String ids = getIDs(household.getId(), r);
						if (ids != null) {
							int nights_so_far = countNights(Array.split(ids), date.withDayOfYear(1), date.withMonth(12).withDayOfMonth(31), r.db);
							r.w.write("<div class=\"alert alert-info\" role=\"alert\">Your " + m_household_or_home + " has used " + nights_so_far + " out of " + m_nights_per_year + " nights in " + year + "</div>");
						}
					}
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						return false;
					}
				}.setDisplayName("").setHideOnForms(Mode.EDIT_FORM, Mode.READ_ONLY_FORM));
			}
			setLocationColumn(view_def, column_names_form);
			column_names_form.add("date");
			DateColumn start_date_column = new DateColumn("date").setCheckNotSame(m_events_have_start_time).setDisplayName(m_start_date_label).setIsRequired(true);
			view_def.setColumn(start_date_column);
			if (m_events_have_start_time) {
				column_names_form.add("start_time");
				if (m_default_start_time != null) {
					view_def.setColumn(new TimeColumn("start_time").setCheckOrder(false).setDateColumn("date").setDefaultValue(m_default_start_time).setResolution(m_resolution));
					start_date_column.setTimeColumn("start_time");
				}
			}
			column_names_form.add("end_date");
			DateColumn end_date_column = new DateColumn("end_date").setCheckNotSame(m_events_have_start_time).setDisplayName(m_end_date_label).setIsRequired(true);
			view_def.setColumn(end_date_column);
			if (m_events_have_start_time) {
				column_names_form.add("end_time");
				if (m_default_end_time != null) {
					view_def.setColumn(new TimeColumn("end_time").setCheckOrder(false).setDateColumn("end_date").setDefaultValue(m_default_end_time).setResolution(m_resolution));
					end_date_column.setTimeColumn("end_time");
				}
			}
			if (m_use_guest_field) {
				column_names_form.add("guest_name");
				view_def.setColumn(new Column("guest_name").setDisplayName("guest"));
			}
			column_names_form.add("event");
			view_def.setColumn(new Column("event").setDisplayName("notes"));
			if (m_charge_for_use) {
				column_names_form.add("invoiced");
				view_def.setColumn(new Column("invoiced").setViewRole("administrator"));
			}
			column_names_form.add("repeat");
			view_def.setColumn(new Column("repeat").setDefaultValue("every day").setIsHidden(true));
			column_names_form.add("_owner_");
			LookupColumn owner_column = new PersonColumn("_owner_", false).setDisplayName("posted by").setDefaultToUserId();
			if (m_role == null)
				owner_column.setIsReadOnly(true);
			view_def.setColumn(owner_column);
			if (m_support_reminders)
				view_def.addRelationshipDef(new OneToMany(m_name + "_reminders"));
			column_names_form.add("_timestamp_");
			view_def.setColumn(new Column("_timestamp_").setDisplayName("added").setIsReadOnly(true).setHideOnForms(Mode.ADD_FORM));
			adjustViewDef(column_names_form, view_def);
			addAgreements(column_names_form, view_def);
			view_def.setColumnNamesForm(column_names_form);
			if (m_text_after_form != null)
				view_def.setAfterForm(m_text_after_form);
			return view_def;
		}
		if ("guest room reservations".equals(name)) {
			ViewDef view_def = new ViewDef(name)
				.addFeature(m_reservation_report_by_quarter ? new QuarterFilter(m_events_table, Location.TOP_LEFT) : new MonthFilter(m_events_table, Location.TOP_LEFT))
				.addFeature(new YearFilter(m_events_table, Location.TOP_LEFT))
				.addSection(new Dividers(m_household_or_home, new OrderBy("1,2")))
				.setAccessPolicy(new AccessPolicy())
				.setFrom(m_events_table);
			String amount = "(end_date - date + 1)*" + getRateColumn();
			if (m_support_multiple_locations)
				amount += "*(SELECT COUNT(*) FROM " + m_events_table + "_location_links WHERE " + m_events_table + "_id=" + m_events_table + ".id)";
			String num_nights = ",(end_date - date + 1)";
			if (m_support_multiple_locations)
				num_nights += "*(SELECT COUNT(*) FROM " + m_events_table + "_location_links WHERE " + m_events_table + "_id=" + m_events_table + ".id)";
			num_nights += " AS num_nights";
			ArrayList<String> column_names_table = new ArrayList<>();
			column_names_table.add(m_start_date_label);
			column_names_table.add(m_end_date_label);
			if ("home/unit".equals(m_household_or_home))
				column_names_table.add("household");
			if (m_events_have_location)
				column_names_table.add("location");
			column_names_table.add("num_nights");
			if (m_charge_for_use) {
				column_names_table.add("rate");
				column_names_table.add("amount_paid");
			}
			view_def.setColumnNamesTable(column_names_table);
			String select_columns = "date AS \"" + m_start_date_label + "\",end_date AS \"" + m_end_date_label + "\"";
			if (m_events_have_location)
				select_columns += "," + Locations.getSelectColumn(m_events_table, m_support_multiple_locations);
			select_columns += num_nights;
			if (m_charge_for_use)
				select_columns += "," + getRateColumn() + " AS rate," + amount + " AS amount_paid";
			String household = "(SELECT name FROM households WHERE id=(SELECT households_id FROM people WHERE people.id=_owner_)) AS household";
			if ("household".equals(m_household_or_home))
				view_def.setSelectColumns(household + "," + select_columns);
			else
				view_def.setSelectColumns("(SELECT number FROM homes WHERE id=(SELECT homes_id FROM households JOIN people ON households_id=households.id WHERE people.id=_owner_)) AS \"home/unit\"," + household + "," + select_columns);
			view_def.setColumn(new CurrencyColumn("amount_paid").setTotal(true))
				.setColumn(new Column("num_nights").setTextAlign("right").setTotal(true).setTotalIsInt(true))
				.setColumn(new CurrencyColumn("rate"));
			return view_def;
		}
		return super._newViewDef(name);
	}

	//--------------------------------------------------------------------------

	@Override
	public EventProvider
	overlaps(LocalDate date, LocalDate end_date, LocalTime start_time, LocalTime end_time, List<Integer> locations, int event_id, DBConnection db) {
		if (date == null)
			return null;
		Select query = new Select().from(m_events_table);
		if (end_date != null)
			query.where("'" + date.toString() + "' <= end_date AND '" + end_date.toString() + "' >= date");
		else
			query.where("'" + date.toString() + "' = date");
		if (m_calendar_location_id != 0) {
			if (!locations.contains(m_calendar_location_id))
				return null;
		} else if (m_events_have_location) {
			if (locations == null || locations.size() == 0)
				return null;
			Locations.adjustOverlapsQuery(query, locations, m_events_table, m_support_multiple_locations);
		}
		if (event_id != 0)
			query.andWhere("id != " + event_id);
		return db.exists(query) ? this : null;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	repeatingEventsCanBeSplit() {
		return false;
	}

	//--------------------------------------------------------------------------

	boolean
	showEventOwner() {
		return m_show_event_owner;
	}

	//--------------------------------------------------------------------------

//	boolean
//	showEventOwnerOnMouseOver() {
//		return m_show_event_owner_on_mouse_over;
//	}

	//--------------------------------------------------------------------------

	@Override
	protected String
	validateTime(LocalTime start_time, LocalTime end_time) {
		return null; // if times are used they are always valid
	}

	//--------------------------------------------------------------------------

//	void
//	writeAccountingPane(Request r) {
//		// find invoices on same day: select * from guest_room_events join transactions on transactions.date=guest_room_events.end_date and accounts_id=66 where guest_room_events.id in (select guest_room_events.id from guest_room_events join transactions on transactions.date=guest_room_events.end_date and accounts_id=66 and type='Invoice' group by guest_room_events.id having count(guest_room_events.id)>1);
//		HTMLWriter w = r.w;
//		w.h3("Guest Room");
//		Table table = w.ui.table().addStyle("padding", "20px");
//		w.addStyle("vertical-align: top;");
//		table.td();
//		ViewState.setBaseFilter("invoices", "type='Invoice' AND accounts_id=" + m_invoice_account_id, r);
//		Site.site.newView("invoices", r).writeComponent();
//		w.addStyle("vertical-align: top;");
//		table.td();
//		if (m_accounting_view_def == null)
//			m_accounting_view_def = newPrivateViewDef(m_events_table)
//				.addFeature(new YearFilter("transactions", Location.TOP_LEFT))
//				.setAllowQuickEdit(true)
//				.setDefaultOrderBy("date")
//				.setRecordName("Event")
//				.setReplaceOnQuickEdit(true)
//				.setColumn(new LookupColumn("_owner_", "people", "first,last"))
//				.setColumnNamesTable(new String[] { "date", "end_date", "_owner_", "invoiced" });
//		m_accounting_view_def.newView(r).writeComponent();
//		w.addStyle("vertical-align: top;");
//		table.td();
//		new MonthView((EventProvider)Site.site.getModule("Guest Room"), r).writeComponent(r);
//		table.close();
//		w.js("app.subscribe('" + m_accounting_view_def.getName() + "', function(object, message, data){if(message=='update'){app.reload_main()}})");
//	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeAfterView(Request r) {
		if (m_support_high_season)
			r.w.write("<div style=\"padding:10px\"><span class=\"peak\" style=\"border:1px solid black\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> High Season</div>");
		super.writeAfterView(r);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeReport(Request r) {
		String view_by = r.getParameter("view_by");
		if ("reservation".equals(view_by)) {
			Site.site.newView("guest room reservations", r).writeComponent();
			return;
		}
		String amount = "(end_date - date + 1) * " + getRateColumn();
		if (m_events_have_location && m_support_multiple_locations)
			amount += " * (SELECT COUNT(*) FROM " + m_events_table + "_location_links WHERE " + m_events_table + "_id=" + m_events_table + ".id)";
		amount += " AS amount_paid";
		String date = "date AS \"" + m_start_date_label + "\"";
		String end_date = "end_date AS \"" + m_end_date_label + "\"";
		String household_id = "(SELECT households_id FROM people WHERE people.id=_owner_)";
		String household = "(SELECT name FROM households WHERE id=" + household_id + ") AS household";
		String locations = Locations.getSelectColumn(m_events_table, m_support_multiple_locations);
		String num_nights = "(end_date - date + 1)";
		if (m_events_have_location && m_support_multiple_locations)
			num_nights += " * (SELECT COUNT(*) FROM " + m_events_table + "_location_links WHERE " + m_events_table + "_id=" + m_events_table + ".id)";
		num_nights += " AS num_nights";
		String rate = getRateColumn() + " AS rate";
		String detail_columns = date + "," + end_date + "," + household;
		if (m_events_have_location)
			detail_columns += "," + locations;
		detail_columns += "," + num_nights + "," + rate + "," + amount;
		switch(view_by) {
		case "amount paid":
			new AnnualReport(new Select("date," + amount).from(m_events_table), "Amount Paid", 0, 2, 2)
				.setDetail(detail_columns, m_events_table, "(end_date - date + 1) * " + getRateColumn())
				.setItemIsInt(true)
				.setParam("view_by")
				.write(null, r);
			return;
		case "category":
			new AnnualReport(new Select("rate_category,date," + amount).from(m_events_table), "Rate Category", 0, 1, 3)
				.setDetail(detail_columns, m_events_table, "rate_category")
				.setParam("view_by")
				.write(null, r);
			return;
		case "home/unit":
			detail_columns = date + "," + end_date + "," + household;
			if (m_events_have_location)
				detail_columns += "," + locations;
			detail_columns += "," + num_nights;
			String homes_id = "(SELECT homes_id FROM households JOIN people ON households_id=households.id WHERE people.id=_owner_)";
			new AnnualReport(new Select("(SELECT number FROM homes WHERE id=" + homes_id + ")," + homes_id + ",date," + num_nights).from(m_events_table), "Home", 2, 1, 4)
				.setDetail(detail_columns, m_events_table + " JOIN people ON people.id=_owner_ JOIN households ON households.id=households_id JOIN homes ON homes.id=homes_id", "homes.id")
				.setParam("view_by")
				.setValueIsDouble(false)
				.write(null, r);
			return;
		case "household":
			detail_columns = date + "," + end_date;
			if (m_events_have_location)
				detail_columns += "," + locations;
			detail_columns += "," + num_nights;
			new AnnualReport(new Select(household + "," + household_id + ",date," + num_nights).from(m_events_table), "Household", 2, 1, 4)
				.setDetail(detail_columns, m_events_table + " JOIN people ON people.id=_owner_ JOIN households ON households.id=households_id", "households.id")
				.setParam("view_by")
				.setValueIsDouble(false)
				.write(null, r);
			return;
		case "location":
			String columns = "name,locations_id,date," + (m_charge_for_use ? amount : num_nights);
			String from = m_events_table;
			if (m_support_multiple_locations)
				from += " JOIN " + m_events_table + "_location_links ON " + m_events_table + "_id=" + m_events_table + ".id";
			from += " JOIN locations ON locations.id=locations_id";
			detail_columns = date + "," + end_date + "," + household + "," + num_nights;
			if (m_charge_for_use)
				detail_columns += "," + rate + "," + amount;
			new AnnualReport(new Select(columns).from(from), "Location", 2, 1, 4)
				.setDetail(detail_columns, from, "locations.id")
				.setParam("view_by")
				.setValueIsDouble(m_charge_for_use)
				.write(null, r);
			return;
		}
		super.writeReport(r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, boolean hide_active, Request r) {
		super.writeSettingsForm(new String[] { "m_active", "m_agreements", "m_charge_for_use", "m_check_overlaps", "m_color", "m_display_items", "m_display_name", "m_end_date_label", "m_events_have_location", "m_events_have_start_time", "m_ical_items", "m_jobs", "m_nights_per_year", "m_reservation_email", "m_reserved_by_label", "m_role", "m_show_event_owner", "m_show_on_menu", "m_show_on_upcoming", "m_start_date_label", "m_support_reminders", "m_text_above_calendar", "m_text_after_form", "m_tooltip_items", "m_use_guest_field", "m_uuid" }, in_dialog, hide_active, r);
	}
}
