package calendar;

import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Map;
import java.util.PriorityQueue;

import accounting.Accounting;
import app.Block;
import app.ClassTask;
import app.Request;
import app.Site;
import app.Site.Message;
import app.Stringify;
import db.AnnualReport;
import db.DBConnection;
import db.Form;
import db.OrderBy;
import db.Rows;
import db.SQL;
import db.Select;
import db.View;
import db.ViewDef;
import db.access.AccessPolicy;
import db.column.CurrencyColumn;
import db.column.NumberColumn;
import db.column.OptionsColumn;
import db.feature.Feature.Location;
import db.feature.MonthFilter;
import db.feature.QuarterFilter;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Dividers;
import mosaic.Jobs;
import mosaic.Person;
import util.Array;
import util.Time;

@Stringify
public class MosaicEventProvider extends EventProvider {
	@JSONField
	boolean						m_allow_multiple_rate_categories;
	@JSONField(label = "charge for events", fields = {"m_charge_hourly","m_fee_description","m_invoice_account_id","m_minimum_rate","m_rate_categories","m_allow_multiple_rate_categories","m_rates","m_rate_post_text","m_household_or_home"})
	boolean						m_charge_for_use;
	@JSONField
	boolean						m_charge_hourly;
	@JSONField
	protected String			m_fee_description = "Event Fee";
	@JSONField(choices = {"household","home/unit"}, label = "organize by")
	String						m_household_or_home = "household";
	@JSONField(label="invoice account", type = "income account")
	public int					m_invoice_account_id;
	@JSONField
	protected Jobs				m_jobs = new Jobs();
	@JSONField
	private double				m_minimum_rate;
	@JSONField
	private String				m_rate_post_text;
	@JSONField(after = "(leave empty to let user enter amount)", label = "rate(s)")
	double	[]					m_rates;
	@JSONField
	protected String[]			m_rate_categories;
	protected ArrayList<String> m_reports = new ArrayList<>();
	@JSONField
	protected boolean			m_reservation_report_by_quarter;
	@JSONField
	private boolean				m_show_on_upcoming;

	//--------------------------------------------------------------------------

	public
	MosaicEventProvider(String name) {
		super(name);
		setShowOnUpcoming(true);
		setShowOnMenu(true);
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	addAddresses(ArrayList<String> a) {
		m_jobs.addAddresses(a);
	}

	//--------------------------------------------------------------------------

	protected void
	addBlock() {
		if (m_reports.size() == 0)
			return;
		Accounting accounting = (Accounting)Site.site.getModule("Accounting");
		if (accounting == null)
			return;
		accounting.addBlock(new Block(getDisplayName(null)) {
			@Override
			public boolean
			handlesContext(String context, Request r) {
				switch (context) {
				case "accounting report":
					return m_charge_for_use && m_invoice_account_id != 0;
				}
				return false;
			}
			@Override
			public void
			write(String context, Request r) {
				switch(context) {
				case "accounting report":
					writeReportPicker(r);
					return;
				}
			}
		});
	}

	//--------------------------------------------------------------------------

	protected void
	addInvoices(int id, DBConnection db) {
		if (!m_charge_for_use || m_invoice_account_id == 0)
			return;
		String columns = "id,date,_owner_";
		if (m_rates == null)
			columns += ",rate";
		if (m_rate_categories != null && m_rate_categories.length > 1)
			if (m_allow_multiple_rate_categories)
				for (String rate_category : m_rate_categories)
					columns += "," + SQL.columnName(rate_category);
			else
				columns += ",rate_category";
		if (m_charge_hourly)
			columns += ",start_time,end_time";
		Select query = new Select(columns).from(m_events_table);
		if (id != 0)
			query.whereIdEquals(id);
		else
			query.where("date <='" + Time.newDate().minusDays(1).toString() + "' AND NOT invoiced");
		Rows rows = new Rows(query, db);
		while (rows.next()) {
			int event_id = rows.getInt(1);
			int household_id = db.lookupInt(new Select("households_id").from("people").whereIdEquals(rows.getInt(3)), 0);
			if (m_allow_multiple_rate_categories)
				for (String rate_category : m_rate_categories) {
					double amount = m_rates[Array.indexOf(m_rate_categories, rate_category)] * rows.getInt(rate_category);
					if (amount != 0.0)
						Accounting.addInvoice(m_invoice_account_id, household_id, rows.getDate(2), rate_category, amount, event_id, db);
				}
			else {
				String rate_category = m_rate_categories != null && m_rate_categories.length > 1 ? rows.getString("rate_category") : null;
				double amount = calcAmount(m_rates == null ? rows.getDouble(4) : 0, rate_category, m_charge_hourly ? rows.getTime("start_time") : null, m_charge_hourly ? rows.getTime("end_time") : null);
				Accounting.addInvoice(m_invoice_account_id, household_id, rows.getDate(2), m_fee_description != null ? m_fee_description : rate_category, amount, event_id, db);
			}
		}
		db.update(m_events_table, "invoiced=TRUE", query.getWhere());
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	addJDBCColumns(JDBCTable table_def) {
		if (m_charge_for_use) {
			table_def.add(new JDBCColumn("invoiced", Types.BOOLEAN));
			table_def.add(new JDBCColumn("transactions_ids", Types.VARCHAR));
			if (m_rates == null)
				table_def.add(new JDBCColumn("rate", Types.DOUBLE));
			if (m_rate_categories != null && m_rate_categories.length > 1)
				if (m_allow_multiple_rate_categories)
					for (String rate_category : m_rate_categories)
						table_def.add(new JDBCColumn(rate_category, Types.INTEGER));
				else
					table_def.add(new JDBCColumn("rate_category", Types.VARCHAR));
		}
		m_jobs.addJDBCColumns(table_def);
		super.addJDBCColumns(table_def);
	}

	//--------------------------------------------------------------------------

	protected void
	addReports() {
		if (m_events_have_category)
			m_reports.add("category");
		if (!m_charge_for_use)
			return;
		if (m_rates == null)
			m_reports.add("amount paid");
		if (m_events_have_location && m_support_multiple_locations)
			m_reports.add("location");
		if (m_rate_categories != null && m_rate_categories.length > 1)
			m_reports.add("rate category");
		m_reports.add("reservation");
		m_reports.sort(String.CASE_INSENSITIVE_ORDER);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustViewDef(ArrayList<String> column_names_form, ViewDef view_def) {
		if (m_charge_for_use) {
			if (m_rate_categories != null && m_rate_categories.length > 1)
				if (m_allow_multiple_rate_categories)
					for (String rate_category : m_rate_categories) {
						column_names_form.add(rate_category);
						view_def.setColumn(new NumberColumn(rate_category));
					}
				else {
					column_names_form.add("rate_category");
					view_def.setColumn(new OptionsColumn("rate_category", m_rate_categories));
				}
			if (m_rates == null) {
				column_names_form.add("rate");
				view_def.setColumn(new CurrencyColumn("rate").setMin(m_minimum_rate).setPostText(m_rate_post_text).setIsRequired(true));
			} else if (m_rates.length == 1) {
				column_names_form.add("rate");
				view_def.setColumn(new CurrencyColumn("rate") {
					@Override
					public void
					writeInput(View v, Form f, boolean inline, Request r) {
						r.w.writeCurrency(m_rates[0]).write(m_charge_hourly ? " / hour" : " / day");
					}
					@Override
					public boolean
					writeValue(View v, Map<String, Object> data, Request r) {
						r.w.writeCurrency(m_rates[0]).write(m_charge_hourly ? " / hour" : " / day");
						return true;
					}
				}.setPostText(m_rate_post_text));
			}
		}
		m_jobs.addJobs(column_names_form);
		m_jobs.setColumns(view_def);
	}

	//--------------------------------------------------------------------------

	private double
	calcAmount(double rate, String rate_category, LocalTime start, LocalTime end) {
		double hours = end != null && start != null ? ChronoUnit.MINUTES.between(start, end) / 60 : 0;
		if (rate > 0)
			return m_charge_hourly ? rate * hours : rate;
		if (rate_category != null && m_rate_categories != null && m_rates != null) {
			int index = Array.indexOf(m_rate_categories, rate_category);
			if (index != -1 && m_rates.length < index)
				rate = m_rates[index];
		}
		if (rate == 0)
			rate = m_minimum_rate;
		return m_charge_hourly ? rate * hours : rate;
	}

	//--------------------------------------------------------------------------

	protected void
	deleteInvoices(int event_id, DBConnection db) {
		if (!m_charge_for_use || m_invoice_account_id == 0)
			return;
		db.delete("transactions", "type='Invoice' AND accounts_id=" + m_invoice_account_id + " AND transactions_id=" + event_id, false);
		db.update(m_events_table, "invoiced=false", event_id);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch (segment_one) {
		case "report":
			writeReport(r);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		switch(r.getPathSegment(2)) {
		case "invoices":
			switch(r.getPathSegment(3)) {
			case "add":
				addInvoices(r.getPathSegmentInt(1), r.db);
				return true;
			case "delete":
				deleteInvoices(r.getPathSegmentInt(1), r.db);
				return true;
			}
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	everyNight(LocalDateTime now, DBConnection db) {
		super.everyNight(now, db);
		addInvoices(0, db);
	}

	//--------------------------------------------------------------------------

	public final Jobs
	getJobs() {
		return m_jobs;
	}

	//--------------------------------------------------------------------------

	final double
	getRate(Rows rows) {
		if (m_rates == null)
			return rows.getDouble("rate");
		if (m_rate_categories != null && m_rate_categories.length > 1)
			return getRate(rows.getString("rate_category"));
		return m_rates[0];
	}

	//--------------------------------------------------------------------------

	private double
	getRate(String rate_category) {
		for (int i=0; i<m_rate_categories.length; i++)
			if (m_rate_categories[i].equals(rate_category))
				return m_rates[i];
		return 0;
	}

	//--------------------------------------------------------------------------

	protected final String
	getRateColumn() {
		StringBuilder rate_column = new StringBuilder();
		if (m_rates == null)
			rate_column.append("rate");
		else if (m_rate_categories == null || m_rate_categories.length == 1)
			rate_column.append(m_rates[0]);
		else if (m_allow_multiple_rate_categories)
			for (int i=0; i<m_rate_categories.length; i++) {
				if (m_rates[i] == 0.0)
					continue;
				if (rate_column.length() > 0)
					rate_column.append("+");
				rate_column.append(SQL.columnName(m_rate_categories[i])).append("*").append(m_rates[i]);
			}
		else {
			rate_column.append("CASE");
			for (int i=0; i<m_rate_categories.length; i++)
				rate_column.append(" WHEN rate_category=").append(SQL.string(m_rate_categories[i])).append(" THEN ").append(m_rates[i]);
			rate_column.append(" END");
		}
		return rate_column.toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		addReports();
		addBlock();
		super.init(was_added, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	isValidAddress(String address) {
		return m_jobs.isValidAddress(address);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals(m_name + " reservations")) {
			ViewDef view_def = new ViewDef(name)
				.addFeature(m_reservation_report_by_quarter ? new QuarterFilter(m_events_table, Location.TOP_LEFT) : new MonthFilter(m_events_table, Location.TOP_LEFT))
				.addFeature(new YearFilter(m_events_table, Location.TOP_LEFT))
				.addSection(new Dividers(m_household_or_home, new OrderBy("1,2")))
				.setAccessPolicy(new AccessPolicy())
				.setFrom(m_events_table);
			ArrayList<String> c = new ArrayList<>();
			c.add(m_start_date_label);
			if ("home/unit".equals(m_household_or_home))
				c.add("household");
			if (m_events_have_location)
				c.add("location");
			if (m_charge_for_use) {
				if (m_allow_multiple_rate_categories)
					for (String rc : m_rate_categories)
						c.add(rc);
				else
					c.add("rate");
				c.add("amount_paid");
			}
			view_def.setColumnNamesTable(c);
			String select_columns = "date AS \"" + m_start_date_label + "\"";
			if (m_events_have_location)
				select_columns += "," + Locations.getSelectColumn(m_events_table, m_support_multiple_locations);
			if (m_charge_for_use) {
				String amount = getRateColumn();
				if (m_charge_hourly)
					amount += "*(EXTRACT(EPOCH FROM CASE WHEN end_time='0:00:00' THEN '24:00:00'::time ELSE end_time END-start_time)/3600)";
				else if (m_events_have_location && m_support_multiple_locations)
					amount += "*(SELECT COUNT(*) FROM " + m_events_table + "_location_links WHERE " + m_events_table + "_id=" + m_events_table + ".id)";
				if (m_allow_multiple_rate_categories)
					for (String rc : m_rate_categories)
						select_columns += "," + SQL.columnName(rc) + "," + amount + " AS amount_paid";
				else
					select_columns += "," + getRateColumn() + " AS rate," + amount + " AS amount_paid";
			}
			String household = "(SELECT name FROM households WHERE id=(SELECT households_id FROM people WHERE people.id=_owner_)) AS household";
			if ("household".equals(m_household_or_home))
				view_def.setSelectColumns(household + "," + select_columns);
			else
				view_def.setSelectColumns("(SELECT number FROM homes WHERE id=(SELECT homes_id FROM households JOIN people ON households_id=households.id WHERE people.id=_owner_)) AS \"home/unit\"," + household + "," + select_columns);
			if (m_charge_for_use) {
				view_def.setColumn(new CurrencyColumn("amount_paid").setTotal(true));
				if (!m_allow_multiple_rate_categories)
					view_def.setColumn(new CurrencyColumn("rate"));
			}
			return view_def;
		}
		return super._newViewDef(name);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	publish(Object object, Message message, Object data, DBConnection db) {
		super.publish(object, message, data, db);
		if (message == Message.DELETE && ((String)object).equals(m_events_table) && data != null)
			deleteInvoices(Integer.parseInt(((String)data).substring(3)), db);
	}

	//--------------------------------------------------------------------------

	public final void
	redoInvoices(int event_id, DBConnection db) {
		deleteInvoices(event_id, db);
		addInvoices(event_id, db);
	}

	//--------------------------------------------------------------------------

	@ClassTask({"start", "end"})
	public final void
	redoInvoices(LocalDate start, LocalDate end, Request r) {
		PriorityQueue<Event> events = new PriorityQueue<>();
		addCalendarsEvents(events, start, end, r);
		for (Event event : events)
			redoInvoices(event.getID(), r.db);
	}

	//--------------------------------------------------------------------------

	public final MosaicEventProvider
	setShowOnUpcoming(boolean show_on_upcoming) {
		m_show_on_upcoming = show_on_upcoming;
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showOnMenu(Request r) {
		if (r.getData("upcoming") != null)
			return isActive() && m_show_on_upcoming;
		return super.showOnMenu(r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeControlButtons(Request r) {
		if (m_reports.size() > 0)
			r.w.setAttribute("title", "reports")
				.addStyle("margin-left:2px")
				.ui.buttonIconOnClick("file-ruled", "new Dialog({url:context+'/api/Accounting/blocks/" + getDisplayName(r) + "/accounting report'}).open()");
		Person user = (Person)r.getUser();
		super.writeControlButtons(r);
		if (user != null)
			r.w.setAttribute("title", "subscribe to this calendar")
				.addStyle("margin-left:2px")
				.ui.buttonOutlineOnClick("ICS", "Calendar.$().ics_dialog('" + Site.site.getDomain() + "'," + user.getId() + ",'" + user.getUUID(r.db) + "')");
	}

	//--------------------------------------------------------------------------

	protected void
	writeReport(Request r) {
		String view_by = r.getParameter("view_by");
		if ("reservation".equals(view_by)) {
			Site.site.newView(m_name + " reservations", r).writeComponent();
			return;
		}
		String amount = "ROUND((" + getRateColumn();
		if (m_charge_hourly)
			amount += "*(EXTRACT(EPOCH FROM CASE WHEN end_time='0:00:00' THEN '24:00:00'::time ELSE end_time END-start_time)/3600)";
		else if (m_events_have_location && m_support_multiple_locations)
			amount += "*(SELECT COUNT(*) FROM " + m_events_table + "_location_links WHERE " + m_events_table + "_id=" + m_events_table + ".id)";
		amount += ")::numeric,2) AS amount_paid";
		String date = "date AS \"" + m_start_date_label + "\"";
		String household_id = "(SELECT households_id FROM people WHERE people.id=_owner_)";
		String household = "(SELECT name FROM households WHERE id=" + household_id + ") AS household";
		String locations = m_support_multiple_locations
			? "(SELECT string_agg(name, ', ') FROM " + m_events_table + "_location_links JOIN locations ON locations_id=locations.id WHERE " + m_events_table + "_id=" + m_events_table + ".id GROUP BY " + m_events_table + ".id) AS location"
			: "(SELECT string_agg(name, ', ') FROM locations WHERE id=locations_id GROUP BY id) AS location";
		String rate = getRateColumn() + "::money AS rate";
		String detail_columns = date;
		if (m_events_can_repeat)
			detail_columns += ",end_date AS \"" + m_end_date_label + "\"";
		detail_columns += "," + household;
		if (m_events_have_location)
			detail_columns += "," + locations;
		detail_columns += "," + rate + "," + amount;
		switch(view_by) {
		case "amount paid":
			new AnnualReport(new Select("date," + amount).from(m_events_table), "Amount Paid", 0, 2, 2)
				.setDetail(detail_columns, m_events_table, m_events_can_repeat ? "(end_date-date+1)*" + getRateColumn() :
				m_charge_hourly ? "(EXTRACT(EPOCH FROM CASE WHEN end_time='0:00:00' THEN '24:00:00'::time ELSE end_time END-start_time)/3600)*" + getRateColumn() :
				getRateColumn())
				.setItemIsDouble(true)
				.setParam("view_by")
				.write(null, r);
			return;
		case "category":
			new AnnualReport(new Select("(SELECT text FROM " + m_events_table + "_categories WHERE " + m_events_table + "_categories.id=" + m_events_table + "_categories_id)," + m_events_table + "_categories_id,date,1").from(m_events_table), "Category", 2, 1, 4)
				.setDetail(date + ",event", m_events_table, m_events_table + "_categories_id")
				.setParam("view_by")
				.setTotalLastDetailColumn(false)
				.setValueIsDouble(false)
				.write(null, r);
			return;
		case "location":
			String from = m_events_table;
			if (m_support_multiple_locations)
				from += " JOIN " + m_events_table + "_location_links ON " + m_events_table + "_id=" + m_events_table + ".id";
			from += " JOIN locations ON locations.id=locations_id";
			detail_columns = date + "," + household;
			if (m_charge_for_use)
				detail_columns += "," + rate + "," + amount;
			AnnualReport ar = new AnnualReport(new Select("name,locations_id,date," + amount).from(from), "Location", 2, 1, 4)
				.setDetail(detail_columns, from, "locations.id")
				.setParam("view_by");
			if (!m_charge_for_use)
				ar.setValueIsDouble(false);
			ar.write(null, r);
			return;
		case "rate category":
			String detail_id = r.getParameter("detail_id");
			if (m_allow_multiple_rate_categories) {
				StringBuilder columns = new StringBuilder();
				for (String c : m_rate_categories)
					columns.append(SQL.columnName(c)).append(",");
				columns.append("date,").append(amount);
				if (detail_id != null) {
					double d = getRate(detail_id);
					amount = SQL.columnName(detail_id) + "*" + d + " AS amount";
					detail_columns = date + "," + household + "," + SQL.columnName(detail_id) + " AS count," + d + "::money AS rate," + amount;
				}
				new AnnualReport(new Select(columns.toString()).from(m_events_table), "Rate Category", m_rate_categories)
					.setDetail(detail_columns, m_events_table, detail_id == null ? null : SQL.columnName(detail_id))
					.setDetailOneColumnsIsCounter(true)
					.setDisplayAsCurrency(detail_id != null)
					.setParam("view_by")
					.setValueIsDouble(detail_id != null)
					.write(null, r);
			} else
				new AnnualReport(new Select("rate_category,date," + amount).from(m_events_table), "Rate Category", 0, 1, 3)
					.setDetail(detail_columns, m_events_table, "rate_category")
					.setDisplayAsCurrency(detail_id != null)
					.setParam("view_by")
					.write(null, r);
			return;
		}
	}

	//--------------------------------------------------------------------------

	public final void
	writeReportPicker(Request r) {
		if (m_reports.size() > 1) {
			r.w.write("<h3>").write(getDisplayName(r)).write(" Report by ");
			new ui.Select(null, m_reports).setInline(true).setOnChange("net.replace('#annual_report',context+'/" + m_name + "/report?view_by='+this.options[this.selectedIndex].text)").write(r.w);
			r.w.write("</h3>");
		} else
			r.w.h3(getDisplayName(r) + " Report");
		r.w.write("<div id=\"annual_report\" style=\"padding-top:20px;\"></div>")
			.js("net.replace('#annual_report',context+'/" + m_name + "/report?view_by=" + m_reports.get(0) + "');");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, boolean hide_active, Request r) {
		if (column_names == null)
			column_names = new String[] { "m_active", "m_agreements", "m_charge_for_events", "m_check_overlaps", "m_color", "m_default_view", "m_display_items", "m_display_name", "m_end_date_label", "m_events_can_repeat", "m_events_have_category", "m_events_have_color", "m_events_have_event", "m_events_have_location", "m_events_have_start_time", "m_ical_items", "m_jobs", "m_reservation_report_by_quarter", "m_role", "m_show_on_menu", "m_show_on_upcoming", "m_start_date_label", "m_support_announcing_new_events", "m_support_attachments", "m_support_registrations", "m_support_reminders", "m_text_above_calendar", "m_text_after_form", "m_tooltip_items", "m_uuid" };
		super.writeSettingsForm(column_names, in_dialog, hide_active, r);
	}
}
