package calendar;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

import app.Request;
import app.Site;
import biweekly.component.VEvent;
import db.Rows;
import util.Time;

final class GuestRoomEvent extends Event {
	String					m_guest;
	boolean					m_invoiced;
	private LocalDate		m_original_start_date;

	//--------------------------------------------------------------------------

	GuestRoomEvent(GuestRoomEventProvider event_provider, Rows rows) {
		super(event_provider, rows);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	adjustTimezone(ZoneId zone_id) {
		if (m_start_time != null) {
			ZonedDateTime zoned_date_time = ZonedDateTime.of(m_start_date, m_start_time, Time.zone_id);
			zoned_date_time = zoned_date_time.withZoneSameInstant(zone_id);
			m_start_date = zoned_date_time.toLocalDate();
			m_start_time = zoned_date_time.toLocalTime();
		}
		if (m_end_date != null && m_end_time != null) {
			ZonedDateTime zoned_date_time = ZonedDateTime.of(m_end_date, m_end_time, Time.zone_id);
			zoned_date_time = zoned_date_time.withZoneSameInstant(zone_id);
			m_end_date = zoned_date_time.toLocalDate();
			m_end_time = zoned_date_time.toLocalTime();
		}
	}

	//--------------------------------------------------------------------------

//	@Override
//	public void
//	appendEventHTML(LocalDate date, StringBuilder s, boolean black_text, Request r) {
//		super.appendEventHTML(date, s, black_text, request);
//		if (m_charge_for_use && m_invoice_account_id != 0 && request.getUser() != null && "Accounting".equals(request.getSessionAttribute("current page")))
//			if (m_invoiced)
//				s.append("<br />invoiced");
//			else
//				s.append("<br />").append(request.w.ui.button("invoice").setOnClick("event.stopPropagation();net.post(context+'/" + m_ep.getName() + "/" + m_id + "/invoices/add',null,app.reload_main);return false;").toString());
//	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	appendItem(String item, boolean black_text, boolean html, boolean ics, LocalDate date, StringBuilder s, Request r) {
		switch(item) {
		case "guest":
			super.appendItem(item, m_guest, false, "calendar_event_text", black_text, html, ics, s);
			return;
		case "posted by":
			if ((((GuestRoomEventProvider)m_ep).showEventOwner() || m_event_text == null || m_event_text.length() == 0 || ics) && m_owner != 0) {
				/*if (html && ((GuestRoomEventProvider)m_ep).showEventOwnerOnMouseOver())
					s.append("<div onmouseover=\"this.firstChild.style.visibility='';\" onmouseout=\"this.firstChild.style.visibility='hidden';\"><div style=\"visibility:hidden;\">");
				else*/ if (s.length() > 0)
					s.append(ics ? " " : "\n");
				StringBuilder i = new StringBuilder();
				String label = ((GuestRoomEventProvider)m_ep).m_reserved_by_label;
				if (label != null)
					i.append(label).append(": ");
				i.append(Site.site.lookupName(m_owner, r.db));
				super.appendItem(null, i.toString(), false, "calendar_event_text", black_text, html, ics, s);
//				if (html && ((GuestRoomEventProvider)m_ep).showEventOwnerOnMouseOver())
//					s.append("</div></div>");
			}
			return;
		case "time":
			if (m_start_time != null && !ics)
				if (date == null) {
					if (html)
						s.append("<span class=\"calendar_time\">");
					else if (s.length() > 0)
						s.append("\n");
					s.append("check in: ").append(Time.formatter.getTime(m_start_time, true));
					if (m_end_time != null)
						s.append("\n").append("check out: ").append(Time.formatter.getTime(m_end_time, true));
					if (html)
						s.append("</span>");
				} else {
					boolean start = ChronoUnit.DAYS.between(date, m_original_start_date) == 0;
					boolean end = ChronoUnit.DAYS.between(date, m_end_date) == 0;
					if (start || end) {
						if (html)
							s.append("<span class=\"calendar_time\">");
						else if (s.length() > 0)
							s.append("\n");
						if (start && end)
							Time.formatter.appendTimeRange(m_start_time, m_end_time, s);
						else if (start)
							s.append("check in: ").append(Time.formatter.getTime(m_start_time, true));
						else if (end)
							s.append("check out: ").append(Time.formatter.getTime(m_end_time, true));
						if (html)
							s.append("</span>");
					}
				}
			break;
		default:
			super.appendItem(item, black_text, html, ics, date, s, r);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public int
	compareTo(Event event) {
		if (!(event instanceof GuestRoomEvent))
			return super.compareTo(event);
		int relation = m_start_date.compareTo(event.getStartDate());
		if (relation == 0 && m_timestamp != null)
			relation = m_timestamp.compareTo(((GuestRoomEvent)event).m_timestamp);
		return relation;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	load(Rows rows) {
		super.load(rows);
		m_notes = m_event_text;
		m_event_text = null;
		if (((GuestRoomEventProvider)m_ep).m_use_guest_field)
			m_guest = rows.getString("guest_name");
		if (rows.hasColumn("invoiced"))
			m_invoiced = rows.getBoolean("invoiced");
		m_original_start_date = m_start_date;
	}

	//--------------------------------------------------------------------------

	@Override
	public VEvent
	newVEvent(Request r) {
		VEvent vevent = new VEvent();
		StringBuilder summary = new StringBuilder();
		for (String item: m_ep.getIcalItems())
			appendItem(item, true, false, true, null, summary, r);
		vevent.setSummary(summary.toString());
		ZoneId zone_id = ZoneId.of(Site.settings.getString("time zone"));
		if (m_start_time != null)
			vevent.setDateStart(java.util.Date.from(ZonedDateTime.of(m_start_date, m_start_time, zone_id).toInstant()));
		else
			vevent.setDateStart(java.util.Date.from(m_start_date.atStartOfDay(zone_id).toInstant()), false);
		if (m_end_time != null)
			vevent.setDateEnd(java.util.Date.from(ZonedDateTime.of(m_end_date, m_end_time, zone_id).toInstant()));
		else
			vevent.setDateEnd(java.util.Date.from(m_end_date.plusDays(1).atStartOfDay(zone_id).toInstant()), false);
		return vevent;
	}
}
