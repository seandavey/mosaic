package calendar;

import java.time.LocalDate;

import app.Request;
import db.Rows;
import util.Text;

public class LaundryEvent extends Event {
	private int m_washes;
	private int m_drys;

	//--------------------------------------------------------------------------

	public
	LaundryEvent(EventProvider event_provider, Rows rows) {
		super(event_provider, rows);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	appendItem(String item, boolean black_text, boolean html, boolean ics, LocalDate date, StringBuilder s, Request r) {
		if (item.equals("washes/drys")) {
			super.appendItem(item, m_washes + " " + Text.pluralize("wash", m_washes) + ", " + m_drys + " " + Text.pluralize("dry", m_drys), false, "calendar_event_text", black_text, html, ics, s);
			return;
		}
		super.appendItem(item, black_text, html, ics, date, s, r);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	load(Rows rows) {
		super.load(rows);
		for (String rate_category : ((LaundryEventProvider)m_ep).m_rate_categories)
			if (rate_category.endsWith(" washes"))
				m_washes += rows.getInt(rate_category);
			else
				m_drys += rows.getInt(rate_category);
	}
}
