package calendar;

import app.Request;
import db.Rows;
import db.ViewDef;
import db.access.RecordOwnerAccessPolicy;
import db.column.OptionsColumn;
import db.object.DBObject;

public class LaundryEventProvider extends MosaicEventProvider {
	public
	LaundryEventProvider(String name) {
		super(name);
		setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit());
		m_allow_multiple_rate_categories = true;
		m_display_items = new String[] { "time", "posted_by", "washes/drys" };
		m_events_have_event = false;
		m_events_have_start_time = true;
	}

	//--------------------------------------------------------------------------

	@Override
	protected String[]
	getFieldOptions(String field_name) {
		return getFieldOptions(new String[] { "notes", "posted by", "time", "washes/drys" });
	}

	//--------------------------------------------------------------------------

	@Override
	protected Event
	loadEvent(Rows rows) {
		return new LaundryEvent(this, rows);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		ViewDef view_def = super._newViewDef(name);
		if (name.equals(m_name)) {
			view_def.getColumn("start_time").setIsRequired(true);
			view_def.getColumn("end_time").setIsRequired(true);
			String[] options = new String[] { "0", "1", "2", "3", "4", "5" };
			for (String rate_category : m_rate_categories)
				view_def.setColumn(new OptionsColumn(rate_category, options));
		}
		return view_def;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request r) {
		DBObject.set(this, r);
		m_events_can_repeat = false;
		super.updateSettings(r);
	}
}
