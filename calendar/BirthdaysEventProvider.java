package calendar;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.PriorityQueue;

import app.Request;
import db.DBConnection;
import db.Rows;
import db.Select;
import db.access.AccessPolicy;
import db.object.JSONField;
import util.Text;
import util.Time;

public class BirthdaysEventProvider extends MosaicEventProvider {
	class BirthdayReminder extends Reminder {
		int m_people_id;
		public BirthdayReminder(int people_id, EventProvider event_provider, Rows rows) {
			super(0, null, null, event_provider, rows);
			m_people_id = people_id;
		}
	}

	@JSONField
	private boolean m_show_on_login_page = true;

	//--------------------------------------------------------------------------

	public
	BirthdaysEventProvider() {
		super("Birthdays");
		m_check_overlaps = false;
		m_events_table = null;
		m_show_on_menu = true;
		m_support_reminders = true;
		setAccessPolicy(new AccessPolicy());
		setColor("#FF851B");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addAllEvents(ArrayList<Event> events, Request r) {
		Rows rows = new Rows(new Select("first,last,birthday").from("people").where("active AND show_birthday_on_calendar AND birthday IS NOT NULL"), r.db);
		while (rows.next())
			events.add(new Event(this, rows.getDate(3), Text.join(" ", "Birthday:", rows.getString(1), rows.getString(2)), 0).setRepeat("every year"));
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addCalendarsEvents(PriorityQueue<Event> events, LocalDate from, LocalDate to, Request r) {
		if (!m_show_on_login_page && r.getUser() == null)
			return;
		addEvents(events, from, to, r);
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	addEvents(PriorityQueue<Event> events, LocalDate from, LocalDate to, Request r) {
		to = to.minusDays(1);
		String from_date = from.toString();
		String to_date = to.toString();
		int from_year = from.getYear();
		int to_year = to.getYear();
		String where = "active AND birthday IS NOT NULL AND show_birthday_on_calendar AND (birthday + (" + from_year + " - date_part('year',birthday)) * interval '1 year' BETWEEN '" + from_date + "' AND '" + to_date + "' OR birthday + (" + to_year + " - date_part('year',birthday)) * interval '1 year' BETWEEN '" + from_date + "' AND '" + to_date + "')";
		Rows rows = new Rows(new Select("first,last,birthday").from("people").where(where), r.db);
		while (rows.next()) {
			LocalDate date = rows.getDate(3);
			int month = date.getMonthValue();
			LocalDate date_with_from_year = date.withYear(from_year);
			date = date_with_from_year.isBefore(from) ? date.withYear(to_year) : date_with_from_year;
			if (date.getMonthValue() != month) // February 29th changed to March 1st
				date = date.minusDays(1);
			events.add(new Event(this, date, /*"<span class=\"calendar_time\">Birthday</span>" + */ Text.join(" ", rows.getString(1), rows.getString(2)), 0));
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	adjustTables(DBConnection db) {
		adjustReminders(db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected String
	buildReminderMessage(Reminder reminder, DBConnection db) {
		return db.lookupString(new Select("first,last").from("people").whereIdEquals(((BirthdayReminder)reminder).m_people_id)) + "'s birthday is on " + Time.formatter.getDateLong(reminder.getDate());
	}

	//--------------------------------------------------------------------------

	@Override
	protected String
	buildReminderSubject(Reminder reminder, DBConnection db) {
		return "Birthday Reminder for " + db.lookupString(new Select("first,last").from("people").whereIdEquals(((BirthdayReminder)reminder).m_people_id));
	}

	//--------------------------------------------------------------------------

	@Override
	protected int
	countEvents(int target_max, LocalDate c, Request r) {
		return (m_show_on_login_page || r.getUser() != null) && r.db.exists("people", "birthday IS NOT NULL AND show_birthday_on_calendar") ? target_max : 0;
	}

	// --------------------------------------------------------------------------

	@Override
	protected String
	getEventEditJS(Event event, LocalDate date, Request r) {
		return null;
	}
	//--------------------------------------------------------------------------

	@Override
	protected String
	getRemindersTable() {
		return "birthday_reminders";
	}

	//--------------------------------------------------------------------------

	@Override
	protected ArrayList<Reminder>
	loadReminders(boolean nightly, DBConnection db) {
		if (!nightly)
			return null;

		final ArrayList<Reminder> reminders = new ArrayList<>();
		final String table = getRemindersTable();
		final LocalDate today = Time.newDate();

		Select query = new Select(table + ".id AS reminder_id,num,unit,before,email,note,repeat_days")
			.from(table)
			.where("(unit='day' OR unit='week')");
		Rows rows = new Rows(query, db);
		while (rows.next()) {
			int num_days = Reminder.numDays(rows.getInt("num"), rows.getString("unit"), rows.getBoolean("before"));
			LocalDate reminder_date = today.minusDays(num_days);
			Rows birthdays = new Rows(new Select("id,birthday").from("people").where("active AND birthday IS NOT NULL"), db);
			while (birthdays.next())
				if (Reminders.occursOn(today, reminder_date, rows.getString("repeat_days"), birthdays.getDate(2), null, "every year"))
					reminders.add(new BirthdayReminder(birthdays.getInt(1), this, rows));
			birthdays.close();
		}
		rows.close();

		return reminders;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showAddButton(Request r) {
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, boolean hide_active, Request r) {
		super.writeSettingsForm(new String[] { "m_active", "m_color", "m_display_name", "m_show_on_login_page", "m_show_on_menu", "m_show_on_upcoming", "m_uuid" }, in_dialog, hide_active, r);
	}
}
