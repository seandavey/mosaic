package mosaic;

import java.time.LocalDate;

import app.Site;
import util.Time;

public class MailTemplate implements app.MailTemplate {
	private StringBuilder m_s = new StringBuilder();
	
	// --------------------------------------------------------------------------

	public
	MailTemplate() {
		m_s.append("""
			<html>
			<head>
				<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
				<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
				<style>
					.amount {
						font-size: 300%;
					}
					.body {
						background-color: #f6f5f1;
						color: rgb(73, 80, 87);
						font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
						padding: 20px;
					}
					.due {
						background-color: white;
						margin-bottom: 20px;
						padding: 20px;
						text-align: center;
					}
					.footer {
						background: white;
						border: #2551B3 solid 10px;
						padding: 10px;
					}
					h3 {
						color: #2551B3;
						font-size: 1.75rem;
					}
					.header {
						background: radial-gradient(at left top, #2551B3, #7EA5ED);
						font-family: Times New Roman;
						padding: 40px;
					}
					.header {
						a {
							color: white;
							text-decoration: none;
						}
						h1 {
							font-size: 300%;
							font-style: italic;
						}
						h1,h2,h3,h4,h5,h6 {
							color: white;
						}
						h2 {
							font-size: 1.5rem;
						}
					}
					.table {
						background-color: white;
						width: 100%;
					}
					@media (max-width:670px) {
						.table {
							font-size: 10pt;
						}
					}
					.table th, td {
						padding: 10px;
					}
					.table thead {
						background-color: #eddab2;
						color: #222222;
						font-size: 16px;
						line-height: 120%
					}
					.table tr:nth-child(even) {
					    background-color: #f9f9f9;
					}
				</style>
			</head>
			<body>
				<div style="margin:0 auto;max-width:1024px">""");
	}
	
	// --------------------------------------------------------------------------

	public final MailTemplate
	amount(String amount, LocalDate date) {
		m_s.append("<div class=\"due\">Amount Due<div class=\"amount\">" + amount + "</div>Due on " + Time.formatter.getDateShort(date) + "</div>");
		return this;
	}
	
	// --------------------------------------------------------------------------

	@Override
	public final MailTemplate
	append(String s) {
		m_s.append(s);
		return this;
	}
	
	// --------------------------------------------------------------------------

	@Override
	public final MailTemplate
	append(int i) {
		m_s.append(i);
		return this;
	}
	
	// --------------------------------------------------------------------------

	@Override
	public final MailTemplate
	header(String title) {
		m_s.append("""
			<div class="header">
				<h2 style="margin-top:0"><a href=\"""" + Site.site.getAbsoluteURL() + "\" target=\"_blank\">" + Site.site.getDisplayName() + """
					</a><span style="float:right">""" + Time.formatter.getDateShort(Time.newDate()) + """
					</span>
				</h2>
				<h1 style="margin-bottom:0">""" + title + """
				</h1>
			</div>
			<div class="body">""");
		return this;
	}

	// --------------------------------------------------------------------------

	private MailTemplate
	footer() {
		m_s.append("""
			</div>
			<div class="footer">
				<a href="https://mosaicsoftware.org" target="_blank">
					<img src="https://mosaicsoftware.org/user/themes/zl/images/mosaic.svg" style="height:50px" />
					<img src="https://mosaicsoftware.org/user/themes/zl/images/mosaic_text.png" style="height:50px" />
				</a>
			</div>""");
		return this;
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	toString() {
		footer();
		m_s.append("</div></body></html>");
		return m_s.toString();
	}
}
