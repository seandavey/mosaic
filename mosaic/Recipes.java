package mosaic;

import java.io.IOException;
import java.sql.Types;
import java.util.ArrayList;

import app.Request;
import app.Site;
import db.Categories;
import db.DBConnection;
import db.Select;
import db.ViewDef;
import db.ViewState;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.PersonColumn;
import db.column.TextAreaColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Tabs;
import pages.Page;
import social.NewsProvider;
import web.HTMLWriter;

public class Recipes extends NewsProvider {
	private Categories	m_categories = new Categories("recipes", false);

	//--------------------------------------------------------------------------

	public Recipes() {
		super("recipes", "recipe");
		m_description = "A list of recipes with ingredients and instructions; interfaces with the News Feed";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page(getName(), this, false){
			@Override
			public void
			writeContent(Request r) {
				if (r.db.hasColumn("recipes", "coho"))
					ViewState.setBaseFilter("recipes", "coho", r);
				Site.site.newView("recipes", r).writeComponent();
				int v = r.getInt("v", 0);
				if (v != 0)
					r.w.js("window.addEventListener('load',function(){_.table.dialog_view(this,'recipes',context+'/Views/recipes/component?db_mode=READ_ONLY_FORM&db_key_value=" + v + "')})");
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("recipes")
			.add(new JDBCColumn("directions", Types.VARCHAR))
			.add(new JDBCColumn("ingredients", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people").setOnDeleteSetNull(true))
			.add(new JDBCColumn("title", Types.VARCHAR));
		m_categories.addJDBCColumn(table_def);
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, true, db);
		m_categories.adjustTables(db);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getItemAction(int id, DBConnection db) {
		return "posted a recipe";
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getItemURL(int item_id, Request r) {
		return Site.site.getAbsoluteURL("Recipes").set("v", item_id).toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("recipes")) {
			ArrayList<String> column_names = new ArrayList<>();
			column_names.add("title");
			column_names.add("ingredients");
			column_names.add("directions");
			column_names.add(m_categories.getColumnName());
			column_names.add("_owner_");
			ViewDef view_def = new ViewDef(name);
			addColumnSendEmail(column_names, view_def);
			addColumnShowOnNewsFeed(column_names, view_def);
			return addHooks(view_def
				.addSection(new Tabs(m_categories.getColumnName(), m_categories.getOrderBy()))
				.setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit())
				.setDefaultOrderBy("lower(title)")
				.setRecordName("Recipe", true)
				.setViewLinkColumn("title")
				.setColumnNamesForm(column_names)
				.setColumnNamesTable(new String[] { "title" })
				.setColumn(m_categories.getColumn())
				.setColumn(new TextAreaColumn("directions").setIsRichText(true, false))
				.setColumn(new TextAreaColumn("ingredients").setIsRichText(true, false))
				.setColumn(new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("posted by").setIsReadOnly(true)))
				.setColumn(new Column("title").setIsRequired(true));
		}
		ViewDef view_def = m_categories.newViewDef(name);
		if (view_def != null)
			return view_def;
		return null;
	}

	//--------------------------------------------------------------------------
	/**
	 * @throws IOException
	 */

	@Override
	public void
	writeAddButton(Request r) {
		writeAddButton("journal-text", "recipes", r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, boolean for_mail, HTMLWriter w, DBConnection db) {
		w.write("<a onclick=\"new Dialog({cancel:true,ok:{text:'done',click:function(){Dialog.top().close()}},cancel:false,title:'View Recipe',url:context+'/Views/recipes?db_key_value=")
			.write(item_id).write("&amp;db_mode=READ_ONLY_FORM',owner:_.c(this)});return false;\" href=\"#\">")
			.write(db.lookupString(new Select("title").from("recipes").whereIdEquals(item_id)))
			.write("</a>");
	}
}
