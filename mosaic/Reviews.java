package mosaic;

import java.sql.Types;

import app.Request;
import app.Site;
import attachments.Attachments;
import attachments.ViewDefWithAttachments;
import db.Categories;
import db.DBConnection;
import db.OneToMany;
import db.Select;
import db.View.Mode;
import db.ViewDef;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.PersonColumn;
import db.column.TextAreaColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Tabs;
import pages.Page;
import social.NewsProvider;
import web.HTMLWriter;

class Reviews extends NewsProvider {
	private Attachments	m_attachments = new Attachments("reviews");
	private Categories	m_categories = new Categories("reviews", false);

	// --------------------------------------------------------------------------

	public
	Reviews() {
		super("reviews", "review");
		m_description = "Share reviews of anything, including movies, books, plumbers, dentists, etc.";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		m_attachments.init(db);
		Site.pages.add(new Page("Reviews", this, false){
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("reviews", r).writeComponent();
				int v = r.getInt("v", 0);
				if (v != 0)
					r.w.js("window.addEventListener('load',function(){_.table.dialog_view(this,'reviews',context+'/Views/reviews/component?db_mode=READ_ONLY_FORM&db_key_value=" + v + "')})");
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("reviews")
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("contact", Types.VARCHAR))
			.add(new JDBCColumn("review", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"));
		m_categories.addJDBCColumn(table_def);
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
		m_categories.adjustTables(db);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getItemAction(int id, DBConnection db) {
		return "posted a review";
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getItemURL(int item_id, Request r) {
		return getAbsoluteURL().set("v", item_id).toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("reviews"))
			return addHooks(new ViewDefWithAttachments(name, m_attachments)
				.addSection(new Tabs(m_categories.getColumnName(), m_categories.getOrderBy()))
				.setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit())
				.setDefaultOrderBy("lower(title)")
				.setRecordName("Review", true)
				.setViewLinkColumn("title")
				.setColumnNamesForm(
					m_add_new_items_to_news_feed ?
						new String[] { "title", m_categories.getColumnName(), "review", "_owner_", "show_on_news_feed" } :
						new String[] { "title", m_categories.getColumnName(), "review", "_owner_" })
				.setColumnNamesTable(new String[] { "title" })
				.setColumn(m_categories.getColumn())
				.setColumn(new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("posted by").setIsReadOnly(true))
				.setColumn(new TextAreaColumn("review").setIsRichText(true, false))
				.setColumn(new Column("show_on_news_feed").setHideOnForms(Mode.READ_ONLY_FORM)))
				.addRelationshipDef(new OneToMany("reviews_attachments"));
		if (name.equals("reviews_attachments"))
			return m_attachments._newViewDef(name);
		ViewDef view_def = m_categories.newViewDef(name);
		if (view_def != null)
			return view_def;
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, boolean for_mail, HTMLWriter w, DBConnection db) {
		String[] row = db.readRow(new Select("title,review").from("reviews").whereIdEquals(item_id));
		w.h5(row[0])
			.p(row[1]);
		m_attachments.write(item_id, w, db);
	}
}
