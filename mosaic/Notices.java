package mosaic;

import java.io.IOException;
import java.sql.Types;

import app.Request;
import app.Site;
import db.DBConnection;
import db.OneToMany;
import db.Select;
import db.SelectRenderer;
import db.ViewDef;
import db.access.AccessPolicy;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.PersonColumn;
import db.column.PictureColumn;
import db.column.TextAreaColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Tabs;
import pages.Page;
import social.NewsProvider;
import web.HTMLWriter;

public class Notices extends NewsProvider {
	@JSONField
	private String[] m_categories = new String[] {"For Sale", "Free Garden Produce", "Free Items", "Help Wanted", "Help Available", "Wanted to Buy", "Wanted for Free", "To Trade"};

	//--------------------------------------------------------------------------

	public Notices() {
		super("notices", "notice");
		m_description = "List for things like Items For Sale, Free Garden Produce, Free items, Help Wanted, Help Available, Wanted to Buy Items, Items Wanted for Free, Items To Trade, etc.";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Notices", this, false){
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("notices", r).writeComponent();
				int v = r.getInt("v", 0);
				if (v != 0)
					r.w.js("window.addEventListener('load',function(){_.table.dialog_view(this,'notices',context+'/Views/notices/component?db_mode=READ_ONLY_FORM&db_key_value=" + v + "')})");
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("notices")
			.add(new JDBCColumn("category", Types.VARCHAR))
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("description", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", Types.INTEGER))
			.add(new JDBCColumn("title", Types.VARCHAR));
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
		db.createManyTable("notices", "notices_pictures", "filename VARCHAR");
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getItemAction(int id, DBConnection db) {
		return "posted a notice";
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getItemURL(int item_id, Request r) {
		return getAbsoluteURL().set("v", item_id).toString();
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("notices"))
			return addHooks(new ViewDef(name)
				.addSection(new Tabs("category").setPluralize(false))
				.setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit().view())
				.setDefaultOrderBy("lower(title)")
				.setRecordName("Notice", true)
				.setColumnNamesForm(new String[] { "category", "title", "description", "_owner_", "date" })
				.setColumnNamesTable(new String[] { "title", "_owner_", "date" })
				.setColumn(new Column("category").setInputRenderer(new SelectRenderer(m_categories, null)))
				.setColumn(new Column("date").setDefaultToDateNow().setDisplayName("date posted"))
				.setColumn(new TextAreaColumn("description").setIsRichText(true, false))
				.setColumn(new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("posted by").setIsReadOnly(true))
				.setColumn(new Column("title").setIsRequired(true))
				.addRelationshipDef(new OneToMany("notices_pictures")));
		if (name.equals("notices_pictures"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete())
				.setRecordName("Picture", true)
//				.setShowColumnHeads(false)
				.setColumnNamesTable("filename")
				.setColumn(new PictureColumn("filename", "notices_pictures", "notices", 200, 1024).setImageSize(100).setUseImageMagick(true).setDirColumn("notices_id").setMultiple(true));
		return null;
	}

	//--------------------------------------------------------------------------
	/**
	 * @throws IOException
	 */

	@Override
	public void
	writeAddButton(Request r) {
		writeAddButton("newspaper", "notices", r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, boolean for_mail, HTMLWriter w, DBConnection db) {
		w.write("<a onclick=\"new Dialog({cancel:true,ok:{text:'done',click:function(){Dialog.top().close()}},cancel:false,title:'View Notice',url:context+'/Views/notices?db_key_value=")
			.write(item_id).write("&amp;db_mode=READ_ONLY_FORM',owner:_.c(this)});return false;\" href=\"#\">")
			.write(db.lookupString(new Select("title").from("notices").whereIdEquals(item_id)))
			.write("</a>");
	}
}
