package mosaic;

import java.util.Set;

import app.Site;
import calendar.EventProvider;
import db.DBConnection;
import db.Select;
import email.MailLists;
import groups.GroupEventProvider;
import groups.Groups;
import mail.Mail;

public class ReminderHandler implements calendar.ReminderHandler {
	@Override
	public boolean
	handleReminder(EventProvider ep, String from, String recipient, String subject, String content, DBConnection db) {
		if (recipient == null)
			return false;
		if ("group".equals(recipient)) {
			int group_id = ((GroupEventProvider)ep).getGroupID();
			Set<String> emails = Groups.getGroupEmails(group_id, db);
			String list = db.lookupString(new Select("name").from("mail_lists").where("active AND send_to='Group:" + group_id + "'"));
			Mail mail = new Mail(subject, content)
				.to(emails);
			if (list != null)
				mail.from(list);
			mail.send();
			return true;
		}
		if ("poster".equals(recipient) && ep instanceof GroupEventProvider) {
			String list = db.lookupString(new Select("name").from("mail_lists").where("active AND send_to='Group:" + ((GroupEventProvider)ep).getGroupID() + "'"));
			Mail mail = new Mail(subject, content)
				.to(recipient);
			if (list != null)
				mail.from(list);
			mail.send();
			return true;
		}
		if (!recipient.endsWith(Site.site.getDomain()))
			return false;
		String list = recipient.substring(0, recipient.indexOf('@'));
		if (!db.exists("mail_lists", "active AND name ILIKE '" + list + "'"))
			return false;
		((MailLists)Site.site.getModule("MailLists")).send(list, from, subject, content, true, db);
		return true;
	}
}
