package mosaic;

import java.util.List;
import java.util.TreeSet;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import app.Request;
import app.Site;
import app.Stringify;
import blocks.Blocks;
import db.DBConnection;
import db.SQL;
import db.Select;
import db.View.Mode;
import db.ViewState;
import db.object.DBField;
import email.MailLists;
import groups.Groups;
import households.Household;
import households.Households;
import util.Text;
import web.HTMLWriter;
import web.JS;

@Stringify
public class Person extends app.Person {
	@DBField
	private int			m_households_id;
	private Household	m_household;

	//--------------------------------------------------------------------------

	public
	Person(Request r) {
		super(r);
	}

	//--------------------------------------------------------------------------

	public
	Person(int id, DBConnection db) {
		super(id, db);
	}

	//--------------------------------------------------------------------------

	public final Household
	getHousehold(DBConnection db) {
		if (m_household == null)
			m_household = Households.getHousehold(m_households_id, db);
		return m_household;
	}

	//--------------------------------------------------------------------------

	public final int
	getHouseholdId() {
		return m_households_id;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isInGroup(String group_name, DBConnection db) {
		return db.rowExists("people_groups", "people_id=" + m_id + " AND groups_id=" + db.lookupInt(new Select("id").from("groups").where("name=" + SQL.string(group_name)), -1));
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeProfile(Request r) {
		app.Person user = r.getUser();
		if (user == null)
			return;
		boolean is_user = m_id == user.getId();
		String picture = getPicture();
		HTMLWriter w = r.w;
		Blocks b = new Blocks(is_user, r.w).lookup(new Select("*").from("people").whereIdEquals(m_id), r.db);

		if (is_user)
			w.js("Dialog.top().set_title('Profile','h3')");
		else {
			String header = "<div style=\"display:inline-block;vertical-align:middle;\"><div>" + getName() + "</div>";
			if (m_pronouns != null)
				header += "<div style=\"font-size:small\">" + m_pronouns + "</div>";
			if (picture != null)
				header = "<img src=\"" + picture + "\" alt=\"\" style=\"max-height:100px\" /> " + header;
			w.js("Dialog.top().set_title(" + JS.string(header + "</div>") + ",'h1')");
		}

		JsonObject page = ((People)Site.site.getModule("People")).getPageTemplate();
		JsonArray blocks = page.get("blocks").asArray();
		for (int i=0; i<blocks.size(); i++) {
			JsonObject block = blocks.get(i).asObject();
			if (b.write(block))
				continue;
			switch(block.getString("type", null)) {
			case "groups":
				Groups groups = (Groups)Site.site.getModule("Groups");
				if (groups.isActive()) {
					TreeSet<String> g = new TreeSet<>();
					g.addAll(groups.getGroups(m_id, r.db));
					List<String[]> groups_jobs = groups.getGroupsJobs(m_id, r.db);
					for (String[] gj : groups_jobs) {
						if (g.contains(gj[0]))
							g.remove(gj[0]);
						g.add(gj[0] + " (" + gj[1] + ")");
					}
					if (g.size() > 0)
						w.h5(groups.getDisplayName(r))
							.addStyle("max-width:1024px")
							.p(Text.join(", ", g));
				}
				break;
			case "household":
				if (getHousehold(r.db) != null) {
					String number = r.db.lookupString(new Select("number").from("households JOIN homes ON households.homes_id=homes.id").whereEquals("households.id", m_household.getId()));
					if (number != null)
						w.ui.iconText("house", number);
				}
				break;
			case "img":
				b.writeImg(block, picture);
				break;
			case "mail lists":
				MailLists mail_lists = (MailLists)Site.site.getModule("MailLists");
				if (mail_lists.isActive()) {
					List<String> lists = mail_lists.getMailLists(m_id, r);
					if (lists.size() > 0)
						w.h5("Mail Lists")
							.addStyle("max-width:1024px")
							.p(Text.join(", ", lists));
				}
				break;
			case "pictures":
				if (is_user) {
					w.h5("Pictures")
						.addClass("mb-3 bg-light px-1 pt-1")
						.tagOpen("div");
					ViewState.setBaseFilter("people_bio_pictures", "people_id=" + m_id, r);
					Site.site.getViewDef("people_bio_pictures", r.db).newView(r).setMode(Mode.LIST).writeComponent();
					w.tagClose();
				} else {
					List<String> filenames = r.db.readValues(new Select("filename").from("people_bio_pictures").where("people_id=" + m_id));
					if (filenames.size() > 0) {
						w.write("<div onclick=\"new Gallery({dir:'people/").write(m_id).write("'}).open(event.target)\">");
						for (String filename : filenames)
							w.write("\n<img src=\"").write(Site.context).write("/people/").write(m_id).write("/thumbs/").write(filename).write("\" style=\"cursor:pointer;max-width:100%;\"/>");
						w.write("</div>");
					}
				}
				break;
			case "skills":
				if (is_user) {
					w.h5("Skills")
						.addClass("mb-3 bg-light px-1 pt-1")
						.tagOpen("div");
					ViewState.setBaseFilter("people_skills", "people_id=" + m_id, r);
					Site.site.getViewDef("people_skills", r.db).newView(r).setMode(Mode.LIST).writeComponent();
					w.tagClose();
				} else {
					List<String> skills = r.db.readValues(new Select("skill").from("people_skills").whereEquals("people_id", m_id).orderBy("skill"));
					if (skills.size() > 0)
						w.h5("Skills")
							.addStyle("max-width:1024px")
							.p(Text.join(", ", skills));
				}
				break;
			}
		}
		b.close();
	}
}
