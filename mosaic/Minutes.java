package mosaic;

import app.Request;
import app.Site;
import db.DBConnection;
import db.View;
import db.ViewDef;
import db.ViewState;
import db.column.LookupColumn;
import db.feature.Feature.Location;
import db.feature.LookupFilter;
import groups.GroupMinutesView;
import pages.Page;
import util.Text;

public class Minutes extends minutes.Minutes {
	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page(getName(), this, false){
			@Override
			public void
			writeContent(Request r) {
				ViewState.setQuicksearch("minutes", null, null, r);
				ViewState.setBaseFilter("minutes", "groups_id IS NULL OR (show_on_main_minutes_page AND (SELECT active AND NOT private FROM groups WHERE groups.id=groups_id))", r);
				Site.site.getViewDef("minutes", r.db).newView(r).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
				int v = r.getInt("v", 0);
				if (v != 0)
					r.w.js("window.addEventListener('load',function(){new Dialog({url:context+'/Minutes/" + v + "',title:'" + r.getParameter("label") + " Minutes',overflow:'auto'})})");
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("minutes"))
			return super._newViewDef(name)
				.addFeature(new LookupFilter("name", "groups", "active AND NOT private", "View", Location.TOP_LEFT).setNoneLabel(Text.pluralize(getMainMeetingLabel())))
				.setAfterForm("<script>if(_.$('#groups_id')){var s=_.$('#show_on_main_minutes_page_row');if(s)s.style.display=s.selectedIndex==0?'none':''}</script>")
				.setDefaultOrderBy("groups_id NULLS FIRST,date")
				.setViewClass(GroupMinutesView.class)
				.setColumnNamesTable(new String[] { "date", "groups_id", "summary" })
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setOnChange("var s=_.$('#show_on_main_minutes_page_row');if(s)s.style.display=this.selectedIndex==0?'none':''"));
		return super._newViewDef(name);
	}

}
