package mosaic;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import app.Request;
import app.Stringify;
import db.DBConnection;
import db.Form;
import db.NameValuePairs;
import db.Rows;
import db.SQL;
import db.Select;
import db.SelectRenderer;
import db.View;
import db.ViewDef;
import db.column.Column;
import db.column.ColumnBase;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import util.Text;
import web.HTMLWriter;

@Stringify
public class Jobs {
	@JSONField
	private String[] m_jobs;

	// --------------------------------------------------------------------------

	public final void
	addAddresses(ArrayList<String> a) {
		if (m_jobs == null)
			return;
		for (String job : m_jobs)
			a.add(job);
	}

	// --------------------------------------------------------------------------

	public final void
	addJobs(ArrayList<String> a) {
		if (m_jobs == null)
			return;
		for (String job : m_jobs)
			a.add(job);
	}

	//--------------------------------------------------------------------------

	public final int[]
	getIDs(Rows rows) {
		if (m_jobs == null)
			return null;
		int[] ids = new int[m_jobs.length];
		int i = 0;
		for (String job : m_jobs)
			ids[i++] = rows.getInt(job);
		return ids;
	}

	//--------------------------------------------------------------------------

	public final void
	addJDBCColumns(JDBCTable table_def) {
		if (m_jobs == null)
			return;
		for (String job : m_jobs)
			table_def.add(new JDBCColumn(job, Types.INTEGER));
	}

	//--------------------------------------------------------------------------

	public final void
	appendLabels(Rows rows, int id, StringBuilder labels) {
		if (m_jobs == null)
			return;
		for (String job : m_jobs)
			if (id == rows.getInt(job)) {
				if (labels.length() > 0)
					labels.append(", ");
				labels.append(job);
			}
	}

	//--------------------------------------------------------------------------

	public final void
	appendLabels(Rows rows, List<Integer> ids, StringBuilder labels) {
		if (m_jobs == null)
			return;
		for (String job : m_jobs)
			if (ids.contains(rows.getInt(job))) {
				if (labels.length() > 0)
					labels.append(", ");
				labels.append(job);
			}
	}

	//--------------------------------------------------------------------------

	public final void
	appendWhere(int id, StringBuilder where) {
		if (m_jobs == null)
			return;
		boolean first = true;
		for (String job : m_jobs) {
			if (first)
				first = false;
			else
				where.append(" OR ");
			where.append(SQL.columnName(job)).append('=').append(id);
		}
	}

	//--------------------------------------------------------------------------

	public final void
	appendWhere(String ids, StringBuilder where) {
		if (m_jobs == null)
			return;
		boolean first = true;
		for (String job : m_jobs) {
			if (first)
				first = false;
			else
				where.append(" OR ");
			where.append(SQL.columnName(job)).append(" IN(").append(ids).append(")");
		}
	}

	//--------------------------------------------------------------------------

	public final String[]
	getLabels() {
		if (m_jobs == null)
			return null;
		String[] labels = new String[m_jobs.length];
		for (int i=0; i<m_jobs.length; i++)
			labels[i] = m_jobs[i];
		return labels;
	}

	//--------------------------------------------------------------------------

	public final boolean
	isValidAddress(String address) {
		if (m_jobs == null)
			return false;
		for (String job : m_jobs)
			if (job.equals(address))
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	public final Rows
	selectJobs(String table, int id, DBConnection db) {
		return new Rows(new Select(Text.join(",", m_jobs)).from(table).whereIdEquals(id), db);
	}

	//--------------------------------------------------------------------------

	public final void
	setColumns(ViewDef view_def) {
		setColumns(view_def, null, false);
	}

	//--------------------------------------------------------------------------

	public final void
	setColumns(ViewDef view_def, SelectRenderer column_renderer, boolean no_set_value) {
		if (m_jobs == null)
			return;
		if (column_renderer == null)
			column_renderer = new SelectRenderer(new Select("id,first,last").from("people").where("active").orderBy("first,last"), "first,last", "id")
				.setAllowNoSelection(true);
		for (String job : m_jobs) {
			Column column = no_set_value ? new Column(job){
				@Override
				public void
				setValue(String value, NameValuePairs nvp, ViewDef view_def, int id, Request r) {
				}
			} : new Column(job);
			view_def.setColumn(column.setInputAndValueRenderers(column_renderer, false));
		}
	}

	//--------------------------------------------------------------------------

	public final void
	setColumnsHidden(View v) {
		if (m_jobs == null)
			return;
		for (String job : m_jobs)
			v.setColumn((ColumnBase<?>)v.getViewDef().cloneColumn(job).setIsHidden(true));
	}

	//--------------------------------------------------------------------------

	public final void
	setColumnsReadOnly(View v) {
		if (m_jobs == null)
			return;
		for (String job : m_jobs)
			v.setColumn((ColumnBase<?>)v.getViewDef().cloneColumn(job).setIsReadOnly(true));
	}

	//--------------------------------------------------------------------------

	public final Jobs
	setJobs(String... default_jobs) {
		m_jobs = default_jobs;
		return this;
	}

	//--------------------------------------------------------------------------

	public final int
	timesWorked(String table, int id, DBConnection db) {
		if (m_jobs == null)
			return 0;
		int times_worked = 0;
		for (String job : m_jobs)
			times_worked += db.lookupInt("COUNT(*)", table, job + "=" + id, 0);
		return times_worked;
	}

	//--------------------------------------------------------------------------

	public final void
	update(String table, int id, Request r) {
		if (m_jobs == null)
			return;
		if (!r.getBoolean("db_update_jobs"))
			return;
		StringBuilder s = new StringBuilder();
		for (String job : m_jobs) {
			int i = r.getInt(job, -1);
			if (s.length() > 0)
				s.append(",");
			s.append(SQL.columnName(job) + "=" + (i != -1 ? i : "NULL"));
		}
		r.db.update(table, s.toString(), id);
	}

	//--------------------------------------------------------------------------

	public final void
	writeFormRows(Form f, View v) {
		if (m_jobs == null)
			return;
		for (String job : m_jobs)
			f.writeColumnRow(job, v.getColumn(job));
	}

	//--------------------------------------------------------------------------

	public final void
	writeJobsJS(HTMLWriter w) {
		if (m_jobs == null)
			return;
		for (int i=0; i<m_jobs.length; i++) {
			if (i > 0)
				w.comma();
			w.jsString(m_jobs[i]);
		}
	}
}
