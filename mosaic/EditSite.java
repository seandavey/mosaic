package mosaic;

import java.util.Map;
import java.util.TreeMap;

import app.Module;
import app.Request;
import app.Site;
import calendar.EventProvider;
import calendar.GuestRoomEventProvider;
import calendar.LaundryEventProvider;
import calendar.MosaicEventProvider;
import households.HouseholdEventProvider;
import ui.NavBar;
import web.JS;

class EditSite extends app.EditSite {
	@Override
	protected void
	addItems(Map<String,String> items, Request r) {
		super.addItems(items, r);
		items.put("Calendars", null);
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	addItemToNavBar(String item, NavBar nav_bar, Request r) {
		if (item.equals("Calendars")) {
			Map<String,String> calendars = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
			for (Module module: Site.site.getModules())
				if (module instanceof EventProvider && !(module instanceof HouseholdEventProvider)) {
					String name = module.getDisplayName(r);
					if (name != null)
						calendars.put(name, "new Dialog({cancel:true,modal:true,ok:true,title:'" + JS.escape(module.getDisplayName(r)) + " Settings',url:context+'/" + module.apiURL("settings") + "'})");
				}
			nav_bar.dropdownOpen("Calendars", false);
			for (String c : calendars.keySet())
				nav_bar.aOnClick(c, calendars.get(c));
			if (calendars.size() > 0)
				nav_bar.divider();
			nav_bar.aOnClick("Add New Calendar", "new_calendar()");
			nav_bar.dropdownClose();
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		switch (r.getPathSegment(1)) {
		case "Calendars":
			String segment_two = r.getPathSegment(2);
			if ("add".equals(segment_two)) {
				String name = r.getParameter("name");
				String type = r.getParameter("type");
				EventProvider ep = switch(type) {
					case "Guest Room" -> new GuestRoomEventProvider("calendar " + name);
					case "Laundry" -> new LaundryEventProvider("calendar " + name);
					default -> new MosaicEventProvider("calendar " + name);
				};
				ep.setCanBeDeleted(true);
				ep.setDisplayName(name);
				ep.setShowOnMenu(true);
				Site.site.addModule(ep, r.db);
				ep.store(r.db);
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	writePageMenu(Request r) {
		r.w.js("""
			function new_calendar(){
				new Dialog({cancel:true, ok:{
					click:function(){
						let n = this.get_value('name')
						if (!n) {
							Dialog.alert('Name Required','Please enter a name for the new calendar.')
							return
						}
						net.post(context+'/EditSite/Calendars/add','name='+n+'&type='+this.get_value('type'),
							function(){
								Dialog.top().close()
								app.refresh_page_menu(function(){app.page_menu_select(n)})
						})
					},
					text:'ok'},
					title:'Add Calendar'
				}).tr('Name').add_input('name','text').tr('Type').add_select('type',['Regular','Guest Room','Laundry']).open()
			}""");
		return super.writePageMenu(r);
	}
}
