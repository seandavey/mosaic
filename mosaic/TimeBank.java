package mosaic;

import java.sql.Types;

import app.Request;
import app.Site;
import app.Site.Message;
import app.SiteModule;
import app.Subscriber;
import bootstrap5.ButtonNav;
import bootstrap5.ButtonNav.Panel;
import db.DBConnection;
import db.OrderBy;
import db.Rows;
import db.RowsTable;
import db.Select;
import db.ViewDef;
import db.access.AccessPolicy;
import db.access.Or;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.PersonColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Dividers;
import mail.Mail;
import pages.Page;
import social.Comments;
import ui.Table;
import util.Text;
import web.HTMLWriter;

class TimeBank extends SiteModule implements Subscriber {
	public
	TimeBank() {
		m_description = "Track time that members volunteer in tasks for each other";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Time Bank", this, false) {
			@Override
			public void
			writeContent(Request r) {
				int id = r.getUser().getId();
				float balance = r.db.lookupFloat(new Select("coalesce((SELECT sum(hours) FROM timebank WHERE _owner_=" + id + "),0.0) -  coalesce((SELECT sum(hours) FROM timebank WHERE recipient=" + id + "),0.0)"), 0);
				HTMLWriter w = r.w;
				w.write("<div class=\"alert alert-secondary\">Your current time bank balance is ").write(balance).space().write(Text.pluralize("hour", balance))
					.write("</div>");
				ButtonNav bn = new ButtonNav("button_nav", 1, r);
				bn.add(new Panel("By Date", "calendar", "/TimeBank/By Date"));
				if (r.getPathSegment(1) == null)
					Site.site.newView("timebank", r).writeComponent();
				bn.add(new Panel("By Person", "person", "/TimeBank/By Person"))
					.add(new Panel("Balances", "card-list", "/TimeBank/Balances"))
					.add(new Panel("Requests", "question", "/TimeBank/Requests"))
					.add(new Panel("Skills", "wrench", "/TimeBank/Skills"));
				bn.close();
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("timebank")
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("recipient", "people"))
			.add(new JDBCColumn("hours", Types.REAL))
			.add(new JDBCColumn("_owner_", "people").setOnDeleteSetNull(true))
			.add(new JDBCColumn("task", Types.VARCHAR));
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("timebank_requests")
			.add(new JDBCColumn("description", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		if (!"content".equals(r.getPathSegment(2)))
			return false;
		switch(segment_one) {
		case "Balances":
			Table table = r.w.ui.table().addClass("table").addClass("table-striped").addClass("table-condensed").addStyle("margin-top", "10px").open();
			table.th("Name").th("Balance");
			Rows rows = new Rows(new Select("first,last,coalesce((SELECT sum(hours) FROM timebank WHERE _owner_=people.id),0.0) -  coalesce((SELECT sum(hours) FROM timebank WHERE recipient=people.id),0.0)").from("people").where("people.id IN(SELECT _owner_ FROM timebank UNION SELECT recipient FROM timebank)").orderBy("first,last"), r.db);
			while (rows.next())
				table.tr().td(Text.join(" ", rows.getString(1), rows.getString(2))).td(Float.toString(rows.getFloat(3)));
			rows.close();
			table.close();
			return true;
		case "By Date":
			Site.site.newView("timebank", r).writeComponent();
			return true;
		case "By Person":
			Site.site.newView("timebank by person", r).writeComponent();
			return true;
		case "Requests":
			Site.site.newView("timebank_requests", r).writeComponent();
			return true;
		case "Skills":
			RowsTable.write(new Rows(new Select("skill,string_agg(first || ' ' || last, ', ' ORDER BY first,last)").from("people").join("people", "people_skills").where("active").groupBy("skill"), r.db), r.w);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		Site.site.addModule(new Comments("timebank"), db);
		Site.site.subscribe("timebank", this);
		Site.site.subscribe("timebank_comments", this);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("timebank"))
			return Comments.addFormHook(new ViewDef(name)
				.setAccessPolicy(new Or(new RecordOwnerAccessPolicy().add().delete().edit(), new RecordOwnerAccessPolicy().setColumn("recipient").add().delete().edit()))
				.setDefaultOrderBy("date")
				.setRecordName("Entry", true)
				.setColumnNamesForm("_owner_", "recipient", "task", "date", "hours")
				.setColumnNamesTable("date", "_owner_", "recipient", "task", "hours")
				.setColumn(new Column("date").setIsRequired(true))
				.setColumn(new PersonColumn("recipient", false))
				.setColumn(new Column("hours").setIsRequired(true))
				.setColumn(new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("provider").setIsReadOnly(true))
				.setColumn(new Column("task").setIsRequired(true)));
		if (name.equals("timebank_requests"))
			return new ViewDef(name)
				.setAccessPolicy(new RecordOwnerAccessPolicy().add().delete().edit())
				.setDefaultOrderBy("description")
				.setRecordName("Request", true)
				.setColumnNamesForm("description", "_owner_")
				.setColumnNamesTable("description", "_owner_")
				.setColumn(new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("posted by").setIsReadOnly(true));
		if (name.equals("timebank by person"))
			return new ViewDef(name)
				.addSection(new Dividers("name", new OrderBy("name")))
				.setAccessPolicy(new AccessPolicy())
				.setDefaultOrderBy("date")
				.setFrom("timebank")
				.setRecordName("Entry", true)
				.setSelectColumns("first||' '||last AS name,date,_owner_,recipient,task,hours,timebank.id")
				.setSelectFrom("people JOIN timebank ON people.id=_owner_ OR people.id=recipient")
				.setColumnNamesTable("date", "_owner_", "recipient", "task", "hours")
				.setColumn(new PersonColumn("recipient", false))
				.setColumn(new PersonColumn("_owner_", false).setDefaultToUserId().setDisplayName("provider").setIsReadOnly(true));
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	publish(Object object, Message message, Object data, DBConnection db) {
		if (object.equals("timebank") && message.equals(Message.INSERT)) {
			String email = db.lookupString(new Select("email").from("people").whereIdEquals(db.lookupInt(new Select("recipient").from("timebank").whereIdEquals((int)data), 0)));
			if (email != null)
				new Mail(Site.site.getDisplayName() + " Time Bank Entry Added", "Hello,\n\na new entry with you as the recipient has been added to the " + Site.site.getDisplayName() + " Time Bank. Click <a href=\"" + Site.site.getAbsoluteURL("Time Bank").set("edit", (int)data) + "\">here</a> to view the entry.")
					.to(email)
					.send();
			return;
		}
		if (object.equals("timebank_comments") && message.equals(Message.INSERT)) {
			int timebank_id = db.lookupInt(new Select("timebank_id").from("timebank_comments").whereIdEquals((int)data), 0);
			int comment_owner = db.lookupInt(new Select("_owner_").from("timebank_comments").whereIdEquals((int)data), 0);
			int provider = db.lookupInt(new Select("_owner_").from("timebank").whereIdEquals(timebank_id), 0);
			if (comment_owner != provider) {
				String email = db.lookupString(new Select("email").from("people").whereIdEquals(provider));
				if (email != null)
					new Mail(Site.site.getDisplayName() + " Time Bank Comment Added", "Hello,\n\na new comment for a time bank entry with you as the provider has been added. Click <a href=\"" + Site.site.getAbsoluteURL("Time Bank").set("edit", timebank_id) + "\">here</a> to view the entry.")
						.to(email)
						.send();
			}
			int recipient = db.lookupInt(new Select("recipient").from("timebank").whereIdEquals(timebank_id), 0);
			if (comment_owner != recipient) {
				String email = db.lookupString(new Select("email").from("people").whereIdEquals(recipient));
				if (email != null)
					new Mail(Site.site.getDisplayName() + " Time Bank Comment Added", "Hello,\n\na new comment for a time bank entry with you as the recipient has been added. Click <a href=\"" + Site.site.getAbsoluteURL("Time Bank").set("edit", timebank_id) + "\">here</a> to view the entry.")
						.to(email)
						.send();
			}
		}
	}
}