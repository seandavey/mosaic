package mosaic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import accounting.Accounting;
import app.Block;
import app.ClassTask;
import app.EditSite;
import app.ImageMagick;
import app.ImageUploads;
import app.MailTemplate;
import app.Module;
import app.Request;
import app.Roles;
import app.Site;
import calendar.BirthdaysEventProvider;
import calendar.Calendars;
import calendar.EventProvider;
import calendar.ListView;
import calendar.Locations;
import calendar.MosaicEventProvider;
import calendar.UpcomingEventsProvider;
import db.DBConnection;
import db.NameValuePairs;
import db.OneTable;
import db.Result;
import db.Rows;
import db.RowsSelect;
import db.SQL;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.CurrencyColumn;
import db.column.FileColumn;
import db.column.LookupColumn;
import db.column.OptionsColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import decisions.CommunityMeetingsEventProvider;
import decisions.Decisions;
import email.MailLists;
import email.PeopleMailHandlerFactory;
import email.WebMail;
import groups.GroupEventProvider;
import groups.GroupMailHandlerFactory;
import groups.Groups;
import households.Household;
import households.HouseholdEventProvider;
import households.HouseholdIdColumn;
import households.Households;
import jakarta.mail.Session;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lending.Lending;
import lists.Lists;
import mail.Mail;
import meals.MealEventProvider;
import meetings.Meetings;
import pictures.Pictures;
import sitemenu.SiteMenu;
import social.Comments;
import social.Discussions;
import social.News;
import social.NewsFeed;
import surveys.Surveys;
import todos.ToDos;
import ui.NavBar;
import ui.Option;
import ui.Table;
import ui.Tags;
import util.Text;
import web.Head;
import work.Work;

public class MOSAIC extends Site {
	public static MOSAIC mosaic;

	//--------------------------------------------------------------------------

	public
	MOSAIC(String base_directory) {
		super(base_directory, "Home", "coho");
		themes.setDefaultTheme("cerulean");
		mosaic = this;
	}

	//--------------------------------------------------------------------------

	@Override
	@ClassTask({"first","last","email","send activate email","make admin + send email"})
	public Result
	addPerson(String first, String last, String email, boolean send_activate_email, boolean make_admin, Request r) {
		Result result = super.addPerson(first, last, email, send_activate_email, make_admin, r);
		if (result.error != null)
			return result;
		NameValuePairs nvp = new NameValuePairs();
		nvp.set("name", last)
			.set("cm_account_active", true);
		int household_id = r.db.insert("households", nvp).id;
		nvp.clear()
			.set("households_id", household_id)
			.set("resident", true);
		r.db.update("people", nvp, result.id);
		r.db.insert("user_roles", "people_id,user_name,role", result.id + ",'" + first + last + "','coho'");
		r.db.update("households", "contact=" + result.id, household_id);
		if (make_admin)
			sendNewAdminEmail(result.id, r.db);
		return result;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addAddresses(ArrayList<String> addresses, DBConnection db) {
		((MailLists)Site.site.getModule("MailLists")).addAddresses(addresses, db);
	}

	//--------------------------------------------------------------------------

//	@ClassTask({"id"})
//	public void
//	copyPersonToCoHoLink(int id, Request r) {
//		try {
//			ResultSet person = r.db.select(new Select("*").from("people").whereIdEquals(id));
//			if (person.next()) {
//				Connection c = DriverManager.getConnection("jdbc:postgresql:coholink?user=" + (Site.site.isDevMode() ? "postgres" : "mosaic") + "&password=postgres");
//				Statement s = c.createStatement();
//				int groups_id = 0;
//				ResultSet group = s.executeQuery("SELECT id FROM groups WHERE name=" + SQL.string(Site.site.getDisplayName()));
//				if (group.next()) {
//					System.out.println("adding group " + Site.site.getDisplayName());
//					groups_id = group.getInt(1);
//					if (groups_id == 0) {
//						s.execute("INSERT INTO groups(mosaic_site,name,self_joining) VALUES(" + SQL.string(Site.site.getDomain()) + "," + SQL.string(Site.site.getDisplayName()) + ",false");
//						group = s.getResultSet();
//						if (group.next())
//							groups_id = group.getInt(1);
//					}
//				}
//				String user_name = person.getString("user_name");
//				s.execute("INSERT INTO people(first,last,email,user_name,password,active,theme) VALUES(" +
//					SQL.string(person.getString("first")) + "," +
//					SQL.string(person.getString("last")) + "," +
//					SQL.string(person.getString("email")) + "," +
//					SQL.string(user_name) + "," +
//					SQL.string(person.getString("password")) + ",true,'cerulean') RETURNING id");
//				person = s.getResultSet();
//				if (person.next()) {
//					int people_id = person.getInt(1);
//					s.execute("INSERT INTO user_roles(people_id,user_name,role) VALUES(" + people_id + "," + SQL.string(user_name) + ",'coho')");
//					s.execute("INSERT INTO people_groups(people_id,groups_id) VALUES(" + people_id + "," + groups_id + ")");
//				} else
//					System.out.println("person not added");
//				s.close();
//				c.close();
//			} else
//				System.out.println("person with id " + id + " not found");
//		} catch (SQLException e) {
//			System.out.println(e);
//		}
//	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(HttpServletRequest http_request, String[] path_segments, HttpServletResponse http_response) throws IOException {
		String path = http_request.getServletPath();
		if (path.endsWith(".ics")) {
			Request r = new Request(http_request, path_segments, http_response);
			int user_id = r.getInt("u", 0);
			String uuid = r.getParameter("uuid");
			if (user_id != 0 && uuid != null && uuid.length() == 36 && uuid.indexOf(' ') == -1 && r.db.rowExists("people", "id=" + user_id + " AND uuid='" + uuid + "'")) {
				if (path.startsWith("/HouseholdCalendar")) {
					Person user = new Person(user_id, r.db);
					Household household = user.getHousehold(r.db);
					if (household == null || household.getId() != Integer.parseInt(path.substring(15, path.length() - 4))) {
						r.release();
						return true;
					}
				}
				File file = newFile("calendars", path.substring(1));
				if (!file.exists()) {
					String module_name = path.substring(1, path.lastIndexOf('.'));
					EventProvider event_provider = (EventProvider)Site.site.getModule(module_name);
					if (event_provider != null)
						event_provider.writeICS(r);
					else {
						http_response.getWriter().write("calendar " + module_name + " not found\n");
						r.release();
						return true;
					}
				}
				PrintWriter w = http_response.getWriter();
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line = br.readLine();
				while (line != null) {
					w.println(line);
					line = br.readLine();
				}
				br.close();
            }
			r.release();
			return true;
		}
		if (path.endsWith("/receive_mail")) {
			Request r = new Request(http_request, path_segments, http_response);
			Session session = Session.getInstance(new Properties(), null);
			((MailLists)getModule("MailLists")).readAndSend(newFile("inbox", r.getPathSegment(-2)), session, r.db);
			r.release();
			return true;
		}
		if (path.startsWith("/upcoming/")) {
			Request r = new Request(http_request, path_segments, http_response);
			new ListView(new UpcomingEventsProvider(true), r).writeComponent(false, r);
			r.release();
			return true;
		}
		return super.doGet(http_request, path_segments, http_response);
	}
	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(HttpServletRequest http_request, String[] path_segments, HttpServletResponse http_response) throws IOException {
		if (!http_request.getServletPath().startsWith("/ImageMagick/"))
			return super.doPost(http_request, path_segments, http_response);
		Request r = new Request(http_request, path_segments, http_response);
		int n = path_segments.length - 2;
		String filename = path_segments[n];
		boolean is_thumb = "thumbs".equals(path_segments[n - 1]);
		if (is_thumb)
			--n;
		Path file_path = Site.site.getPath();
		for (int i=1; i<n; i++)
			file_path = file_path.resolve(path_segments[i]);
		int dot = filename.lastIndexOf('.');
		String new_filename = dot == -1 ? Text.uuid() : Text.uuid() + filename.substring(dot);
		ImageMagick.rotate(file_path, filename, new_filename, path_segments[path_segments.length - 1].endsWith(" clockwise"), r.getInt("thumb_size", 0));
		int id = r.getInt("id", 0);
		if (id != 0) {
			NameValuePairs nvp = new NameValuePairs();
			nvp.set(r.getParameter("column"), new_filename);
			String table = r.getParameter("table");
			if (r.db.hasColumn(table, "width")) {
				int[] size = ImageMagick.getSize(file_path.resolve(new_filename));
				if (size != null) {
					nvp.set("width", size[0]);
					nvp.set("height", size[1]);
				}
			}
			r.db.update(table, nvp , id);
		}
		http_response.setContentType("text/html");
		file_path = Site.site.getPath();
		if (is_thumb)
			++n;
		for (int i=1; i<n; i++)
			file_path = file_path.resolve(path_segments[i]);
		r.w.write(file_path.resolve(new_filename).toString());
		r.release();
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public Class<?>[]
	getClasses() {
		return new Class<?>[] { FileColumn.class };
	}

	//--------------------------------------------------------------------------

//	@AdminTask
//	public void
//	importPeople(Request r) {
//		DBConnection db = r.db;
//		db.delete("people", "id != 1");
//		db.delete("user_roles", "people_id != 1");
//		db.delete("households", null);
//		db.delete("homes", null);
//		db.delete("units", null);
//
//		CSVReader csv = new CSVReader(Site.site.getBaseFilePath().append("people.csv").toString(), 24);
//
//		NameValuePairs nvp = new NameValuePairs();
//		nvp.set("active", true);
//		String[] line = csv.readLine();
//		while (line != null) {
//			if (db.exists("people", "first='" + line[1] + "' AND last='" + line[7] + "'"))
//				continue;
//			int household_id = db.lookupInt(new Select("id").from("households").where("name='" + line[7] + "'"), -1);
//			if (household_id == -1) {
//				String home_number = line[4];
//				if (!line[6].equals("0"))
//					home_number += " Apt " + line[6];
//				int homes_id = db.lookupInt(new Select("id").from("homes").where("number='" + home_number + "'"), -1);
//				if (homes_id == -1) {
//					int units_id = db.lookupInt(new Select("id").from("units").where("unit='" + line[5] + "'"), -1);
//					if (units_id == -1)
//						units_id = db.insert("units", "unit", "'" + line[5] + "'");
//					homes_id = db.insert("homes", "number,units_id", "'" + home_number + "'," + units_id);
//				}
//				household_id = db.insert("households", "name,homes_id", "'" + line[7] + "'," + homes_id);
//			}
////			First Name,Last Name,Username,Password,House Number,Unit Number,Apartment,Household,Cell Phone,Home Phone,Email,Birthday,Female / Male,Owner / Renter,Adult/Child,Custom 1,Custom 2,Custom 3,Custom 4,Custom 5,Current Resident,Move In Date,Move Out Date,Bio
//			nvp.set("households_id", household_id);
//			nvp.set("email", line[0]);
//			nvp.set("first", line[0]);
//			nvp.set("last", line[1]);
//			nvp.set("user_name", line[0] + line[1]);
//			nvp.set("password", "md5('" + line[0] + line[1] + "')");
//			nvp.set("cell_phone", line[8]);
//			nvp.set("phone", line[9]);
//			nvp.set("email", line[10]);
//			nvp.set("birthday", line[11]);
//			nvp.set("female/male", line[12]);
//			nvp.set("owner", "Owner".equals(line[13]));
//			nvp.set("adult/child", line[14]);
//			nvp.set("bio", line[23]);
//			int people_id = db.insert("people", nvp);
//			db.insert("user_roles", "people_id,role,user_name", people_id + ",'coho','" + line[0] + line[1] + "'");
//			line = csv.readLine();
//		}
//	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getFooter() {
		return """
			<footer class="bg-white" style="margin-top:1rem">
				<div class="container">
					<div>
						<a href="https://mosaicsoftware.org" target="_blank">""" + 
							"<img src=\"" + Site.context + "/images/mosaic.svg\" style=\"height:50px\" /><img src=\"" + Site.context + """
							/images/mosaic_text.png" style="color:#325492;margin-left:5px;height:50px" /></a>
						<a href="https://docs.mosaicsoftware.org" style="color:#325492;text-decoration:none" target="_blank">Documentation</a>
						<a href="mailto:mosaic@mosaicsoftware.org" style="color:#325492;text-decoration:none">Send feedback / Report problem / Suggest new feature</a>
					</div>
				</div>
			</footer>""";
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(DBConnection db) {
		pages.setSupportPagesTable(true);
		Roles.add("admin", "can edit almost anything on the site with this role");
		Roles.add("calendar editor", "can edit events on any calendar with this role");
		addModule(newEditSite(), db);
		db.createTable("units", "unit VARCHAR,hoa_dues REAL");
		JDBCTable.adjustTable(newHomesTable(db), true, false, db);
		JDBCTable.adjustTable(newHouseholdsTable(), true, false, db);
		db.createManyToManyLinkTable("households", "homes");

		News news = new News();
		addModule(news, db);

		Accounting accounting = new Accounting();
		addModule(accounting, db);
		addModule(new Backups(), db);
		addModule(new Blackboard(), db);
		addModule(new Calendars(), db);
		addModule(new Contacts()
				.setDescription("Lists of contacts public for the whole site, groups, and/or private to households")
				.addJDBCColumn(new JDBCColumn("households")).addJDBCColumn(new JDBCColumn("groups")).addJDBCColumn(new JDBCColumn("public", Types.BOOLEAN)), db);
		CommunityMeetingsEventProvider community_meetings = newCommunityMeetingsEventProvider("Community Meetings");
		addModule(community_meetings, db);
		Decisions decisions = newDecisions(community_meetings);
		addModule(decisions, db);
		news.addProvider(decisions, db);
		app.People people = newPeople();
		if (decisions.systemIsOptIn())
			people.addJDBCColumn(new JDBCColumn("participant", Types.BOOLEAN));
		addModule(people, db);
		Discussions discussions = new Discussions();
		addModule(discussions, db);
		news.addProvider(discussions, db);
		((Comments)getModule(discussions.getTable() + "_comments")).setMaxHeight("none");
		Households households = newHouseholds();
		if (households != null) {
			String label = accounting.getAccountLabel();
			households.addBlock(new Block(label == null ? "HOA" : label) {
				@Override
				public boolean
				handlesContext(String c, Request r) {
					return accounting.isActive();
				}
				@Override
				public void
				write(String c, Request r) {
					r.w.h3(accounting.getAccountLabel());
					Households.writeAccountHistory((Person)r.getUser(), r.getInt("id", 0), r);
				}
			});
			households.addBlock(new Block("Community Work") {
				@Override
				public boolean
				handlesContext(String c, Request r) {
					return Site.site.moduleIsActive("Work") && r.userHasRole("coho");
				}
				@Override
				public void
				write(String c, Request r) {
					r.w.h3("Community Work");
					ViewState.setBaseFilter("work_hours person", "_owner_=" + r.getUser().getId(), r);
					Site.site.newView("work_hours person", r).writeComponent();
				}
			});
			addModule(households, db);
		}
		addModule(newGroups(), db);
		Documents documents = new Documents();
		addModule(documents, db); // must be after groups
		news.addProvider(documents, db);
		addModule(new HomePage(), db);
		if (households != null && households.isActive())
			addModule(new HouseholdEventProvider(), db);
		addModule(new ImageUploads(), db);
		addModule(new Lending(), db);
		addModule(new Lists("groups").addJDBCColumn(new JDBCColumn("groups")), db);
		addModule(new LoginPage(), db);
		MailLists mail_lists = new MailLists();
		mail_lists.addMailHandlerFactory(new GroupMailHandlerFactory());
		mail_lists.addMailHandlerFactory(new PeopleMailHandlerFactory("Everyone"));
		mail_lists.addMailHandlerFactory(new PeopleMailHandlerFactory("Owners").setFilter("owner"));
		mail_lists.addMailHandlerFactory(new PeopleMailHandlerFactory("Owners and Residents").setFilter("owner OR resident"));
		mail_lists.addMailHandlerFactory(new PeopleMailHandlerFactory("Residents").setFilter("resident"));
		addModule(mail_lists, db);
		EventProvider.setReminderHandler(new ReminderHandler());
		addModule(new Meetings(), db);
		Minutes minutes = (Minutes)new Minutes() {
			@Override
			protected String
			getItemTitle(int item_id, DBConnection db2) {
				int groups_id = db2.lookupInt(new Select("groups_id").from("minutes").whereIdEquals(item_id), 0);
				if (groups_id != 0)
					return db2.lookupString(new Select("name").from("groups").whereIdEquals(groups_id));
				return super.getItemTitle(item_id, db2);
			}
		}.addJDBCColumn(new JDBCColumn("groups")).addJDBCColumn(new JDBCColumn("show_on_main_minutes_page", Types.BOOLEAN).setDefaultValue(true));
		addModule(minutes, db);
		news.addProvider(minutes, db);
		NewsFeed news_feed = new NewsFeed();
		addModule(news_feed, db);
		news.addProvider(news_feed, db);
		Notices notices = new Notices();
		addModule(notices, db);
		news.addProvider(notices, db);
		Pictures pictures = new Pictures();
		pictures.addJDBCColumn(new JDBCColumn("can_show_on_login_page", Types.BOOLEAN).setDefaultValue(true));
		addModule(pictures, db);
		news.addProvider(pictures, db);
		Recipes recipes = newRecipes();
		addModule(recipes, db);
		news.addProvider(recipes, db);
		Reviews reviews = new Reviews();
		addModule(reviews, db);
		news.addProvider(reviews, db);
		addModule(new SiteMenu(), db);
		addModule(new Surveys(), db);
		addModule(new TimeBank(), db);
		addModule(new ToDos(){
			@Override
			public boolean
			accept(Rows rows, Request r) {
				int household_id = rows.getInt("households_id");
				if (household_id != 0)
					return household_id == ((Person)r.getUser()).getHousehold(r.db).getId();
				return false;
			}
		}.setAssignTasks(true).setGroupByProject(true).setOneTable("households"), db);
		addModule(new WebMail(), db);
		addModule(new Work(), db);

		if (db.hasColumn("people", "birthday"))
			addModule(new BirthdaysEventProvider(), db);
		addModule(newMainCalendar(), db);
		EventProvider common_meals = newMealEventProvider();
		addModule(common_meals, db);
		if (households != null && households.isActive())
			households.addBlock(new Block(common_meals.getDisplayName(null)) {
				@Override
				public boolean
				handlesContext(String c, Request r) {
					return common_meals.isActive();
				}
				@Override
				public void
				write(String c, Request r) {
					r.w.h3(common_meals.getDisplayName(r));
					((MealEventProvider)common_meals).writeMealHistory(r);
				}
			});

		super.init(db);
		startFiveMinuteThread();
	}

	//--------------------------------------------------------------------------

	@ClassTask({"date"})
	public static void
	listReminders(String date, Request r) {
		Table table = r.w.ui.table().addClass("table table-condensed table-bordered");
		table.tr().th("id").th("note").th("event").th("repeat").th("end_date").th("num").th("unit").th("before").th("email").th("date");
		for (EventProvider ep : Site.site.getModules(EventProvider.class))
				if (ep.supportsReminders())
					ep.listReminders(null, table, r);
		table.close();
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	loadModule(Module module, DBConnection db) {
		if (module instanceof GroupEventProvider && !Groups.isActive(((GroupEventProvider)module).getGroupID(), db))
			return false;
		return super.loadModule(module, db);
	}

	//--------------------------------------------------------------------------

	@ClassTask({"all"})
	public void
	loadDecisionsTokens(boolean all, DBConnection db) {
		String where = "text IS NOT NULL";
		if (!all)
			where += " AND tsvector IS NULL";
		Rows decisions = new Rows(new Select("id,text,title").from("decisions").where(where).orderBy("id"), db);
		while (decisions.next())
			db.update("decisions", "tsvector=to_tsvector('" + SQL.escape(decisions.getString(3) + "\n" + decisions.getString(2)) + "')", decisions.getInt(1));
		decisions.close();
	}

	//--------------------------------------------------------------------------

	public CommunityMeetingsEventProvider
	newCommunityMeetingsEventProvider(String name) {
		return new CommunityMeetingsEventProvider(name);
	}

	//--------------------------------------------------------------------------

	protected Decisions
	newDecisions(CommunityMeetingsEventProvider community_meetings) {
		return new Decisions(community_meetings).setOneTable(new OneTable("groups", "group"));
	}

	//--------------------------------------------------------------------------

	@Override
	protected EditSite
	newEditSite() {
		return new mosaic.EditSite();
	}

	//--------------------------------------------------------------------------

	protected Groups
	newGroups() {
		return new Groups();
	}

	//--------------------------------------------------------------------------

	protected JDBCTable
	newHomesTable(DBConnection db) {
		return new JDBCTable("homes")
			.add(new JDBCColumn("number", Types.VARCHAR).setUnique(true))
			.add(new JDBCColumn("units_id", Types.INTEGER))
			.add(new JDBCColumn("pays_hoa", "households").setOnDeleteSetNull(true))
			.add(new JDBCColumn("pays_water", "households").setOnDeleteSetNull(true));
	}

	//--------------------------------------------------------------------------

	protected Households
	newHouseholds() {
		return (Households)new Households().setRequired(true);
	}

	//--------------------------------------------------------------------------

	protected JDBCTable
	newHouseholdsTable() {
		return Households.getTable();
	}

	// --------------------------------------------------------------------------

	@Override
	public final MailTemplate
	newMailTemplate() {
		return new mosaic.MailTemplate();
	}

	//--------------------------------------------------------------------------

	protected EventProvider
	newMainCalendar() {
		return new MosaicEventProvider("Main Calendar")
			.setAllowViewSwitching(true)
			.setColor("#F0F8A1")
			.setEventsCanRepeat(true)
			.setEventsHaveCategory(true)
			.setEventsHaveLocation(true)
			.setEventsHaveTime(true)
			.setSupportRegistrations(true)
			.setSupportReminders();
	}

	//--------------------------------------------------------------------------

	protected EventProvider
	newMealEventProvider() {
		return new MealEventProvider().setColor("#7FDBFF");
	}

	//--------------------------------------------------------------------------

	protected app.People
	newPeople() {
		return (People)new mosaic.People()
			.addJDBCColumn(new JDBCColumn("address", Types.VARCHAR))
			.addJDBCColumn(new JDBCColumn("bio", Types.VARCHAR))
			.addJDBCColumn(new JDBCColumn("birthday", Types.DATE))
			.addJDBCColumn(new JDBCColumn("can_post_to_mail_lists", Types.BOOLEAN).setDefaultValue(true))
			.addJDBCColumn(new JDBCColumn("date_added", Types.DATE).setDefaultToCurrentTimestamp())
			.addJDBCColumn(new JDBCColumn("email_updates_to_my_conversations", Types.BOOLEAN).setDefaultValue(true))
			.addJDBCColumn(new JDBCColumn("emergency_contacts", Types.VARCHAR))
			.addJDBCColumn(new JDBCColumn("households_id", "households").setOnDeleteSetNull(true))
			.addJDBCColumn(new JDBCColumn("middle", Types.VARCHAR))
			.addJDBCColumn(new JDBCColumn("move_in_date", Types.DATE))
			.addJDBCColumn(new JDBCColumn("move_out_date", Types.DATE))
			.addJDBCColumn(new JDBCColumn("phone", Types.VARCHAR))
			.addJDBCColumn(new JDBCColumn("remove_me_when_i_post_to_lists", Types.BOOLEAN))
			.addJDBCColumn(new JDBCColumn("send_email_when_someone_posts_to_the_site", Types.BOOLEAN))
			.addJDBCColumn(new JDBCColumn("show_birthday_on_calendar", Types.BOOLEAN).setDefaultValue(true));
	}

	//--------------------------------------------------------------------------

	@Override
	public app.Person
	newPerson(Request r) {
		return new Person(r);
	}

	//--------------------------------------------------------------------------

	@Override
	public app.Person
	newPerson(int id, DBConnection db) {
		return new Person(id, db);
	}

	//--------------------------------------------------------------------------

	protected Recipes
	newRecipes() {
		return (Recipes)new Recipes() {
				{
					m_description = "Store recipes for households that can also be shared with the community.";
				}
				@Override
				public void
				afterInsert(int id, NameValuePairs nvp, Request r) {
					if (nvp.getBoolean("coho"))
						super.afterInsert(id, nvp, r);
				}
				@Override
				public String
				beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
					if (nvp.valueChanged("coho", m_table, id, r.db) && nvp.getBoolean("coho"))
						super.beforeUpdate(id, nvp, previous_values, r);
					return null;
				}
			}
			.addJDBCColumn(new JDBCColumn("coho", Types.BOOLEAN))
			.addJDBCColumn(new JDBCColumn("households_id", "households"));
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	newViewDef(String name) {
		if (name.equals("decisions"))
			return super.newViewDef(name)
//				.addSection(new Dividers("groups_id", new OrderBy("groups_id", false, true)).setNullValueLabel(null))
//				.setBaseFilter("groups_id IS NULL OR (SELECT active FROM groups WHERE groups.id=groups_id)")
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true));
		if (name.equals("edit lists"))
			return super.newViewDef(name)
				.setColumn(new LookupColumn("groups_id", "groups", "name"));
		if (name.equals("homes") || name.equals("homes edit") || name.equals("households homes")) {
			ViewDef view_def = new ViewDef(name)
				.setDefaultOrderBy("naturalsort(number)")
				.setFrom("homes")
				.setRecordName("Home", true)
				.setColumnNamesForm(new String[] { "number", "units_id", "pays_hoa", "pays_water" })
				.setColumnNamesTable(new String[] { "number", "units_id", "x" })
				.setColumn(new Column("number").setIsUnique(true))
				.setColumn(new LookupColumn("pays_hoa", "households", "name", "active", "name").setAllowNoSelection(true))
				.setColumn(new LookupColumn("pays_water", "households", "name", "active", "name").setAllowNoSelection(true).setViewRole("administrator"))
				.setColumn(new LookupColumn("units_id", "units", "unit").setAdjustQuery(true).setAllowEditing().setAllowNoSelection(true).setDisplayName("unit"))
				.setColumn(new Column("x") {
					@Override
					public boolean
					writeValue(View v, Map<String, Object> data, Request r) {
						Set<String> households = new TreeSet<>();
						Rows rows = new Rows(new Select("name").from("households").whereEquals("homes_id", v.data.getInt(1)).andWhere("active"), r.db);
						while (rows.next()) {
							String n = rows.getString(1);
							if (n != null)
								households.add(n);
						}
						rows.close();
						Set<String> owners = new TreeSet<>();
						rows = new Rows(new Select("name").from("households").joinOn("households_homes", "households.id=households_id").whereEquals("households_homes.homes_id", v.data.getInt(1)), r.db);
						while (rows.next())
							owners.add(rows.getString(1));
						rows.close();
						for (String owner : owners) {
							households.remove(owner);
							households.add(owner + " (owner)");
						}
						r.w.write(Text.join(", ", households));
						return true;
					}
				}.setDisplayName("Households").setIsSortable(false));
				// commented out because other homes_id column in households viewdef
				//				.addRelationshipDef(new ManyToMany("households", "households_homes", "name").setRecordName("Owner"));
			if (name.equals("homes"))
				view_def.setAccessPolicy(new AccessPolicy());
			else if (name.equals("households homes"))
				view_def.setAccessPolicy(new AccessPolicy().add().delete());
			else
				view_def.setAccessPolicy(new RoleAccessPolicy("people").add().delete().edit());
			return view_def;
		}
		if (name.equals("lostfound"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete().edit())
				.setRecordName("Item", true)
				.setColumn(new Column("date").setDefaultToDateNow())
				.setColumn(new OptionsColumn("what", "lost", "found"));
		if (name.equals("recipes")) {
			ViewDef view_def = super.newViewDef(name);
			ArrayList<String> column_names = new ArrayList<>(Arrays.asList(view_def.getColumnNamesForm()));
			column_names.add(0, "households_id");
			column_names.add("coho");
			return view_def
				.setColumnNamesForm(column_names)
				.setColumn(new Column("coho").setDisplayName("show on community recipes page").setHideOnForms(Mode.READ_ONLY_FORM))
				.setColumn(new HouseholdIdColumn());
		}
		if (name.equals("recommendations"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete().edit())
				.setRecordName("Recommendation", true);
//		if (name.endsWith("suggestions"))
//			return new ViewDef(name)
//				.setDefaultOrderBy("date")
//				.setRecordName("Suggestion", true)
//				.setColumn(new Column("date").setDefaultToDateNow().setIsSortable(true))
//				.setColumn(new LookupColumn("people_id", "people", "first,last").setDisplayName("suggested by").setDefaultToUserId().setIsReadOnly(true));
		if (name.equals("todos"))
			return super.newViewDef("todos")
				.setColumn(new HouseholdIdColumn());
		if (name.equals("units"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("people").add().delete().edit())
				.setRecordName("Unit", true)
				.setDefaultOrderBy("unit")
				.setColumn(new CurrencyColumn("hoa_dues"));
		if (name.equals("values"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("docs").add().delete().edit());

		return super.newViewDef(name);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	pageOpen(String current_page, Head head, boolean open_main, Request r) {
		if (head != null)
			head.styleSheet("flatpickr.min", false).close();
		if ("household".equals(current_page)) {
			Person person = (Person)r.getUser();
			if (person != null) {
				Household household = person.getHousehold(r.db);
				if (household != null)
					current_page = household.getName();
			}
		}
		SiteMenu site_menu = (SiteMenu)getModule("SiteMenu");
		if (site_menu != null) {
			site_menu.write(current_page, r);
			r.w.setId("page").addStyle("flex-grow:1").tagOpen("div");
		}
		if (open_main)
			r.w.mainOpen();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	removeViewDef(String name) {
		super.removeViewDef(name);
		if ("minutes".equals(name))
			super.removeViewDef("group_minutes");
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public static void
	searchPeople(int id, Request r) {
		List<String> tables = r.db.getTableNames(false);
		for (String table : tables) {
			if (r.db.hasColumn(table, "_owner_") && r.db.rowExists(table, "_owner_=" + id))
				r.w.write(table);
			if (r.db.hasColumn(table, "people_id") && r.db.rowExists(table, "people_id=" + id))
				r.w.write(table);
		}
	}

	//--------------------------------------------------------------------------

	@ClassTask({"people_id"})
	public void
	sendNewAdminEmail(int people_id, DBConnection db) {
		String reset_id = Text.uuid();
		db.update("people", "password=NULL,reset_id=" + SQL.string(reset_id), people_id);
		String[] values = db.readRow(new Select("first,email").from("people").whereIdEquals(people_id));
		new Mail("New Mosaic Website",
			"hi " + values[0] + ",<br/><br/>A Mosaic website has been set up for your community at " + getAbsoluteURL() +
				". To activate your account, please click <a href=\"" + getAbsoluteURL("activate_account.jsp").set("r", reset_id) + "\">here</a>.<br/><br/>" +
				"You have been assigned the \"admin\" role. To begin setting up your site, please go to the Documentation site (see page footer or go to https://docs.cohousing.site) and select \"Initial Setup\" in the \"Site Configuration\" section on the left.<br/><br/>" +
				"Please feel free to contact me with any questions.<br/><br/>best,<br/>Sean<br>Mosaic Software")
			.replyTo("mosaic")
			.to(values[1])
			.send();
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeMethodFormControl(String name, Class<?> parameter_type, String label, Request r) {
		if ("household_id".equals(label)) {
			new RowsSelect(name, new Select("id,name").from("households").where("active").orderBy("name"), "name", "id", r).write(r.w);
			return true;
		}
		if ("mail list".equals(label)) {
			new RowsSelect(name, new Select("id,name").from("mail_lists").where("active").orderBy("name"), "name", "id", r).setAllowNoSelection(true).write(r.w);
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	writeSettingsInput(Object object, Field field, Request r) {
		String name = field.getName();
		if (name.startsWith("m_"))
			name = name.substring(2);
		String type = field.getAnnotation(JSONField.class).type();
		switch(type) {
		case "expense account":
			try {
				Integer id  = (Integer)field.get(object);
				RowsSelect rs = new RowsSelect(name, new Select("id,name").from("accounts").where("active AND (type=2 OR type=5)").orderBy("name"), "name", "id", r);
				if (rs.size() > 0)
					rs.addFirstOption(new Option(null, "0")).setSelectedOption(null, id == null ? null : id.toString()).write(r.w);
				else
					r.w.write("<span class=\"invalid-tooltip\" style=\"display:inline;position:unset\">no active expense accounts found</span>");
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			return true;
		case "income account":
			try {
				Integer id  = (Integer)field.get(object);
				RowsSelect rs = new RowsSelect(name, new Select("id,name").from("accounts").where("active AND (type=1 OR type=4)").orderBy("name"), "name", "id", r);
				if (rs.size() > 0)
					rs.addFirstOption(new Option(null, "0")).setSelectedOption(null, id == null ? null : id.toString()).write(r.w);
				else
					r.w.write("<span class=\"invalid-tooltip\" style=\"display:inline;position:unset\">no active income accounts found</span>");
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			return true;
		case "location":
			try {
				Integer id  = (Integer)field.get(object);
				RowsSelect rs = new RowsSelect(name, new Select("id,name").from("locations").orderBy("lower(name)"), "name", "id", r);
				if (rs.size() > 0)
					rs.setAllowNoSelection(true).setSelectedOption(null, id == null ? null : id.toString()).write(r.w);
				else
					r.w.write("<span class=\"invalid-tooltip\" style=\"display:inline;position:unset\">no active locations found</span>");
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			return true;
		case "locations":
			ArrayList<String> s = new ArrayList<>();
			try {
				int[] ids = (int[])field.get(object);
				if (ids != null)
					for (int i : ids)
						s.add(Locations.getOptionByValue(i).getText());
			} catch (Exception e) {
			}
			String[] array = s.toArray(new String[s.size()]);
			Arrays.sort(array);
			new Tags(name, array, Locations.locations)
				.setAllowDragging(true)
				.setOnEditUpdated(field.getAnnotation(JSONField.class).on_edit_updated())
				.setRestrictToChoices(true)
				.write(r.w);
			return true;
		case "mail list":
			try {
				if (moduleIsActive("MailLists")) {
					RowsSelect rs = new RowsSelect(name, new Select("name").from("mail_lists").where("active").orderBy("name"), "name", null, r);
					if (rs.size() > 0)
						rs.setAllowNoSelection(true).setInline(true).setSelectedOption((String)field.get(object), null).write(r.w);
					else
						r.w.write("noreply");
				} else
					r.w.write("noreply");
				r.w.write("@").write(getDomain());
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			return true;
		}
		return super.writeSettingsInput(object, field, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeUserDropdownItems(NavBar nav_bar, Request r) {
		if (moduleIsActive("Households")) {
			Person person = (Person)r.getUser();
			if (person != null && person.getHousehold(r.db) != null) {
				nav_bar.a(person.getHousehold(r.db).getName() + " Household", context + "/Households");
				nav_bar.divider();
			}
		}
		super.writeUserDropdownItems(nav_bar, r);
	}
}
