package mosaic;

import java.sql.Types;

import app.Request;
import app.Site;
import app.SiteModule;
import db.DBConnection;
import db.OrderBy;
import db.ViewDef;
import db.ViewState;
import db.column.LookupColumn;
import db.feature.ColumnFilter;
import db.feature.Feature.Location;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.section.Dividers;
import pages.Page;

public class Contacts extends SiteModule {
	public
	Contacts() {
		m_description = "Lists of contacts";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page(getName(), this, false){
			@Override
			public void
			writeContent(Request r) {
				ViewState.setQuicksearch("main contacts", null, null, r);
				Site.site.newView("main contacts", r).writeComponent();
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("contacts")
			.add(new JDBCColumn("first", Types.VARCHAR))
			.add(new JDBCColumn("last", Types.VARCHAR))
			.add(new JDBCColumn("relationship", Types.VARCHAR))
			.add(new JDBCColumn("phone", Types.VARCHAR))
			.add(new JDBCColumn("email", Types.VARCHAR))
			.add(new JDBCColumn("address", Types.VARCHAR))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("category", Types.VARCHAR));
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("contacts"))
			return new ViewDef(name)
				.addFeature(new ColumnFilter("category", "category", Location.TOP_LEFT).setAllowNone(true))
				.setDefaultOrderBy("first,last")
				.setPrintButtonLocation(Location.LIST_HEAD)
				.setRecordName("Contact", true)
				.setColumnNamesForm("first", "last", "relationship", "phone", "email", "address", "notes", "category")
				.setColumnNamesTable("first", "last", "relationship", "phone", "email", "address", "notes");
		if (name.equals("main contacts"))
			return _newViewDef("contacts")
				.addSection(new Dividers("groups_id", new OrderBy("groups_id", false, true)).setNullValueLabel(null))
				.setBaseFilter("households_id IS NULL OR (SELECT active FROM households WHERE id=households_id) AND public")
				.setDefaultOrderBy("(SELECT name FROM households WHERE households_id=households.id),first,last")
				.setName(name)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true))
				.setColumn(new LookupColumn("households_id", "households", "name").setAllowNoSelection(true))
				.setColumnNamesForm("households_id", "first", "last", "relationship", "phone", "email", "address", "notes", "category")
				.setColumnNamesTable("households_id", "first", "last", "relationship", "phone", "email", "address", "notes");
		return null;
	}
}
