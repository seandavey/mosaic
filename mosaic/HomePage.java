package mosaic;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import app.Request;
import app.Site;
import app.Stringify;
import calendar.Calendars;
import calendar.EventProvider;
import calendar.ListView;
import calendar.UpcomingEventsProvider;
import db.DBConnection;
import db.object.JSONField;
import pages.Page;
import social.NewsFeed;

@Stringify
public class HomePage extends app.HomePage {
	private JsonObject	m_page_template = Json.parse("{\"blocks\":["
			+ "{\"type\":\"banner\"},"
			+ "{\"type\":\"blackboard\"},"
			+ "{\"type\":\"html\"},"
			+ "{\"type\":\"pictures\"},"
			+ "{\"type\":\"news feed\"},"
			+ "{\"type\":\"upcoming\"}"
			+ "]}").asObject();
	@JSONField(choices = { "random", "recent" }, label="null")
	private String		m_pictures = "random";
	@JSONField
	private boolean		m_show_blackboard;
	@JSONField
	private boolean		m_show_news_feed = true;
	@JSONField(fields = {"m_pictures","m_use_ken_burns_effect"})
	private boolean		m_show_pictures;
	@JSONField
	private boolean		m_show_upcoming = true;
	@JSONField(admin_only=true)
	private boolean		m_upcoming_all_calendars = true;
	@JSONField
	private boolean		m_use_ken_burns_effect = true;

	// --------------------------------------------------------------------------

	public
	HomePage() {
		m_description = "A Home page for the site with options including the News Feed, upcomming events, photos, and more";
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Home", this, false) {
			@Override
			public void
			writeContent(Request r) {
				writePage(r);
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	writeBlock(JsonObject block, Request r) {
		if (super.writeBlock(block, r))
			return true;
		switch(block.getString("type", null)) {
		case "blackboard":
			if (m_show_blackboard) {
				Blackboard blackboard = (Blackboard)Site.site.getModule("Blackboard");
				if (blackboard != null && blackboard.isActive())
					blackboard.writeComponent(r);
			}
			return true;
		case "news feed":
			if (m_show_news_feed && Site.site.moduleIsActive("NewsFeed")) {
				r.w.write("<div class=\"col-sm-6\">");
				((NewsFeed)Site.site.getModule("NewsFeed")).write(r);
				r.w.write("</div>");
			}
			return true;
		case "pictures":
			if (m_show_pictures) {
				r.w.write("<div class=\"col-md-12\">");
				new SlideShow().write(m_pictures, false, m_use_ken_burns_effect, r);
				r.w.write("</div>");
			}
			return true;
		case "upcoming":
			if (m_show_upcoming) {
				EventProvider ep = m_upcoming_all_calendars ? new UpcomingEventsProvider(true) : Calendars.getMainCalendar();
				if (ep != null) {
					r.setData("upcoming", Boolean.TRUE);
					r.w.write("<div id=\"upcoming\" class=\"col-sm-6\"><style>#upcoming > div {margin: 0 auto;}#upcoming > div > table {padding:0}</style>");
					new ListView(ep, r).writeComponent(false, r);
					r.w.write("</div>");
					r.setData("upcoming", null);
				}
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	private void
	writePage(Request r) {
		r.w.script("gallery-min", true, "new Gallery({dir:'pictures',show_comments:true})")
			.styleSheet("gallery-min")
			.write("<div class=\"row\">");
		JsonArray blocks = m_page_template.get("blocks").asArray();
		for (int i=0; i<blocks.size(); i++)
			writeBlock(blocks.get(i).asObject(), r);
		r.w.write("</div>");
	}
}