package mosaic;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import com.eclipsesource.json.Json;

import app.Request;
import app.Roles;
import app.Site;
import app.Stringify;
import bootstrap5.ButtonNav;
import bootstrap5.ButtonNav.Panel;
import db.DBConnection;
import db.Rows;
import db.Select;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.PictureColumn;
import db.jdbc.JDBCColumn;
import db.object.DBObject;
import db.object.JSONField;
import pages.Page;
import ui.Card;
import util.Text;
import web.HTMLWriter;

@Stringify
public class People extends app.People {
	@JSONField(after = "uncheck to only record day and month")
	public boolean		m_birthday_is_date = true;
	@JSONField
	String[]			m_categories = {"owner", "resident"};

	//--------------------------------------------------------------------------

	public
	People() {
		super();
		m_page_template = Json.parse("""
			{"blocks":[
				{"type":"icon", "icon":"person", "edit_only":true},
				{"type":"img", "style":"margin-right:.25em;max-height:100px;", "view":"person settings", "edit_only":true, "label":"your profile picture"},
				{"type":"text", "column":"first", "view":"person settings", "edit_only":true, "label":"first name"},
				{"type":"text", "column":"last", "view":"person settings", "edit_only":true, "label":"last name"},
				{"type":"text", "column":"pronouns", "view":"person settings", "edit_only":true},
				{"type":"text", "icon":"envelope", "column":"email", "view":"person settings"},
				{"type":"text", "icon":"phone", "column":"phone", "view":"person settings", "last":true},
				{"type":"household"},
				{"type":"html", "title":"Bio", "column":"bio", "view":"person settings"},
				{"type":"groups"},
				{"type":"mail lists"},
				{"type":"pictures"},
				{"type":"skills"}
			]}""").asObject();
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("People", false) {
			@Override
			public void
			writeContent(Request r) {
				r.w.script("gallery-min", true, null).styleSheet("gallery-min");
				r.w.style("#people_div .card{margin-right:1em;width:300px}#households_div .card{margin-right:1em}");
				ButtonNav bn = new ButtonNav("button_nav", 1, r);
				bn.add(new Panel("Contact List", "card-list", apiURL("Contact List")));
				if (r.getPathSegment(1) == null)
					writeContactList(r);
				bn.add(new Panel("People", "person", apiURL("People")));
				if (Site.site.moduleIsActive("Households"))
					bn.add(new Panel(Text.pluralize(Site.site.getViewDef("households", r.db).getRecordName()), "people", apiURL("Households")))
						.add(new Panel("Homes", "house-door", apiURL("Homes")));
				bn.close();
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		switch (path_segments[0]) {
		case "Contact List":
			writeContactList(r);
			return true;
		case "Homes":
			Site.site.newView("homes", r).writeComponent();
			return true;
		case "Households":
			if (r.db.countRows("homes", null) > 0)
				r.w.write("Order by: <label style=\"font-weight:normal;cursor:pointer;\"><input type=\"radio\" id=\"view_by_name\" name=\"view_by\" onclick=\"set_view_by('name')\"> name</label>&nbsp;&nbsp;&nbsp;&nbsp;<label style=\"font-weight:normal;cursor:pointer;\"><input type=\"radio\" id=\"view_by_home\" name=\"view_by\" onclick=\"set_view_by('home')\"> home</label>");
			r.w.write("<div id=\"households_div\" style=\"display:flex;flex-wrap:wrap;padding-top:10px;\"></div>");
			r.w.js("""
				function get_households() {
					var view_by = localStorage.getItem('households view by') || 'name'
					var radio = _.$('#view_by_' + view_by)
					if (radio)
						radio.checked = true
					net.replace(_.$('#button_nav_2').lastElementChild, context+'/api/Households/households/'+view_by)
				}
				function set_view_by(view_by) {
					localStorage.setItem('households view by', view_by)
					get_households()
				}
				get_households()""");
			return true;
		case "households edit":
			Site.site.newView("households", r).writeComponent();
			return true;
		case "homes edit":
			Site.site.newView("homes edit", r).writeComponent();
			return true;
		case "People":
			writeCards(r);
			return true;
		case "people edit":
			Site.site.newView("people edit", r).writeComponent();
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	public ArrayList<String>
	getColumnNamesForm(String view_def, List<String> before, List<String> after) {
		ArrayList<String> column_names = new ArrayList<>();
		column_names.addAll(before);
		if (m_categories != null)
			for (String c : m_categories)
				if (c.indexOf('|') == -1) // don't allow editing of hard coded categories
					column_names.add(c);
		column_names.addAll(after);
		if (view_def.equals("people edit"))
			column_names.add("last_login");
		return column_names;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		if (m_categories != null)
			for (String c : m_categories)
				if (c.indexOf('|') == -1) // don't create column for  hard coded categories
					addJDBCColumn(new JDBCColumn(c, Types.BOOLEAN));
		super.init(was_added, db);
		Roles.add("people", "can edit the people, households, and homes with this role");
		db.createManyTable("people", "people_bio_pictures", "filename VARCHAR");
		db.createManyTable("people", "people_skills", "skill VARCHAR");
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	initEditPage(DBConnection db) {
		Site.site.addUserDropdownItem(new Page("Edit People", this, false) {
			@Override
			public void
			writeContent(Request r) {
				ViewState.setBaseFilter("people edit", "active AND user_name != 'admin' AND password IS NOT NULL", r);
				if (Site.site.moduleIsActive("Households")) {
					ButtonNav bn = new ButtonNav("button_nav", 1, r);
					bn.add(new Panel("People", "person", apiURL("people edit")));
					if (r.getPathSegment(1) == null)
						Site.site.newView("people edit", r).writeComponent();
					bn.add(new Panel("Households", "people", apiURL("households edit")))
						.add(new Panel("Homes", "house-door", apiURL("homes edit")));
					bn.close();
					ViewState.setBaseFilter("households", "active", r);
				} else
					Site.site.newView("people edit", r).writeComponent();
			}
		}.addRole("people"));
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("people"))
			return new mosaic.PeopleViewDef(name, new AccessPolicy());
		if (name.equals("people edit"))
			return new mosaic.PeopleViewDef(name, new RoleAccessPolicy("people").add().delete().edit());
		if (name.equals("people_bio_pictures"))
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete())
				.setRecordName("Picture", true)
				.setColumnNamesTable("filename")
				.setColumn(new Column("people_id").setDefaultToUserId().setIsHidden(true))
				.setColumn(new PictureColumn("filename", "people_bio_pictures", "people", 200, 1024).setImageSize(100).setUseImageMagick(true).setDirColumn("people_id").setMultiple(true));
		if (name.equals("people_skills"))
			return new ViewDef(name)
				.setRecordName("Skill", true)
				.setColumnNamesForm("people_id", "skill")
				.setColumnNamesTable("skill")
				.setColumn(new Column("people_id").setDefaultToUserId().setIsHidden(true))
				.setColumn(new Column("skill").setIsRequired(true));
		if (name.equals("person settings"))
			return new mosaic.PeopleViewDef(name, null);
		return super._newViewDef(name);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request r) {
		if (m_categories != null)
			for (String c : m_categories)
				if (c.indexOf('|') == -1)
					removeJDBCColumn(c);
		DBObject.set(this, r);
		if (m_categories != null)
			for (String c : m_categories)
				if (c.indexOf('|') == -1)
					addJDBCColumn(new JDBCColumn(c, Types.BOOLEAN));
		super.updateSettings(r);
	}

	//--------------------------------------------------------------------------
	
	private void
	writeCards(Request r) {
		HTMLWriter w = r.w;
		w.write("<div id=\"people_div\" style=\"display:flex;flex-wrap:wrap;padding-top:10px;\">");
		Rows rows = new Rows(new Select("id,first,last,phone,email,picture,(SELECT number FROM households JOIN homes ON homes.id=homes_id WHERE households.id=households_id) home").from("people").where("active").orderBy("LOWER(first),LOWER(last)"), r.db);
		while (rows.next()) {
			Card card = w.ui.card().headerOpen();
			int id = rows.getInt(1);
			w.addStyle("float:right;").ui.buttonOnClick("...", "Person.popup(" + id + ")");
			w.write(rows.getString(2))
				.space()
				.write(rows.getString(3));
			card.bodyOpen();
			String picture = rows.getString(6);
			if (picture != null)
				w.addStyle("max-width:100%;max-height:100px;cursor:pointer")
					.setAttribute("onclick", "Img.show(this)")
					.img("people", rows.getString(1), picture);
			String email = rows.getString(5);
			if (email != null)
				w.ui.iconText("envelope", email);
			String phone = rows.getString(4);
			if (phone != null)
				w.ui.iconText("phone", phone);
			String home = rows.getString(7);
			if (home != null)
				w.ui.iconText("house", home);
			card.close();
		}
		rows.close();
		w.write("</div>");
	}

	//--------------------------------------------------------------------------

	private void
	writeContactList(Request r) {
		if (Site.site.getViewDef("people", r.db).getBaseFilter() == null)
			ViewState.setBaseFilter("people", "active AND user_name != 'admin' AND password IS NOT NULL", r);
		Site.site.newView("people", r).writeComponent();
	}
}
