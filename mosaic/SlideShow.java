package mosaic;

import app.Request;
import db.Rows;
import db.Select;
import web.HTMLWriter;

public class SlideShow {
	public void
	write(String show, boolean login_page, boolean use_ken_burns, Request r) {
		HTMLWriter w = r.w;
		w.write("""
			<style>
			#slideshow {max-width:100%;position:relative;width:500px;height:530px;overflow:hidden;margin:0 auto;}
			#slideshow img {position:absolute;object-fit:cover;top:50%;left:50%;max-width:min(600px,100%);min-width:500px;min-height:530px;margin-left:-250px;margin-top:-265px;opacity:0;transition-property:opacity,transform;}
			#slideshow img{transform-origin: bottom left;}
			#slideshow:nth-child(2n+1) {transform-origin: top right;}
			#slideshow:nth-child(3n+1) {transform-origin: top left;}
			#slideshow:nth-child(4n+1) {transform-origin: bottom right;}
			#slideshow.fx:first-child + img ~ img{z-index:-1;}
			#slideshow .fx {opacity:1;transform: scale(1.1);}
			</style>""");
		w.write("<div id=\"slideshow\" onclick=\"toggle_slideshow()\">");
		Select query = new Select("file").from("pictures");
		if (login_page)
			query.where("can_show_on_login_page");
		query.andWhere("file IS NOT NULL")
			.orderBy(show.equals("random") ? "random()" : "_timestamp_ DESC").limit(20);
		Rows rows = new Rows(query, r.db);
		boolean first = false;
		while (rows.next()) {
			if (rows.isFirst())
				first = true;
			w.addStyle("transition-duration:" + (use_ken_burns ? "3s,10s" : "0s,0s"))
				.img("pictures", rows.getString(1));
		}
		rows.close();
		w.write("</div>");
		if (first)
			w.js("""
				var slideshow_interval
				var do_slideshow=localStorage.getItem('slideshow')
				document.addEventListener('DOMContentLoaded',slideshow)
				function slideshow(){
					var d=_.$('#slideshow')
					d.i=0
					if(do_slideshow!=='off'){
						d.children[0].className='fx'
						slideshow_interval=window.setInterval(kenBurns,6000)
					}else
						d.children[0].style.opacity=1
				}
				function kenBurns(){
					var d=_.$('#slideshow')
					if(!d)
						return
					if(!d.i)
						d.i=0
					d.children[d.i].className=''
					d.i=d.i==d.children.length-1?0:d.i+1
					d.children[d.i].className='fx'
				}
				function toggle_slideshow(){
					do_slideshow=do_slideshow==='on'?'off':'on'
					localStorage.setItem('slideshow',do_slideshow)
					if (do_slideshow==='off')
						window.clearInterval(slideshow_interval)
					else{
						let d=_.$('#slideshow')
						d.children[d.i].className='fx'
						slideshow_interval=window.setInterval(kenBurns,6000)
					}
				}""");
	}
}
