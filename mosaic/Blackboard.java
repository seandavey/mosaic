package mosaic;

import java.sql.Types;
import java.time.LocalDateTime;

import app.Request;
import app.Site;
import app.SiteModule;
import db.DBConnection;
import db.Rows;
import db.SQL;
import db.Select;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import ui.Table;
import util.Text;
import web.HTMLWriter;

class Blackboard extends SiteModule {
	@JSONField
	private boolean	m_erase_every_night = true;
	@JSONField
	private String	m_title = "Community Notices";

	//--------------------------------------------------------------------------

	public
	Blackboard() {
		m_description = "Provides a place on the login page where anyone can write a message; messages are cleared before each day";
		m_is_secured = false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		String add = r.getParameter("add");
		if (add != null)
			r.db.insert("blackboard", "text", SQL.string(add));
		int id = r.getInt("delete", 0);
		if (id != 0)
			r.db.delete("blackboard", id, false);
		write(r);
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	everyNight(LocalDateTime now, DBConnection db) {
		if (m_erase_every_night)
			db.truncateTable("blackboard");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		JDBCTable table_def = new JDBCTable("blackboard")
			.add(new JDBCColumn("text", Types.VARCHAR))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	public void
	write(Request r) {
		HTMLWriter w = r.w;
		w.addClass("bb");
		int mark = w.tagOpen("div");
		w.addClass("bb_title")
			.tag("span", m_title)
			.addClass("bb_frame")
			.tagOpen("div");
		w.addClass("bb_board")
			.tagOpen("div");
		Table table = w.ui.table().addStyle("width", "100%").borderSpacing(10);
		Rows rows = new Rows(new Select("id,text").from("blackboard").orderBy("_timestamp_"), r.db);
		while (rows.next()) {
			if (rows.getRow() % 3 == 1)
				table.tr();
			w.setAttribute("onmouseout", "this.lastChild.style.visibility='hidden';")
				.setAttribute("onmouseover", "this.lastChild.style.visibility='';");
			table.td();
			w.write(rows.getString(2))
				.space()
				.addStyle("visibility:hidden")
				.ui.buttonOnClick(Text.delete, "let c=_.c(this);Dialog.confirm(null,'Delete this entry?','delete',function(){net.replace(c,context+'/Blackboard?delete=" + rows.getString(1) + "')})");
		}
		rows.close();
		table.close();
		w.tagClose()
			.write("<div style=\"display:flex;padding-top:5px;\"><input type=\"text\" id=\"bbtext\" maxlength=\"80\" class=\"form-control input-sm\" style=\"flex-grow:1;padding:2px;\">")
			.addStyle("margin-left:5px")
			.ui.buttonOnClick("Add", "net.replace(_.c(this),context+'/Blackboard?add='+encodeURIComponent(_.$('#bbtext').value));return false;")
			.write("</div>")
			.tagsCloseTo(mark);
	}

	//--------------------------------------------------------------------------

	public void
	writeComponent(Request r) {
		r.w.addStyle("margin:0 auto").addStyle("width:100%").componentOpen(Site.context + "/Blackboard");
		write(r);
		r.w.tagClose();
	}
}
