package mosaic;

import java.sql.Types;

import app.Request;
import app.Site;
import db.DBConnection;
import db.OrderBy;
import db.Rows;
import db.Select;
import db.ViewDef;
import db.ViewState;
import db.column.FileColumn;
import db.column.LookupColumn;
import db.feature.ColumnFilter;
import db.feature.Feature.Location;
import db.feature.LookupFilter;
import db.feature.TSVectorSearch;
import db.jdbc.JDBCColumn;
import db.object.JSONField;
import db.section.Dividers;
import pages.Page;
import web.HTMLWriter;
import web.URLBuilder;

public class Documents extends documents.Documents {
	@JSONField
	private boolean	m_show_group_documents = true;

	//--------------------------------------------------------------------------

	@Override
	public void
	addPages(DBConnection db) {
		Site.pages.add(new Page(getName(), this, false){
			@Override
			public void
			writeContent(Request r) {
				ViewState.setQuicksearch("docs", null, null, r);
				r.w.componentOpen(Site.context + "/Documents/docs");
				writeDocs(r);
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		removeJDBCColumn("groups_id");
		removeJDBCColumn("show_on_main_docs_page");
		if (Site.site.moduleIsActive("Groups")) {
			setDirColumn("groups_id");
			addJDBCColumn(new JDBCColumn("groups"));
			if (m_show_group_documents)
				addJDBCColumn(new JDBCColumn("show_on_main_docs_page", Types.BOOLEAN).setDefaultValue(true));
		}
		super.adjustTables(db);
	}

	//--------------------------------------------------------------------------

	private URLBuilder
	buildURL(String type, int group_id) {
		URLBuilder url = Site.site.getAbsoluteURL(group_id != 0 ? "Groups/" + group_id : "Documents");
		if (group_id != 0)
			url.set("item", "documents");
		if (type != null)
			url.set("type", type);
		return url;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		if ("docs".equals(r.getPathSegment(1))) {
			writeDocs(r);
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	getItemURL(int item_id, Request r) {
		String url = null;
		Rows data = new Rows(new Select("type,groups_id").from("documents").whereIdEquals(item_id), r.db);
		if (data.next()) {
			URLBuilder u = buildURL(data.getString(1), data.getInt(2));
			u.set("v", item_id);
			url = u.toString();
		}
		data.close();
		return url;
	}

	//--------------------------------------------------------------------------

	private LookupColumn
	newGroupColumn() {
		return new LookupColumn("groups_id", "groups", "name")
				.setAllowNoSelection(true)
				.setHelpText("Select a group for this document or leave blank to put the document on the main Docs page")
				.setOnChange("let s=_.$('#show_on_main_docs_page_row');if(s)s.style.display=this.selectedIndex==0?'none':''");
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		boolean groups_active = Site.site.moduleIsActive("Groups");
		if (name.equals("docs")) {
			ViewDef view_def = super._newViewDef("documents")
				.setName("docs");
			if (groups_active) {
				if (m_show_group_documents)
					view_def.addFeature(new LookupFilter("name", "groups", "active AND NOT private", "View", Location.TOP_LEFT).setNoneLabel("Community Documents"))
						.addSection(new Dividers("groups_id", new OrderBy("groups_id", false, true)).setCollapseRows(true).setNullValueLabel(null))
						.setAfterForm("<script>if(_.$('#groups_id')){var s=_.$('#show_on_main_docs_page_row');if(s)s.style.display=s.selectedIndex==0?'none':''}</script>")
						.setBaseFilter(m_allow_folders ?
							"groups_id IS NULL OR (SELECT active AND NOT private FROM groups WHERE groups.id=groups_id) AND ((kind='f' OR show_on_main_docs_page))" :
							"groups_id IS NULL OR (SELECT active AND NOT private FROM groups WHERE groups.id=groups_id) AND show_on_main_docs_page");
				else
					view_def.setBaseFilter("groups_id IS NULL");
				view_def.setColumn(newGroupColumn().setIsHidden(!m_show_group_documents));
			}
			view_def.addFeature(new ColumnFilter("type", m_show_group_documents && groups_active ? "<span style=\"margin-left:20px\">Type</span>" : "Type", Location.TOP_LEFT).setAllowNone(true).setFilter("kind='f' OR type"))
				.addFeature(new TSVectorSearch(null, Location.TOP_RIGHT));
			return view_def;
		}
		if (name.equals("edit documents")) {
			ViewDef view_def = super._newViewDef(name);
			if (groups_active)
				view_def.addFeature(new LookupFilter("name", "groups", "active AND NOT private", "View", Location.TOP_LEFT).setNoneLabel("Community Documents"))
					.setAfterForm("<script>if(_.$('#groups_id')){var s=_.$('#show_on_main_docs_page_row');if(s)s.style.display=s.selectedIndex==0?'none':''}</script>")
					.setBaseFilter("groups_id IS NULL OR (SELECT active AND NOT private FROM groups WHERE groups.id=groups_id)")
					.setDefaultOrderBy("groups_id NULLS FIRST;LOWER(title)")
					.setColumnNamesTable(new String[] { "groups_id", "filename", "type", "_timestamp_" });
			else
				view_def.setDefaultOrderBy("LOWER(title)")
					.setColumnNamesTable(new String[] { "filename", "type", "_timestamp_" });
			view_def.addFeature(new ColumnFilter("type", groups_active ? "<span style=\"margin-left:20px\">Type</span>" : "Type", Location.TOP_LEFT).setAllowNone(true).setFilter("kind='f' OR type"))
				.setColumn(newGroupColumn());
			return view_def;
		}
		return super._newViewDef(name);
	}

	//--------------------------------------------------------------------------

	public boolean
	showGroupDocuments() {
		return m_show_group_documents;
	}

	//--------------------------------------------------------------------------

	private void
	writeDocs(Request r) {
		r.w.js("JS.get('documents-min')");
		String type = r.getParameter("type");
		if (type != null)
			ViewState.setFilter("docs", "type='" + type + "'", r);
		Site.site.newView("docs", r).writeComponent();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeNewsItem(int item_id, boolean for_mail, HTMLWriter w, DBConnection db) {
		FileColumn file_column = (FileColumn)Site.site.getViewDef("documents", db).getColumn("filename");
		Rows rows = new Rows(new Select("*").from("documents").whereIdEquals(item_id), db);
		if (rows.next()) {
			file_column.writeValue(rows, w);
			w.addClass("news_item_header")
				.tagOpen("div");
			int group_id = m_show_group_documents && Site.site.moduleIsActive("Groups") ? rows.getInt("groups_id") : 0;
			w.addStyle("text-decoration:none")
				.a(group_id != 0 ? db.lookupString("name", "groups", group_id) : Site.pages.getPageByURL("/Documents").getName() + " page", buildURL(rows.getString("type"), group_id).toString())
				.tagClose();
		}
		rows.close();
		if (m_attachments != null)
			m_attachments.write(item_id, w, db);
	}
}
