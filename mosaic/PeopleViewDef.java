package mosaic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import app.Person;
import app.Request;
import app.Site;
import app.Stringify;
import db.ManyToMany;
import db.NameValuePairs;
import db.OneToMany;
import db.SQL;
import db.Select;
import db.View;
import db.View.Mode;
import db.access.AccessPolicy;
import db.column.BirthdayColumn;
import db.column.Column;
import db.column.ColumnBase;
import db.column.ColumnValueRenderer;
import db.column.LookupColumn;
import db.column.OptionsColumn;
import db.column.TextAreaColumn;
import db.feature.Feature.Location;
import email.MailLists;
import groups.Groups;
import meals.MealEventProvider;
import sitemenu.SiteMenu;
import util.Text;

@Stringify
public class PeopleViewDef extends app.PeopleViewDef {
	public
	PeopleViewDef(String name, AccessPolicy access_policy) {
		super(name, access_policy, ((People)Site.site.getModule("people")).m_user_pic_size);
		People people = (People)Site.site.getModule("People");
		setFrom("people");
		if (name.equals("person settings")) {
			setColumnNamesForm("additional_emails", "birthday", "hide_email_on_site", "password", "email_updates_to_my_conversations", "remove_me_when_i_post_to_lists", "send_email_when_someone_posts_to_the_site", "theme", "timezone", "top menu", "user_name");
			setColumn(new BirthdayColumn("birthday", ((People)Site.site.getModule("people")).m_birthday_is_date).setSubColumns(false, "show_birthday_on_calendar"));
			setColumn(new Column("remove_me_when_i_post_to_lists").setDisplayName("remove me from recipients when I post to email lists"));
			setColumn(Site.themes.getColumn().setOnChange("var t=this.options[this.selectedIndex].text;net.post(context+'/Themes/'+t+'/set',null,function(){app.set_theme(this,t)}.bind(this))"));
			setColumn(((SiteMenu)Site.site.getModule("SiteMenu")).newColumn("top menu"));
		} else {
			setDeleteText("Are you absolutely sure you want to delete this person? If they have left the community then you may want to edit and uncheck the \"active\" checkbox instead. Deleting a person also deletes data they posted including calendar events, comments, job history, news feed items, reviews, work hours, etc.");
			setPrintButtonLocation(Location.LIST_HEAD);
			setPrintFunction("function(w){['page','picture','last_login'].forEach(c=>_.table.delete_column(w.document.querySelector('thead').parentNode,c))}");
			setRecordName("Person", true);
			ArrayList<String> column_names = people.getColumnNamesForm(name,
				List.of("first", "last", "pronouns", "picture", "bio", "households_id"),
				List.of("phone", "email", "additional_emails", "can_post_to_mail_lists", "birthday", "show_birthday_on_calendar", "theme", "timezone", "user_name", "password", "active", "date_added", "move_in_date", "move_out_date"));
			setColumnNamesForm(column_names);
			setColumnNamesTable(name.equals("people edit")
				? new String[] { "picture", "first", "last", "home", "phone", "email", "last_login" }
				: new String[] { "picture", "first", "last", "home", "phone", "email", "page" });
			setColumn(new Column("page").setDisplayName("").setIsFilterable(false).setValueRenderer(new ColumnValueRenderer() {
				@Override
				public void
				writeValue(View v, ColumnBase<?> column, Request r) {
					r.w.ui.buttonOnClick("...", "Person.popup(" + v.data.getInt("id") + ")");
				}
			}, false));
			setColumn(new TextAreaColumn("bio").setIsRichText(true, false));
			setColumn(new BirthdayColumn("birthday", ((People)Site.site.getModule("people")).m_birthday_is_date));
			setColumn(new Column("cm_account_active").setDisplayName("Common Meal Account Active").setViewRole("administrator"));
			setColumn(new Column("date_added").setHideOnForms(Mode.ADD_FORM).setIsReadOnly(true));
			setColumn(new LookupColumn("households_id", "households", "name").setAllowEditing().setAllowNoSelection(true).setDisplayName("household").setViewRole("people"));
			setColumn(new LookupColumn("home", "households JOIN homes ON households.homes_id=homes.id", "number").setDisplayName("home").setNaturalSort(true).setOneColumn("households_id"));
			MealEventProvider meal_event_provider = (MealEventProvider)Site.site.getModule("Common Meals");
			if (meal_event_provider != null)
				setColumn(new OptionsColumn("meal_age", meal_event_provider.getAges()).setAllowNoSelection(true));
			setColumn(new Column("owner").setEditRole("people"));
			setColumn(new Column("resident").setEditRole("people"));
			setColumn(Site.themes.getColumn());
			if (Site.site.moduleIsActive("Groups"))
				addRelationshipDef(new ManyToMany("edit people groups", "people_groups", "name").setViewRole(Site.site.getDefaultRole()));
			if (Site.site.moduleIsActive("MailLists")) {
				addRelationshipDef(new ManyToMany("people mail_lists", "mail_lists_people", "name").setViewRole(Site.site.getDefaultRole()));
				addRelationshipDef(new ManyToMany("people mail_lists_digest", "mail_lists_digest", "name").setViewRole(Site.site.getDefaultRole()));
			}
		}
		addRelationshipDef(new OneToMany("additional_emails"));
		ArrayList<String> a = new ArrayList<>();
		a.add("Current Members|active AND user_name != 'admin' AND password IS NOT NULL");
		if (people.m_categories != null)
			for (String c : people.m_categories)
				if (c.indexOf('|') == -1)
					a.add(Text.capitalize(Text.pluralize(c)) + "|active AND " + SQL.columnName(c));
				else
					a.add(c);
		a.add("Past Members|NOT active AND user_name != 'admin' AND password IS NOT NULL");
		a.add("All|user_name != 'admin'");
		if (name.equals("people edit") && Site.site.allowAccountRequests())
			a.add("Account Requests|password IS NULL");
		m_filters = a.toArray(new String[a.size()]);
	}

	//--------------------------------------------------------------------

	@Override
	public String
	beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		Person user = r.getUser();
		if (user == null)
			return "Unable to update record, user is not logged in";
		if (nvp.containsName("active") && !nvp.getBoolean("active") && r.db.lookupBoolean(new Select("active").from("people").whereIdEquals(id))) {
			((Groups)Site.site.getModule("Groups")).removePerson(id, r.db);
			((MailLists)Site.site.getModule("MailLists")).removePerson(id, r.db);
			r.db.delete("menu_items", "people_id=" + id, false);
			r.db.delete("people_skills", "people_id=" + id, false);
		}
		return super.beforeUpdate(id, nvp, previous_values, r);
	}
}
