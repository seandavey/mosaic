package mosaic;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;

import app.Module;
import app.Request;
import app.Site;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import ui.NavList;
import util.Time;
import util.Zip;

class Backups extends Module {
	@Override
	public boolean
	doGet(HttpServletRequest http_request, String[] path_segments, HttpServletResponse http_response) throws IOException {
		if (http_request.getRemoteUser() == null)
			return false;
		String path = http_request.getServletPath();
		String backup = path.substring(path.lastIndexOf('/') + 1);
		return switch(backup) {
		case "decisions" -> Zip.sendDirectoryAsZip("decisions", null, null, http_response);
		case "documents" -> Zip.sendDirectoryAsZip("documents", null, null, http_response);
		case "households" -> Zip.sendDirectoryAsZip("households", null, null, http_response);
		case "mail" -> Zip.sendDirectoryAsZip("mail", null, null, http_response);
		case "minutes attachments" -> Zip.sendDirectoryAsZip("minutes", null, null, http_response);
		case "people" -> Zip.sendDirectoryAsZip("people", null, null, http_response);
		case "pictures" -> Zip.sendDirectoryAsZip("pictures", new String[] { "thumbs" }, null, http_response);
		default -> false;
		};
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		write(r);
		return true;
	}

	//--------------------------------------------------------------------------

	@Override
	public String[][]
	getConfigTasks(Request r) {
		return new String[][] { { "Backups", "Backups" } };
	}

	//--------------------------------------------------------------------------

	private void
	write(Request r) {
		NavList nav_list = r.w.ui.navList().open();
		Path path = Site.site.getPath();
		File[] files = path.toFile().listFiles(new FilenameFilter(){
			@Override
			public boolean
			accept(File directory, String name) {
				return name.endsWith(".zip");
			}
		});
		if (files != null && files.length > 0) {
			Arrays.sort(files, Comparator.comparingLong(File::lastModified).reversed());
			nav_list.heading("Database Backups");
			for (File file : files)
				nav_list.a(file.getName() + " (" + Time.formatter.getDateTime(app.Files.modified(file)) + ")" , Site.context + "/" + file.getName());
		}
		nav_list.heading("File Backups");
		nav_list.a("decisions", Site.context + "/Backups/decisions");
		nav_list.a("documents", Site.context + "/Backups/documents");
		nav_list.a("household photos", Site.context + "/Backups/households");
		nav_list.a("minutes attachments", Site.context + "/Backups/minutes attachments");
		nav_list.a("mail list archives", Site.context + "/Backups/mail");
		nav_list.a("people avatars and photos", Site.context + "/Backups/people");
		nav_list.a("pictures", Site.context + "/Backups/pictures");
//		nav_list.a("site", Site.context + "/Backups/site");
		nav_list.close();
	}
}
