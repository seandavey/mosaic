package mosaic;

import java.lang.reflect.Field;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import app.Module;
import app.Request;
import app.Site;
import calendar.CalendarView;
import calendar.CalendarView.View;
import calendar.Calendars;
import calendar.EventProvider;
import calendar.ListView;
import calendar.MonthView;
import calendar.UpcomingEventsProvider;
import db.object.JSONField;
import jakarta.servlet.http.Part;
import ui.Select;
import util.Time;
import web.HTMLWriter;

public class LoginPage extends Module {
	@JSONField(label="null", type="file")
	private String 				m_banner = "banner.png";
	private JsonObject			m_page_template = Json.parse("""
		{"blocks":[
			{"type":"banner"},
			{"type":"blackboard"},
			{"type":"html"},
			{"type":"login form"},
			{"type":"upcoming"},
			{"type":"pictures"}
		]}""").asObject();
	@JSONField(choices = { "random", "recent" }, label="null")
	private String				m_pictures = "random";
	@JSONField(fields = {"m_banner"})
	private boolean				m_show_banner = true;
	@JSONField
	private boolean				m_show_blackboard;
	@JSONField(fields = {"m_pictures","m_use_ken_burns_effect"})
	private boolean				m_show_pictures = true;
	@JSONField(fields = {"m_text_box"})
	private boolean				m_show_text_box;
	@JSONField(fields = {"m_upcoming_mode"})
	private boolean				m_show_upcoming = true;
	@JSONField(label="null", type="html")
	private String				m_text_box;
	@JSONField(admin_only=true)
	private boolean				m_upcoming_all_calendars;
	@JSONField
	private CalendarView.View	m_upcoming_mode = CalendarView.View.LIST;
	@JSONField
	private boolean				m_use_ken_burns_effect = true;

	// --------------------------------------------------------------------------

	public
	LoginPage() {
		m_required = true;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		switch(r.getPathSegment(1)) {
		case "Banner":
			try {
				Part part = r.request.getPart("file");
				String filename = part.getSubmittedFileName();
				part.write(Site.site.getPath("images", filename).toString());
				m_banner = filename;
				store(r.db);
				r.response.js("_.$('#banner_img').src='images/" + filename + "'");
			} catch (Exception e) {
				r.log(e);
			}
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public String[][]
	getConfigTasks(Request r) {
		return new String[][] { { "Login Page", "settings" } };
	}

	//--------------------------------------------------------------------------

	public boolean
	showBlackboard() {
		return m_show_blackboard;
	}

	//--------------------------------------------------------------------------

	public final void
	write(Request r) {
		HTMLWriter w = r.w;
		w.addClass("login");
		Site.site.newHead(r).close();
		w.write("<div id=\"top\" style=\"height:2em;\"></div>");
		w.js("if(_.$('#top').parentNode.tagName!='BODY')document.location=document.location;");
		w.mainOpen();
		w.write("<div class=\"row\">");
		JsonArray blocks = m_page_template.get("blocks").asArray();
		for (int i=0; i<blocks.size(); i++) {
			JsonObject block = blocks.get(i).asObject();
			switch(block.getString("type", null)) {
			case "banner":
				if (m_show_banner && m_banner != null)
					w.write("<div style=\"text-align:center\"><img src=\"").write(Site.context).write("/images/" + m_banner + "\" style=\"max-width:100%\"/></div>");
				break;
			case "blackboard":
				if (m_show_blackboard) {
					Blackboard blackboard = (Blackboard)Site.site.getModule("Blackboard");
					if (blackboard != null && blackboard.isActive())
						blackboard.writeComponent(r);
				}
				break;
			case "html":
				if (m_show_text_box)
					w.write("<div class=\"alert alert-secondary ck-content\" style=\"margin:10px auto\">").write(m_text_box).write("</div></div><div class=\"row\">");
				break;
			case "login form":
				Site.credentials.writeLoginForm(r);
				w.br();
				break;
			case "pictures":
				if (m_show_pictures) {
					w.write("<div class=\"col-md-6\">");
					new SlideShow().write(m_pictures, false, m_use_ken_burns_effect, r);
					w.write("</div>");
				}
				break;
			case "upcoming":
				if (m_show_upcoming) {
					w.write("<div id=\"upcoming\" class=\"col-md-6\"><style>#upcoming > div {margin: 0 auto;}#upcoming > div > table {padding:0}</style>");
					r.setData("upcoming", Boolean.TRUE);
					if (m_upcoming_mode == View.MONTH)
						w.write("<div style=\"text-align:center;\"><h3>").write(Time.formatter.getMonthNameLong(Time.newDate())).write("</h3></div>");
					EventProvider ep = m_upcoming_all_calendars ? new UpcomingEventsProvider(true) : Calendars.getMainCalendar();
					if (ep != null)
						(m_upcoming_mode == View.MONTH ? new MonthView(ep, r) : new ListView(ep, r)).writeComponent(false, r);
					else
						w.write("Main Calendar not set");
					w.write("</div>");
					r.setData("upcoming", null);
				}
				break;
			}
		}
		w.write("</div>");
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeSettingsInput(Field field, Request r) {
		String field_name = field.getName();
		if ("m_banner".equals(field_name))
			r.w.setId("banner_img").addStyle("max-height:100px").img("images", m_banner).space().aOnClick("replace", "Dialog.upload(context+'/LoginPage/Banner')");
		else if ("m_upcoming_mode".equals(field_name))
			new Select("upcoming_mode", CalendarView.View.class).setSelectedOption(m_upcoming_mode.toString(), null).write(r.w);
		else
			super.writeSettingsInput(field, r);
	}
}
