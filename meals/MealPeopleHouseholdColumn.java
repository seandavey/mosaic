package meals;

import app.Request;
import db.View;
import db.ViewState;
import db.column.LookupColumn;
import households.Household;
import mosaic.Person;

class MealPeopleHouseholdColumn extends LookupColumn {
	private boolean m_allow_signup_of_other_households;

    //--------------------------------------------------------------------------

	public
	MealPeopleHouseholdColumn(boolean allow_signup_of_other_households) {
		super("households_id", "households", "name", "active", "name");
		m_allow_signup_of_other_households = allow_signup_of_other_households;
		setDisplayName("household");
	}

    //--------------------------------------------------------------------------

	@Override
	public String
	getDefaultValue(ViewState view_state, Request r) {
		Person user = (Person)r.getUser();
		if (user == null)
			return null;
		Household household = user.getHousehold(r.db);
		return household != null ? Integer.toString(household.getId()) : null;
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	isReadOnly(View v, Request r) {
		if (m_allow_signup_of_other_households)
			return false;
		Boolean is_cook = (Boolean)r.getSessionAttribute("iscook");
		return (is_cook == null || !is_cook.booleanValue()) && !r.userHasRole("cm account manager");
	}
}