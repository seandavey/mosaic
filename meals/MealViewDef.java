package meals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import app.Person;
import app.Request;
import app.Site;
import app.Stringify;
import attachments.Attachments;
import calendar.EventViewDef;
import db.Form;
import db.NameValuePairs;
import db.OneToMany;
import db.Rows;
import db.SQL;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.column.Column;
import db.column.ColumnBase;
import db.column.CurrencyColumn;
import db.column.LookupColumn;
import db.column.MultiColumnRenderer;
import db.column.PersonColumn;
import mail.Mail;
import mosaic.Jobs;
import util.Array;
import util.Text;
import util.Time;
import web.HTMLWriter;

@Stringify class MealViewDef extends EventViewDef {
	public
	MealViewDef(MealEventProvider ep, Attachments attachments, boolean report) {
		super(ep, attachments);
		setAccessPolicy(new MealAccessPolicy());
		setDefaultOrderBy("date");
		setFrom("meal_events");
		setRecordName("Meal", true);
		setShowPrintLinkForm(true);

		// form column names
		ArrayList<String> column_names = new ArrayList<>();
		column_names.add("date");
		ep.setLocationColumn(this, column_names);
		column_names.add("start_time");
		column_names.add("end_time");
		if (ep.m_use_title_field)
			column_names.add("title");
		column_names.add("menu");
		column_names.add("no_charge");
		column_names.add("potluck");
		column_names.add("notes");
		if (ep.m_option_columns != null)
			for (String option_column : ep.m_option_columns)
				column_names.add(option_column);
		column_names.add("_owner_");
		Jobs jobs = ep.getJobs();
		if (jobs != null)
			jobs.addJobs(column_names);
		if (ep.m_use_max_people_field)
			column_names.add("max_people");
		if (ep.m_use_closed_field) {
			column_names.add("closed");
			column_names.add("automatic_close_time");
			column_names.add("closed_message");
		}
		if (ep.m_use_cooks_cost_field)
			column_names.add("cooks_cost");
		if (ep.m_other_columns != null)
			for (String other_column : ep.m_other_columns)
				column_names.add(other_column);
		setColumnNamesForm(column_names);

		column_names.clear();
		column_names.add("date");
		if (!report) {
			column_names.add("start_time");
			column_names.add("end_time");
			column_names.add("menu");
		}
		column_names.add("_owner_");
		if (jobs != null)
			jobs.addJobs(column_names);
		if (!ep.m_signup_individuals)
			column_names.add("total people");
		else
			for (String age : ep.m_ages)
				column_names.add("num_" + age + "s");
		column_names.add("total");
		if (ep.hasExtras())
			column_names.add("total budget");
		setColumnNamesTable(column_names);

		if (jobs != null)
			jobs.setColumns(this, new MealJobRenderer(ep), false);
		for (int i=0,n=ep.m_ages.length; i<n; i++) {
			if (ep.m_budgets != null && ep.m_extras != null) {
				double budget = getBudget(i);
				double extra = getExtra(i);
				setColumn(new CurrencyColumn(ep.m_ages[i] + "_price").setDefaultValue(Text.two_digits.format(budget + extra)).setDisplayName(ep.m_display_names[i]).setOnInput("calc_totals()"));
				setColumn(new CurrencyColumn(ep.m_ages[i] + "_budget_amount").setDefaultValue(Text.two_digits.format(budget)).setDisplayName(ep.m_display_names[i]).setOnInput("calc_totals()"));
			}
			setColumn(new Column("num_" + ep.m_ages[i] + "s"){
				@Override
				public double
				getDouble(View v, Request r) {
					return r.db.lookupInt("SUM(" + m_name + ")", "meal_people", "meal_events_id=" + v.data.getString("id"), 0);
				}
				@Override
				public boolean
				writeValue(View v, Map<String,Object> data, Request r) {
					r.w.write((int)getDouble(v, r));
					return true;
				}
			}.setTextAlign("right").setDisplayName("num " + ep.m_display_names[i]).setIsComputed(true).setTotalIsInt(true));
		}
		if (ep.m_use_closed_field) {
			setColumn(new Column("automatic_close_time").setInputAndValueRenderers(new MultiColumnRenderer(new String[] {"time_to_automatically_close", "close_days_before_meal"}, false, false), false));
			if (ep.m_default_time_to_automatically_close != null)
				setColumn(new Column("time_to_automatically_close").setDefaultValue(ep.m_default_time_to_automatically_close.toString()));
			setColumn(new Column("close_days_before_meal").setDefaultValue(ep.m_default_close_days_before_meal).setPostText("day(s) before meal"));
		}
		if (ep.m_use_cooks_cost_field)
			setColumn(new CurrencyColumn("cooks_cost").setDisplayName("cook's cost").setHideOnForms(Mode.ADD_FORM));
		setColumn(new Column("date").setDisplayName(ep.getStartDateLabel()).setIsRequired(true));
		if (ep.m_use_max_people_field)
			setColumn(new Column("max_people").setDisplayName(ep.m_max_people_label));
		setColumn(new Column("no_charge").setOnChange("_.$('#prices').style.display=this.checked?'none':'';_.$('#budgets').style.display=this.checked?'none':''"));
		setColumn(new PersonColumn("_owner_", false).setAllowNoSelection(true).setDefaultToUserId().setDisplayName(ep.m_cook_label));
		setColumn(new Column("potluck").setOnChange("if(this.checked){let c=_.$('#no_charge_cb');if(!c.checked)c.click()}"));
		setColumn(new Column("start_time").setDefaultValue(ep.getDefaultStartTime()));
		setColumn(new Column("end_time").setDefaultValue(ep.getDefaultEndTime()));
		setColumn(new CurrencyColumn("total"){
			@Override
			public double
			getDouble(View v, Request r) {
				float total = 0;
				if (ep.m_signup_individuals) {
					List<String> ids = r.db.readValues(new Select("people_id").from("meal_people").where("meal_events_id=" + v.data.getString("id")));
					total = r.db.lookupFloat(new Select("SUM(meal_price)").from("people").where("id IN(" + Text.join(",", ids) + ")"), 0);
				} else
					for (String age : ep.m_ages) {
						int num = r.db.lookupInt("SUM(" + SQL.columnName("num_" + age + "s") + ")", "meal_people", "meal_events_id=" + v.data.getString("id"), 0);
						total += v.data.getFloat(age + "_price") * num;
					}
				return total;
			}
		}.setTextAlign("right").setIsComputed(true));
		setColumn(new CurrencyColumn("total budget"){
			@Override
			public double
			getDouble(View v, Request r) {
				float total = 0;
				for (String age : ep.m_ages) {
					int num = r.db.lookupInt("SUM(" + SQL.columnName("num_" + age + "s") + ")", "meal_people", "meal_events_id=" + v.data.getString("id"), 0);
					total += v.data.getFloat(age + "_budget_amount") * num;
				}
				return total;
			}
		}.setTextAlign("right").setDisplayName("total cook's budget").setIsComputed(true));
		if (!ep.m_signup_individuals)
			setColumn(new Column("total people").setIsComputed(true).setValueRenderer(new TotalsRenderer(ep.m_ages, ep.m_display_names, ep.m_ignore_ages_for_max_count), false));
		if (ep.m_other_columns != null)
			for (String other_column : ep.m_other_columns)
				setColumn(new CurrencyColumn(other_column));
		addRelationshipDef(new OneToMany("meal_people"));
		if (ep.supportsReminders())
			addRelationshipDef(new OneToMany(m_name + "_reminders"));

	}

	//--------------------------------------------------------------------------
	// FormHook

	@Override
	public void
	afterForm(int id, Rows data, ViewDef view_def, View.Mode mode, boolean printer_friendly, Request r) {
		MealEventProvider ep = (MealEventProvider)m_ep;
		HTMLWriter w = r.w;
		if (printer_friendly && ep.m_print_text != null) {
			w.write("<tr><td><div style=\"width:500px;\"><ol><li style=\"padding-bottom:20px;\">Final totals:");
			for (String display_name : ep.m_display_names)
				w.write("  ______ ").write(display_name);
			w.write("</li>");
			w.write(ep.m_print_text);
			w.write("</ol></div></td></tr>");
		} else if (mode == View.Mode.EDIT_FORM || mode == View.Mode.READ_ONLY_FORM) {
			boolean closed = ep.mealIsClosed(data, r);
			int meal_events_id = data.getInt("id");
			w.write("<tr><td>");
			w.ui.aButton("Email Diners", "mailto:" + Text.join(",", ep.getDinerEmails(meal_events_id, r.db))).space();
			if (!ep.isPotluck(id, r.db) || ep.m_show_jobs_on_potlucks)
				if (!closed) {
					w.write("<script>function update_workers(){" +
						"var f=_.$('#Meal');" +
						"var workers=[");
					Jobs jobs = ep.getJobs();
					if (jobs != null)
						jobs.writeJobsJS(w);
					w.write("];" +
						"var ids='").write(data.getInt("_owner_")).write("';" +
						"for (var i=0; i<workers.length; i++){" +
							"var s=f[workers[i]];" +
							"if (s) {" +
								"var id=s.options[s.selectedIndex].value;" +
								"if(id)" +
									"ids += ','+id;" +
							"}" +
						"}" +
						"net.get_json(context+'/" + ep.getName() + "/worker emails?meal_events_id=" + meal_events_id + "&ids='+ids,function(r){_.$('#email_workers').href='mailto:'+r.emails});" +
					"}\n" +
					"update_workers()</script>");
					w.setId("email_workers").ui.aButton("Email Workers", "#");
				} else
					w.ui.aButton("Email Workers", "mailto:" + Text.join(",", ep.getWorkerEmails(data, r.db))).space();
			w.write("</td></tr>");

			if (closed)
				r.w.js("var t=_.$('.modal-footer');if(t)t.style.display='none';");
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		super.afterInsert(id, nvp, r);

		MealEventProvider meal_event_provider = (MealEventProvider)m_ep;
		if (meal_event_provider.m_automatic_signup) {
			Rows households = new Rows(new Select("id")
					.distinct().from("households")
					.where(r.db.hasColumn("households", "automatic_meal_signup") ? "active AND automatic_meal_signup" : "active"),
				r.db);
			 while (households.next()) {
				int[] counts = new int[meal_event_provider.m_ages.length];
				int household_id = households.getInt(1);
				Rows people = new Rows(new Select("meal_age").from("people").whereEquals("households_id", household_id), r.db);
				while (people.next()) {
					String age = people.getString(1);
					if (age != null && age.length() > 0)
						counts[Array.indexOf(meal_event_provider.m_ages, age)]++;
				}
				people.close();
				int num = 0;
				NameValuePairs nvp2 = new NameValuePairs();
				for (int i=0; i<counts.length; i++)
					if (counts[i] > 0) {
						nvp2.set("num_" + meal_event_provider.m_ages[i] + "s", counts[i]);
						++num;
					}
				if (num > 0) {
					nvp2.set("meal_events_id", id);
					nvp2.set("households_id", household_id);
					r.db.insert("meal_people", nvp2);
				}
			}
			households.close();
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		((MealEventProvider)m_ep).redoInvoices(id, r.db);
		super.afterUpdate(id, nvp, previous_values, r);
	}

	//--------------------------------------------------------------------------

	final double
	getBudget(int i) {
		MealEventProvider ep = (MealEventProvider)m_ep;
		return ep.m_budgets != null && i < ep.m_budgets.length ? ep.m_budgets[i] : 0;
	}

	//--------------------------------------------------------------------------

	final double
	getExtra(int i) {
		MealEventProvider ep = (MealEventProvider)m_ep;
		return ep.m_extras != null && i < ep.m_extras.length ? ep.m_extras[i] : 0;
	}

	//--------------------------------------------------------------------------

	final double
	getPrice(int i) {
		return getBudget(i) + getExtra(i);
	}

	//--------------------------------------------------------------------------

	@Override
	public View
	newView(Request r) {
		View v = new View(this, r);
		if (r.getParameter("db_relationship") != null)
			return v;
		View.Mode mode = r.getEnum("db_mode", View.Mode.class);
		if (mode == View.Mode.ADD_FORM) {
			r.setSessionFlag("iscook", true);
			return v;
		}
		if (mode != View.Mode.EDIT_FORM && mode != View.Mode.READ_ONLY_FORM)
			return v;
		MealEventProvider ep = (MealEventProvider)m_ep;
		int meal_id = v.getID();
		if (meal_id == 0)
			return null;
		if (!v.selectByID(meal_id)) {
			r.w.write("The meal you are trying to view was not found. It may have been deleted.");
			return null;
		}
		boolean is_potluck = v.data.getBoolean("potluck");
		if (is_potluck) {
			if (!ep.m_show_jobs_on_potlucks) {
				v.setColumn((ColumnBase<?>)cloneColumn("_owner_").setIsHidden(true));
				ep.getJobs().setColumnsHidden(v);
			}
			if (ep.m_use_max_people_field)
				v.setColumn((ColumnBase<?>)cloneColumn("max_people").setIsHidden(true));
			if (ep.m_use_cooks_cost_field)
				v.setColumn((ColumnBase<?>)cloneColumn("cooks_cost").setIsHidden(true));
			if (ep.m_option_columns != null)
				for (String option_column : ep.m_option_columns)
					v.setColumn(new Column(option_column).setIsHidden(true));
		}
		int owner_id = v.data.getInt("_owner_");
		Person user = r.getUser();
		boolean is_cook = user != null && owner_id == user.getId();
		r.setSessionFlag("iscook", is_cook);
		if (!getAccessPolicy().showEditButtonForRow(v.data, r)) {
			v.setColumns(new TimeToCloseColumn("automatic_close_time"),
				new Column("date").setIsReadOnly(true),
				new Column("menu").setIsReadOnly(true),
				new Column("no_charge").setIsReadOnly(true),
				new Column("notes").setIsReadOnly(true),
				new Column("potluck").setIsReadOnly(true),
				new Column("start_time").setIsReadOnly(true),
				new Column("end_time").setIsReadOnly(true),
				new Column("title").setIsReadOnly(true));
			if (ep.m_use_closed_field) {
				v.setColumn(new Column("closed") {
					@Override
					public boolean
					writeValue(View v2, Map<String, Object> d, Request r2) {
						if (!v2.data.getBoolean("closed")) {
							String reason = ep.mealIsAutomaticallyClosed(v2.data);
							if (reason != null) {
								r2.w.ui.formLabel("closed")
									.ui.helpText("automatically closed because " + reason);
								return true;
							}
						}
						return super.writeValue(v2, d, r);
					}

				}.setIsReadOnly(true));
				v.setColumn(new Column("closed_message") {
					@Override
					public boolean
					showOnForm(View.Mode m, Rows data, Request r2) {
						if (!ep.mealIsClosed(data, r2) || data.getString("closed_message") == null)
							return false;
						return super.showOnForm(m, data, r2);
					}
					@Override
					public String
					getLabel(View v2, Request r2) {
						return null;
					}
				}.setIsReadOnly(true));
			}
			if (ep.m_use_max_people_field)
				v.setColumn(new Column("max_people").setIsReadOnly(true));
			if (!is_potluck) {
				if (owner_id != 0)
					v.setColumn(new LookupColumn("_owner_", "people", "first,last").setIsReadOnly(true).setDisplayName(ep.m_cook_label));
				if (ep.m_use_cooks_cost_field)
					v.setColumn((ColumnBase<?>)cloneColumn("cooks_cost").setIsHidden(true));
				for (int i=0; i<ep.m_ages.length; i++)
					v.setColumn(new CurrencyColumn(ep.m_ages[i] + "_price").setDisplayName(ep.m_display_names[i]).setIsReadOnly(true));
			}
			if (ep.m_option_columns != null)
				for (String option_column : ep.m_option_columns)
					v.setColumn(new Column(option_column).setIsReadOnly(true));
			if (ep.m_other_columns != null)
				for (String other_column : ep.m_other_columns)
					v.setColumn((ColumnBase<?>)cloneColumn(other_column).setIsHidden(true));
		} else if (ep.m_use_closed_field && !is_potluck)
			v.setColumn(new Column("closed") {
				@Override
				public void
				writeInput(View v2, Form f, boolean i, Request r2) {
					String reason = ep.mealIsAutomaticallyClosed(v.data);
					if (reason != null)
						r2.w.ui.formLabel("closed")
							.ui.helpText("automatically closed because " + reason);
					else
						super.writeInput(v2, f, i, r2);
				}
				@Override
				public boolean
				writeValue(View v2, Map<String, Object> d, Request r2) {
					if (!v2.data.getBoolean("closed")) {
						String reason = ep.mealIsAutomaticallyClosed(v2.data);
						if (reason != null) {
							r2.w.ui.formLabel("closed")
								.ui.helpText("automatically closed because " + reason);
							return true;
						}
					}
					return super.writeValue(v2, d, r2);
				}
			});
		if (!is_potluck && ep.mealIsClosed(v.data, r)) {
			Jobs jobs = ep.getJobs();
			if (jobs != null)
				jobs.setColumnsReadOnly(v);
		}
		return v;
	}

	// --------------------------------------------------------------------------

	@Override
	protected void
	sendAnnouncement(int id, Request r) {
		Object[] o = r.db.readRowObjects(new Select("date,start_time,end_time,menu,_owner_").from("meal_events").whereIdEquals(id));
		StringBuilder message = new StringBuilder("<p>A meal has been added to the common meal calendar with the following details:</p><table border=\"0\" padding=\"2px\"><tr><th>date:</th><td>");
		message.append(Time.formatter.getWeekdayMonthDate((LocalDate)o[0]));
		if (o[1] != null)
			message.append("</td></tr><tr><th>time:</th><td>").append(Time.formatter.getTimeRange((LocalTime)o[1], (LocalTime)o[2]));
		String menu = (String)o[3];
		if (menu != null && menu.length() > 0)
			message.append("</td></tr><tr><th>menu:</th><td>").append(menu);
		if (o[4] != null)
			message.append("</td></tr><tr><th>cook:</th><td>").append(Site.site.lookupName((Integer)o[4], r.db));
		message.append("</td></tr></table><p>Click <a href=\"")
			.append(Site.site.getAbsoluteURL("Calendars").set("edit", "'Common Meals'," + id))
			.append("\">here</a> to sign up for the meal.</p>");
		String address = Site.site.getEmailAddress(m_ep.getSendAnnouncementTo());
		new Mail(m_ep.getDisplayName(r) + " Announcement", message.toString())
			.from(address)
			.to(address)
			.send();
	}

	// --------------------------------------------------------------------------

	@Override
	public String
	validate(int id, Request r) {
		if (r.getParameter("date") == null && id != 0) // date is read-only
			return null;
		return super.validate(id, r);
	}
}
