package meals;

import app.Request;
import db.View;
import db.column.PersonColumn;

class MealPeoplePersonColumn extends PersonColumn {
	public
	MealPeoplePersonColumn() {
		super("people_id", false);
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	isReadOnly(View v, Request r) {
		return !r.testSessionFlag("iscook") && !r.userIsAdmin();
	}
}