package meals;

import app.Request;
import app.Site;
import db.Rows;
import db.SQL;
import db.Select;
import db.View;
import db.column.ColumnBase;
import db.column.ColumnValueRenderer;
import util.Array;
import util.Text;
import web.HTMLWriter;

class TotalsRenderer implements ColumnValueRenderer {
	private final String[]	m_ages;
	private final String[]	m_display_names;
	private final String[]	m_ignore_ages_for_max_count;

	//--------------------------------------------------------------------------

	public
	TotalsRenderer(String[] ages, String[] display_names, String[] ignore_ages_for_max_count) {
		m_ages = ages;
		m_display_names = display_names;
		m_ignore_ages_for_max_count = ignore_ages_for_max_count;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeValue(View v, ColumnBase<?> column, Request r) {
		if (m_ages.length == 0)
			return;
		MealEventProvider ep = (MealEventProvider)Site.site.getModule("Common Meals");
		StringBuilder columns = new StringBuilder();
		for (int i=0,n=m_ages.length; i<n; i++) {
			if (i > 0)
				columns.append(',');
			columns.append("SUM(").append(SQL.columnName("num_" + m_ages[i] + "s")).append(")");
		}
		if (ep.m_support_take_aways)
			columns.append(",SUM(take_aways)");
		int meal_event_id = v.data.getInt("id");
		Rows people = new Rows(new Select(columns.toString()).from("meal_people").whereEquals("meal_events_id", meal_event_id), r.db);
		people.next();
		int[] nums = new int[m_ages.length];
		for (int i=0,n=nums.length; i<n; i++)
			nums[i] = people.getInt(i + 1);
		int take_aways = 0;
		if (ep.m_support_take_aways)
			take_aways = people.getInt(m_ages.length + 1);
		people.close();

		HTMLWriter w = r.w;
		int total = 0;
		boolean first = true;
		for (int i=0,n=m_ages.length; i<n; i++) {
			if (Array.indexOf(m_ignore_ages_for_max_count, m_ages[i]) != -1)
				continue;
			if (first)
				first = false;
			else
				w.write(" + ");
			total += nums[i];
			w.write(nums[i]).space().write(Text.singularize(m_display_names[i], nums[i]).replaceAll("_", " "));
		}
		if (m_ages.length > 1)
			w.write(" = ").write(total).space().write(Text.singularize("people", total));
		for (int i=0,n=m_ages.length; i<n; i++) {
			if (Array.indexOf(m_ignore_ages_for_max_count, m_ages[i]) == -1)
				continue;
			w.write(" + ").write(nums[i]).space().write(Text.singularize(m_display_names[i], nums[i]).replaceAll("_", " "));
		}
		if (ep.m_support_take_aways)
			w.write(", ").write(take_aways).write(take_aways == 1 ? " is " : " are ").write(Text.singularize(ep.m_take_aways_label, take_aways));
	}
}
