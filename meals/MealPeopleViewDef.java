package meals;

import java.util.ArrayList;
import java.util.Map;

import accounting.Accounting;
import app.Person;
import app.Request;
import app.Site;
import db.Form;
import db.FormHook;
import db.NameValuePairs;
import db.Rows;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.access.AccessPolicy;
import db.column.Column;
import db.column.ColumnBase;
import households.Household;
import mosaic.Jobs;
import ui.Table;
import util.Text;
import web.JS;
import web.JSONBuilder;

class MealPeopleViewDef extends ViewDef {
	private final boolean			m_check_for_at_least_one;
	private final MealEventProvider m_ep;

	//--------------------------------------------------------------------------

	public
	MealPeopleViewDef(AccessPolicy access_policy, MealEventProvider ep, boolean check_for_at_least_one) {
		super("meal_people");
		m_check_for_at_least_one = check_for_at_least_one;
		m_ep = ep;
		setAccessPolicy(access_policy);
		setDefaultOrderBy(ep.m_signup_individuals ? "people_id" : "households_id");
		MealPeopleInsertUpdateHook insert_update_hook = new MealPeopleInsertUpdateHook(ep.m_signup_individuals ? null : ep.m_ages, ep.m_ignore_ages_for_max_count);
		addInsertHook(insert_update_hook);
		addUpdateHook(insert_update_hook);
		setRecordName(ep.m_signup_individuals ? "Person" : "Household", true);
		setColumn(ep.m_signup_individuals ? new MealPeoplePersonColumn() : new MealPeopleHouseholdColumn(ep.m_allow_signup_of_other_households));
		String meal_work_ratio = Site.settings.getString("meal work ratio");
		if (meal_work_ratio != null)
			setColumn(new Column("adult eating/work ratio"){
				@Override
				public boolean
				isHidden(Request r) {
					return ((mosaic.Person)r.getUser()).getHousehold(r.db).isMealWorkExempt();
				}
			}.setInputRenderer(new MealRatioRenderer()).setPostText("<div style=\"font-size:9pt;width:300px\">(current policy is " + meal_work_ratio + " times eating per 1 time working. Higher ratios should work more, lower ratios should eat more :-)</span>"));
		if (!ep.m_signup_individuals)
			for (int i=0,n=ep.m_ages.length; i<n; i++)
				setColumn(new Column("num_" + ep.m_ages[i] + "s")
					.setTextAlign("center")
					.setDisplayName(ep.m_display_names[i])
					.setTotal(true)
					.setTotalIsInt(true));
		if (m_ep.m_support_take_aways)
			setColumn(new Column("take_aways")
				.setTextAlign("center")
				.setDisplayName(ep.m_take_aways_label)
				.setTotal(true)
				.setTotalIsInt(true));
		addFormHook(new FormHook() {
			@Override
			public void
			afterForm(int id, Rows data, ViewDef view_def, Mode mode, boolean printer_friendly, Request r) {
				if (!m_ep.m_show_balance_message)
					return;
				Person user = r.getUser();
				if (user == null)
					return;
				Household household = ((mosaic.Person)user).getHousehold(r.db);
				if (household == null)
					return;
				double balance = Accounting.calcBalanceHousehold(household.getId(), null, r.db);
				if (balance <= 0)
					return;
				r.w.write("<tr><td><div class=\"alert alert-danger\">You have an outstanding balance of ").writeCurrency(balance).write(".<br/>Please submit a payment as soon as possible to bring your credit back up<br/>(we suggest maintaining a 50-100 dollar credit).</div></td></tr>");
			}
		});
		Jobs jobs = ep.getJobs();
		if (jobs != null)
			jobs.setColumns(this, new MealJobRenderer(ep), true);
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	addToViewJSON(JSONBuilder json) {
		json.string("add_title", "Sign Up for Meal");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterDelete(String where, Request r) {
		m_ep.redoInvoices(r.getSessionInt("meal_id", 0), r.db);
		super.afterDelete(where, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		int meal_id = r.db.lookupInt(new Select("meal_events_id").from("meal_people").whereIdEquals(id), 0);
		m_ep.redoInvoices(meal_id, r.db);
		super.afterInsert(id, nvp, r);
		m_ep.getJobs().update("meal_events", meal_id, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		int meal_id = r.db.lookupInt(new Select("meal_events_id").from("meal_people").whereIdEquals(id), 0);
		m_ep.redoInvoices(meal_id, r.db);
		super.afterUpdate(id, nvp, previous_values, r);
		m_ep.getJobs().update("meal_events", meal_id, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeDelete(String where, Request r) {
		int meal_id = r.db.lookupInt(new Select("meal_events_id").from("meal_people").whereIdEquals(Integer.parseInt(where.substring(3))), 0);
		r.setSessionInt("meal_id", meal_id);
		return super.beforeDelete(where, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public Form
	newForm(int id, View v, Request r) {
		return new MealPeopleForm(id, v, m_ep, m_check_for_at_least_one, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public View
	newView(Request r) {
		return new View(this, r) {
			@Override
			public Form
			newEditForm() {
				setColumnNames(this, m_r);
				return super.newEditForm();
			}
			@Override
			protected void
			tableClose(Table table, String[] columns) {
				m_w.write("</tr>");
				boolean no_charge = m_ep.isNoCharge(getOneId(r), m_r.db);
				boolean potluck = m_ep.isPotluck(getOneId(r), r.db);
				if (no_charge)
					m_w.addStyle("display:none");
				m_w.setId("prices")
					.tagOpen("tr");
				m_w.write("<td style=\"font-weight:bold;text-align:right;\">Prices</td>");
				View one = m_relationship.one;
				for (int i=0; i<m_ep.m_ages.length; i++) {
					m_w.write("<td style=\"text-align:right\">");
					if (one.getMode() == View.Mode.READ_ONLY_FORM)
						m_w.writeCurrency(one.data.getDouble(m_ep.m_ages[i] + "_price"));
					else {
						ColumnBase<?> column = one.getColumn(m_ep.m_ages[i] + "_price");
						if (isPrinterFriendly())
							column.writeValue(one, null, r);
						else
							column.writeInput(one, null, false, false, r);
					}
					m_w.write("</td>");
				}
				if (potluck)
					m_w.tag("td", null);
				if (m_ep.m_support_take_aways)
					m_w.tag("td", null);
				if (m_has_buttons_column)
					m_w.tag("td", null);
				m_w.tag("td", null);
				m_w.tagClose();
				if (no_charge)
					m_w.addStyle("display:none");
				m_w.setId("budgets")
					.tagOpen("tr");
				m_w.write("<td style=\"font-weight:bold;text-align:right;\">Cook's Budgets</td>");
				for (int i=0; i<m_ep.m_ages.length; i++) {
					m_w.write("<td style=\"text-align:right\">");
					if (one.getMode() == View.Mode.READ_ONLY_FORM)
						m_w.writeCurrency(one.data.getDouble(m_ep.m_ages[i] + "_budget_amount"));
					else {
						ColumnBase<?> column = one.getColumn(m_ep.m_ages[i] + "_budget_amount");
						if (isPrinterFriendly())
							column.writeValue(one, null, r);
						else
							column.writeInput(one, null, false, false, r);
					}
					m_w.write("</td>");
				}
				if (potluck)
					m_w.tag("td", null);
				if (m_ep.m_support_take_aways)
					m_w.tag("td", null);
				if (m_has_buttons_column)
					m_w.tag("td", null);
				m_w.tag("td", null)
					.tagClose();
				super.tableClose(table, columns);
				m_w.js("calc_totals()");
			}
			@Override
			protected Table
			tableOpen() {
				m_w.js("var ages=" + JS.array(m_ep.m_ages) + """
					;
					function amount(td) {
						let v = td.firstElementChild ? td.firstElementChild.value : td.textContent
						if (v)
							return parseFloat(v.replace('$',''))
						return 0
					}
					function calc_totals() {
						let t = _.$('#total_people')
						if (!t)
							return
						t = t.children
						let p = _.$('#prices').children
						let b = _.$('#budgets').children
						let price_total = 0.0
						let budget_total = 0.0
						for (let i=1; i<=ages.length; i++) {
							let c = parseFloat(t[i].textContent)
							if (c) {
								price_total += c * amount(p[i])
								budget_total += c * amount(b[i])
							}
						}
						let formatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', trailingZeroDisplay: 'stripIfInteger' })
						p[p.length-1].innerText = formatter.format(price_total)
						b[b.length-1].innerText = formatter.format(budget_total)
					}""");
				setColumnNames(this, m_r);
				return super.tableOpen();
			}
			@Override
			protected void
			writeTotalsRow(String label, double[] totals) {
				if (m_ep.m_signup_individuals || totals == null || m_num_rows == 0)
					return;
				String[] columns = getColumnNamesTable();
				m_w.write("<tr class=\"table-secondary\" id=\"total_people\"><td style=\"font-weight:bold;text-align:right;\">").write(label).write("</td>");
				int sum = 0;
				for (int i=1; i<columns.length-1; i++) {
					m_w.write("<td class=\"num\" style=\"text-align:center\">");
					ColumnBase<?> column = getColumn(columns[i]);
					if (column != null && column.total()) {
						column.writeTotal(totals[i], m_w);
						m_grand_totals[i] += totals[i];
						if (!"take_aways".equals(column.getName()))
							sum += totals[i];
						totals[i] = 0;
					}
					m_w.write("</td>");
				}
				if (m_has_buttons_column)
					m_w.tag("td", null);
				m_w.write("<td>");
				m_w.write(sum)
					.space()
					.write(Text.pluralize("person", sum));
				m_w.write("</td></tr>");
				++m_num_totals_written;
			}
		};
	}

	//--------------------------------------------------------------------------

	private void
	setColumnNames(View v, Request r) {
		ArrayList<String> a = new ArrayList<>();
		if (m_ep.m_signup_individuals)
			a.add("people_id");
		else {
			a.add("households_id");
			for (String age : m_ep.m_ages)
				a.add("num_" + age + "s");
		}
		if (m_ep.m_support_take_aways)
			a.add("take_aways");
		boolean is_pot_luck = false;
		int one_id = r.getInt("one_id", 0);
		if (one_id == 0 && v.getRelationship().one.data != null)
			one_id = v.getRelationship().one.data.getInt("id");
		if (one_id != 0) {
			is_pot_luck = m_ep.isPotluck(one_id, r.db);
			if (is_pot_luck)
				a.add("dish");
		}
		a.add("notes");
		String[] c = a.toArray(new String[a.size()]);
		v.setColumnNamesFormTable(c)
			.setColumnNamesTable(c);
		if (is_pot_luck)
			v.setColumnNamesForm(c);
		else {
			String meal_work_ratio = Site.settings.getString("meal work ratio");
			if (meal_work_ratio != null)
				a.add("adult eating/work ratio");
			v.setColumnNamesForm(a.toArray(new String[a.size()]));
		}
	}
}
