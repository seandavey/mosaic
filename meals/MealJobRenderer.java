package meals;

import app.Request;
import db.Filter;
import db.Form;
import db.Rows;
import db.Select;
import db.SelectRenderer;
import db.View;
import db.View.Mode;
import db.column.ColumnBase;
import mosaic.Person;

public class MealJobRenderer extends SelectRenderer {
	private final MealEventProvider m_ep;

	//--------------------------------------------------------------------------

	public
	MealJobRenderer(MealEventProvider event_provider) {
		super(event_provider.m_workers_are_families ? new Select("id,name").from("households").where("active AND cm_account_active").orderBy("name") : event_provider.m_restrict_to_household ? new Select("id,first,last,households_id").from("people").where("active").orderBy("first,last") : new Select("id,first,last").from("people").where("active").orderBy("first,last"), event_provider.m_workers_are_families ? "name" : "first,last", "id");
		m_ep = event_provider;
		if (event_provider.m_restrict_to_household)
			setFilter(new Filter() {
				@Override
				public boolean
				accept(Rows rows, Request r) {
					if (r.userIsAdmin() || r.testSessionFlag("iscook"))
						return true;
					Person person = (mosaic.Person)r.getUser();
					if (person != null)
						return rows.getInt(4) == person.getHouseholdId();
					return false;
				}
			});
		setAllowNoSelection(true);
		setOnChange("update_workers()");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, ColumnBase<?> column, boolean inline, Request r) {
		if (!m_ep.m_workers_are_families && m_ep.m_restrict_to_household && v.getMode() == Mode.EDIT_FORM) {
			int people_id = v.data.getInt(column.getName());
			if (people_id != 0) {
				Person person = new Person(people_id, r.db);
				if (person.getHouseholdId() != ((mosaic.Person)r.getUser()).getHouseholdId() && !r.userIsAdmin() && !r.testSessionFlag("iscook")) {
					r.w.write(person.getName());
					return;
				}
			}
		}
		super.writeInput(v, f, column, inline, r);
	}
}
