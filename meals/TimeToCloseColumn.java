package meals;

import java.util.Map;

import app.Person;
import app.Request;
import db.Rows;
import db.View;
import db.column.Column;
import db.column.MultiColumnRenderer;
import util.Text;

public class TimeToCloseColumn extends Column {
	public TimeToCloseColumn(String name) {
		super(name);
		setInputAndValueRenderers(new MultiColumnRenderer(new String[] {"time_to_automatically_close", "close_days_before_meal"}, false, false), false);
		setIsReadOnly(true);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showOnForm(View.Mode mode, Rows data, Request r) {
		int owner = data.getInt("_owner_");
		Person u = r.getUser();
		boolean iscook = u != null && owner == u.getId();
		if (!iscook && data.getTime("time_to_automatically_close") == null)
			return false;
		return super.showOnForm(mode, data, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String, Object> d, Request r) {
		v.writeColumnHTML("time_to_automatically_close");
		int days = v.data.getInt("close_days_before_meal");
		if (days == 0)
			r.w.write(" on day of meal");
		else
			r.w.space().write(days).space().write(Text.pluralize("day", days)).write(" before meal");
		return true;
	}

}
