package meals;

import java.time.LocalDate;
import java.time.LocalTime;

import app.Person;
import app.Request;
import db.Rows;
import db.Select;
import db.View;
import db.access.AccessPolicy;

class MealPeopleAccessPolicy extends AccessPolicy {
	private final boolean			m_allow_signup_of_other_households;
	private final MealEventProvider	m_ep;

    //--------------------------------------------------------------------------

	public
	MealPeopleAccessPolicy(MealEventProvider event_provider, boolean allow_signup_of_other_households) {
		m_ep = event_provider;
		m_allow_signup_of_other_households = allow_signup_of_other_households;
		add();
		delete();
		edit();
	}

    //--------------------------------------------------------------------------

	private boolean
	canEdit(boolean for_row, Rows data, Request r) {
		Person user = r.getUser();
		if (user == null)
			return false;
		if (data == null)
			return true;
		if (r.one_id == 0)
			r.one_id = r.getInt("one_id", 0);
		if (r.one_id == 0)
			r.one_id = r.db.lookupInt(new Select("meal_events_id").from("meal_people").whereIdEquals(r.getInt("db_key_value", 0)), 0);
		Object[] meal = r.db.readRowObjects(
			new Select("_owner_,closed,date,time_to_automatically_close,close_days_before_meal")
				.from("meal_events")
				.whereIdEquals(r.one_id));
		if (meal == null) {
			r.log("meal " + r.one_id + " not found", false);
			return false;
		}
		int owner = 0;
		if (meal[0] != null)
			owner = ((Integer)meal[0]).intValue();
		if (m_ep.userCanAlwaysEditMeal(owner, r))
			return true;
		int close_days_before = 0;
		if (meal[4] != null)
			close_days_before = ((Integer)meal[4]).intValue();
		if ((Boolean)meal[1] || m_ep.mealIsAutomaticallyClosed((LocalDate)meal[2], (LocalTime)meal[3], close_days_before) != null)
			return false;
		if (m_allow_signup_of_other_households)
			return true;
		return !for_row || r.db.rowExists("people", "id=" + user.getId() + " AND households_id=" + data.getString("households_id"));
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	showAddButton(View v, Request r) {
		Person user = r.getUser();
		if (user == null)
			return false;
		if (v.getMode() == View.Mode.ADD_FORM)
			return true;
		int meal_id = r.one_id; // getInt("one_id", 0);
//		if (meal_id == 0)
//			meal_id = v.getRelationship().one.data.getInt("id");
		Object[] meal = r.db.readRowObjects(new Select("_owner_,closed,date,time_to_automatically_close,close_days_before_meal").from("meal_events").whereIdEquals(meal_id));
		int owner = 0;
		if (meal[0] != null)
			owner = ((Integer)meal[0]).intValue();
		if (m_ep.userCanAlwaysEditMeal(owner, r))
			return true;
		int close_days_before = 0;
		if (meal[4] != null)
			close_days_before = ((Integer)meal[4]).intValue();
		if ((Boolean)meal[1] || m_ep.mealIsAutomaticallyClosed((LocalDate)meal[2], (LocalTime)meal[3], close_days_before) != null)
			return false;
		if (m_allow_signup_of_other_households)
			return true;
		int household_id = r.db.lookupInt(new Select("households_id").from("people").whereIdEquals(user.getId()), 0);
		if (household_id == 0)
			return false;
		return !r.db.rowExists("meal_people", "meal_events_id=" + meal_id + " AND households_id=" + household_id);
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtonForRow(Rows data, Request r) {
		return canEdit(true, data, r);
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtons(Rows data, Request r) {
		return canEdit(false, data, r);
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtonForRow(Rows data, Request r) {
		return canEdit(true, data, r);
	}

    //--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtons(Rows data, Request r) {
		return canEdit(false, data, r);
	}
}