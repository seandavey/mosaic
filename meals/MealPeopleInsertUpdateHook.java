package meals;

import java.util.Map;

import app.Request;
import app.Site;
import db.InsertHook;
import db.NameValuePairs;
import db.Rows;
import db.SQL;
import db.Select;
import db.UpdateHook;
import mail.Mail;
import util.Array;
import util.Text;

class MealPeopleInsertUpdateHook implements InsertHook, UpdateHook {
	private String[] m_ages;
	private String[] m_ignore_ages_for_max_count;

	//--------------------------------------------------------------------------

	public
	MealPeopleInsertUpdateHook(String[] ages, String[] ignore_ages_for_max_count) {
		m_ages = ages;
		m_ignore_ages_for_max_count = ignore_ages_for_max_count;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterInsert(int id, NameValuePairs nvp, Request r) {
		updateMeal(nvp.getInt("meal_events_id", 0), r);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	afterUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		updateMeal(r.db.lookupInt(new Select("meal_events_id").from("meal_people").whereIdEquals(id), 0), r);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeInsert(NameValuePairs nvp, Request r) {
		return checkMeal(nvp.getInt("meal_events_id", 0), 0, nvp, r);
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
		int num_people = 0;
		if (m_ages != null) {
			Select query = new Select();
			for (String age : m_ages)
				query.addColumn(SQL.columnName("num_" + age + 's'));
			query.from("meal_people").whereIdEquals(id);
			Rows rows = new Rows(query, r.db);
			if (rows.next())
				for (int i=1,n=m_ages.length; i<=n; i++)
					if (Array.indexOf(m_ignore_ages_for_max_count, m_ages[i - 1]) == -1)
						num_people += rows.getInt(i);
			rows.close();
		} else
			num_people = 1;
		return checkMeal(r.db.lookupInt(new Select("meal_events_id").from("meal_people").whereIdEquals(id), 0), num_people, nvp, r);
	}

	//--------------------------------------------------------------------------

	private String
	checkMeal(int meal_id, int previous_people, NameValuePairs nvp, Request r) {
		MealEventProvider meal_event_provider = (MealEventProvider)Site.site.getModule("Common Meals");
		if (!meal_event_provider.m_use_max_people_field)
			return null;
		Select query = new Select("max_people").from("meal_events").leftJoin("meal_events", "meal_people").whereEquals("meal_events.id", meal_id).groupBy("max_people");
		if (m_ages != null) {
			for (String age : m_ages)
				if (Array.indexOf(m_ignore_ages_for_max_count, age) == -1)
					query.addColumn("SUM(\"num_" + age + "s\") \"num_" + age + "s\"");
		} else
			query.addColumn("COUNT(*)");
		Rows rows = new Rows(query, r.db);
		if (!rows.next()) {
			rows.close();
			return null;
		}
		int max_people = rows.getInt(1);
		if (max_people == 0) {
			rows.close();
			return null;
		}
		int new_people = 0;
		int num_people = 0;
		if (m_ages != null) {
			for (String m_age : m_ages)
				if (Array.indexOf(m_ignore_ages_for_max_count, m_age) == -1) {
					String column = "num_" + m_age + "s";
					new_people += nvp.getInt(column, 0);
					num_people += rows.getInt(column);
				}
		} else {
			new_people = 1;
			num_people = rows.getInt(2);
		}
		rows.close();
		if (num_people - previous_people + new_people > max_people) {
			int num_openings = max_people - num_people;
			if (num_openings <= 0)
				return "There aren't any openings available for this meal. Please check with the cook to see if they can increase the number of openings.";
			return "There " + (num_openings == 1 ? "is" : "are") + " only " + num_openings + " " + Text.singularize("openings", num_openings) + " available for this meal. Please reduce the number of people you are signing up or ask the cook to increase the number of openings.";
		}
		return null;
	}

	//--------------------------------------------------------------------------
	// contact cook if meal is automatically closed because of number of people signed up > max people

	private void
	updateMeal(int meal_id, Request r) {
		MealEventProvider meal_event_provider = (MealEventProvider)Site.site.getModule("Common Meals");
		if (!meal_event_provider.m_use_closed_field)
			return;
		if (!meal_event_provider.m_use_max_people_field)
			return;
		StringBuilder columns = new StringBuilder("max_people,closed,_owner_,menu,date");
		if (m_ages != null)
			for (String age : m_ages)
				columns.append(",SUM(\"num_").append(age).append("s\")");
		else
			columns.append(",COUNT(*)");
		try (Rows rows = new Rows(new Select(columns.toString()).from("meal_events").leftJoinOn("meal_people", "meal_people.meal_events_id=meal_events.id").whereEquals("meal_events.id", meal_id).groupBy("max_people,closed,_owner_,menu,date"), r.db)) {
			if (!rows.next())
				return;
			int max_people = rows.getInt(1);
			int num_people = 0;
			if (m_ages != null) {
				for (int i=0,n=m_ages.length; i<n; i++)
					if (Array.indexOf(m_ignore_ages_for_max_count, m_ages[i]) == -1)
						num_people += rows.getInt(i + 6);
			} else
				num_people += rows.getInt(5);
			if (max_people > 0 && num_people >= max_people && !rows.getBoolean(2)) {
				int cook = rows.getInt(3);
				if (!rows.wasNull()) {
					String email = r.db.lookupString("email", "people", cook);
					if (email != null) {
						String meal = rows.getString(5);
						if (meal != null)
							meal = meal.trim();
						if (meal != null)
							meal += " " + rows.getString(5);
						else
							meal = rows.getString(5);
						new Mail("Meal Closed", Integer.toString(num_people) + " people have signed up for your meal (" + meal + ") and so it has been closed. You may reopen it and increase the value of 'max people' if you wish.")
							.to(email)
							.send();
					}
				}
				r.db.update("meal_events", "closed=true", meal_id);
			}
		}
	}
}
