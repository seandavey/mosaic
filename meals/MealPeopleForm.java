package meals;

import app.Person;
import app.Request;
import app.Site;
import db.Form;
import db.Rows;
import db.SQL;
import db.Select;
import db.View;
import db.column.ColumnBase;
import util.Text;
import util.Time;

public class MealPeopleForm extends Form {
	private final boolean			m_check_for_at_least_one;
	private final MealEventProvider m_ep;
	private boolean					m_has_cost;

	//--------------------------------------------------------------------------

	public
	MealPeopleForm(int id, View v, MealEventProvider ep, boolean check_for_at_least_one, Request r) {
		super(id, v, r);
		m_check_for_at_least_one = check_for_at_least_one;
		m_ep = ep;
//		if (id == 0) {
//			id = ep.userIsSignedUp(m_one_id, r);
//			if (id != 0) {
//				m_id = id;
//				setMode(View.Mode.EDIT_FORM);
//				v.setMode(View.Mode.EDIT_FORM);
//				if (v.selectByID(id))
//					m_one_id = v.data.getInt("meal_events_id");
//			}
//		}
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeBody() {
		m_has_cost = m_ep.hasCost(m_one_id, m_r.db);
		View v = new View(Site.site.getViewDef("Common Meals", m_r.db), m_r);
		v.selectByID(m_one_id);
		Rows meal = v.data;
		m_w.write("<div class=\"alert alert-light\" style=\"max-width:500px\">");
		{
			StringBuilder s = new StringBuilder(Time.formatter.getMonthDate(meal.getDate("date"))).append(" ");
			Time.formatter.appendTimeRange(meal.getTime("start_time"), meal.getTime("end_time"), s);
			m_w.tag("div", s.toString());
		}
		if (m_ep.m_use_title_field) {
			m_w.ui.display6(meal.getString("title"));
			m_w.tag("div", meal.getString("menu"));
		} else
			m_w.ui.display6(meal.getString("menu"));
		m_w.write("<b>cook:</b> ").write(new Person(meal.getInt("_owner_"), m_r.db).getName());
		String notes = meal.getString("notes");
		if (notes != null)
			m_w.ui.subHead(notes);
		m_w.write("</div>");
		boolean sign_up = m_r.getBoolean("sign_up");
		if (sign_up)
			m_v.setColumn(m_ep.m_signup_individuals ? new MealPeoplePersonColumn().setIsHidden(true) : new MealPeopleHouseholdColumn(m_ep.m_allow_signup_of_other_households).setIsHidden(true));
		super.writeBody();
		if (!m_ep.m_signup_individuals && m_check_for_at_least_one) {
			StringBuilder s = new StringBuilder("Please add at least one person");
			for (String age : m_ep.m_ages)
				s.append(",num_" + age + "s");
			m_w.hiddenInput("db_required_set", s.toString());
		}
		if (sign_up) {
			m_w.br()
				.h5("Sign Up For Jobs")
				.hiddenInput("db_update_jobs", "true");
			Form f = new Form(m_one_id, v, m_r);
			m_ep.getJobs().writeFormRows(f, v);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeLabel(String column_name) {
		if (column_name.startsWith("num_")) {
			ColumnBase<?> column = m_v.getColumn(column_name);
			String label = column.getLabel(m_v, m_r);
			if (m_has_cost) {
				Select query = new Select(SQL.columnName(column_name.substring(4, column_name.length() - 1) + "_price")).from("meal_events").whereIdEquals(m_one_id);
				label += " - " + Text.currency.format(m_r.db.lookupDouble(query, 0));
			}
			super.writeLabel(label);
		} else
			super.writeLabel(column_name);
	}
}
