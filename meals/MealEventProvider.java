package meals;

import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import accounting.Accounting;
import app.Block;
import app.Person;
import app.Request;
import app.Roles;
import app.Site;
import app.Stringify;
import calendar.Event;
import calendar.EventProvider;
import calendar.MonthView;
import calendar.MosaicEventProvider;
import calendar.Reminders;
import db.AnnualReport;
import db.DBConnection;
import db.OrderBy;
import db.Rows;
import db.SQL;
import db.Select;
import db.View;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.CurrencyColumn;
import db.column.LookupColumn;
import db.feature.Feature.Location;
import db.feature.MonthFilter;
import db.feature.MonthYearFilter;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Dividers;
import households.Household;
import ui.Table;
import util.Array;
import util.Text;
import util.Time;
import web.HTMLWriter;

@Stringify
public class MealEventProvider extends MosaicEventProvider {
	@JSONField(fields = {"m_display_names", "m_budgets", "m_extras", "m_ignore_ages_for_max_count"}, label = "age groups")
	String[]			m_ages = new String[] { "adult", "kid", "under_five" };
	@JSONField(admin_only=true)
	boolean 			m_allow_signup_of_other_households;
	@JSONField(admin_only=true)
	boolean				m_automatic_signup;
	@JSONField(label = "age group budgets")
	float[]				m_budgets = new float[] { 4, 2, 0 };
	@JSONField(admin_only = true)
	private boolean		m_check_for_at_least_one = true;
	@JSONField
	String				m_cook_label = "cook";
	@JSONField(after="set for automatic reimbursement", label="credit account", type="expense account")
	protected int		m_credit_account_id;
	@JSONField(label = "days before meal")
	int					m_default_close_days_before_meal;
	@JSONField(fields = {"m_default_close_days_before_meal"})
	LocalTime			m_default_time_to_automatically_close;
	@JSONField(label = "age group labels")
	String[]			m_display_names = new String[] { "adults (13+)", "kids (5-12)", "under_fives" };
	@JSONField
	private boolean		m_event_text_show_jobs = true;
	@JSONField(label = "age group extras")
	float[]				m_extras = new float[] { (float)0.4, (float)0.2, 0 };
	@JSONField
	String[]			m_ignore_ages_for_max_count;
	@JSONField(fields = {"m_num_days_after_meal_to_invoice"})
	boolean				m_invoice_nightly;
	@JSONField(label = "label")
	String				m_max_people_label = "max people";
	@JSONField
	protected int 		m_num_days_after_meal_to_invoice = 2;
	@JSONField
	String[]			m_option_columns;
	@JSONField
	String[]			m_other_columns;
	@JSONField
	String				m_print_text;
	@JSONField(admin_only = true)
	boolean				m_restrict_to_household;
	@JSONField
	boolean				m_show_balance_message;
	@JSONField
	boolean				m_show_jobs_on_potlucks = true;
	@JSONField
	boolean				m_show_sign_up_button_in_calendar_view = true;
	@JSONField
	boolean				m_signup_individuals;
	@JSONField(fields = {"m_take_aways_label"})
	boolean				m_support_take_aways;
	@JSONField
	String				m_take_aways_label = "take aways";
	@JSONField(fields = {"m_default_time_to_automatically_close"})
	boolean				m_use_closed_field= true;
	@JSONField(fields = {"m_credit_account_id"}, label = "use cook's cost field")
	protected boolean	m_use_cooks_cost_field;
	@JSONField(fields = {"m_max_people_label"})
	boolean				m_use_max_people_field = true;
	@JSONField
	protected boolean	m_use_title_field;
	@JSONField(choices={"individuals", "households"}, label="workers are ")
	boolean				m_workers_are_families;

	//--------------------------------------------------------------------------

	public
	MealEventProvider() {
		super("Common Meals");
		m_check_overlaps = false;
		m_display_item_labels = true;
		m_display_items = new String[] { "menu", "cook", "jobs", "open/closed" };
		m_events_table = "meal_events";
		m_events_have_event = false;
		m_events_have_start_time = true;
		m_jobs.setJobs(new String[] { "additional_cooks", "assist", "setup", "clean1", "clean2" });
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	addAddresses(ArrayList<String> a) {
		a.add("diners");
		a.add("workers");
		a.add("cook");
		super.addAddresses(a);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	addBlock() {
		((Accounting)Site.site.getModule("Accounting")).addBlock(new Block(getDisplayName(null)) {
			@Override
			public boolean
			handlesContext(String context, Request r) {
				switch (context) {
				case "accounting module":
					return isActive() && m_invoice_account_id != 0;
				case "accounting report":
					return isActive();
				}
				return false;
			}
			@Override
			public void
			write(String context, Request r) {
				switch(context) {
				case "accounting module":
					writeAccountingPane(r);
					return;
				case "accounting report":
					writeReportPicker(r);
					return;
				}
			}
		});
	}

	//--------------------------------------------------------------------------

	public final void
	addCredit(int household_id, LocalDate date, double amount, int meal_event_id, DBConnection db) {
		if (amount == 0.0)
			return;
		if (m_credit_account_id == 0)
			throw new RuntimeException("credit account not set");
		Accounting.addCredit(m_credit_account_id, household_id, date, "Common Meal", amount, meal_event_id, null, db);
	}

	//--------------------------------------------------------------------------

	protected void
	addInvoice(int household_id, LocalDate date, double amount, int meal_event_id, DBConnection db) {
		if (date.isAfter(Time.newDate().minusDays(m_num_days_after_meal_to_invoice)))
			return;
		if (amount == 0.0)
			return;
		if (m_invoice_account_id == 0)
			throw new RuntimeException("invoice account not set");
		Accounting.addInvoice(m_invoice_account_id, household_id, date, "Common Meal", amount, meal_event_id, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addInvoices(int meal_event_id, DBConnection db) {
		if (m_invoice_account_id == 0)
			return;
		String where = (meal_event_id == 0 ? "NOT invoiced" : "id=" + meal_event_id) + " AND date<=CURRENT_DATE-" + m_num_days_after_meal_to_invoice;
		Select query = new Select("id,date,menu,no_charge").from("meal_events").where(where);
		if (m_use_title_field)
			query.addColumn("title");
		if (m_use_cooks_cost_field)
			query.addColumn("_owner_,cooks_cost");
		for (String age : m_ages)
			query.addColumn(SQL.columnName(age + "_price"));
		Rows meal_events = new Rows(query, db);
		query = new Select("households_id").from("meal_people");
		for (String age : m_ages)
			query.addColumn(SQL.columnName("num_" + age + 's'));
		while (meal_events.next()) {
			if (meal_events.getBoolean("no_charge"))
				continue;
			int meal_events_id = meal_events.getInt(1);
			Rows meal_people = new Rows(query.where("meal_events_id=" + meal_events_id), db);
			while (meal_people.next()) {
				double amount = 0;
				for (int i=0,n=m_ages.length; i<n; i++)
					amount += meal_events.getFloat(m_ages[i] + "_price") * meal_people.getInt(i + 2);
				addInvoice(meal_people.getInt(1), meal_events.getDate("date"), amount, meal_events_id, db);
			}
			meal_people.close();
			if (m_use_cooks_cost_field) {
				float cooks_cost = meal_events.getFloat("cooks_cost");
				if (cooks_cost != 0 && m_credit_account_id != 0) {
					Household household = new mosaic.Person(meal_events.getInt("_owner_"), db).getHousehold(db);
					if (household != null)
						addCredit(household.getId(), meal_events.getDate("date"), cooks_cost, meal_events_id, db);
				}
			}
		}
		db.update("meal_events", "invoiced=TRUE", where);
		meal_events.close();
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	addReports() {
		m_reports.add("date");
		m_reports.add("household");
		if (!m_workers_are_families)
			m_reports.add("person");
		super.addReports();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable(m_events_table)
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("menu", Types.VARCHAR))
			.add(new JDBCColumn("no_charge", Types.BOOLEAN))
			.add(new JDBCColumn("potluck", Types.BOOLEAN))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("_owner_", "people"))
			.add(new JDBCColumn("start_time", Types.TIME))
			.add(new JDBCColumn("end_time", Types.TIME))
			.add(new JDBCColumn("invoiced", Types.BOOLEAN))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP));
		if (m_use_closed_field)
			table_def.add(new JDBCColumn("closed", Types.BOOLEAN))
				.add(new JDBCColumn("closed_message", Types.VARCHAR))
				.add(new JDBCColumn("time_to_automatically_close", Types.TIME))
				.add(new JDBCColumn("close_days_before_meal", Types.INTEGER));
		if (m_use_cooks_cost_field)
			table_def.add(new JDBCColumn("cooks_cost", Types.REAL));
		if (m_use_max_people_field)
			table_def.add(new JDBCColumn("max_people", Types.INTEGER));
		if (m_use_title_field)
			table_def.add(new JDBCColumn("title", Types.VARCHAR));
		if (m_ages != null)
			for (String age : m_ages) {
				table_def.add(new JDBCColumn(age + "_budget_amount", Types.REAL));
				table_def.add(new JDBCColumn(age + "_price", Types.REAL));
			}
		if (m_option_columns != null)
			for (String option_column : m_option_columns)
				table_def.add(new JDBCColumn(option_column, Types.BOOLEAN));
		if (m_other_columns != null)
			for (String other_column : m_other_columns)
				table_def.add(new JDBCColumn(other_column, Types.REAL));
		addJDBCColumns(table_def);
		adjustLocation(table_def, db);
		JDBCTable.adjustTable(table_def, true, true, db);
		db.createIndex(m_events_table, "date");
		if (db.exists(m_events_table, "_timestamp_ IS NULL"))
			db.update(m_events_table, "_timestamp_=LEAST(now(),date) + interval '1 second' * id", "_timestamp_ IS NULL");

		adjustReminders(db);

		table_def = new JDBCTable("meal_people")
			.add(new JDBCColumn("meal_events"))
			.add(new JDBCColumn("dish", Types.VARCHAR))
			.add(new JDBCColumn(m_signup_individuals ? "people" : "households"))
			.add(new JDBCColumn("notes", Types.VARCHAR));
		if (!m_signup_individuals) {
			for (String age : m_ages)
				table_def.add(new JDBCColumn("num_" + age + "s", Types.INTEGER));
			if (m_support_take_aways)
				table_def.add(new JDBCColumn("take_aways", Types.INTEGER));
		}
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	deleteInvoices(int meal_event_id, DBConnection db) {
		if (m_credit_account_id != 0)
			db.delete("transactions", "type='Credit' AND accounts_id=" + m_credit_account_id + " AND transactions_id=" + meal_event_id, false);
		if (m_invoice_account_id != 0) {
			db.delete("transactions", "type='Invoice' AND accounts_id=" + m_invoice_account_id + " AND transactions_id=" + meal_event_id, false);
			db.update(m_events_table, "invoiced=false", meal_event_id);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch (segment_one) {
		case "worker emails":
			String ids = r.getParameter("ids");
			if (ids.length() == 0)
				return true;
			int comma = ids.indexOf(',');
			String owner = comma == -1 ? ids : ids.substring(0, comma);
			StringBuilder s = new StringBuilder();
			String email = r.db.lookupString(new Select("email").from("people").whereIdEquals(Integer.parseInt(owner)));
			if (email != null)
				s.append(email);
			if (comma != -1) {
				ids = ids.substring(comma + 1);
				Rows emails = new Rows(new Select("email").from(m_workers_are_families ? "households JOIN people ON (people.id=households.contact)" : "people").where((m_workers_are_families ? "households" : "people") + ".id IN(" + ids + ")").andWhere("email IS NOT NULL"), r.db);
				while (emails.next())
					s.append(',').append(Text.escapeDoubleQuotes(emails.getString(1)));
				emails.close();
			}
			r.response.add("emails", s.toString())
				.ok();
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	everyNight(LocalDateTime now, DBConnection db) {
		if (m_support_reminders)
			new Reminders(loadReminders(true, db), getRemindersTable(), null).send(now, db);
		if (m_invoice_nightly && m_invoice_account_id != 0)
			addInvoices(0, db);
	}

	//--------------------------------------------------------------------------

	public final String[]
	getAges() {
		return m_ages;
	}

	//--------------------------------------------------------------------------

	final String[]
	getDinerEmails(int meal_events_id, DBConnection db) {
		if (m_signup_individuals) {
			List<String> emails = db.readValues(new Select("email").from("meal_people JOIN people ON (people.id=meal_people.people_id)").whereEquals("meal_events_id", meal_events_id).andWhere("email IS NOT NULL"));
			return emails.toArray(new String[emails.size()]);
		}
		List<String> emails = db.readValues(new Select("email").from("meal_people JOIN households ON (households.id=meal_people.households_id) JOIN people ON (people.id=households.contact)").whereEquals("meal_events_id", meal_events_id).andWhere("email IS NOT NULL"));
		return emails.toArray(new String[emails.size()]);
	}

	// --------------------------------------------------------------------------

	@Override
	protected String
	getEventEditJS(Event event, LocalDate date, Request r) {
		return ((MealEvent)event).getEventEditJS(date, r);
	}

	//--------------------------------------------------------------------------

	@Override
	protected String[]
	getFieldOptions(String field_name) {
		if ("m_reminder_subject_items".equals(field_name))
			return getFieldOptions(new String[] { "cook", "date", "menu", "location" });
		if (m_use_title_field)
			return getFieldOptions(new String[] { "cook", "jobs", "location", "menu", "open/closed", "time", "title" });
		return getFieldOptions(new String[] { "cook", "jobs", "location", "menu", "open/closed", "time" });
	}

	//--------------------------------------------------------------------------

	final String[]
	getWorkerEmails(Rows meal, DBConnection db) {
		int[] job_ids = m_jobs.getIDs(meal);
		if (job_ids.length == 0)
			return new String[] { db.lookupString(new Select("email").from("people").whereIdEquals(meal.getInt("_owner_"))) };
		List<String> emails;
		if (m_workers_are_families) {
			emails = db.readValues(new Select("email").from("people JOIN households ON (households.contact=people.id)").where("households.id IN (" + Text.join(",", job_ids) + ")").andWhere("email IS NOT NULL"));
			emails.add(db.lookupString(new Select("email").from("people").whereIdEquals(meal.getInt("_owner_"))));
		} else
			emails = db.readValues(new Select("email").from("people").where("id IN (" + meal.getInt("_owner_") + "," + Text.join(",",  job_ids) + ")").andWhere("email IS NOT NULL"));
		return emails.toArray(new String[emails.size()]);
	}

	//--------------------------------------------------------------------------

	protected boolean
	hasCost(Rows rows) {
		if (rows.getBoolean("no_charge"))
			return false;
		for (String age : m_ages)
			if (rows.getDouble(age + "_price") > 0)
				return true;
		return false;
	}

	//--------------------------------------------------------------------------

	final boolean
	hasCost(int meal_id, DBConnection db) {
		StringBuilder s = new StringBuilder();
		for (String age : m_ages) {
			if (s.length() > 0)
				s.append("+");
			s.append(SQL.columnName(age + "_price"));
		}
		return db.lookupDouble(new Select("CASE WHEN no_charge THEN 0 ELSE " + s.toString() + " END").from("meal_events").whereIdEquals(meal_id), 0.0) > 0.0;
	}

	//--------------------------------------------------------------------------

	final boolean
	hasExtras() {
		if (m_extras != null)
			for (double extra : m_extras)
				if (extra != 0.0)
					return true;
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		Roles.add("cm account manager", "can edit common meal events with this role");
	}

	//--------------------------------------------------------------------------

	final boolean
	isNoCharge(int meal_event_id, DBConnection db) {
		return db.lookupBoolean(new Select("no_charge").from("meal_events").whereIdEquals(meal_event_id));
	}

	//--------------------------------------------------------------------------

	final boolean
	isPotluck(int meal_event_id, DBConnection db) {
		return db.lookupBoolean(new Select("potluck").from("meal_events").whereIdEquals(meal_event_id));
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	isValidAddress(String address) {
		if ("cook".equals(address) || "diners".equals(address) || "workers".equals(address))
			return true;
		return super.isValidAddress(address);
	}

	//--------------------------------------------------------------------------

	@Override
	protected Event
	loadEvent(Rows rows) {
		return new MealEvent(this, rows);
	}

	//--------------------------------------------------------------------------

	final boolean
	mealHasInvoices(int meal_event_id, DBConnection db) {
		return db.rowExists("transactions", "type='Invoice' AND accounts_id=" + m_invoice_account_id + " AND transactions_id=" + meal_event_id);
	}

	//--------------------------------------------------------------------------

	final boolean
	mealHasSignups(int meal_event_id, DBConnection db) {
		return db.rowExists("meal_people", "meal_people.meal_events_id=" + meal_event_id);
	}

	//--------------------------------------------------------------------------

	final String
	mealIsAutomaticallyClosed(LocalDate date, LocalTime time_to_automatically_close, int close_days_before_meal) {
		LocalDate today = Time.newDate();
		if (today.isAfter(date.plusDays(1)))
			return "the meal already happened";
		if (time_to_automatically_close != null) {
			LocalDate day_before = date.minusDays(close_days_before_meal);
			if (today.isAfter(day_before) || today.equals(day_before) && !Time.newTime().isBefore(time_to_automatically_close))
				return "the meal is set to close " + close_days_before_meal + " days before at " + Time.formatter.getTime(time_to_automatically_close, true);
		}
		return null;
	}

	//--------------------------------------------------------------------------

	final String
	mealIsAutomaticallyClosed(Rows data) {
		return mealIsAutomaticallyClosed(data.getDate("date"), data.getTime("time_to_automatically_close"), data.getInt("close_days_before_meal"));
	}

	//--------------------------------------------------------------------------

	final boolean
	mealIsClosed(Rows data, Request r) {
		if (!m_use_closed_field)
			return false;
		if (userCanAlwaysEditMeal(data.getInt("_owner_"), r))
			return false;
		return data.getBoolean("closed") || mealIsAutomaticallyClosed(data) != null;
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals(m_name))
			return new MealViewDef(this, m_attachments, false);
		if (name.equals("meal_events list")) {
			ViewDef view_def = new MealViewDef(this, m_attachments, true)
				.addFeature(new MonthYearFilter("meal_events", Location.TOP_LEFT))
				.setColumnHeadsCanWrap(true)
				.setDefaultOrderBy("date")
				.setFrom("meal_events")
				.setName(name);
			for (String age : m_ages)
				view_def.setColumn(((Column)view_def.cloneColumn("num_" + age + "s")).setTotal(true));
			view_def.setColumn(((CurrencyColumn)view_def.cloneColumn("total")).setTotal(true))
				.setColumn(((CurrencyColumn)view_def.cloneColumn("total budget")).setTotal(true));
			return view_def;
		}
		if (name.equals("meal_people"))
			return new MealPeopleViewDef(new MealPeopleAccessPolicy(this, m_allow_signup_of_other_households), this, m_check_for_at_least_one);
		if (name.equals("meal history")) {
			ViewDef view_def = new ViewDef(name)
				.addFeature(new YearFilter("meal_events LEFT JOIN meal_people ON meal_people.meal_events_id=meal_events.id", Location.TOP_LEFT))
				.setAccessPolicy(new AccessPolicy())
				.setDefaultOrderBy("date")
				.setFrom("meal_events LEFT JOIN meal_people ON meal_people.meal_events_id=meal_events.id")
				.setRecordName("Meal", true);
			ArrayList<String> column_names = new ArrayList<>();
			column_names.add("date");
			column_names.add("menu");
			column_names.add("jobs");
			for (String age : m_ages) {
				String c = "num_" + age + "s";
				column_names.add(c);
				view_def.setColumn(new Column(c).setTextAlign("right").setTotal(true).setTotalIsInt(true));
			}
			column_names.add("cost");
			view_def.setColumnNamesTable(column_names)
				.setColumn(new CurrencyColumn("cost"){
					@Override
					public double
					getDouble(View v, Request r) {
						double cost = 0;
						if (!v.data.getBoolean("no_charge"))
							for (String age : m_ages)
								cost += v.data.getDouble(age + "_price") * v.data.getDouble("num_" + age + "s");
						return cost;
					}
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						r.w.writeCurrency(getDouble(v, r));
						return true;
					}
				}.setTextAlign("right").setTotal(true))
				.setColumn(new Column("jobs"){
					@Override
					public double
					getDouble(View v, Request r) {
						double num = 0;
						Household household = ((mosaic.Person)r.getUser()).getHousehold(r.db);
						String[] household_members = household.getPeopleIdsString().split(",");
						if (Array.indexOf(household_members, v.data.getString("_owner_")) != -1)
							num++;
						int[] job_ids = m_jobs.getIDs(v.data);
						for (int job_id : job_ids)
							if (m_workers_are_families) {
								if (household.getId() == job_id)
									num++;
							} else
								if (Array.indexOf(household_members, Integer.toString(job_id)) != -1)
									num++;
						return num;
					}
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						StringBuilder jobs = new StringBuilder();
						Household household = ((mosaic.Person)r.getUser()).getHousehold(r.db);
						List<Integer> household_members = household.getPeopleIds();
						if (household_members.contains(v.data.getInt("_owner_")))
							jobs.append("cook");
						if (m_workers_are_families)
							m_jobs.appendLabels(v.data, household.getId(), jobs);
						else
							m_jobs.appendLabels(v.data, household_members, jobs);
						if (jobs.length() > 0) {
							r.w.write(jobs.toString());
							return true;
						}
						return false;
					}
				}.setIsSortable(false).setTotal(true).setTotalIsInt(true));
			return view_def;
		}
		if (name.equals("report invoices"))
			return new ViewDef(name)
				.addFeature(new MonthFilter("transactions", Location.TOP_LEFT))
				.addFeature(new YearFilter("transactions", Location.TOP_LEFT))
				.addSection(new Dividers("date", new OrderBy("date")).setCollapseRows(true))
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setDefaultOrderBy("households_id")
				.setFrom("transactions")
				.setRecordName("Invoice", true)
				.setColumnNamesTable(new String[] { "households_id", "amount" })
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(new LookupColumn("households_id", "households", "name", "active", "name").setDisplayName("household"));
		return super._newViewDef(name);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request r) {
		String[] option_columns = m_option_columns;
		super.updateSettings(r);
		if (option_columns != null)
			for (String option_column : option_columns)
				if (Array.indexOf(m_option_columns, option_column) == -1)
					r.db.dropColumn("meal_events", option_column);
	}

	// --------------------------------------------------------------------------

	final boolean
	userCanAlwaysEditMeal(int owner, Request r) {
		if (!m_use_closed_field)
			return true;
		if (owner != 0) {
			Person user = r.getUser();
			if (user != null && owner == user.getId())
				return true;
		}
		return r.userHasRole("cm account manager");
	}

	// --------------------------------------------------------------------------

	final int
	userIsSignedUp(int meal_id, Request r) {
		mosaic.Person user = (mosaic.Person)r.getUser();
		if (user == null)
			return 0;
		return r.db.lookupInt(
			new Select("id")
				.from("meal_people")
				.where("meal_events_id=" + meal_id + " AND " + (m_signup_individuals ? "people_id" : "households_id") + "=" + (m_signup_individuals ? user.getId() : user.getHouseholdId())), 0);
	}

	// --------------------------------------------------------------------------

	final void
	writeAccountingPane(Request r) {
		HTMLWriter w = r.w;
		w.h3(getName());
		Table table = w.ui.table().addStyle("padding", "20px");
		table.tr();
		w.setId("invoices")
			.addStyle("min-width:250px;vertical-align:top");
		table.td();
		ViewState.setBaseFilter("report invoices", "type='Invoice' AND accounts_id=" + m_invoice_account_id, r);
		Site.site.newView("report invoices", r).writeComponent();
		w.addStyle("vertical-align:top");
		table.td();
		new MonthView((EventProvider)Site.site.getModule("Common Meals"), r).writeComponent(r);
		table.close();
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeReport(Request r) {
		String view_by = r.getParameter("view_by");
		switch (view_by) {
		case "date":
			Site.site.newView("meal_events list", r).writeComponent();
			return;
		case "household":
			new AnnualReport(new Select("*").from(m_events_table), "Household", 0, 0, 0) {
					@Override
					protected void
					add(Rows rows, Map<Object,Sums> items, Map<String,Map<Object,Sums>> groups, int period) {
						add(r.db.lookupInt("households_id", "people", "id=" + rows.getInt("_owner_"), 0), items, period);
						int[] job_ids = m_jobs.getIDs(rows);
						if (m_workers_are_families)
							for (Integer job_id : job_ids) {
								if (job_id != 0)
									add(job_id, items, period);
							}
						else
							for (Integer job_id : job_ids)
								if (job_id != 0) {
									int household = r.db.lookupInt("households_id", "people", "id=" + job_id, 0);
									if (household != 0)
										add(household, items, period);
								}
					}
					private void
					add(int household, Map<Object,Sums> items, int period) {
						String name = r.db.lookupString("name", "households", household);
						if (name == null)
							return;
						Sums item_sums = items.get(name);
						if (item_sums == null) {
							item_sums = new Sums(0);
							items.put(name, item_sums);
						}
						item_sums.add(1, period);
					}
				}
				.setParam("view_by")
				.setValueIsDouble(false)
				.write("Times Worked", r);
			return;
		case "person":
			new AnnualReport(new Select("*").from(m_events_table), "Person", 0, 0, 0) {
					@Override
					protected void
					add(Rows rows, Map<Object,Sums> items, Map<String,Map<Object,Sums>> groups, int period) {
						add(rows.getInt("_owner_"), items, period);
						int[] job_ids = m_jobs.getIDs(rows);
						for (Integer job_id : job_ids)
							if (job_id != 0)
								add(job_id, items, period);
					}
					private void
					add(int person, Map<Object,Sums> items, int period) {
						String name = r.db.lookupString("first,last", "people", person);
						if (name == null)
							return;
						Sums item_sums = items.get(name);
						if (item_sums == null) {
							item_sums = new Sums(0);
							items.put(name, item_sums);
						}
						item_sums.add(1, period);
					}
				}
				.setParam("view_by")
				.setValueIsDouble(false)
				.write("Times Worked", r);
			return;
		}
		super.writeReport(r);
	}

	//--------------------------------------------------------------------------

	public final void
	writeMealHistory(Request r) {
		mosaic.Person user = (mosaic.Person)r.getUser();
		int household_id = user.getHousehold(r.db).getId();
		if (household_id == 0)
			return;
		String people_ids = user.getHousehold(r.db).getPeopleIdsString();
		if (people_ids.length() == 0) {
			r.w.alert("danger", "There are no people in this household");
			return;
		}

		StringBuilder where = new StringBuilder();
		if (m_signup_individuals)
			where.append("(people_id=").append(user.getId()).append(" OR people_id IS NULL)");
		else
			where.append("(households_id=").append(household_id).append(" OR households_id IS NULL)");
		where.append(" AND (_owner_ IN(").append(people_ids).append(')').append(" OR ");
		if (m_workers_are_families)
			m_jobs.appendWhere(household_id, where);
		else
			m_jobs.appendWhere(people_ids, where);
		if (m_signup_individuals)
			where.append(" OR people_id=").append(user.getId());
		else
			where.append(" OR households_id=").append(household_id);
		where.append(')');
		ViewState.setBaseFilter("meal history", where.toString(), r);
		Site.site.newView("meal history", r)
			.writeComponent();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, boolean hide_active, Request r) {
		super.writeSettingsForm(new String[] { "m_active", "m_ages", "m_agreements", "m_allow_signup_of_other_households", "m_color", "m_cook_label", "m_default_view", "m_display_item_labels", "m_display_items", "m_display_name", "m_events_have_location", "m_events_have_start_time", "m_event_text_show_jobs", "m_ical_items", "m_invoice_account_id", "m_invoice_nightly", "m_jobs", "m_option_columns", "m_print_text", "m_role", "m_send_announcement_to", "m_show_jobs_on_potlucks", "m_show_on_menu", "m_show_on_upcoming", "m_show_sign_up_button_in_calendar_view", "m_start_date_label", "m_support_announcing_new_events", "m_support_reminders", "m_support_take_aways", "m_text_above_calendar", "m_time_to_automatically_close", "m_tooltip_item_labels", "m_tooltip_items", "m_use_closed_field", "m_use_cooks_cost_field", "m_use_max_people_field", "m_use_title_field", "m_uuid", "m_workers_are_families" }, in_dialog, hide_active, r);
	}
}
