package meals;

import app.Request;
import app.Site;
import db.DBConnection;
import db.Form;
import db.View;
import db.column.ColumnBase;
import db.column.ColumnInputRenderer;
import mosaic.Jobs;
import mosaic.Person;
import util.Text;

public class MealRatioRenderer implements ColumnInputRenderer {
	public static void
	writeRatio(int household_id, Jobs jobs, boolean show_math, Request r) {
		DBConnection db = r.db;
		int num_adults = db.lookupInt("SUM(num_adults)", "meal_people", "households_id=" + household_id, 0);
		int times_worked = db.lookupInt("COUNT(*)", "meal_events JOIN people ON people.id=meal_events._owner_", "households_id=" + household_id, 0);
		times_worked += jobs.timesWorked("meal_events", household_id, db);
		float ratio = (float)num_adults / times_worked;
		if (Float.isInfinite(ratio)) {
			r.w.write("num adult meals: ");
			r.w.write(num_adults);
			r.w.write(", times worked: 0");
		} else if (Float.isNaN(ratio))
			r.w.write("never worked or eaten");
		else {
			if (show_math) {
	//			request.writer.write("(num adult meals: ");
				r.w.write(num_adults);
				r.w.write(" / ");
	//			request.writer.write(") / (num times household worked: ");
				r.w.write(times_worked);
	//			request.writer.write(") = ");
				r.w.write(" = ");
			}
			r.w.write(Text.two_digits_max.format(ratio));
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeInput(View v, Form f, ColumnBase<?> c, boolean inline, Request r) {
		writeRatio(((Person)r.getUser()).getHousehold(r.db).getId(), ((MealEventProvider)Site.site.getModule("Common Meals")).getJobs(), false, r);
	}
}
