package meals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import app.Person;
import app.Request;
import app.Site;
import calendar.Event;
import db.DBConnection;
import db.Rows;
import db.Select;
import util.Time;
import web.JS;

public class MealEvent extends Event {
	private int			m_close_days_before_meal;
	private boolean		m_closed;
	private boolean		m_has_cost;
	private String		m_menu;
	private boolean		m_potluck;
	private LocalTime	m_time_to_automatically_close;
	private String		m_title;
	private int[]		m_workers;

	//--------------------------------------------------------------------------

	public
	MealEvent(MealEventProvider event_provider, Rows rows) {
		super(event_provider, rows);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	appendEventHTML(LocalDate date, StringBuilder s, boolean black_text, Request r) {
		super.appendEventHTML(date, s, black_text, r);
		boolean accounting_page = "Accounting".equals(r.getSessionAttribute("current page"));
		MealEventProvider ep = (MealEventProvider)m_ep;
		if (!accounting_page && ep.m_show_sign_up_button_in_calendar_view) {
			Rows meal = new Rows(new Select("*").from("meal_events").whereIdEquals(m_id), r.db);
			if (meal.next() && !ep.mealIsClosed(meal, r)) {
				int signed_up = ep.userIsSignedUp(m_id, r);
				s.append(r.w.captureStart().ui.buttonOutlineOnClick(signed_up != 0 ? "Edit Sign Up" : "Sign Up", """
					event.stopPropagation()
					new Dialog({
						button:this,
						cancel:true,
						component:true,
						ok:{text:'Sign Up'},
						owner:_.$('#c_calendar_view'),
						title:'Sign Up for Meal',
						url:context+'/Views/meal_people""" + (signed_up != 0 ? "?db_mode=EDIT_FORM&db_key_value=" + signed_up + "&" : "/add?") + "sign_up=true&one=Common Meals&one_id=" + m_id + "'})"
					).captureEnd());
			}
			meal.close();
			return;
		}
		if (ep.m_invoice_account_id != 0 && r.getUser() != null && accounting_page)
			if (ep.mealHasInvoices(m_id, r.db))
				s.append("<br />").append(r.w.ui.button("Delete Invoices")
					.setOnClick("net.post(context+'/Common Meals/" + m_id + "/invoices/delete',null,function(){Calendar.$().update();net.replace(_.$('#invoices').firstChild)});event.stopPropagation();return false;")
					.toString());
			else if (m_start_date.isBefore(Time.newDate()) && ep.mealHasSignups(m_id, r.db) && m_has_cost)
				s.append("<br />").append(r.w.ui.button("Invoice")
					.setOnClick("net.post(context+'/Common Meals/" + m_id + "/invoices/add',null,function(){Calendar.$().update();net.replace(_.$('#invoices').firstChild)});event.stopPropagation();return false;")
					.toString());
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	appendItem(String item, boolean black_text, boolean html, boolean ics, LocalDate date, StringBuilder s, Request r) {
		switch (item) {
		case "cook":
			if (!m_potluck)
				appendItem(((MealEventProvider)m_ep).m_cook_label, m_owner == 0 ? null : Site.site.lookupName(m_owner, r.db), true, null, black_text, html, ics, s);
			return;
		case "menu":
			if (m_menu != null || m_potluck) {
				String menu = m_menu;
				if (m_potluck)
					if (menu != null)
						menu += " + potluck";
					else
						menu = "potluck";
				appendItem(item, menu, false, null, black_text, html, ics, s);
			}
			return;
		case "jobs":
			if (!m_potluck || ((MealEventProvider)m_ep).m_show_jobs_on_potlucks) {
				String[] labels = ((MealEventProvider)m_ep).getJobs().getLabels();
				for (int i=0; i<labels.length; i++)
					appendItem(labels[i],
						r.db.lookupString(((MealEventProvider)m_ep).m_workers_are_families ? "name" : "first,last",
							((MealEventProvider)m_ep).m_workers_are_families ? "households" : "people",
							m_workers[i]),
						true, null, black_text, html, ics, s);
			}
			return;
		case "open/closed":
			if (!((MealEventProvider)m_ep).m_use_closed_field)
				return;
			if (html)
				s.append("<div>- ");
			else if (s.length() > 0)
				s.append(ics ? "\\n" : "\n");
			s.append(m_closed || ((MealEventProvider)m_ep).mealIsAutomaticallyClosed(m_start_date, m_time_to_automatically_close, m_close_days_before_meal) != null ? "closed" : "open");
			if (html)
				s.append(" -</div>");
			return;
		case "title":
			if (m_title != null)
				appendItem(null, m_title, false, null, black_text, html, ics, s);
			return;
		}
		super.appendItem(item, black_text, html, ics, date, s, r);
	}

	// --------------------------------------------------------------------------

	final String
	getEventEditJS(LocalDate date, Request r) {
		Person user = r.getUser();
		if (user == null)
			return null;
		return "Calendar.$().edit(" + JS.string(m_ep.getName()) + "," + JS.string(m_ep.getDisplayName(r)) + "," + m_id + ",'" + date.toString() + "'," + (isClosed(r) ? "true" : "false") + ")";
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getEventText() {
		if (m_title != null)
			return m_title;
		else if (m_menu != null)
			return m_menu;
		return null;
	}

	//--------------------------------------------------------------------------

	private boolean
	isClosed(Request r) {
		if (((MealEventProvider)m_ep).userCanAlwaysEditMeal(m_owner, r))
			return false;
		return m_closed || ((MealEventProvider)m_ep).mealIsAutomaticallyClosed(m_start_date, m_time_to_automatically_close, m_close_days_before_meal) != null;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	load(Rows rows) {
		super.load(rows);
		MealEventProvider ep = (MealEventProvider)m_ep;
		if (ep.m_use_closed_field) {
			m_closed = rows.getBoolean("closed");
			m_time_to_automatically_close = rows.getTime("time_to_automatically_close");
			m_close_days_before_meal = rows.getInt("close_days_before_meal");
		}
		m_menu = rows.getString("menu");
		m_potluck = rows.getBoolean("potluck");
		if (ep.m_use_title_field)
			m_title = rows.getString("title");
		m_workers = ep.getJobs().getIDs(rows);
		m_has_cost = ep.hasCost(rows);
	}

	//--------------------------------------------------------------------------

	@Override
	public String[]
	lookupEmail(String email, DBConnection db) {
		if ("cook".equals(email))
			return new String[] { db.lookupString(new Select("email").from("people").whereIdEquals(m_owner)) };
		if ("diners".equals(email))
			return ((MealEventProvider)m_ep).getDinerEmails(m_id, db);
		if ("workers".equals(email)) {
			ArrayList<String> emails = new ArrayList<>();
			emails.add(db.lookupString(new Select("email").from("people").whereIdEquals(m_owner)));
			for (Integer worker : m_workers)
				if (worker != 0) {
					boolean workers_are_households = ((MealEventProvider)m_ep).m_workers_are_families;
					email = db.lookupString(new Select("email").from(workers_are_households ? "households JOIN people ON (households.contact=people.id)" : "people").whereEquals(workers_are_households ? "households.id" : "people.id", worker));
					if (email != null)
						emails.add(email);
				}
			return emails.toArray(new String[emails.size()]);
		}
		return super.lookupEmail(email, db);
	}
}
