package meals;

import app.Person;
import app.Request;
import db.Rows;
import db.access.AccessPolicy;

class MealAccessPolicy extends AccessPolicy {
	public
	MealAccessPolicy() {
		add();
		delete();
		edit();
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtonForRow(Rows data, Request r) {
		Person user = r.getUser();
		int cook = data.getInt("_owner_");
		return user != null && (cook == 0 || cook == user.getId() || r.userHasRole("cm account manager"));
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtonForRow(Rows data, Request r) {
		Person user = r.getUser();
		int cook = data.getInt("_owner_");
		return user != null && (cook == 0 || cook == user.getId() || r.userHasRole("cm account manager"));
	}
}
