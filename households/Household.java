package households;

import java.util.List;

import db.DBConnection;
import db.Select;
import db.object.DBField;
import db.object.DBObject;
import util.Text;

public class Household {
	@DBField
	private int				m_contact;
	private int				m_id;
	@DBField
	private boolean			m_meal_work_exempt;
	@DBField
	private String			m_name;
	private List<Integer>	m_people_ids;

	//--------------------------------------------------------------------------

	public final boolean
	belongsTo(int people_id) {
		return m_people_ids.contains(people_id);
	}

	//--------------------------------------------------------------------------

	public final int
	getContact() {
		return m_contact;
	}

	//--------------------------------------------------------------------------

	public final double
	getDues(DBConnection db) {
		return db.lookupFloat(new Select("sum(hoa_dues)").from("homes JOIN units ON units.id=homes.units_id").where("pays_hoa=" + m_id), 0);
	}

	//--------------------------------------------------------------------------

	public final int
	getId() {
		return m_id;
	}

	//--------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public final List<Integer>
	getPeopleIds() {
		return m_people_ids;
	}

	//--------------------------------------------------------------------------

	public final String
	getPeopleIdsString() {
		return Text.join(",", m_people_ids);
	}

	//--------------------------------------------------------------------------

	public final boolean
	isMealWorkExempt() {
		return m_meal_work_exempt;
	}

	//--------------------------------------------------------------------------

	static Household
	load(int id, DBConnection db) {
		Household household = new Household();
		if (DBObject.load(household, "households", id, db)) {
			household.m_id = id;
			household.m_people_ids = db.readValuesInt(new Select("id").from("people").whereEquals("households_id", id));
			return household;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public final void
	setName(String name, DBConnection db) {
		m_name = name;
// this is only called from afterUpdate() so name already stored
//		DBObject.store(this, "households", m_id, db);
	}
}
