package households;

import java.time.LocalDate;

import app.Request;
import calendar.MosaicEventProvider;
import db.Rows;
import db.Select;
import db.ViewDef;
import db.ViewState;
import db.column.Column;
import db.jdbc.JDBCColumn;
import mosaic.Person;
import util.Array;

public class HouseholdEventProvider extends MosaicEventProvider {
	public
	HouseholdEventProvider() {
		super("Household Calendar");
		m_check_overlaps = false;
		addJDBCColumn(new JDBCColumn("households"));
		setColor("#39CCCC");
		setEventsCanRepeat(true);
		setEventsHaveTime(true);
		setEventsTable("households_events");
		setSupportReminders();
	}

	//--------------------------------------------------------------------------

	@Override
	protected int
	countEvents(int target_max, LocalDate date, Request r) {
		Person person = (mosaic.Person)r.getUser();
		if (person == null)
			return 0;
		Household household = person.getHousehold(r.db);
		if (household == null)
			return 0;
		String date_string = date.toString();
		Select query = new Select().from(m_events_table).where("date>='" + date_string + "'");
		query.andWhere("repeat='never'").andWhere("households_id=" + household.getId());
		int count = r.db.countRows(m_events_table, query.getWhere());
		if (count < target_max) {
			Rows rows = new Rows(new Select("date,end_date,repeat").from(m_events_table).where("households_id=" + household.getId() + " AND (end_date IS NULL OR end_date>='" + date_string + "')"), r.db);
			while (rows.next()) {
				LocalDate end_date = rows.getDate(2);
				if (end_date == null) {
					rows.close();
					return target_max;
				}
				LocalDate d = date;
				while (count < target_max && d.isBefore(end_date)) {
					if (occursOn(d, rows.getDate(1), end_date, rows.getString(3)))
						count++;
					d = d.plusDays(1);
				}
			}
			rows.close();
		}
		return count;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getDisplayName(Request r) {
		if (r != null) {
			Household h = ((mosaic.Person)r.getUser()).getHousehold(r.db);
			if (h != null) {
				String display_name = h.getName();
				if (display_name != null)
					return display_name;
			}
		}
		return "Household Calendar";
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals(m_name)) {
			ViewDef view_def = super._newViewDef(name)
				.setColumn(new Column("households_id") {
					@Override
					public String
					getDefaultValue(ViewState view_state, Request r) {
						return Integer.toString(((mosaic.Person)r.getUser()).getHousehold(r.db).getId());
					}
				}.setIsHidden(true));
			view_def.setColumnNamesForm(Array.concat(view_def.getColumnNamesForm(), "households_id"));
			return view_def;
		}
		return super._newViewDef(name);
	}

	//--------------------------------------------------------------------------

	@Override
	protected Select
	selectEvents(LocalDate from, LocalDate to, int num_events, Request r) {
		Person person = (mosaic.Person)r.getUser();
		if (person == null)
			return null;
		Household household = person.getHousehold(r.db);
		if (household == null)
			return null;
		Select select = super.selectEvents(from, to, num_events, r);
		if (select == null)
			return null;
		return select.andWhere("households_id=" + ((mosaic.Person)r.getUser()).getHousehold(r.db).getId());
	}

	//--------------------------------------------------------------------------

	@Override
	protected Select
	selectRepeatingEvents(LocalDate from, LocalDate to, Request r) {
		Person person = (mosaic.Person)r.getUser();
		if (person == null)
			return null;
		Household household = person.getHousehold(r.db);
		if (household == null)
			return null;
		return super.selectRepeatingEvents(from, to, r).andWhere("households_id=" + ((mosaic.Person)r.getUser()).getHousehold(r.db).getId());
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showOnMenu(Request r) {
		Person user = (Person)r.getUser();
		return user != null && user.getHousehold(r.db) != null;
	}
}
