package households;

import java.util.ArrayList;

import accounting.Accounting;
import app.Block;
import app.Request;
import ui.Form;
import util.Text;

public class TransactionBlock extends Block {
	class Type {
		public
		Type(String type, String description, double cost, int accounts_id) {
			this.type = type;
			this.description = description;
			this.cost = cost;
			this.accounts_id = accounts_id;
		}
		final int		accounts_id;
		final double	cost;
		final String	description;
		final String	type;
	}

	private final ArrayList<Type>	m_types = new ArrayList<>();

	//--------------------------------------------------------------------------

	public
	TransactionBlock(String name) {
		super(name);
	}

	//--------------------------------------------------------------------------

	public TransactionBlock
	addType(String type, int accounts_id, double cost, String description) {
		m_types.add(new Type(type, description, cost, accounts_id));
		return this;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	doPost(Request r) {
		int household_id = ((mosaic.Person)r.getUser()).getHousehold(r.db).getId();
		int num_transactions = 0;
		for (int i=0; i<m_types.size(); i++) {
			Type type = m_types.get(i);
			switch(type.type) {
			case "Check":
				double amount = r.getDouble("amount" + i, 0.0);
				if (amount != 0.0) {
					Accounting.addCheck(0, 0, 0, null, null, type.description, amount, r.getParameter("notes" + i), r.db);
					num_transactions++;
				}
				break;
			case "Credit":
				amount = r.getDouble("amount" + i, 0.0);
				if (amount != 0.0) {
					Accounting.addCredit(0, household_id, null, type.description, amount, 0, r.getParameter("notes" + i), r.db);
					num_transactions++;
				}
				break;
			case "Invoice":
				int num = r.getInt("num" + i, 0);
				if (num != 0) {
					Accounting.addInvoice(type.accounts_id, household_id, null, type.description, type.cost * num, r.db);
					num_transactions++;
				}
				break;
			}
		}
		r.response.addMessage(num_transactions + " " + Text.pluralize("transaction", num_transactions) + " submitted");
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	handlesContext(String context, Request r) {
		return context.equals("household accounting");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	write(String context, Request r) {
		Form f = new Form(null, m_module.getAbsoluteURL("blocks", m_name).toString(), r.w).open();
		for (int i=0; i<m_types.size(); i++) {
			Type type = m_types.get(i);
			f.rowOpen(type.description);
			if (type.cost == 0.0)
				r.w.write("amount: ").textInput("amount" + i, 0, null).br().write("notes: ").textArea("notes" + i, null);
			else
				r.w.write("number of uses: ").numberInput("num" + i, "0", null, "1", null, true);
			f.rowClose();
		}
		f.setSubmitText(Text.submit).close();
	}
}
