package households;

import app.Request;
import db.ViewState;
import db.column.LookupColumn;
import mosaic.Person;

public class HouseholdIdColumn extends LookupColumn {
	public
	HouseholdIdColumn() {
		super("households_id", "households", "name");
		m_is_hidden = true;
	}

	//--------------------------------------------------------------------------

	@Override
	public String
	getDefaultValue(ViewState view_state, Request r) {
		Person user = (Person)r.getUser();
		if (user == null)
			return null;
		Household household = user.getHousehold(r.db);
		return household == null ? null : Integer.toString(household.getId());
	}
}
