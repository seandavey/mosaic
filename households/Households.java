 package households;

import java.sql.Types;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import accounting.Accounting;
import app.Block;
import app.Module;
import app.Request;
import app.Site;
import app.SiteModule;
import blocks.Blocks;
import db.DBConnection;
import db.ManyToMany;
import db.NameValuePairs;
import db.OneToMany;
import db.OneToManyLink;
import db.Rows;
import db.RowsSelect;
import db.SQL;
import db.Select;
import db.UpdateHook;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.Or;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.LookupColumn;
import db.column.PersonColumn;
import db.column.PictureColumn;
import db.column.TextAreaColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Tabs;
import mosaic.People;
import pages.Page;
import ui.Card;
import ui.NavBar;
import util.Array;
import util.Text;
import web.HTMLWriter;
import web.JS;

public class Households extends SiteModule {
	static Households						s_module;

	private Map<String,Assets>				m_assets = new TreeMap<>();
	private JsonArray						m_blocks = new JsonArray();
	@JSONField(admin_only = true)
	private String[]						m_extra_columns;
	private final Map<Integer,Household>	m_households = new HashMap<>();
	@JSONField
	private String							m_hoa_text;
	@JSONField(choices = { "contacts", "notes", "recipes", "todos" })
	private final String[]					m_lists = { "contacts", "notes", "recipes", "todos" };
	private Page							m_page;

	//--------------------------------------------------------------------------

	public
	Households() {
		m_assets.put("bicycles", new Assets("bicycles").setAvailableForSharing(true).setHasParkingSpot(true));
		m_assets.put("pets", new Assets("pets").setHasName(true));
		m_assets.put("vehicles", new Assets("vehicles").setAvailableForSharing(true).setHasLicensePlate(true).setHasParkingSpot(true));
		m_description = "Organize households and their bios, pets, vehicles, and bicycles, and their private contacts, notes, recipes, and todos";
		m_renameable = true;
		s_module = this;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	addPages(DBConnection db) {
		m_page = new Page("Households", this, true) {
			@Override
			public void
			writeContent(Request r) {
				String v = r.getParameter("v");
				if (v != null)
					r.w.js("app.page_menu_select('" + v + "')");
			}
		};
		for (Assets a : m_assets.values())
			a.addPages(db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	adjustTables(DBConnection db) {
		db.createManyTable("households", "households_pictures", "filename VARCHAR");
		JDBCTable table_def = new JDBCTable("notes")
			.add(new JDBCColumn("households"))
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("note", Types.BINARY))
			.add(new JDBCColumn("category", Types.VARCHAR))
			.add(new JDBCColumn("public", Types.BOOLEAN));
		JDBCTable.adjustTable(table_def, true, false, db);
		for (Assets a : m_assets.values())
			a.adjustTables(db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		Assets a = m_assets.get(path_segments[0]);
		if (a != null) {
			a.doAPIGet(path_segments, r);
			return true;
		}
		switch (path_segments[0]) {
		case "households":
			writeHouseholds(path_segments[1], r);
			return true;
		case "profile":
			writeProfile(Integer.parseInt(path_segments[1]), r);
			return true;
		}
		mosaic.Person user = (mosaic.Person)r.getUser();
		if (user == null)
			return true;
		Household household = user.getHousehold(r.db);
		if (household == null)
			return true;
		int household_id = household.getId();
		switch (path_segments[0]) {
		case "Contacts":
			r.w.h3("Contacts").ui.subHead("These are private to your household. Check the \"show on public household page\" checkbox to have some contacts shared on the People page.");
			ViewState.setBaseFilter("households_contacts", "households_id=" + household_id, r);
			Site.site.newView("households_contacts", r).writeComponent();
			return true;
		case "Notes":
			r.w.h3("Notes").ui.subHead("These are private to your household. Check the \"show on public household page\" checkbox to have some notes shared on the People page.");
			ViewState.setBaseFilter("notes", "households_id=" + household_id, r);
			Site.site.newView("notes", r).writeComponent();
			return true;
		case "Recipes":
			r.w.h3("Recipes").ui.subHead("These are private to your household. Check the \"share with community\" checkbox to have some recipes shown on the main Recipes page.");
			String people_ids = household.getPeopleIdsString();
			if (people_ids.length() == 0)
				r.w.alert("danger", "There are no people in this household");
			else {
				ViewState.setBaseFilter("recipes", "_owner_ IN(" + people_ids + ")", r);
				Site.site.newView("recipes", r).writeComponent();
			}
			return true;
		case "Todos":
			r.w.h3("Todos").ui.subHead("These are private to your household.");
			ViewState.setBaseFilter("todos", "households_id=" + household_id, r);
			Site.site.newView("todos", r).writeComponent();
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		m_page.write(true, r);
		return true;
	}

	//--------------------------------------------------------------------------

	public static Household
	getHousehold(int id, DBConnection db) {
		if (!s_module.isActive())
			return null;
		if (id == 0)
			return null;
		Household household = s_module.m_households.get(id);
		if (household == null) {
			household = Household.load(id, db);
			if (household != null)
				s_module.m_households.put(id, household);
		}
		return household;
	}

	//--------------------------------------------------------------------------

	private String
	getProfileJS(String household_name, int id) {
		return "new Dialog({id:" + id + ",title:'" + JS.escape(household_name) + " Household',url:context+'/" + apiURL("profile", Integer.toString(id)) + "'})";
	}

	//--------------------------------------------------------------------------

	public static JDBCTable
	getTable() {
		return new JDBCTable("households")
			.add(new JDBCColumn("name", Types.VARCHAR))
			.add(new JDBCColumn("homes_id", "homes").setOnDeleteSetNull(true))
			.add(new JDBCColumn("active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("contact", "people").setOnDeleteSetNull(true))
			.add(new JDBCColumn("cm_account_active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("meal_work_exempt", Types.BOOLEAN))
			.add(new JDBCColumn("bio", Types.VARCHAR))
			.add(new JDBCColumn("emergency_info", Types.VARCHAR));
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		m_blocks.add(new JsonObject(new JsonObject().add("type", "people")))
			.add(new JsonObject(new JsonObject().add("type", "person").add("edit_only", true)))
			.add(new JsonObject(new JsonObject().add("type", "html").add("title", "Bio").add("column", "bio").add("view", "households")))
			.add(new JsonObject(new JsonObject().add("type", "contacts")))
			.add(new JsonObject(new JsonObject().add("type", "notes")));
		Set<String> set = new TreeSet<>();
		set.add("pictures");
		for (String a : m_assets.keySet())
			set.add(a);
		for (String s : set)
			m_blocks.add(new JsonObject().add("type", s));
		m_blocks.add(new JsonObject(new JsonObject().add("type", "html").add("title", "Emergency Info").add("column", "emergency_info").add("view", "households")));
		super.init(was_added, db);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		Assets a = m_assets.get(name);
		if (a != null)
			return a.newViewDef();
		switch (name) {
		case "households":
			ViewDef view_def = new ViewDef(name)
				.addUpdateHook(new UpdateHook() {
					@Override
					public void
					afterUpdate(int id, NameValuePairs nvp, Map<String,Object> previous_values, Request r) {
						String n = nvp.getString("name");
						if (n == null || n.length() == 0)
							return;
						Module calendar = Site.site.getModule("HouseholdCalendar" + id);
						if (calendar != null)
							calendar.setDisplayName(n);
						Households.getHousehold(id, r.db).setName(n, r.db);
					}
				})
				.setAccessPolicy(new Or(new HouseholdAccessPolicy(), new RoleAccessPolicy("people").add().delete().edit()))
				.setDefaultOrderBy("lower(name)")
				.setDeleteText("Are you absolutely sure you want to delete this household? If they have left the community then you may want to edit and uncheck the \"active\" checkbox instead. Deleting a household also deletes the link between accounting transactions and the household.")
				.setFilters(new String[] { "Active Households|active", "Inactive Households|NOT active" })
				.setRecordName("Household", true)
				.setColumnNamesForm("name", "contact", "homes_id", "emergency_info", "cm_account_active", "meal_work_exempt", "active")
				.setColumnNamesTable("name", "homes_id", "contact")
				.setColumn(new Column("active").setDefaultValue(true).setViewRole("people"))
				.setColumn(new PersonColumn("contact", false))
				.setColumn(new TextAreaColumn("emergency_info").setIsRichText(true, true))
				.setColumn(new LookupColumn("homes_id", "homes", "number").setAllowNoSelection(true).setDisplayName("home"))
				.addRelationshipDef(new OneToManyLink("people", "first,last").setManyTableColumn("households_id"))
				.addRelationshipDef(new ManyToMany("households homes", "households_homes", "number").setRecordName("Owned Home"));
			for (String k : m_assets.keySet())
				view_def.addRelationshipDef(new OneToMany(k));
			return view_def;
		case "households_contacts": {
			view_def = Site.site.getModule("Contacts")._newViewDef("contacts")
				.setName(name)
				.setColumn(new HouseholdIdColumn())
				.setColumn(new Column("public").setDisplayName("show on public household page"));
			view_def.setColumnNamesForm(Array.concat(view_def.getColumnNamesForm(), "households_id", "public"));
			return view_def;
		}
		case "households_pictures":
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy().add().delete())
				.setRecordName("Picture", true)
//				.setShowColumnHeads(false)
				.setColumnNamesTable("filename")
				.setColumn(new Column("households_id").setDefaultToSessionAttribute().setIsHidden(true))
				.setColumn(new PictureColumn("filename", "households_pictures", "households", 200, 1024).setUseImageMagick(true).setDirColumn("households_id").setMultiple(true));
		case "notes":
			return new ViewDef(name)
				.addSection(new Tabs("category").setPluralize(false))
				.setDefaultOrderBy("lower(title)")
				.setRecordName("Note", true)
				.setColumnNamesTable("title", "note", "public")
				.setColumn(new Column("category").setShowPreviousValuesDropdown(true))
				.setColumn(new HouseholdIdColumn())
				.setColumn(new TextAreaColumn("note").setEncryptionColumn("households_id").setIsRichText(true, false))
				.setColumn(new Column("public").setDisplayName("show on public household page"));
		default:
			break;
		}
		return null;
	}

	//--------------------------------------------------------------------------

	public static void
	writeAccountHistory(mosaic.Person user, int household_id, Request r) {
		HTMLWriter w = r.w;
		w.write("<table style=\"margin: 0px auto;\"><tr><td><div class=\"alert alert-secondary\">");
		if (((Accounting)Site.site.getModule("Accounting")).createHOAInvoices()) {
			double dues = user.getHousehold(r.db).getDues(r.db);
			if (dues != 0)
				w.write("<p>Your HOA dues are ").writeCurrency(dues).write(" per month.</p>");
		}
		double balance = Accounting.calcBalanceHousehold(household_id, null, r.db);
		w.write("<p>Your current account balance is ").writeCurrency(balance).write(". A negative number means you have a credit.</p>");
		if (s_module.m_hoa_text != null)
			w.space().write(s_module.m_hoa_text);
		w.write("</div></td></tr><tr><td>");
		ViewState.setBaseFilter("household transactions", "households_id=" + household_id, r);
		Site.site.newView("household transactions", r).writeComponent();
		w.write("</td></tr></table>");
	}

	//--------------------------------------------------------------------------

	private void
	writeHouseholds(String view_by, Request r) {
		HTMLWriter w = r.w;
//		mosaic.Person user = (mosaic.Person)request.getUser();
//		int user_pic_size = ((People)Site.site.getModule("people")).m_user_pic_size;

		StringBuilder columns = new StringBuilder("households.id,name,number,first,last,people.id pid,picture,length(households.bio)");
		if (m_extra_columns != null)
			for (String column : m_extra_columns)
				columns.append(',').append(column);
		Rows rows = new Rows(new Select(columns.toString())
			.from("households JOIN people ON people.households_id=households.id LEFT JOIN homes ON households.homes_id=homes.id")
			.where("households.active AND people.active")
			.orderBy("home".equals(view_by) ? "naturalsort(number),name,first,last" : "name,first,last"), r.db);
		Card card = null;
		String prev_household_name = null;
		while (rows.next()) {
			String household_name = rows.getString(2);
			if (prev_household_name == null || !prev_household_name.equals(household_name)) {
				if (card != null) {
					w.write("</div>");
					card.close();
				}
				card = w.ui.card().headerOpen();
				int id = rows.getInt(1);
				w.addStyle("float:right;margin-left:10px")
					.ui.buttonOnClick("...", getProfileJS(household_name, id));
				w.write(household_name);
				String home_number = rows.getString(3);
				if (home_number != null)
					w.write(" - " + home_number);
				card.bodyOpen();
				w.write("<div style=\"display:flex;flex-wrap:wrap;gap:1em\">");
			}
			w.write("<div class=\"mb-3\">");
			String people_id = rows.getString(6);
			String picture = rows.getString(7);
			if (picture != null)
				w.addStyle("cursor:pointer;max-height:150px;max-width:100%")
					.setAttribute("onclick", "Img.show(this)")
					.img("people", people_id, picture);
			w.addStyle("align-items:center;display:flex;justify-content:space-between")
				.tagOpen("div");
			w.write(rows.getString(4))
				.nbsp()
				.write(rows.getString(5))
				.addStyle("margin-left:10px")
				.ui.buttonOnClick("...", "Person.popup(" + people_id + ")")
				.tagClose();
			if (m_extra_columns != null)
				for (String column : m_extra_columns) {
					int column_index = rows.findColumn(column);
					if (rows.getColumnType(column_index) == Types.DATE) {
						LocalDate date = rows.getDate(column_index);
						if (date != null)
							w.br().writeDate(date);
					} else {
						String s = rows.getString(column_index);
						if (s != null)
							w.br().write(s);
					}
				}
			w.write("</div>");
			prev_household_name = household_name;
		}
		if (card != null) {
			w.write("</div>");
			card.close();
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	@Override
	public NavBar
	writePageMenu(Request r) {
		mosaic.Person user = (mosaic.Person)r.getUser();
		if (user == null)
			return null;
		Household household = user.getHousehold(r.db);
		if (household == null)
			return null;
		NavBar nav_bar = navBar("main", 1, r).setBrand(household.getName() + " Household", null).open();
		if (m_lists != null && m_lists.length > 0)
			for (String item : m_lists)
				nav_bar.item(Text.capitalize(item));
		Map<String,Block> blocks = getBlocks();
		if (blocks != null) {
			boolean first = true;
			for (Block block : blocks.values())
				if (block.handlesContext("household accounting", r)) {
					if (first) {
						first = false;
						nav_bar.divider();
					}
					nav_bar.item(block.getName(), "blocks/" + block.getName() + "?id=" + household.getId());
				}
		}
		nav_bar.divider()
			.aOnClick("Profile", getProfileJS(household.getName(), household.getId()))
			.close();
		return nav_bar;
	}

	//--------------------------------------------------------------------------

	private void
	writeProfile(int household_id, Request r) {
		Household household = Household.load(household_id, r.db);
		if (household == null)
			return;
		boolean belongs_to = household.belongsTo(r.getUser().getId());
		HTMLWriter w = r.w;
		Blocks b = new Blocks(belongs_to, r.w).lookup(new Select("*").from("households").whereIdEquals(household_id), r.db);
		for (int i=0; i<m_blocks.size(); i++) {
			JsonObject block = m_blocks.get(i).asObject();
			if (b.write(block))
				continue;
			String type = block.getString("type", null);
			Assets a = m_assets.get(type);
			if (a != null) {
				a.writeProfileSection(belongs_to, household_id, r);
				continue;
			}
			switch(type) {
			case "contacts":
				writePublicContacts(household_id, r);
				break;
			case "notes":
				writePublicNotes(household_id, r);
				break;
			case "people":
				int user_pic_size = ((People)Site.site.getModule("people")).m_user_pic_size;
				Rows rows = new Rows(new Select("id,first,last,picture").from("people").where("households_id=" + household_id + " AND active").orderBy("first"), r.db);
				while (rows.next()) {
					String id = rows.getString(1);
					String filename = rows.getString(4);
					if (filename != null)
						w.addStyle("cursor:pointer;max-height:" + user_pic_size + "px;max-width:" + user_pic_size + "px")
							.setAttribute("onclick", "Img.show(this)")
							.img("people", id, filename);
					w.addStyle("float:right")
						.ui.buttonOnClick("...", "Person.popup(" + id + ")")
						.nbsp().write(rows.getString(2)).nbsp().write(rows.getString(3)).hr();
				}
				rows.close();
				break;
			case "person":
				if (!belongs_to)
					continue;
				w.h5("Household Contact")
					.addClass("mb-3")
					.tagOpen("div");
				new RowsSelect(null, new Select("id,first,last").from("people").whereEquals("households_id", household_id), "first,last", "id", r)
					.setOnChange("net.post(context+'/Views/households/update','db_key_value=" + household_id + "&contact='+this.options[this.selectedIndex].value)")
					.setSelectedOption(null, Integer.toString(household.getContact()))
					.write(w);
				w.tagClose();
				break;
			case "pictures":
				if (belongs_to) {
					r.setSessionInt("households_id", household_id);
					ViewState.setBaseFilter("households_pictures", "households_id=" + household_id, r);
					w.h5("Pictures")
						.addClass("mb-3 bg-light px-1 pt-1")
						.tagOpen("div");
					Site.site.getViewDef("households_pictures", r.db).newView(r).setMode(Mode.LIST).writeComponent();
					w.tagClose();
				} else {
					boolean first = true;
					List<String> filenames = r.db.readValues(new Select("filename").from("households_pictures").whereEquals("households_id", household_id));
					for (String filename : filenames) {
						if (filename == null || filename.length() == 0)
							continue;
						if (first) {
							first = false;
							w.h5("Pictures")
								.write("<div onclick=\"new Gallery({dir:'households/").write(household_id).write("'}).open(event.target)\">");
						}
						w.addStyle("cursor:pointer;max-width:100%")
							.img("households", Integer.toString(household_id), "thumbs", filename);
					}
					if (!first)
						w.write("</div>");
				}
				break;
			}
		}
		b.close();
	}

	//--------------------------------------------------------------------------

	private void
	writePublicContacts(int household_id, Request r) {
		Rows rows = new Rows(new Select("first,last,relationship,phone,email,address,notes").from("contacts").where("households_id=" + household_id + " AND public"), r.db);
		if (rows.isBeforeFirst()) {
			r.w.h5("Contacts");
			while (rows.next()) {
				r.w.write("<p><b>");
				r.w.write(rows.getString(1));
				String relationship = rows.getString(2);
				if (relationship != null && relationship.length() > 0) {
					r.w.write(" - ");
					r.w.write(relationship);
				}
				r.w.write("</b><br />");
				String phone = rows.getString(3);
				if (phone != null) {
					r.w.write(phone);
					r.w.write("<br />");
				}
				String email = rows.getString(4);
				if (email != null) {
					r.w.write(email);
					r.w.write("<br />");
				}
				String address = rows.getString(5);
				if (address != null) {
					r.w.write(address);
					r.w.write("<br />");
				}
				String notes = rows.getString(6);
				if (notes != null) {
					r.w.write(notes);
					r.w.write("<br />");
				}
				r.w.write("</p>");
			}
		}
		rows.close();
	}

	//--------------------------------------------------------------------------

	private void
	writePublicNotes(int household_id, Request r) {
		Rows rows = new Rows(new Select("title,note").from("notes").where("households_id=" + household_id + " AND public"), r.db);
		if (rows.isBeforeFirst()) {
			r.w.h5("Notes");
			while (rows.next()) {
				r.w.write("<p><b>");
				r.w.write(rows.getString(1));
				r.w.write("</b><br />");
				r.w.write(Text.scramble(SQL.decodeBinary(rows.getString(2)), household_id));
				r.w.write("</p>");
			}
		}
		rows.close();
	}
}
