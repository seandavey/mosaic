package households;

import app.Request;
import db.access.AccessPolicy;

public class HouseholdAccessPolicy extends AccessPolicy {
	@Override
	public boolean
	canUpdateRow(String from, int id, Request r) {
		mosaic.Person user = (mosaic.Person)r.getUser();
		return user != null && user.getHouseholdId() == id;
	}
}
