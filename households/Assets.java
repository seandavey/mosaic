package households;

import java.sql.Types;
import java.util.ArrayList;

import app.Request;
import app.Site;
import bootstrap5.ButtonNav;
import bootstrap5.ButtonNav.Panel;
import db.DBConnection;
import db.Rows;
import db.Select;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.column.Column;
import db.column.PictureColumn;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import pages.Page;
import ui.Card;
import util.Text;
import web.HTMLWriter;

public class Assets {
	private boolean	m_available_for_sharing;
	private boolean	m_has_license_plate;
	private boolean	m_has_name;
	private boolean	m_has_parking_spot;
	private String	m_type;

	//--------------------------------------------------------------------------

	public
	Assets(String type) {
		m_type = type;
	}

	//--------------------------------------------------------------------------
	
	public final void
	addPages(DBConnection db) {
		Site.pages.add(new Page(Text.capitalize(m_type), false){
			@Override
			public void
			writeContent(Request r) {
				r.w.style(".assets{margin:20px}.assets>div{align-items:flex-start;display:flex;flex-wrap:wrap;margin-top:20px;}.assets>div>div{margin-right:1rem;}");
				ButtonNav bn = new ButtonNav("button_nav", 1, r);
				bn.setPanelDisplay("flex")
					.addClass("assets");
				if (m_has_name) {
					bn.add(new Panel("By Name", null, Households.s_module.apiURL(m_type, "By Name")));
					if (r.getPathSegment(1) == null)
						writeAssets(false, r);
				}
				bn.add(new Panel("By Household", null, Households.s_module.apiURL(m_type, "By Household")));
				if (!m_has_name)
					if (r.getPathSegment(1) == null)
						writeAssets(true, r);
				if (m_has_parking_spot)
					bn.add(new Panel("By Parking Spot", null, Households.s_module.apiURL(m_type, "By Parking Spot")));
				bn.close();
			}
		}, db);
	}

	//--------------------------------------------------------------------------

	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable(m_type)
				.add(new JDBCColumn("households"))
				.add(new JDBCColumn("notes", Types.VARCHAR))
				.add(new JDBCColumn("photo", Types.VARCHAR));
		if (m_available_for_sharing)
			table_def.add(new JDBCColumn("available_for_sharing", Types.BOOLEAN));
		if (m_has_license_plate)
			table_def.add(new JDBCColumn("license_plate", Types.VARCHAR));
		if (m_has_name)
			table_def.add(new JDBCColumn("name", Types.VARCHAR));
		else
			table_def.add(new JDBCColumn("description", Types.VARCHAR));
		if (m_has_parking_spot)
			table_def.add(new JDBCColumn("parking_spot", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	private Card
	cardOpen(Rows rows, boolean by_household, HTMLWriter w) {
		Card card = w.ui.card().addStyle("max-width", "500px").headerOpen();
		if (by_household) {
			w.write(rows.getString("name"));
			String number = rows.getString("number");
			if (number != null)
				w.space().addStyle("vertical-align:baseline").ui.icon("house").space().write(number);
		} else
			w.write(rows.getString(m_has_name ? m_type + "_name" : "parking_spot"));
		card.bodyOpen();
		return card;
	}

	//--------------------------------------------------------------------------

	public void
	doAPIGet(String[] path_segments, Request r) {
		switch(path_segments[1]) {
		case "By Household":
			writeAssets(true, r);
			return;
		case "By Name":
			writeAssets(false, r);
			return;
		case "By Parking Spot":
			writeAssets(false, r);
			return;
		}
	}

	//--------------------------------------------------------------------------
	
	public ViewDef
	newViewDef() {
		ArrayList<String> column_names_form = new ArrayList<>();
		column_names_form.add("households_id");
		if (m_has_name)
			column_names_form.add("name");
		else
			column_names_form.add("description");
		if (m_has_license_plate)
			column_names_form.add("license_plate");
		column_names_form.add("photo");
		if (m_has_parking_spot)
			column_names_form.add("parking_spot");
		if (m_available_for_sharing)
			column_names_form.add("available_for_sharing");
		column_names_form.add("notes");
		ArrayList<String> column_names_table = new ArrayList<>();
		column_names_table.add("photo");
		if (m_has_name)
			column_names_form.add("name");
		else
			column_names_table.add("description");
		if (m_has_license_plate)
			column_names_table.add("license_plate");
		if (m_has_parking_spot)
			column_names_table.add("parking_spot");
		if (m_available_for_sharing)
			column_names_table.add("available_for_sharing");
		column_names_table.add("notes");
		ViewDef view_def = new ViewDef(m_type)
			.setRecordName(Text.capitalize(Text.singularize(m_type)), true)
			.setColumnNamesForm(column_names_form)
			.setColumnNamesTable(column_names_table)
			.setColumn(new HouseholdIdColumn())
			.setColumn(new PictureColumn("photo", m_type, "households", 200, 1024).setUseImageMagick(true).setDirColumn("households_id"));
		if (m_has_name)
			view_def.setColumn(new Column("name").setIsRequired(true));
		else
			view_def.setColumn(new Column("description").setIsRequired(true));
		if (m_has_name)
			view_def.setDefaultOrderBy("lower(name)");
		else if (m_has_parking_spot)
			view_def.setDefaultOrderBy("naturalsort(parking_spot)");
		return view_def;
	}

	//--------------------------------------------------------------------------
	
	public final Assets
	setAvailableForSharing(boolean available_for_sharing) {
		m_available_for_sharing = available_for_sharing;
		return this;
	}

	//--------------------------------------------------------------------------
	
	public final Assets
	setHasLicensePlate(boolean has_license_plate) {
		m_has_license_plate = has_license_plate;
		return this;
	}

	//--------------------------------------------------------------------------
	
	public final Assets
	setHasName(boolean has_name) {
		m_has_name = has_name;
		return this;
	}

	//--------------------------------------------------------------------------
	
	public final Assets
	setHasParkingSpot(boolean has_parking_spot) {
		m_has_parking_spot = has_parking_spot;
		return this;
	}

	//--------------------------------------------------------------------------

	private void
	writeAsset(Card card, Rows rows, boolean by_household, HTMLWriter w) {
		if (!by_household)
			if (m_has_name)
				w.write("Name: ").write(rows.getString(m_type + "_name")).br();
			else
				w.write(rows.getString("description"));
		if (m_has_license_plate) {
			String license_plate = rows.getString("license_plate");
			if (license_plate != null)
				w.write(" - ").write(license_plate);
		}
		w.br();
		if (!by_household) {
			w.write(rows.getString("name"));
			String number = rows.getString("number");
			if (number != null)
				w.space().addStyle("vertical-align:baseline").ui.icon("house").space().write(number);
			w.br();
		}
		if (by_household && m_has_parking_spot) {
			String parking_spot = rows.getString("parking_spot");
			if (parking_spot != null)
				w.write("Parking Spot: ").write(parking_spot).br();
		}
		if (m_available_for_sharing && rows.getBoolean("available_for_sharing"))
			w.write("available for sharing<br/>");
		String photo = rows.getString("photo");
		if (photo != null && photo.length() > 0)
			w.addStyle("max-width:100%")
				.img("households", rows.getString("id"), photo);
		String notes = rows.getString("notes");
		if (notes != null)
			card.text(notes);
	}

	//--------------------------------------------------------------------------

	final void
	writeAssets(boolean by_household, Request r) {
		ArrayList<String> columns = new ArrayList<>();
		if (m_available_for_sharing)
			columns.add("available_for_sharing");
		columns.add("households.id");
		if (m_has_license_plate)
			columns.add("license_plate");
		if (m_has_name)
			columns.add(m_type + ".name " + m_type + "_name");
		else
			columns.add("description");
		columns.add("households.name");
		columns.add("number");
		columns.add("notes");
		if (m_has_parking_spot)
			columns.add("parking_spot");
		columns.add("photo");
		Select query = new Select(Text.join(",", columns))
			.from(m_type)
			.joinOne(m_type, "households")
			.leftJoinOne("households", "homes")
			.where("households.active")
			.orderBy(by_household ? (m_has_name ? "name," + m_type + "_name" : "name,description") : m_has_name ? m_type + "_name" : "naturalsort(parking_spot)");
		if (!by_household && m_has_parking_spot)
			query.andWhere("parking_spot IS NOT NULL");
		try (Rows rows = new Rows(query, r.db)) {
			if (!rows.isBeforeFirst()) {
				r.w.write("No " + m_type + " have been added. To add a " + Text.singularize(m_type) + ": go to your household page then click on \"Profile\".");
				return;
			}
			Card card = null;
			boolean first = true;
			String previous = null;
			while (rows.next()) {
				String v = rows.getString(by_household ? "name" : m_has_name ? m_type + "_name" : "parking_spot");
				if (!v.equals(previous)) {
					if (card != null)
						card.close();
					card = cardOpen(rows, by_household, r.w);
					previous = v;
					first = true;
				}
				if (first)
					first = false;
				else
					r.w.hr();
				writeAsset(card, rows, by_household, r.w);
			}
			if (card != null)
				card.close();
		}
	}

	//--------------------------------------------------------------------------

	final void
	writeProfileSection(boolean belongs_to, int household_id, Request r) {
		HTMLWriter w = r.w;
		if (belongs_to) {
			r.setSessionInt("households_id", household_id);
			ViewState.setBaseFilter(m_type, "households_id=" + household_id, r);
			w.h5(Text.capitalize(m_type))
				.addClass("mb-3 bg-light px-1 pt-1")
				.tagOpen("div");
			Site.site.getViewDef(m_type, r.db).newView(r).setMode(Mode.LIST).writeComponent();
			w.tagClose();
		} else {
			ArrayList<String> columns = new ArrayList<>();
			if (m_has_license_plate)
				columns.add("license_plate");
			if (m_has_name)
				columns.add("name");
			else
				columns.add("description");
			columns.add("photo");
			columns.add("notes");
			if (m_available_for_sharing)
				columns.add("available_for_sharing");
			try (Rows rows = new Rows(new Select(Text.join(",", columns)).from(m_type).whereEquals("households_id", household_id), r.db)) {
				if (!rows.isBeforeFirst())
					return;
				w.h5(Text.capitalize(m_type))
					.write("<div onclick=\"new Gallery({dir:'households/").write(household_id).write("'}).open(event.target)\">");
				while (rows.next()) {
					String caption = "";
					if (m_has_name) {
						String name = rows.getString("name");
						if (name != null)
							caption = name;
					} else {
						String description = rows.getString("description");
						if (description != null)
							caption = description;
					}
					if (m_has_license_plate) {
						String license_plate = rows.getString("license_plate");
						if (license_plate != null)
							caption += " - " + license_plate;
					}
					String notes = rows.getString("notes");
					if (notes != null)
						caption += "<br/>" + notes;
					if (m_available_for_sharing) {
						boolean available_for_sharing = rows.getBoolean("available_for_sharing");
						if (available_for_sharing)
							caption += "<br/>available for sharing";
					}
					String photo = rows.getString("photo");
					w.addStyle("max-width:100px")
						.ui.figure(photo == null ? null : "households/" + household_id + "/" + photo, caption, "max-height:100px;max-width:100px");
				}
				w.write("</div>");
			}
		}
	}
}
