import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Collectors;

public class LoadMail {
	private static final String s_tika_dir = "/home/mosaic/vendor";

	//--------------------------------------------------------------------------

	static void
	convertNewlines(Path path) {
		try {
			new ProcessBuilder("sed", "-i", "s/\\\\n/\\n/g", path.toString()).start().waitFor();
			new ProcessBuilder("sed", "-i", "s/\\\\r//g", path.toString()).start().waitFor();
			new ProcessBuilder("sed", "-i", "s/\\\\t/\\t/g", path.toString()).start().waitFor();
		} catch (IOException | InterruptedException e) {
			System.out.println("convertNewlines " + e.toString());
		}
	}

	//--------------------------------------------------------------------------

	static String
	escape(String s) {
		if (s == null)
			return null;

		StringBuilder sb = new StringBuilder(s);
		for (int i=0; i<sb.length(); i++) {
			char c = sb.charAt(i);
			if (c == '\'') {
				sb.insert(i, '\'');
				i++;
			}
		}
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	static void
	handleMessage(Path path, String list, String message_id, String subject, Connection db) {
		if (!path.toFile().exists())
			return;
		System.out.println(message_id);
		String text = loadTokens(path);
		if (text.indexOf("\\n") != -1) {
			convertNewlines(path);
			text = loadTokens(path);
		}
		if (text.indexOf("<div") != -1) {
			text = stripHTML(text);
			if (text == null)
				return;
		}
		text = removeAttachments(text);
		try {
			Statement st = db.createStatement();
			st.execute("UPDATE " + list + " SET tsvector=to_tsvector('" + escape(subject + "\n" + text) + "') WHERE id=" + message_id);
			st.close();
		} catch (SQLException e) {
			System.out.println("problem with " + message_id);
			System.out.println(e.toString());
		}
	}

	//--------------------------------------------------------------------------

	static void
	loadList(String mail_dir, String list_id, String start_id, Connection db) {
		String list = "ml_" + list_id;
		int message_id = 0;
		try {
			ResultSet rs = db.createStatement().executeQuery("SELECT id,subject FROM " + list + (start_id == null ? " WHERE tsvector IS NULL" : " WHERE id>=" + start_id) + " ORDER BY id");
			while (rs.next()) {
				Path path = Paths.get(mail_dir, list_id, rs.getString(1));
				handleMessage(path, list, rs.getString(1), rs.getString(2), db);
			}
			rs.getStatement().close();
		} catch (SQLException e) {
			System.out.println("loadList " + list_id + " " + message_id + " " + e.toString());
		}
	}

	//--------------------------------------------------------------------------

	static String
	loadTokens(Path path) {
		String jar_path = Paths.get(s_tika_dir, "tika-app-1.21.jar").toString();
		try {
			Process p = new ProcessBuilder("java", "-jar", jar_path, "-t", path.toString()).start();
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String text = br.lines().collect(Collectors.joining("\n"));
			return text;
		} catch (IOException e) {
			System.out.println("loadTokens " + e.toString());
			return null;
		}
	}

	//--------------------------------------------------------------------------

	public static void
	main(String[] args) {
		try {
			Connection db = DriverManager.getConnection("jdbc:postgresql:" + args[0] + "?user=mosaic&password=" + args[3]);
			String mail_dir = "/home/mosaic/tomcat/" + args[0] + "/ROOT/mail";
			if (args.length > 1)
				loadList(mail_dir, args[1], args.length > 2 ? args[2] : null, db);
			else {
				ResultSet rs = db.createStatement().executeQuery("SELECT id FROM mail_lists ORDER BY id");
				while (rs.next()) {
					String id = rs.getString(1);
					System.out.println("loading list " + id);
					loadList(mail_dir, id, null, db);
				}
				rs.getStatement().close();
			}
		} catch (Exception e) {
			System.out.println("main " + e.toString());
		}
	}

	//--------------------------------------------------------------------------

	static String
	removeAttachments(String text) {
		StringBuilder sb = new StringBuilder();
		String boundary = null;
		boolean keep = true;
		String[] lines = text.split("\n");
		for (int i=0; i<lines.length; i++)
			if (boundary != null && lines[i].startsWith(boundary) && lines[i].endsWith("--")) {
				System.out.println("end of " + boundary);
				keep = true;
			} else if (lines[i].startsWith("--") && i < lines.length - 1) {
				if (lines[i+1].startsWith("Content-Type: image") || lines[i+1].startsWith("Content-Transfer-Encoding: base64")) {
					System.out.println(lines[i+1]);
					boundary = lines[i];
					keep = false;
				}
			} else if (keep)
				sb.append(lines[i]).append("\n");
		return sb.toString();
	}

	//--------------------------------------------------------------------------

	static String
	stripHTML(String text) {
		String jar_path = Paths.get(s_tika_dir, "tika-app-1.21.jar").toString();
		try {
			String html_file = "/tmp/t.html";
			Files.write(Paths.get(html_file), text.getBytes());
			Process p = new ProcessBuilder("java", "-jar", jar_path, "-t", html_file).start();
			BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
			text = br.lines().collect(Collectors.joining("\n"));
			new File(html_file).delete();
			return text;
		} catch (IOException e) {
			System.out.println("stripHTML " + e.toString());
		}
		return null;
	}
}
