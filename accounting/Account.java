package accounting;

import db.DBConnection;
import db.Rows;
import db.Select;
import web.HTMLWriter;
import web.JS;

class Account {
	private final double	m_budget;
	private final int		m_id;
	private final String	m_name;
	private final double[]	m_sums = new double[12];
	private final int		m_type;

	//--------------------------------------------------------------------------

	Account(String name, int year, DBConnection db) {
		m_name = name;
		Rows rows = new Rows(new Select("accounts.id,type,amount").from("accounts LEFT JOIN accounts_budgets ON accounts_budgets.accounts_id=accounts.id AND year=" + year + " WHERE name='" + name + "'"), db);
		rows.next();
		m_id = rows.getInt(1);
		m_type = rows.getInt(2);
		m_budget = rows.getDouble(3);
		rows.close();
	}

	//--------------------------------------------------------------------------

	final void
	add(double amount, int period) {
		m_sums[period] += amount;
	}

	//--------------------------------------------------------------------------

	public final double
	getBudget() {
		return m_budget;
	}

	//--------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public final double[]
	getSums() {
		return m_sums;
	}

	//--------------------------------------------------------------------------

	public final int
	getType() {
		return m_type;
	}

	//--------------------------------------------------------------------------

	final void
	writeRow(int period, int year, boolean show_periods, boolean support_budgets, HTMLWriter w) {
		w.write("<tr><td class=\"qr_account\">");
		w.aOnClick(m_name, "new Dialog({title:" + JS.string(m_name) + ",url:context+'/api/Accounting/IncomeStatement?account=" + m_id + "&year=" + year + "'}).open()");
		if (support_budgets) {
			w.write("</td><td style=\"text-align:right\">");
			if (m_budget > 0)
				w.writeCurrency(m_budget);
			else
				w.nbsp();
		}
		double total = 0;
		for (int q=0; q<=period; q++) {
			if (show_periods) {
				w.write("</td><td style=\"text-align:right\">");
				if (m_sums[q] == 0)
					w.nbsp();
				else
					w.writeCurrency(m_sums[q]);
			}
			total += m_sums[q];
		}
		if (period > 0 || !show_periods) {
			w.write("</td><td style=\"text-align:right\">");
			w.writeCurrency(total);
		}
		if (support_budgets) {
			w.write("</td><td style=\"text-align:right\">");
			if (m_budget > 0)
				w.writeCurrency(m_budget - total);
			else
				w.nbsp();
			w.write("</td><td style=\"text-align:right\">");
			if (m_budget > 0)
				w.writePercent(1.0 - total / m_budget);
			else
				w.nbsp();
		}
		w.write("</td></tr>");
	}
}
