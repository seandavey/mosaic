package accounting;

import java.util.Map;

import app.Request;
import db.View;
import db.column.CurrencyColumn;

class CreditDebitColumn extends CurrencyColumn {
	public
	CreditDebitColumn(String name) {
		super(name);
		m_text_align = "right";
		setTotal(true);
	}

	// --------------------------------------------------------------------------

	@Override
	public double
	getDouble(View v, Request r) {
		if (!v.data.getBoolean("reconciled"))
			return 0;
		return v.data.getDouble(m_name);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	writeValue(View v, Map<String,Object> data, Request r) {
		double d = v.data.getDouble(m_name);
		if (v.data.wasNull())
			return false;
		r.w.writeCurrency(d);
		return true;
	}
}
