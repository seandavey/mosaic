package accounting;

import java.time.LocalDate;

import db.DBConnection;
import db.SQL;
import db.Select;
import ui.Table;
import web.HTMLWriter;
import web.JS;

class OngoingAccount {
	private final double[]	m_expense = new double[12];
	private final String	m_ids;
	private final double[]	m_income = new double[12];
	private final String	m_name;
	private double			m_starting_balance;
	private final int		m_year;

	//--------------------------------------------------------------------------

	OngoingAccount(String name, int year, DBConnection db) {
		m_name = name;
		m_year = year;
		int income_id = db.lookupInt("id", "accounts", "name='" + SQL.escape(name) + " Income'", 0);
		int expense_id = db.lookupInt("id", "accounts", "name='" + SQL.escape(name) + " Expense'", 0);
		m_ids = income_id + "," + expense_id;
		m_starting_balance = db.lookupDouble(new Select("initial_balance").from("accounts").whereIdEquals(income_id), 0) + db.lookupDouble(new Select("initial_balance").from("accounts").whereIdEquals(expense_id), 0);
	}

	//--------------------------------------------------------------------------

	final void
	addExpense(LocalDate date, LocalDate start_date, int period, double amount) {
		if (date.isBefore(start_date))
			m_starting_balance -= amount;
		else
			m_expense[period] += amount;
	}

	//--------------------------------------------------------------------------

	final void
	addIncome(LocalDate date, LocalDate start_date, int period, double amount) {
		if (date.isBefore(start_date))
			m_starting_balance += amount;
		else
			m_income[period] += amount;
	}

	//--------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	public final double
	totalExpense() {
		float total = 0;
		for (double value : m_expense)
			total += value;
		return total;
	}

	//--------------------------------------------------------------------------

	public final double
	totalIncome() {
		float total = 0;
		for (double value : m_income)
			total += value;
		return total;
	}

	//--------------------------------------------------------------------------

	public final double
	writeRow(int period, Table table, HTMLWriter w) {
		table.tr();
		w.setAttribute("rowspan", "2");
		table.td();
		w.aOnClick(m_name, "new Dialog({title:" + JS.string(m_name) + ",url:context+'/api/Accounting/IncomeStatement?account=" + m_ids + "&year=" + m_year + "'})")
			.addStyle("text-align:right")
			.setAttribute("rowspan", "2");
		table.td();
		w.writeCurrency(m_starting_balance);
		table.td("income");
		for (int q=0; q<=period; q++) {
			w.addStyle("text-align:right");
			table.td();
			w.writeCurrency(m_income[q]);
		}
		double total_income = totalIncome();
		double total_expense = totalExpense();
		if (period > 0) {
			w.addStyle("text-align:right");
			table.td();
			w.writeCurrency(total_income);
		}
		w.addStyle("text-align:right")
			.setAttribute("rowspan", "2");
		table.td();
		w.writeCurrency(total_income - total_expense)
			.addStyle("text-align:right")
			.setAttribute("rowspan", "2");
		table.td();
		w.writeCurrency(m_starting_balance + total_income - total_expense);
		table.tr().td("expense");
		for (int q=0; q<=period; q++) {
			w.addStyle("text-align:right");
			table.td();
			w.writeCurrency(m_expense[q]);
		}
		if (period > 0) {
			w.addStyle("text-align:right");
			table.td();
			w.writeCurrency(total_expense);
		}
		return m_starting_balance + total_income - total_expense;
	}
}
