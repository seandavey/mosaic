package accounting;

import app.Request;
import app.Site;
import db.RowsSelect;
import db.Select;
import db.View;
import db.ViewDef;
import ui.Form;
import ui.FormBase.Location;
import util.Time;

public class TransferView extends View {
	public
	TransferView(ViewDef view_def, Request r) {
		super(view_def, r);
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	write() {
		if (m_mode == Mode.ADD_FORM && "bank".equals(m_r.getParameter("kind"))) {
			Form f = new Form(null, Site.context + "/Accounting/transfer", m_w).setButtonsLocation(Location.NONE).open();
			f.rowOpen("date");
			m_w.setAttribute("required", "required").dateInput("date", Time.newDate());
			f.rowOpen("from");
			new RowsSelect("from", new Select("id,name").from("bank_accounts").orderBy("name"), "name", "id", m_r).write(m_w);
			f.rowOpen("to");
			new RowsSelect("to", new Select("id,name").from("bank_accounts").orderBy("name"), "name", "id", m_r).write(m_w);
			f.rowOpen("amount");
			m_w.setAttribute("required", "required").numberInput("amount", null, null, "0.01", null, false);
			f.rowOpen("description");
			m_w.textInput("description", 0, null);
			f.close();
			return;
		}
		super.write();
	}
}
