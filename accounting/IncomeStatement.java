package accounting;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

import app.Request;
import db.DBConnection;
import db.Rows;
import db.Select;
import ui.Table;
import util.Text;
import util.Time;
import web.HTMLWriter;

class IncomeStatement extends AccountReport {
	private final boolean						m_support_budgets;
	private final boolean						m_ytd;

	//--------------------------------------------------------------------------

	IncomeStatement(String report_title, boolean support_budgets, boolean ytd, Request r) {
		super(report_title, r);
		m_support_budgets = support_budgets;
		m_ytd = ytd;
	}

	//--------------------------------------------------------------------------
	// add accounts that don't have any transactions for the report but do have budgets

	private void
	readAccounts(DBConnection db) {
		if (!m_support_budgets)
			return;
		try (Rows rows = new Rows(new Select("name,type").from("accounts JOIN accounts_budgets ON accounts_budgets.accounts_id=accounts.id").where("year=" + m_year), db)) {
			while (rows.next())
				getAccount(rows.getString(1), rows.getInt(2), db);
		}
	}

	//--------------------------------------------------------------------------

	private int
	readTransactions() {
		LocalDate now = Time.newDate();
		String end_date = m_ytd || m_year != now.getYear() ? "<='12/31/" + m_year + "'" : "<'" + now.getMonthValue() + "/1/" + m_year + "'";
		int period = 0;
		try (Rows rows = new Rows(new Select("a.name,a.type,t.amount,t.date,t.type")
				.from("transactions t JOIN accounts a ON t.accounts_id=a.id")
				.where("a.type<4 AND date>='" + m_year + "-01-01' AND date" + end_date)
				.orderBy("date"), m_r.db)) {
			while (rows.next()) {
				while (period < m_num_periods - 1 && rows.getDate(4).compareTo(m_periods[period]) > 0)
					++period;
				Account a = getAccount(rows.getString(1), rows.getInt(2), m_r.db);
				if (a != null) {
					double amount = rows.getDouble(3);
					a.add("Payment".equals(rows.getString(5)) ? -amount : amount, period);
				}
			}
		}
		return period;
	}

	//--------------------------------------------------------------------------

	private double[]
	total(int type) {
		double[] totals = new double[m_num_periods];
		for (Account a : m_accounts)
			if (a.getType() == type) {
				double[] sums = a.getSums();
				for (int i=0; i<m_num_periods; i++)
					totals[i] += sums[i];
			}

		return totals;
	}

	//--------------------------------------------------------------------------

	private void
	writeIncomeExpenseTable(int period, LocalDate today, HTMLWriter w) {
		w.addClass("table table-condensed table-bordered")
			.addStyle("white-space:nowrap")
			.tagOpen("table");
		w.write("<thead><tr><th>Account</th>");
		if (m_support_budgets)
			w.write("<th>Budget</th>");
		for (int p=0; p<=period; p++) {
			w.write("<th>");
			if (m_period_size == 3) {
				w.write('Q');
				w.write(p + 1);
			} else
				w.write(Time.formatter.getMonthNameShort(p + 1));
			w.write("</th>");
		}
		if (period > 0)
			if (today.getYear() == m_year && m_ytd)
				w.write("<th>YTD</th>");
			else
				w.write("<th>Total</th>");
		if (m_support_budgets)
			w.write("<th>Remaining</th><th>Percent</th>");
		w.write("</tr></thead>")
			.write("<tr><td colspan=\"")
			.write(period > 0 ? period + m_num_periods + 2 : m_num_periods + 1)
			.write("\"><b>Income</b></td></tr>");
		for (Account a : m_accounts)
			if (a.getType() == 1)
				a.writeRow(period, m_year, true, m_support_budgets, w);
		/*total_income =*/ writeTotals(1, period, true, w);

		w.write("<tr><td colspan=\"")
			.write(period > 0 ? period + m_num_periods + 2 : m_num_periods + 1)
			.write("\"><b>Expense</b></td></tr>");
		for (Account a : m_accounts)
			if (a.getType() == 2)
				a.writeRow(period, m_year, true, m_support_budgets, w);
		/*total_expense =*/ writeTotals(2, period, true, w);

		w.write("<tr><td ");
		if (m_support_budgets)
			w.write("colspan=\"2\"");
		w.write("><b>Net Income</b>");
		double[] sum_income = total(1);
		double[] sum_expense = total(2);
		for (int p=0; p<=period; p++)
			w.write("</td><td style=\"text-align:right\">")
				.writeCurrency(sum_income[p] - sum_expense[p]);
		if (period > 0)
			w.write("</td><td style=\"text-align:right\">")
				.writeCurrency(total(sum_income) - total(sum_expense));
		w.write("</td>");
		if (m_support_budgets)
			w.write("<td>&nbsp;</td><td>&nbsp;</td>");
		w.write("</tr>");
		for (Account a : m_accounts)
			if (a.getType() == 0)
				for (double amount : a.getSums())
					if (amount != 0) {
						a.writeRow(period, m_year, true, m_support_budgets, w);
						break;
					}
		w.tagClose();
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeReport() {
		LocalDate today = Time.newDate();
		HTMLWriter w = m_r.w;

		today.getYear();

		int period = readTransactions();
		readAccounts(m_r.db);
		m_accounts.sort(new Comparator<Account>() {
			@Override
			public int compare(Account a1, Account a2) {
				return a1.getName().compareToIgnoreCase(a2.getName());
			}
		});

		if (m_accounts.size() > 0)
			writeIncomeExpenseTable(period, today, w);
		w.br().br();
	}

	//--------------------------------------------------------------------------

	private double
	writeTotals(int type, int period, boolean show_periods, HTMLWriter w) {
		double[] sums = total(type);
		double sums_sum = total(sums);
		double total_budget = 0;
		w.write("<tr><td>Total");
		if (m_support_budgets) {
			for (Account a : m_accounts)
				if (a.getType() == type && a.getBudget() > 0)
					total_budget += a.getBudget();
			w.write("</td><td style=\"text-align:right\">");
			w.writeCurrency(total_budget);
		}
		if (show_periods)
			for (int p=0; p<=period; p++) {
				w.write("</td><td style=\"text-align:right\">");
				w.writeCurrency(sums[p]);
			}
		if (period > 0 || !show_periods) {
			w.write("</td><td style=\"text-align:right\">");
			w.writeCurrency(sums_sum);
		}
		if (m_support_budgets) {
			w.write("</td><td style=\"text-align:right\">");
			w.writeCurrency(total_budget - sums_sum);
			w.write("</td><td style=\"text-align:right\">");
			if (total_budget > 0)
				w.writePercent(1.0 - sums_sum / total_budget);
		}
		w.write("</td></tr>");
		return sums_sum;
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	writeTransactions() {
		HTMLWriter w = m_r.w;
		String account_id = m_r.getParameter("account");
		if (account_id == null)
			return false;

		String[] ids = account_id.split(",");
		if (ids.length == 1)
			writeTransactions(account_id);
		else {
			Table table = w.ui.table();
			table.tr();
			w.addStyle("vertical-align:top");
			table.td();
			w.h4("Income");
			writeTransactions(ids[0]);
			w.addStyle("vertical-align:top");
			table.td();
			w.h4("Expense");
			writeTransactions(ids[1]);
			table.close();
		}
		return true;
	}

	//--------------------------------------------------------------------------

	private void
	writeTransactions(String account_id) {
		String where = "accounts_id=" + account_id;
		HTMLWriter w = m_r.w;

		List<String> ids = m_r.db.readValues(new Select("id").from("accounts").where("ongoing_account=" + account_id));
		if (ids.size() > 0)
			where = "accounts_id IN (" + account_id + "," + Text.join(",", ids) + ")";
		if (!"all".equals(m_r.getParameter("years")))
			where += " AND date>='1/1/" + m_year + "' AND date<='12/31/" + m_year + "'";
		w.write("<table border=\"1\" class=\"table table-condensed table-bordered\"><thead><tr><th>date</th><th>HOA&nbsp;account</th><th>type</th><th>description</th><th>amount</th><th>total</th></tr></thead>");
		double total = 0;
		Rows rows = new Rows(new Select("date,households_id,type,description,amount").from("transactions").where(where).orderBy("date"), m_r.db);
		while (rows.next()) {
			w.write("<tr><td>");
			LocalDate date = rows.getDate(1);
			if (date != null)
				w.writeDate(date);
			w.write("</td><td>");
			String household_id = rows.getString(2);
			if (household_id != null)
				w.write(m_r.db.lookupString(new Select("name").from("households").whereIdEquals(household_id)));
			w.write("</td><td>");
			String type = rows.getString(3);
			w.write(type);
			w.write("</td><td>");
			w.write(rows.getString(4));
			w.write("</td><td style=\"text-align:right\">");
			double amount = rows.getDouble(5);
			w.writeCurrency(amount);
			w.write("</td><td>");
			total += "Payment".equals(type) ? -amount : amount;
			w.writeCurrency(total);
			w.write("</td></tr>");
		}
		rows.close();
		w.write("</table>");
	}
}
