package accounting;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import app.Block;
import app.ClassTask;
import app.Request;
import app.Roles;
import app.Site;
import app.SiteModule;
import db.AnnualReport;
import db.CSVReader;
import db.CSVWriter;
import db.DBConnection;
import db.NameValuePairs;
import db.OneToMany;
import db.OneToManyLink;
import db.Options;
import db.OrderBy;
import db.Result;
import db.Rows;
import db.RowsSelect;
import db.SQL;
import db.Select;
import db.SelectRenderer;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.RoleAccessPolicy;
import db.column.Column;
import db.column.CurrencyColumn;
import db.column.LookupColumn;
import db.column.OptionsColumn;
import db.feature.Feature.Location;
import db.feature.MonthYearFilter;
import db.feature.OptionsFilter;
import db.feature.YearFilter;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import db.section.Dividers;
import jakarta.servlet.http.Part;
import mail.Mail;
import mosaic.MailTemplate;
import pages.Page;
import ui.Card;
import ui.Form;
import ui.FormBase;
import ui.InlineForm;
import ui.NavBar;
import ui.SelectOption;
import ui.Table;
import util.Array;
import util.Text;
import util.Time;
import web.HTMLWriter;
import web.JS;

public class Accounting extends SiteModule {
	private static final String[]	TYPES = new String[] { "Bank Fee", "Check", "Credit", "Deposit", "Interest", "Invoice", "Payment", "Transfer In", "Transfer Out" };

	private Options					m_accounts;
	private OptionsFilter			m_account_filter;
	private final LookupColumn		m_account_column			= new LookupColumn("accounts_id", "accounts", "name").setAllowNoSelection(true).setDisplayName("account");
	private final LookupColumn		m_account_expense_column	= new LookupColumn("accounts_id", "accounts", "name", "(type=2 OR type=3 OR type=5 OR type=6 OR type=9) AND active", "name").setAllowNoSelection(true).setDisplayName("account").setIsRequired(true);
	private final LookupColumn		m_account_income_column		= new LookupColumn("accounts_id", "accounts", "name", "(type=1 OR type=4 OR type=6 OR type=9) AND active", "name").setAllowNoSelection(true).setDisplayName("account").setIsRequired(true);
	private final Column			m_account_type_column		= new Column("type").setInputAndValueRenderers(new SelectRenderer(
																		new String[] { "Income", "Expense", "Capital Improvement", "Ongoing Income", "Ongoing Expense", "Liability", "Water", "Other" },
																		new String[] { "1", "2", "3", "4", "5", "6", "9", "0" }),
																	false);
	@JSONField(admin_only=true)
	private boolean					m_accrual = true;
	@JSONField(label="account",type="income account")
	private int						m_arrears_account_id;
	@JSONField(after="of each month",label="on day")
	private int						m_arrears_fee_date;
	@JSONField
	String 							m_balance_sheet_title		= "Balance Sheet";
	private final LookupColumn		m_bank_account_column		= new LookupColumn("bank_accounts_id", "bank_accounts", "name").setAllowEditing().setAllowNoSelection(true).setDisplayName("bank account");
	@JSONField(admin_only=true,fields={"m_arrears_account_id","m_arrears_fee_date"})
	private boolean					m_charge_arrears_fees;
	@JSONField(label="create invoices for HOA dues",fields={"m_hoa_invoice_date","m_hoa_account_id"})
	private boolean	 				m_create_hoa_invoices;
	private final Column			m_date_column				= new Column("date").setDefaultToDateNow().setIsRequired(true);
	@JSONField
	int								m_default_bank_account;
	@JSONField(type="income account")
	private int						m_default_interest_account;
	private final LookupColumn		m_household_column			= new LookupColumn("households_id", "households", "name", "active", "name").setAllowNoSelection(true).setDisplayName("household");
	@JSONField(label="account",type="income account")
	private int 					m_hoa_account_id;
	@JSONField(after="of each month",label="on day")
	private int 					m_hoa_invoice_date			= 26;
	@JSONField
	String							m_income_statement_title	= "Income Statement";
	@JSONField(after = "whether or not to include current month")
	private boolean					m_income_statement_ytd		= true;
	@JSONField(label = "household accounts label")
	String							m_label						= "Household";
	@JSONField(choices={"accrual","cash"})
	private String					m_method = "accrual";
	private final MonthYearFilter	m_month_year_filter			= new MonthYearFilter("transactions", Location.TOP_LEFT);
	private final CurrencyColumn	m_next_year_column			= new CurrencyColumn("next_year"){
																		@Override
																		public void
																		adjustQuery(Select query, Mode mode) {
																			if (mode == View.Mode.LIST || mode == View.Mode.READ_ONLY_LIST)
																				query.addColumn("(SELECT amount FROM accounts_budgets WHERE accounts_budgets.accounts_id=accounts.id AND year=" + (Time.newDate().getYear() + 1) + ')', m_name);
																		}
																	}.setTotal(true);
	private Column					m_payee_column;
	private final CurrencyColumn	m_remaining_column			= new CurrencyColumn("remaining"){
																		@Override
																		public double
																		getDouble(View v, Request r) {
																			double budget = v.data.getDouble("this_year");
																			if (budget == 0)
																				return 0;
																			return budget - getTotalYTD(v.data.getInt("id"), Time.newDate().getYear(), r.db);
																		}
																		@Override
																		public boolean
																		writeValue(View v, Map<String,Object> data, Request r) {
																			double budget = v.data.getDouble("this_year");
																			if (budget != 0)
																				r.w.writeCurrency(getDouble(v, r));
																			return budget != 0;
																		}
																	}.setTextAlign("right").setIsComputed(true).setTotal(true);
	@JSONField(label="email statements automatically",fields={"m_statement_date"})
	boolean							m_send_statements;
	@JSONField
	boolean							m_show_arrears_report		= true;
	@JSONField(fields= {"m_balance_sheet_title"})
	boolean							m_show_balance_sheet_report = true;
	@JSONField(fields= {"m_income_statement_title"})
	boolean							m_show_income_statement_report = true;
	@JSONField(after="of each month",label="on day")
	private int						m_statement_date			= 27;
	@JSONField(label="from",type="mail list")
	private String					m_statement_from;
	@JSONField(label="Intro")
	private String					m_statement_intro			= "Hello,<br />  this automatically generated email is your monthly household account statement. Your account balance includes one or more of the following: dues, guest room usage, eggs and/or water bills. Payments are due on the first of the month.<br /><br />Please check over your household account history on a regular basis and contact the Finance Committee with any concerns.";
	@JSONField(label="Negative Balance Text")
	private String					m_statement_negative_balance = "Since your balance is negative you have a credit and therefore don't need to pay at this time.";
	@JSONField(label="Positive Balance Text")
	private String					m_statement_positive_balance = "Please pay off your balance as soon as possible.";
	@JSONField(label="Statement Title")
	private String					m_statement_title 			= "Household Statement";
	@JSONField(label="Zero Balance Text")
	private String					m_statement_zero_balance	= "Since your balance is zero, thank you for being current on your account.";
	@JSONField
	boolean							m_support_assessments		= true;
	@JSONField
	boolean							m_support_automatic_transactions = true;
	@JSONField
	boolean							m_support_banking			= true;
	@JSONField
	boolean							m_support_budgets			= true;
	@JSONField(fields= {"m_statement_from","m_send_statements","m_statement_intro","m_statement_negative_balance","m_statement_positive_balance","m_statement_zero_balance"})
	boolean							m_support_email_statements	= true;
	@JSONField
	boolean							m_support_transfers			= true;
	private final CurrencyColumn	m_this_year_column			= new CurrencyColumn("this_year"){
																	@Override
																	public void
																	adjustQuery(Select query, Mode mode) {
																		if (mode == View.Mode.LIST || mode == View.Mode.READ_ONLY_LIST)
																			query.addColumn("(SELECT amount FROM accounts_budgets WHERE accounts_budgets.accounts_id=accounts.id AND year=" + Time.newDate().getYear() + ')', m_name);
																	}
																}.setDisplayName("budget").setTotal(true);
	private final OptionsColumn		m_type_column				= new OptionsColumn("type", TYPES);
	private final YearFilter		m_year_filter				= new YearFilter("transactions", Location.TOP_LEFT);


	// --------------------------------------------------------------------------

	public
	Accounting() {
		m_description = "Accounting system which can optionally interface with the Common Meals module, Guest Room modules and others";
	}

	// --------------------------------------------------------------------------

	@ClassTask({"household id (blank for all)", "date (blank for today)"})
	public void
	addArrearsInvoices(int household_id, LocalDate date, DBConnection db) {
		if (date == null)
			date = Time.newDate();
		Select query = new Select("id").from("households");
		if (household_id != 0)
			query.whereIdEquals(household_id);
		Rows rows = new Rows(query, db);
		while (rows.next()) {
			household_id = rows.getInt(1);
			double balance = calcBalanceArrears(household_id, date, db);
			if (balance > 0) {
				float dues = db.lookupFloat(new Select("sum(hoa_dues)").from("homes JOIN units ON units.id=homes.units_id").where("pays_hoa=" + household_id), 0);
				if (dues > 0 && balance >= dues * 2) {
					double penalty = balance * .005;
					if (!db.lookupBoolean(new Select("hardship").from("households").whereIdEquals(household_id)))
						penalty += 25;
					addInvoice(m_arrears_account_id, household_id, date, "Arrears Penalty", penalty, db);
				}
			}
		}
		rows.close();
	}

	// --------------------------------------------------------------------------

	public static void
	addCheck(int bank_accounts_id, int accounts_id, int payees_id, String num, LocalDate date, String description, double amount, String notes, DBConnection db) {
		if (amount == 0)
			return;
		if (date == null)
			date = Time.newDate();
		NameValuePairs nvp = new NameValuePairs()
			.set("amount", amount)
			.set("accounts_id", accounts_id)
			.set("bank_accounts_id", bank_accounts_id)
			.setDate("date", date)
			.set("description", description)
			.set("notes", notes)
			.set("num", num)
			.set("payees_id", payees_id)
			.set("type", "Check");
		db.insert("transactions", nvp);
	}

	// --------------------------------------------------------------------------

	public static void
	addCredit(int accounts_id, int household_id, LocalDate date, String description, double amount, int transactions_id, String notes, DBConnection db) {
		if (amount == 0)
			return;
		if (date == null)
			date = Time.newDate();
		NameValuePairs nvp = new NameValuePairs()
			.set("amount", amount)
			.set("accounts_id", accounts_id)
			.setDate("date", date)
			.set("description", description)
			.set("households_id", household_id)
			.set("notes", notes)
			.set("type", "Credit");
		if (transactions_id > 0)
			nvp.set("transactions_id", transactions_id);
		db.insert("transactions", nvp);
	}

	//--------------------------------------------------------------------------

	@ClassTask({"date (leave empty for today)"})
	public void
	addHOAInvoices(LocalDate date, DBConnection db) {
		try (Rows rows = new Rows(new Select("pays_hoa,hoa_dues,number").from("homes").joinOn("units", "units.id=homes.units_id").where("pays_hoa IS NOT NULL"), db)) {
			while (rows.next())
				addInvoice(m_hoa_account_id, rows.getInt(1), date, "HOA Dues for " + rows.getString(3), rows.getFloat(2), db);
		}
	}

	// --------------------------------------------------------------------------

	public static void
	addInvoice(int accounts_id, int household_id, LocalDate date, String description, double amount, DBConnection db) {
		addInvoice(accounts_id, household_id, date, description, amount, 0, db);
	}

	// --------------------------------------------------------------------------

	public static int
	addInvoice(int accounts_id, int household_id, LocalDate date, String description, double amount, int transactions_id, DBConnection db) {
		if (amount == 0)
			return 0;
		if (date == null)
			date = Time.newDate();

		NameValuePairs nvp = new NameValuePairs();
		nvp.set("amount", amount);
		nvp.set("accounts_id", accounts_id);
		nvp.setDate("date", date);
		nvp.set("description", description);
		nvp.set("households_id", household_id);
		nvp.set("type", "Invoice");
		if (transactions_id > 0)
			nvp.set("transactions_id", transactions_id);

		Result result = db.insert("transactions", nvp);
		return result.error != null ? 0 : result.id;
	}

	// --------------------------------------------------------------------------

	private void
	addInvoices(Request r) {
		Enumeration<String> parameter_names = r.request.getParameterNames();
		while (parameter_names.hasMoreElements()) {
			String parameter_name = parameter_names.nextElement();
			if (parameter_name.charAt(0) == 'f')
				addInvoice(r.getInt("accounts_id", 0), Integer.parseInt(parameter_name.substring(1)), r.getDate("date"), r.getParameter("description"), r.getDouble("amount", 0), r.db);
		}
		r.response.addMessage("Invoices added");
	}

	// --------------------------------------------------------------------------

	@Override
	protected final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Accounting", this, true), db);
	}

	// --------------------------------------------------------------------------

	@Override
	protected final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("accounts")
			.add(new JDBCColumn("name", Types.VARCHAR))
			.add(new JDBCColumn("active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("type", Types.INTEGER))
			.add(new JDBCColumn("email", Types.VARCHAR))
			.add(new JDBCColumn("initial_balance", Types.DOUBLE));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("accounts_budgets")
			.add(new JDBCColumn("accounts"))
			.add(new JDBCColumn("year", Types.INTEGER))
			.add(new JDBCColumn("amount", Types.DOUBLE));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("bank_accounts")
			.add(new JDBCColumn("name", Types.VARCHAR))
			.add(new JDBCColumn("reconcile", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("starting_balance", Types.DOUBLE).setDefaultValue("0"));
		JDBCTable.adjustTable(table_def, true, false, db);
		table_def = new JDBCTable("transactions")
			.add(new JDBCColumn("accounts_id", Types.INTEGER))
			.add(new JDBCColumn("amount", Types.DOUBLE))
			.add(new JDBCColumn("bank_accounts_id", Types.INTEGER))
			.add(new JDBCColumn("date", Types.DATE))
			.add(new JDBCColumn("description", Types.VARCHAR))
			.add(new JDBCColumn("households_id", "households").setOnDeleteSetNull(true))
			.add(new JDBCColumn("notes", Types.VARCHAR))
			.add(new JDBCColumn("num", Types.VARCHAR))
			.add(new JDBCColumn("payees_id", Types.INTEGER))
			.add(new JDBCColumn("reconciled", Types.BOOLEAN))
			.add(new JDBCColumn("type", Types.VARCHAR))
			.add(new JDBCColumn("transactions_id", Types.INTEGER));
		JDBCTable.adjustTable(table_def, true, false, db);
		db.createIndex("transactions", "bank_accounts_id");
		db.createIndex("transactions", "date");
		db.createIndex("transactions", "households_id");
		db.createIndex("transactions", "transactions_id");
		db.createTable("payees", "text VARCHAR");
		table_def = new JDBCTable("automatic_transactions")
				.add(new JDBCColumn("accounts_id", Types.INTEGER))
				.add(new JDBCColumn("amount", Types.DOUBLE))
				.add(new JDBCColumn("bank_accounts_id", Types.INTEGER))
				.add(new JDBCColumn("description", Types.VARCHAR))
				.add(new JDBCColumn("households_id", "households"))
				.add(new JDBCColumn("generate_day", Types.INTEGER))
				.add(new JDBCColumn("payees_id", Types.INTEGER))
				.add(new JDBCColumn("transaction_day", Types.INTEGER))
				.add(new JDBCColumn("type", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	// --------------------------------------------------------------------------

	private String
	buildStatement(int household_id, LocalDate from, LocalDate due, DBConnection db) {
		MailTemplate content = new MailTemplate();
		content.header(db.lookupString(new Select("name").from("households").whereIdEquals(household_id)) + " " + m_statement_title);

		double balance = calcBalanceHousehold(household_id, null, db);
		NumberFormat currency_format = new DecimalFormat("$0.00;-$0.00");
		if (due == null) {
			due = Time.newDate();
			if (m_create_hoa_invoices && due.getDayOfMonth() >= m_hoa_invoice_date)
				due = due.plusMonths(1).withDayOfMonth(1);
		}
		content.amount(currency_format.format(balance), due);

		if (m_statement_intro != null) {
			content.append(HTMLWriter.breakNewlines(m_statement_intro));
			content.append("<br /><br />");
		}
		LocalDate last_payment = db.lookupDate(new Select("max(date)").from("transactions").where("households_id=" + household_id + " AND type='Payment'"));
		if (last_payment != null)
			content.append(" Your last payment was made on ").append(Time.formatter.getDateShort(last_payment)).append(".");
		content.append("<br /><br />");
		if (balance == 0.0) {
			if (m_statement_zero_balance != null)
				content.append(HTMLWriter.breakNewlines(m_statement_zero_balance));
		} else if (balance < 0.0) {
			if (m_statement_negative_balance != null)
				content.append(HTMLWriter.breakNewlines(m_statement_negative_balance));
		} else if (m_statement_positive_balance != null)
			content.append(HTMLWriter.breakNewlines(m_statement_positive_balance));

		content.append("<br /><br /><h3>Transactions</h3>");
		ArrayDeque<String> transactions = new ArrayDeque<>();
		Rows rows = new Rows(new Select("date,type,description,amount").from("transactions").where("households_id=" + household_id).andWhere("date>='" + from + "'").orderBy("date DESC,id DESC"), db);
		if (!rows.isBeforeFirst()) {
			rows.close();
			rows = new Rows(new Select("date,type,description,amount").from("transactions").where("households_id=" + household_id).orderBy("date DESC,id DESC").limit(20), db);
		}
		content.append("""
			<table class="table">
				<thead>
					<tr><th>Date</th><th>Type</th><th>Description</th><th>Amount</th><th>Balance</th></tr>
				</thead>""");
		while (rows.next()) {
			StringBuilder r = new StringBuilder("<tr><td>");
			r.append(rows.getString("date"));
			r.append("</td><td>");
			String type = rows.getString("type");
			r.append(type);
			r.append("</td><td>");
			String description = rows.getString("description");
			if (description != null)
				r.append(description);
			r.append("</td><td style=\"text-align:right\">");
			double amount = type.equals("Credit") || type.equals("Payment") ? -rows.getDouble("amount") : rows.getDouble("amount");
			r.append(currency_format.format(amount));
			r.append("</td><td style=\"text-align:right\">");
			r.append(currency_format.format(balance));
			balance -= amount;
			r.append("</td></tr>");
			transactions.addFirst(r.toString());
		}
		rows.close();
		for (String t : transactions)
			content.append(t);
		content.append("</table>");
		content.append("<br /><br />Click <a href=\"")
			.append(Site.site.getAbsoluteURL("Households").set("v", m_label).toString())
			.append("\">here</a> to check your account history.");
		return content.toString();
	}

	// --------------------------------------------------------------------------

	public static double
	calcBalanceArrears(int household_id, LocalDate date, DBConnection db) {
		LocalDate ld = date == null ? Time.newDate() : date;
		int month = ld.getMonthValue();
		int year = ld.getYear();
		String where = "households_id=" + household_id;
		if (date != null)
			where += " AND date <='" + date + "'";
		Select query = new Select("type,amount,date").from("transactions").where(where).orderBy("date");
		Rows rows = new Rows(query, db);
		double balance = 0;
		while (rows.next()) {
			String type = rows.getString(1);
			double amount = type.equals("Credit") || type.equals("Payment") ? -rows.getDouble(2) : rows.getDouble(2);
			ld = rows.getDate(3);
			if (ld.getYear() == year && ld.getMonthValue() == month && ld.getDayOfMonth() < 15 && amount > 0)
				continue;
			balance += amount;
		}
		rows.close();
		return Math.round(balance * 100.0) / 100.0;
	}

	// --------------------------------------------------------------------------

	public static double
	calcBalanceBank(int bank_accounts_id, LocalDate date, DBConnection db) {
		String exists = "SELECT 1 FROM transactions WHERE bank_accounts_id=" + bank_accounts_id;
		if (date != null)
			exists += " AND date<='" + date + "'";
		Select query = new Select("(SELECT starting_balance FROM bank_accounts WHERE id=" + bank_accounts_id + ") + CASE WHEN EXISTS(" + exists + ") THEN sum(CASE WHEN type='Deposit' THEN coalesce(amount,0.0)+coalesce((SELECT sum(amount) FROM transactions t2 WHERE transactions.id=t2.transactions_id),0.0) WHEN type='Interest' OR type='Transfer In' THEN amount ELSE -amount END) ELSE 0 END")
			.from("transactions")
			.where("bank_accounts_id=" + bank_accounts_id)
			.andWhere("reconciled");
		if (date != null)
			query.andWhere("date<='" + date + "'");
		return db.lookupDouble(query, 0);
	}

	// --------------------------------------------------------------------------

	public static double
	calcBalanceHousehold(int household_id, LocalDate date, DBConnection db) {
		Select query = new Select("sum(CASE WHEN type='Credit' OR type='Payment' THEN -amount ELSE amount END)")
			.from("transactions")
			.where("households_id=" + household_id);
		if (date != null)
			query.andWhere("date<='" + date + "'");
		return db.lookupDouble(query, 0);
	}

	// --------------------------------------------------------------------------

	public boolean
	createHOAInvoices() {
		return m_create_hoa_invoices;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		HTMLWriter w = r.w;
		switch(path_segments[0]) {
		case "Account List":
			w.h3(path_segments[0]);
			if (m_support_budgets) {
				ViewState.setFilter("budgets", "active OR type=4 OR type=5", r);
				Site.site.newView("budgets", r).writeComponent();
			} else {
				ViewState.setFilter("accounts", "active OR type=4 OR type=5", r);
				Site.site.newView("accounts", r).writeComponent();
			}
			return true;
		case "Add Assessment Invoices":
			w.h3(path_segments[0]);
			writeAddInvoicesForm(r);
			return true;
		case "All Transactions":
			w.h3(path_segments[0]);
			Site.site.newView("transactions", r).writeComponent();
			return true;
		case "Arrears Report":
			w.h3(path_segments[0]);
			writeArrearsReport(r.getDate("date"), r.userHasRole("accounting"), false, r.db, w);
			return true;
		case "Automatic Transactions":
			w.h3(path_segments[0]);
			Site.site.newView("automatic_transactions", r).writeComponent();
			return true;
		case "BalanceSheet":
			BalanceSheet b = new BalanceSheet(m_balance_sheet_title, m_support_budgets, r);
			if (path_segments.length > 1 && "assets".equals(path_segments[1]))
				b.writeAssets();
			else
				b.write();
			return true;
		case "Bank Accounts":
			w.h3(path_segments[0]);
			Site.site.newView("bank_accounts", r).writeComponent();
			return true;
		case "Budget for Next Year":
			w.h3(path_segments[0]);
			ViewState.setFilter("budget report",
				"EXISTS(SELECT 1 FROM accounts_budgets WHERE (type=1 OR type=2) AND accounts_budgets.accounts_id=accounts.id AND amount>0 AND year>=" + Time.newDate().getYear() + ")", r);
			Site.site.newView("budget report", r).writeComponent();
			return true;
		case "Checks":
			w.h3(path_segments[0]);
			Site.site.newView("checks", r).writeComponent();
			return true;
		case "Checks by Account":
			new AnnualReport(new Select("name,accounts_id,date,amount")
					.from("transactions")
					.joinOn("accounts", "accounts_id=accounts.id")
					.where("transactions.type='Check'"),
					"Account", 2, 1, 4)
				.setDetail("date,num,(SELECT text FROM payees WHERE payees.id=transactions.payees_id) AS payee,description,amount", "transactions", "accounts_id")
				.setDetailFilter("type='Check'")
				.setDisplayAsCurrency(true)
				.write(path_segments[0], r);
			return true;
		case "Checks by Payee":
			new AnnualReport(new Select("text,payees_id,date,amount")
					.from("transactions")
					.joinOn("payees", "payees_id=payees.id")
					.where("type='Check'"),
					"Payee", 2, 1, 4)
				.setDetail("date,num,description,(SELECT name FROM accounts WHERE accounts.id=transactions.accounts_id) AS account,amount", "transactions", "payees_id")
				.setDetailFilter("type='Check'")
				.setDisplayAsCurrency(true)
				.write(path_segments[0], r);
			return true;
		case "Credits":
			w.h3(path_segments[0]);
			Site.site.newView("credits", r).writeComponent();
			return true;
		case "Deposits":
			w.h3(path_segments[0]);
			Site.site.newView("deposits", r).writeComponent();
			return true;
		case "Export Transactions":
			writeExportTransactions(r);
			return true;
		case "export transactions":
			exportTransactions(true, r);
			return true;
		case "Fees":
			w.h3(path_segments[0]);
			Site.site.newView("fees", r).writeComponent();
			return true;
		case "Household Accounts":
			int households_id = r.getInt("households_id", -1);
			if (households_id != -1) {
				w.h3(r.db.lookupString(new Select("name").from("households").whereIdEquals(households_id)));
				ViewState.setBaseFilter("household transactions", "households_id=" + households_id, r);
				Site.site.newView("household transactions", r).writeComponent();
			} else
				writeHouseholdAccountsPane(r);
			return true;
		case "household accounts":
			writeHouseholdAccounts("Active Accounts".equals(path_segments[1]), r);
			return true;
		case "Import Accounts":
			w.h3(path_segments[0]);
			Form f = new Form(null, getRelativeURL("import accounts").toString(), w)
				.setSubmitText("Import")
				.open();
			f.rowOpen("Type");
			w.ui.select("type", new String[] {"Income", "Expense"}, null, null, false);
			f.rowOpen("Accounts");
			w.setAttribute("required", "required")
				.textAreaExpanding("accounts", null, null);
			f.close();
			return true;
		case "Import Transactions":
			writeImportTransactions(path_segments[0], r);
			return true;
		case "IncomeStatement":
			new IncomeStatement(m_income_statement_title, m_support_budgets, m_income_statement_ytd, r).write();
			return true;
		case "Invoices":
			w.h3(path_segments[0]);
			Site.site.newView("invoices", r).writeComponent();
			return true;
		case "Interest":
			w.h3(path_segments[0]);
			Site.site.newView("interest", r).writeComponent();
			return true;
		case "Other Settings":
			writeSettingsForm(null, true, true, r);
			return true;
		case "Payments":
			w.h3(path_segments[0]);
			Site.site.newView("payments", r).writeComponent();
			return true;
		case "Reconciliation":
			writeReconciliation(r);
			return true;
		case "SendView Statements":
			w.h3("Send/View Statements");
			writeSendViewStatements(r);
			return true;
		case "Transfers":
			w.h3(path_segments[0]);
			Site.site.newView("transfers", r).writeComponent();
			return true;
		case "View Statements":
			String[] ids = r.getParameter("ids").split(",");
			for (int i=0; i<ids.length; i++) {
				if (i > 0)
					r.w.hr();
				r.w.write(buildStatement(Integer.parseInt(ids[i]), r.getDate("from"), r.getDate("due"), r.db));
			}
			return true;
		case "view transactions":
			exportTransactions(false, r);
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch (segment_one) {
		case "Add Invoices":
			addInvoices(r);
			return true;
		case "duplicate budgets":
			int max = r.db.lookupInt(new Select("MAX(year)").from("accounts_budgets"), 0);
			if (max == 0)
				return true;
			int year = Time.newDate().getYear();
			if (max < year)
				year = max;
			Rows rows = new Rows(new Select("accounts_id,amount").from("accounts").joinOn("accounts_budgets", "accounts.id=accounts_budgets.accounts_id").where("type != 3 AND year=" + year), r.db);
			year++;
			while (rows.next())
				if (!r.db.exists(new Select("1").from("accounts_budgets").where("accounts_id=" + rows.getInt(1) + " AND year=" + year)))
					r.db.insert("accounts_budgets", "accounts_id,year,amount", rows.getInt(1) + "," + year + "," + rows.getString(2));
			rows.close();
			return true;
		case "import accounts":
			importAccounts(r);
			return true;
		case "import transactions":
			importTransactions(r);
			return true;
		case "Send Statements":
			String[] ids = r.getParameter("ids").split(",");
			for (String id : ids)
				sendStatement(Integer.parseInt(id), r.getDate("from"), r.getDate("due"), r.db);
			return true;
		case "transfer":
			transfer(r);
			return true;
		}
 		return false;
	}

	// --------------------------------------------------------------------------

	@ClassTask
	@Override
	public void
	everyNight(LocalDateTime now, DBConnection db) {
		int day_of_month = now.getDayOfMonth();
		if (m_create_hoa_invoices && day_of_month == m_hoa_invoice_date)
			addHOAInvoices(now.toLocalDate().plusMonths(1).withDayOfMonth(1), db);
		if (m_send_statements && day_of_month == m_statement_date)
			sendStatements(now.toLocalDate().withDayOfMonth(1).minusMonths(1), db);
		if (m_charge_arrears_fees && day_of_month == m_arrears_fee_date)
			addArrearsInvoices(0, null, db);
		if (m_support_automatic_transactions)
			handleAutomaticTransactions(now, db);
	}

	// --------------------------------------------------------------------------

	private void
	exportTransactions(boolean csv, Request r) {
		ArrayList<String> columns = new ArrayList<>();
		ArrayList<String> heads = new ArrayList<>();
		for (String column : new String[] { "date", "num", "description", "amount", "household", "account", "type", "bank account", "payee" })
			if (r.getParameter("col_" + column) != null) {
				if ("household".equals(column))
					columns.add("(SELECT name FROM households WHERE households.id=households_id)");
				else if ("account".equals(column))
					columns.add("(SELECT name FROM accounts WHERE accounts.id=accounts_id)");
				else if ("bank account".equals(column))
					columns.add("(SELECT name FROM bank_accounts WHERE bank_accounts.id=bank_accounts_id)");
				else if ("payee".equals(column))
					columns.add("(SELECT text FROM payees WHERE payees.id=payees_id)");
				else
					columns.add(column);
				heads.add(column);
			}

		Select query = new Select(Text.join(",", columns)).from("transactions").orderBy("date");
		LocalDate start = r.getDate("start");
		if (start != null)
			query.andWhere("date>='" + start.toString() + "'");
		LocalDate end = r.getDate("end");
		if (end != null)
			query.andWhere("date<='" + end.toString() + "'");
		int account_id = r.getInt("account", 0);
		if (account_id != 0)
			query.andWhere("accounts_id=" + account_id);
		int household_id = r.getInt("household", 0);
		if (household_id != 0)
			query.andWhere("households_id=" + household_id);
		String type = r.getParameter("type");
		if (type != null && type.length() > 0)
			query.andWhere("type=" + SQL.string(type));

		DecimalFormat df = new DecimalFormat("0.00");
		Rows rows = new Rows(query, r.db);
		if (!rows.isBeforeFirst()) {
			r.w.write("No transactions found");
			rows.close();
			return;
		}
		if (csv) {
			CSVWriter w = new CSVWriter("transactions", r);
			for (String column : heads)
				w.cell(column);
			while (rows.next()) {
				w.row();
				for (int i=0; i<heads.size(); i++)
					if ("amount".equals(heads.get(i)))
						w.cell(df.format(rows.getDouble(i + 1)));
					else
						w.cell(rows.getString(i + 1));
			}
		} else {
			Table table = r.w.ui.table().addDefaultClasses();
			for (String column : heads)
				table.th(column);
			while (rows.next()) {
				table.tr();
				for (int i=0; i<heads.size(); i++)
					if ("amount".equals(heads.get(i)))
						table.td(df.format(rows.getDouble(i + 1)));
					else
						table.td(rows.getString(i + 1));
			}
			table.close();
		}
		rows.close();
	}

	// --------------------------------------------------------------------------

	public String
	getAccountLabel() {
		return m_label;
	}

	// --------------------------------------------------------------------------

	public static double
	getTotalYTD(int accounts_id, int year, DBConnection db) {
		double total = 0;
		try {
			ResultSet rs = db.select("SELECT sum(amount) " + "FROM transactions " + "WHERE accounts_id=" + accounts_id + " AND date>='1/1/" + year + "'");
			if (rs.next())
				total = rs.getDouble(1);
			rs.getStatement().close();
		} catch (SQLException e) {
		}

		return total;
	}

	// --------------------------------------------------------------------------

	private void
	handleAutomaticTransactions(LocalDateTime now, DBConnection db) {
		int day_of_month = now.getDayOfMonth();
		NameValuePairs nvp = new NameValuePairs();
		Rows rows = new Rows(new Select("*").from("automatic_transactions").where("generate_day=" + day_of_month), db);
		while (rows.next()) {
			int accounts_id = rows.getInt("accounts_id");
			if (!rows.wasNull())
				nvp.set("accounts_id", accounts_id);
			else
				nvp.remove("accounts_id");
			nvp.set("amount", rows.getDouble("amount"));
			int bank_accounts_id = rows.getInt("bank_accounts_id");
			if (!rows.wasNull())
				nvp.set("bank_accounts_id", bank_accounts_id);
			else
				nvp.remove("bank_accounts_id");
			LocalDate date = now.toLocalDate();
			int transaction_day = rows.getInt("transaction_day");
			if (transaction_day != 0) {
				date = date.withDayOfMonth(transaction_day);
				if (transaction_day < day_of_month)
					date = date.plusMonths(1);
			}
			nvp.setDate("date", date);
			nvp.set("description", rows.getString("description"));
			if (!rows.wasNull())
				nvp.set("households_id", rows.getInt("households_id"));
			else
				nvp.remove("households_id");
			int payees_id = rows.getInt("payees_id");
			if (!rows.wasNull())
				nvp.set("payees_id", payees_id);
			else
				nvp.remove("payees_id");
			nvp.set("type", rows.getString("type"));
			db.insert("transactions", nvp);
		}
		rows.close();
	}

	// --------------------------------------------------------------------------

	private void
	importAccounts(Request r) {
		String type = r.getParameter("type");
		String[] accounts = r.getParameter("accounts").split("\n");
		NameValuePairs nvp = new NameValuePairs();
		for (String account : accounts)
			if (account.length() > 0) {
				nvp.clear();
				nvp.set("type", "Expense".equals(type) ? 2 : 1);
				nvp.set("name", account);
				r.db.insert("accounts", nvp);
			}
		r.response.addMessage(accounts.length + " " + Text.pluralize("account", accounts.length) + " added");
	}

	// --------------------------------------------------------------------------

	private void
	importTransactions(Request r) {
		int bank_account_id = r.getInt("bank_account", 0);
		String[] columns = r.getParameter("columns").split(",");
		for (int i=0; i<columns.length; i++)
			columns[i] = columns[i].trim();
		int date_column = Array.indexOf(columns, "Date");
		int account_column = Array.indexOf(columns, "Account");
		int num_column = Array.indexOf(columns, "Num");
		int description_column = Array.indexOf(columns, "Description");
		int debit_column = Array.indexOf(columns, "Debit");
		int credit_column = Array.indexOf(columns, "Credit");
		ArrayList<String> accounts = new ArrayList<>();
		ArrayList<Integer> ids = new ArrayList<>();
		Set<String> missing = new TreeSet<>();
		Rows rows = new Rows(new Select("id,name").from("accounts"), r.db);
		while (rows.next()) {
			ids.add(rows.getInt(1));
			accounts.add(rows.getString(2));
		}
		rows.close();
		Part part = r.getPart("file");
		CSVReader csv = null;
		try {
			csv = new CSVReader(new InputStreamReader(part.getInputStream()), 0);
		} catch(IOException e) {
			r.log(e);
			return;
		}
		NameValuePairs nvp = new NameValuePairs();
		StringBuilder notes = new StringBuilder();
		int num_imported = 0;
		List<String> line = csv.readLine();
		line = csv.readLine(); // skip column headers
		while (line != null) {
			nvp.clear();
			if (bank_account_id != 0)
				nvp.set("bank_accounts_id", bank_account_id);
			nvp.set("date", line.get(date_column));
			String code = line.get(account_column);
			int i;
			for (i=0; i<accounts.size(); i++)
				if (accounts.get(i).startsWith(code)) {
					nvp.set("accounts_id", ids.get(i));
					break;
				}
			if (i == accounts.size())
				missing.add(code);
			if (num_column != -1) {
				String num = line.get(num_column);
				if (num.length() > 0)
					nvp.set("num", num);
			}
			nvp.set("description", line.get(description_column));
			String debit = line.get(debit_column);
			if (debit.length() > 0) {
				nvp.set("amount", debit);
				nvp.set("type", "Check");
			} else {
				nvp.set("amount", line.get(credit_column));
				nvp.set("type", "Deposit");
			}
			notes.setLength(0);
			for (i=columns.length; i<line.size(); i++) {
				if (notes.length() > 0)
					notes.append(" ");
				notes.append(line.get(i));
			}
			if (notes.length() > 0)
				nvp.set("notes", notes.toString());
			r.db.insert("transactions", nvp);
			++num_imported;
			line = csv.readLine();
		}
		csv.close();
		r.response.addMessage(num_imported + " " + Text.pluralize("transaction", num_imported) + " imported");
		if (missing.size() > 0)
			r.response.addMessage(Text.pluralize("account", missing.size()) + " not found for " + Text.join(", ", missing));
	}

	// --------------------------------------------------------------------------

	@Override
	public void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		m_accounts = new Options(new Select("id,name AS text").from("accounts").orderBy("name"));
		m_account_filter = new OptionsFilter("accounts_id", m_accounts, "Account", Location.TOP_LEFT);
		Site.site.addObjects(new Options(new Select("*").from("payees").orderBy("lower(text)")).setAllowEditing(true));
		m_payee_column = ((Options)Site.site.getObjects("payees")).newColumn("payees_id").setDisplayName("payee");
		Roles.add("accounting", "have full access to the Accounting page with this role");
		Roles.add("budget", "can edit the values for the next year on the Budget view with this role");
		Roles.add("hoa history", "can view the Household Accounts report with this role");
		m_accrual = "accrual".equals(m_method);
	}

	// --------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		if (name.equals("accounts")) {
			ViewDef view_def = new ViewDef(name)
				.addSection(new Dividers("type", new OrderBy("type")))
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setDefaultOrderBy("name")
				.setRecordName("Account", true)
				.setColumnNamesForm(new String[] { "name", "type", "email", "active" })
				.setColumnNamesTable(new String[] { "name" })
				.setColumn(m_account_type_column);
			if (m_support_budgets)
				view_def.addRelationshipDef(new OneToMany("accounts_budgets"));
			return view_def;
		}
		if (name.equals("accounts_budgets"))
			return new ViewDef(name) {
					@Override
					public String
					validate(int id, Request r) {
						String error = super.validate(id, r);
						if (error != null)
							return error;
						if (id == 0) {
							int accounts_id = r.getInt("accounts_id", 0);
							int year = r.getInt("year", 0);
							if (r.db.exists(new Select("1").from("accounts_budgets").whereEquals("accounts_id", accounts_id).andWhere("year=" + year)))
								return "there is already a budget for that account and year";
						}
						return null;
					}
				}
				.setDefaultOrderBy("year")
				.setRecordName("Budget", true);
		if (name.equals("automatic_transactions"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setRecordName("Automatic Transaction", true)
				.setColumnNamesForm(new String[] { "type", "generate_day", "transaction_day", "payees_id", "description", "amount", "accounts_id", "bank_accounts_id", "households_id" })
				.setColumnNamesTable(new String[] { "generate_day", "transaction_day", "type", "payees_id", "households_id", "description", "accounts_id", "amount", "bank_accounts_id" })
				.setColumn(new CurrencyColumn("amount")).setColumn(m_account_column)
				.setColumn(m_bank_account_column)
				.setColumn(m_date_column)
				.setColumn(m_household_column).setColumn(m_type_column)
				.setColumn(m_payee_column);
		if (name.equals("bank_accounts"))
			return new ViewDef(name)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit().view())
				.setDefaultOrderBy("name")
				.setRecordName("Bank Account", true)
				.setColumnNamesForm(new String[] { "name", "reconcile", "starting_balance" })
				.setColumnNamesTable(new String[] { "name", "reconcile" })
				.setColumn(new Column("name").setIsRequired(true));
		if (name.equals("budget report"))
			return new ViewDef(name)
				.addSection(new Dividers("type", new OrderBy("type")))
				.setAccessPolicy(new AccessPolicy())
				.setDefaultOrderBy("name")
				.setFrom("accounts")
				.setPrintButtonLocation(Location.LIST_HEAD)
				.setRecordName("Account", true)
				.setShowGrandTotals(false)
				.setColumnNamesTable(new String[] { "name", "this_year", "remaining", "next_year" })
				.setColumn(m_this_year_column)
				.setColumn(m_next_year_column)
				.setColumn(m_remaining_column)
				.setColumn(m_account_type_column);
		if (name.equals("budgets"))
			return new ViewDef(name) {
					@Override
					public void
					afterList(View v, Request r) {
						if (((ViewState)r.getOrCreateState(ViewState.class, m_name)).getFilter() != null)
							r.w.ui.buttonOnClick("View all accounts", "net.post(context+'/ViewStates/budgets','filter=',function(){net.replace(_.c(this))}.bind(this))").br().br();
						r.w.ui.buttonOnClick("create next year's budgets", "net.post(context+'/" + apiURL("duplicate budgets") + "',null,function(){net.replace(_.c(this))}.bind(this))");
					}
				}
				.addSection(new Dividers("type", new OrderBy("type")))
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setDefaultOrderBy("name")
				.setFrom("accounts")
				.setRecordName("Account", true)
				.setShowGrandTotals(false)
				.setColumnNamesForm(new String[] { "name", "type", "initial_balance", "email", "active" })
				.setColumnNamesTable(new String[] { "name", "this_year", "remaining", "next_year" })
				.setColumn(m_this_year_column)
				.setColumn(m_next_year_column)
				.setColumn(m_remaining_column)
				.setColumn(m_account_type_column)
				.addRelationshipDef(new OneToMany("accounts_budgets"));
		if (name.equals("checks"))
			return new ViewDef(name)
				.addFeature(m_account_filter)
				.addFeature(m_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Check'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Check", true)
				.setColumnNamesForm(new String[] { "type", "date", "num", "payees_id", "description", "amount", "accounts_id", "bank_accounts_id", "notes" })
				.setColumnNamesTable(new String[] { "date", "num", "payees_id", "description", "accounts_id", "amount", "bank_accounts_id", "notes" })
				.setColumn(m_account_expense_column)
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(m_bank_account_column.setDefaultValue(m_default_bank_account))
				.setColumn(m_date_column)
				.setColumn(m_payee_column)
				.setColumn(new Column("type").setDefaultValue("Check").setIsHidden(true));
		if (name.equals("credits"))
			return new ViewDef(name)
				.addFeature(m_account_filter)
				.addFeature(m_month_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Credit'")
				.setDefaultOrderBy("date,CASE WHEN num~'^\\d+$' THEN num::integer ELSE 0 END")
				.setFrom("transactions")
				.setRecordName("Credit", true)
				.setColumnNamesForm(new String[] { "type", "date", "num", "description", "amount", "accounts_id", "households_id", "notes" })
				.setColumnNamesTable(new String[] { "date", "num", "description", "accounts_id", "households_id", "amount", "notes" })
				.setColumn(m_account_expense_column)
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(new Column("num").setDefaultToSQL(new Select("MAX(CASE WHEN num~'^\\d+$' THEN num::integer ELSE 0 END) + 1").from("transactions").where("type='Credit'")).setIsReadOnly(true))
				.setColumn(new Column("type").setDefaultValue("Credit").setIsHidden(true));
		if (name.equals("deposits"))
			return new ViewDef(name) {
					@Override
					public void
					afterDelete(String where, Request r) {
						r.db.update("transactions", "transactions_id=NULL", "transactions_id=" + where.substring(where.indexOf('=') + 1));
					}
				}
				.addFeature(m_month_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Deposit'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Deposit", true)
				.setColumnNamesForm(new String[] { "type", "date", "num", "description", "amount", "total", "bank_accounts_id", "notes" })
				.setColumnNamesTable(new String[] { "date", "num", "description", "amount", "total", "bank_accounts_id" })
				.setColumn(new CurrencyColumn("amount").setDisplayName("cash"))
				.setColumn(m_bank_account_column.setDefaultValue(m_default_bank_account))
				.setColumn(m_date_column)
				.setColumn(new CurrencyColumn("total"){
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						r.w.writeCurrency(v.data.getDouble("amount") + r.db.lookupDouble(new Select("sum(amount)").from("transactions").whereEquals("transactions_id", v.data.getInt("id")), 0));
						return true;
					}
				}.setIsComputed(true))
				.setColumn(new Column("type").setDefaultValue("Deposit").setIsHidden(true))
				.addRelationshipDef(new OneToManyLink("payments for deposit", "num").setMode(LookupColumn.Mode.TABLE));
		if (name.equals("fees"))
			return new ViewDef(name)
				.addFeature(m_account_filter)
				.addFeature(m_month_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Bank Fee'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Bank Fee", true)
				.setColumnNamesForm(new String[] { "type", "date", "amount", "accounts_id", "bank_accounts_id", "notes" })
				.setColumnNamesTable(new String[] { "date", "amount", "accounts_id", "bank_accounts_id" })
				.setColumn(m_account_expense_column)
				.setColumn(new CurrencyColumn("amount").setTotal(true))
				.setColumn(m_bank_account_column.setDefaultValue(m_default_bank_account))
				.setColumn(m_date_column)
				.setColumn(new Column("type").setDefaultValue("Bank Fee").setIsHidden(true));
		if (name.equals("household transactions"))
			return new ViewDef(name)
				.addFeature(m_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setAllowSorting(false)
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Transaction", true)
				.setColumnNamesForm(new String[] { "type", "date", "num", "description", "accounts_id", "amount", "households_id", "notes" })
				.setColumnNamesTable(new String[] { "date", "type", "description", "accounts_id", "credit", "debit", "balance" })
				.setColumn(m_account_column)
				.setColumn(new CurrencyColumn("credit") {
					@Override
					public double
					getDouble(View v, Request r) {
						String type = v.data.getString("type");
						return type.equals("Credit") || type.equals("Payment") ? v.data.getDouble("amount") : 0;
					}
					@Override
					public boolean
					writeValue(View v, Map<String, Object> data, Request r) {
						String type = v.data.getString("type");
						if (type.equals("Credit") || type.equals("Payment")) {
							double dbl = getDouble(v, r);
							if (!v.data.wasNull())
								r.w.writeCurrency(dbl);
							return true;
						}
						return false;
					}
				}.setTotal(true))
				.setColumn(new CurrencyColumn("debit") {
					@Override
					public double
					getDouble(View v, Request r) {
						String type = v.data.getString("type");
						return type.equals("Credit") || type.equals("Payment") ? 0 : v.data.getDouble("amount");
					}
					@Override
					public boolean
					writeValue(View v, Map<String, Object> data, Request r) {
						String type = v.data.getString("type");
						if (type.equals("Credit") || type.equals("Payment"))
							return false;
						double dbl = getDouble(v, r);
						if (!v.data.wasNull())
							r.w.writeCurrency(dbl);
						return true;
					}
				}.setTotal(true))
				.setColumn(new Column("balance") {
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						MutableDouble balance = (MutableDouble)data.get("balance");
						if (balance == null) {
							balance = new MutableDouble(calcBalanceHousehold(v.data.getInt("households_id"), v.data.getDate("date").minusDays(1), r.db));
							data.put("balance", balance);
						}
						String type = v.data.getString("type");
						double amount = type.equals("Credit") || type.equals("Payment") ? -v.data.getDouble("amount") : v.data.getDouble("amount");
						balance.add(amount);
						r.w.writeCurrency(balance.value());
						return true;
					}
				}.setTextAlign("right"))
				.setColumn(m_date_column)
				.setColumn(new LookupColumn("households_id", "households", "name", "active", "name").setDefaultToBaseFilter().setDisplayName("household"))
				.setColumn(m_type_column);
		if (name.equals("interest"))
			return new ViewDef(name)
				.addFeature(m_month_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Interest'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Interest", true)
				.setColumnNamesForm(new String[] { "type", "date", "amount", "accounts_id", "bank_accounts_id", "notes" })
				.setColumnNamesTable(new String[] { "date", "amount", "accounts_id", "bank_accounts_id" })
				.setColumn(new LookupColumn("accounts_id", "accounts", "name").setAllowNoSelection(true).setDefaultValue(m_default_interest_account).setDisplayName("account").setIsRequired(true))
				.setColumn(new CurrencyColumn("amount").setTotal(true))
				.setColumn(m_bank_account_column.setDefaultValue(m_default_bank_account))
				.setColumn(m_date_column)
				.setColumn(new Column("type").setDefaultValue("Interest").setIsHidden(true));
		if (name.equals("invoices"))
			return new ViewDef(name)
				.addFeature(m_account_filter)
				.addFeature(m_month_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Invoice'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Invoice", true)
				.setColumnNamesForm(new String[] { "type", "date", "description", "amount", "accounts_id", "households_id", "notes" })
				.setColumnNamesTable(new String[] { "date", "description", "accounts_id", "households_id", "amount" })
				.setColumn(m_account_income_column)
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(new Column("type").setDefaultValue("Invoice").setIsHidden(true));
		if (name.equals("payees"))
			return ((Options)Site.site.getObjects(name)).newViewDef()
				.setRecordName("Payee", true)
				.setColumn(new Column("text").setDisplayName("name").setIsRequired(true));
		if (name.equals("payments"))
			return new ViewDef(name)
				.addFeature(m_month_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type='Payment'")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Payment", true)
				.setColumnNamesForm(new String[] { "type", "date", "description", "amount", "households_id", "num", "notes" })
				.setColumnNamesTable(new String[] { "date", "households_id", "description", "amount" })
				.setColumn(new CurrencyColumn("amount").setTotal(true))
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(new Column("num").setDisplayName("check num"))
				.setColumn(new Column("type").setDefaultValue("Payment").setIsHidden(true));
		if (name.equals("payments for deposit"))
			return new ViewDef(name){
					@Override
					public View
					newView(Request r) {
						return new View(this, r){
							@Override
							public View
							select() {
								if (m_mode == View.Mode.SELECT_LIST)
									m_state.setBaseFilter("type='Payment' AND transactions_id IS NULL AND amount > 0");
								else
									m_state.setBaseFilter("type='Payment'");
								return super.select();
							}
							@Override
							public void
							write() {
								if (m_mode == View.Mode.SELECT_LIST)
									m_state.setBaseFilter("type='Payment' AND transactions_id IS NULL AND amount > 0");
								else
									m_state.setBaseFilter("type='Payment'");
								super.write();
							}
						};
					}
				}
				.addFeature(m_month_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Payment", true)
				.setColumnNamesForm(new String[] { "type", "date", "amount", "households_id", "num", "notes" })
				.setColumnNamesTable(new String[] { "date", "type", "households_id", "amount" })
				.setColumn(new CurrencyColumn("amount").setTotal(true))
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(new Column("type").setDefaultValue("Payment").setIsReadOnly(true));
		if (name.equals("reconciliation"))
			return new ViewDef(name)
				.addFeature(m_month_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting"))
				.setAfterList("<script>accounting.reconciliation_calc_diff()</script>")
				.setAllowQuickEdit(true)
				.setAllowSorting(false)
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Transaction", true)
				.setReplaceOnQuickEdit(true)
				.setSelectColumns("id,accounts_id,CASE WHEN type='Deposit' THEN coalesce(amount,0.0)+coalesce((SELECT sum(amount) FROM transactions t2 WHERE transactions.id=t2.transactions_id),0.0) WHEN type='Interest' OR type='Transfer In' THEN amount ELSE NULL END credit,CASE WHEN type != 'Deposit' AND type != 'Interest' AND type != 'Transfer In' THEN -amount ELSE NULL END debit,bank_accounts_id,date,description,num,payees_id,reconciled,type")
				.setViewClass(ReconciiationView.class)
				.setColumnNamesForm(new String[] { "type", "date", "num", "payees_id", "description", "amount", "accounts_id", "reconciled", "bank_accounts_id" })
				.setColumnNamesTable(new String[] { "date", "type", "num", "payees_id", "description", "accounts_id", "credit", "debit", "reconciled", "balance" })
				.setColumn(m_account_column)
				.setColumn(new Column("balance") {
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						if (!v.data.getBoolean("reconciled"))
							return false;
						MutableDouble balance = (MutableDouble)data.get("balance");
						if (balance == null) {
							LocalDate date = v.data.getDate("date").minusDays(1);
							balance = new MutableDouble(calcBalanceBank(v.data.getInt("bank_accounts_id"), date, r.db));
							data.put("balance", balance);
						}
						balance.add(v.data.getDouble("credit") + v.data.getDouble("debit"));
						r.w.writeCurrency(balance.value());
						return true;
					}
				}.setTextAlign("right"))
				.setColumn(new LookupColumn("bank_accounts_id", "bank_accounts", "name").setDefaultToSessionAttribute().setDisplayName("bank account").setIsReadOnly(true))
				.setColumn(new CreditDebitColumn("credit"))
				.setColumn(m_date_column)
				.setColumn(new CreditDebitColumn("debit"))
				.setColumn(m_payee_column)
				.setColumn(m_type_column);
		if (name.equals("transactions"))
			return new ViewDef(name)
				.addFeature(m_month_year_filter)
				.addFeature(new OptionsFilter("type", TYPES, "Type", Location.TOP_LEFT).setCanBeNull(false))
				.addFeature(m_account_filter)
				.addFeature(new OptionsFilter("bank_accounts_id", new Options(new Select("id,name AS text").from("bank_accounts").orderBy("name")), "Bank Account", Location.TOP_LEFT))
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("type!='Deposit'")
				.setDefaultOrderBy("date")
				.setRecordName("Transaction", true)
				.setColumnNamesForm(new String[] { "type", "date", "num", "payees_id", "description", "amount", "accounts_id", "households_id", "bank_accounts_id", "notes" })
				.setColumnNamesTable(new String[] { "date", "type", "num", "payees_id", "description", "amount", "accounts_id", "households_id", "bank_accounts_id" })
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(m_account_column)
				.setColumn(m_bank_account_column)
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(m_payee_column)
				.setColumn(m_type_column);
		if (name.equals("transfers"))
			return new ViewDef(name) {
					@Override
					public String
					getAddButtonOnClick(String url) {
						return "accounting.add_transfer(this,'" + m_name + "')";
					}
				}.addFeature(m_account_filter)
				.addFeature(m_month_year_filter)
				.setAccessPolicy(new RoleAccessPolicy("accounting").add().delete().edit())
				.setBaseFilter("(type='Transfer In' OR type='Transfer Out')")
				.setDefaultOrderBy("date")
				.setFrom("transactions")
				.setRecordName("Transfer", true)
				.setViewClass(TransferView.class)
				.setColumnNamesForm(new String[] { "type", "date", "description", "amount", "bank_accounts_id", "notes" })
				.setColumnNamesTable(new String[] { "date", "type", "description", "amount", "bank_accounts_id" })
				.setColumn(new CurrencyColumn("amount"))
				.setColumn(m_account_column)
				.setColumn(m_bank_account_column)
				.setColumn(m_date_column)
				.setColumn(m_household_column)
				.setColumn(new OptionsColumn("type", "Transfer In", "Transfer Out"));
		return null;
	}

	// --------------------------------------------------------------------------

	public void
	sendStatement(int household_id, LocalDate from, LocalDate due, DBConnection db) {
		int contact_id = db.lookupInt(new Select("contact").from("households").whereIdEquals(household_id), 0);
		if (contact_id == 0)
			return;
		String email = db.lookupString(new Select("email").from("people").whereIdEquals(contact_id));
		if (email != null) {
			String s = buildStatement(household_id, from, due, db);
			if (s != null)
				new Mail(m_statement_title, s)
					.from(m_statement_from)
					.to(email)
					.send(false);
		}
	}

	// --------------------------------------------------------------------------

	public void
	sendStatements(LocalDate from, DBConnection db) {
		Rows rows = new Rows(new Select("id").from("households").where("active AND contact IS NOT NULL").orderBy("name"), db);
		while (rows.next())
			sendStatement(rows.getInt(1), from, null, db);
		rows.close();
	}

	// --------------------------------------------------------------------------

	private void
	transfer(Request r) {
		int from = r.getInt("from", 0);
		NameValuePairs nvp = new NameValuePairs()
			.set("type", "Transfer Out")
			.setDate("date", r.getDate("date"))
			.set("bank_accounts_id", from)
			.set("amount", r.getDouble("amount", 0))
			.set("description", r.getParameter("description"));
		r.db.insert("transactions", nvp);
		int to = r.getInt("to", 0);
		nvp.set("type", "Transfer In")
			.set("bank_accounts_id", to);
		r.db.insert("transactions", nvp);
		r.response.addMessage("Transfer Out added to " + r.db.lookupString("name", "bank_accounts", from) + " and Transfer In added to " + r.db.lookupString("name", "bank_accounts", to));
	}

	// --------------------------------------------------------------------------

	@Override
	protected void
	updateSettings(Request r) {
		super.updateSettings(r);
		m_accrual = "accrual".equals(m_method);
		m_bank_account_column.setDefaultValue(m_default_bank_account);
	}

	// --------------------------------------------------------------------------

	private void
	writeAddInvoicesForm(Request r) {
		HTMLWriter w = r.w;
		Card card = w.ui.card().bodyOpen();
		w.write("This is for adding similar invoices to some or all households for a one-time assessment. Enter the date, description and amount. Select which account to use for the invoices. Check which households to invoice and then click the 'add invoices' button.");
		card.close();
		Form f = new Form("add invoices", Site.context + "/" + apiURL("Add Invoices"), w).setSubmitText("Add Invoices").setButtonsLocation(Form.Location.BOTTOM).open();
		f.rowOpen("date");
		w.dateInput("date", Time.newDate());
		f.rowOpen("description");
		w.textInput("description", 0, null);
		f.rowOpen("amount");
		w.textInput("amount", 10, null);
		f.rowOpen("account");
		new LookupColumn("accounts_id", "accounts", "name").writeInput((View)null, null, false, r);
		f.rowOpen("households");
		Rows rows = new Rows(new Select("id,name").from("households").where("active").orderBy("name"), r.db);
		while (rows.next()) {
			w.write("<div class=\"checkbox\">");
			w.ui.checkbox("f" + rows.getInt(1), rows.getString(2), null, false, false, false);
			w.write("</div>");
		}
		rows.close();
		f.close();
	}

	// --------------------------------------------------------------------------

	public static void
	writeArrearsReport(LocalDate date, boolean show_names, boolean email, DBConnection db, HTMLWriter w) {
		if (date == null)
			date = Time.newDate();

		int num_arrears = 0;
		int num_due = 0;
		double total_arrears = 0;
		double total_due = 0;

		if (!email) {
			InlineForm f = w.ui.inlineForm(null).open();
			f.label("as of")
				.formGroup();
			w.dateInput(null, date);
			f.formGroup();
			w.ui.buttonOnClick("Go", "net.replace('#main',context+'/api/Accounting/Arrears Report?date='+this.form[0].value)");
			f.close();
		}
		w.write("the <b>due</b> column shows households that owe money but aren't yet in arrears. the <b>arrears</b> column shows households that are in arrears.");
		w.write("<table class=\"table table-condensed table-bordered table-hover\" style=\"width:auto;\"><tr>");
		if (show_names)
			w.write("<th>account</th>");
		w.write("<th>due</th><th>arrears</th><th>last payment/credit date</th></tr>");
		try (Rows rows = new Rows(new Select("id,name").from("households").where("active").orderBy("name"), db)) {
			while (rows.next()) {
				int id = rows.getInt(1);
				double balance = calcBalanceArrears(id, date, db);
				if (balance <= 0)
					continue;
				w.write("<tr>");
				if (show_names)
					w.write("<td>").write(rows.getString(2)).write("</td>");
				float dues = db.lookupFloat(new Select("sum(hoa_dues)").from("homes JOIN units ON units.id=homes.units_id").where("pays_hoa=" + id), 0);
				if (dues > 0 && balance > dues * 2) {
					w.write("<td></td><td style=\"text-align:right\">").writeCurrency(balance).write("</td>");
					total_arrears += balance;
					++num_arrears;
				} else {
					w.write("<td style=\"text-align:right\">").writeCurrency(balance).write("</td><td></td>");
					total_due += balance;
					++num_due;
				}
				w.write("<td style=\"text-align:right\">")
					.writeDate(db.lookupDate(new Select("max(date)").from("transactions").where("households_id=" + id + " AND (type='Payment' OR type='Credit')")))
					.write("</td></tr>");
			}
		}
		w.write("<tr>");
		if (show_names)
			w.write("<td><b>total</b></td>");
		w.write("<td style=\"text-align:right\"><b>")
			.writeCurrency(total_due)
			.write("</b></td><td style=\"text-align:right\"><b>")
			.writeCurrency(total_arrears)
			.write("</b></td><td></td></tr>");
		if (show_names)
			w.write("<tr><td><b>households</b></td><td style=\"text-align:right\"><b>")
				.write(num_due)
				.write("</b></td><td style=\"text-align:right\"><b>")
				.write(num_arrears)
				.write("</b></td><td></td></tr>");
		w.write("</table>");
	}

	// --------------------------------------------------------------------------

	private void
	writeExportTransactions(Request r) {
		HTMLWriter w = r.w;
		w.h3("Export Transactions");
		Form f = new Form(null, null, w)
			.open();
		f.rowOpen("start date");
		w.dateInput("start", Time.newDate().withDayOfMonth(1));
		f.rowOpen("end date");
		w.dateInput("end", null);
		f.rowOpen("household");
		new RowsSelect("household", new Select("id,name").from("households").orderBy("name"), "name", "id", r).setAllowNoSelection(true).write(r.w);
		f.rowOpen("account");
		new RowsSelect("account", new Select("id,name").from("accounts").orderBy("name"), "name", "id", r).setAllowNoSelection(true).write(r.w);
		f.rowOpen("type");
		new ui.Select("type", TYPES).setAllowNoSelection(true).write(r.w);
		f.rowOpen("columns");
		for (String column : m_support_banking ? new String[] { "date", "num", "description", "amount", "household", "account", "type", "bank account", "payee", "notes" } : new String[] { "date", "num", "description", "amount", "household", "account", "type", "notes" })
			w.ui.checkbox("col_" + column, column, null, true, false, false);
		f.addButton(w.ui.button("View").setOnClick("net.replace('#transactions',context+'" + apiURL("view transactions") + "?' + new URLSearchParams(new FormData(this.closest('form'))).toString())"))
			.setKeepSubmit(true)
			.setOnClick("$a({download:'transactions.csv',href:context+'" + apiURL("export transactions") + "?' + new URLSearchParams(new FormData(this.closest('form'))).toString()}).click()")
			.setSubmitText("Export")
			.close();
		w.setId("transactions")
			.tag("p");
	}

	// --------------------------------------------------------------------------

	private void
	writeHouseholdAccounts(boolean active, Request r) {
		HTMLWriter w = r.w;
		double total = 0;
		Table table = w.ui.table().addDefaultClasses().th("Household");
		w.setAttribute("data-sort", "date");
		table.th("Last Payment Date").th("Balance");
		try (Rows rows = new Rows(new Select("id,name").from("households").where(active ? "active" : "NOT active").orderBy("name"), r.db)) {
			if (!rows.isBeforeFirst())
				return;
			LocalDate date = r.getDate("date");
			while (rows.next()) {
				int household_id = rows.getInt(1);
				table.tr().td();
				w.aOnClick(rows.getString(2), "new Dialog({title:" + JS.string(rows.getString(2)) + ",url:context+'/" + apiURL("Household Accounts") + "?households_id=" + household_id + "'})");
				table.td();
				LocalDate d = r.db.lookupDate(new Select("max(date)").from("transactions").whereEquals("households_id", household_id));
				if (d != null)
					w.writeDate(d);
				double balance = calcBalanceHousehold(household_id, date, r.db);
				total += balance;
				table.tdRight();
				w.writeCurrency(balance);
			}
		}
		table.tfoot().td("<strong>Total</strong>");
		w.writeCurrency(total);
		table.close();
		w.js("_.sorter.init(_.$('#household_accounts').firstChild)");
	}

	// --------------------------------------------------------------------------

	private void
	writeHouseholdAccountsPane(Request r) {
		HTMLWriter w = r.w;
		String on_change = "var s=_.$('#report_select');var d=_.$('#report_date');net.replace('#household_accounts',context+'/" + apiURL("household accounts") + "/'+s.options[s.selectedIndex].text+'?date='+d.firstChild.firstChild.value)";
		w.setId("report_select")
			.setAttribute("onchange", on_change)
			.ui.select(null, new String[] {"Active Accounts", "Inactive Accounts"}, null, null, true)
			.br().setId("report_date")
			.addStyle("padding-top:10px")
			.tagOpen("div");
		w.setAttribute("onchange", on_change)
			.dateInput(null, Time.newDate())
			.tagClose();
		w.write("<div id=\"household_accounts\" style=\"padding-top:10px\">");
		writeHouseholdAccounts(true, r);
		w.write("</div>");
	}

	// --------------------------------------------------------------------------

	private void 
	writeImportTransactions(String segment_one, Request r) {
		HTMLWriter w = r.w;
		w.h3(segment_one);
		Form f = new Form(null, apiURL("import transactions"), w)
			.setSubmitText("Import")
			.open();
		f.rowOpen("Bank Account");
		new RowsSelect("bank_account", new Select("id,name").from("bank_accounts").orderBy("2"), "name", "id", r).setAllowNoSelection(true).write(w);
		f.rowOpen("Columns");
		w.textInput("columns", 0, "Date,Account,Num,Description,Debit,Credit");
		w.ui.helpText("The order must match the CSV. Remove unneeded columns. Don't rename columns.");
		f.rowOpen("Transactions");
		w.fileInput("file", "csv", false);
		f.close();
	}

	// --------------------------------------------------------------------------

	@Override
	public NavBar
	writePageMenu(Request r) {
		boolean user_has_accounting_role = r.userHasRole("accounting");
		NavBar nav_bar = navBar("main", 1, r).setBrand(getName(), null).open();

		nav_bar.dropdownOpen("Income", false);
		Map<String,Object> income_items = new TreeMap<>();
		if (m_support_assessments && user_has_accounting_role)
			income_items.put("Add Assessment Invoices", null);
		income_items.put("Invoices", null);
		income_items.put(m_label + " Payments", "Payments");
		nav_bar.items(income_items);
		nav_bar.dropdownClose();

		nav_bar.dropdownOpen("Expense", false);
		if (m_support_banking)
			nav_bar.item("Checks");
		nav_bar.item("Credits");
		nav_bar.dropdownClose();

		if (user_has_accounting_role) {
			nav_bar.dropdownOpen("Other", false);
			nav_bar.item("All Transactions");
			if (m_support_automatic_transactions)
				nav_bar.item("Automatic Transactions");
			if (m_support_transfers)
				nav_bar.item("Transfers");
			nav_bar.dropdownClose();
		}

		if (m_support_banking) {
			nav_bar.dropdownOpen("Banking", false);
			nav_bar.item("Deposits");
			nav_bar.item("Fees");
			nav_bar.item("Interest");
			if (user_has_accounting_role)
				nav_bar.item("Reconciliation");
			nav_bar.dropdownClose();
		}

		Map<String,Block> blocks = getBlocks();
		if (user_has_accounting_role && blocks != null) {
			TreeMap<String,Block> module_blocks = null;
			for (Block block : blocks.values())
				if (block.handlesContext("accounting module", r)) {
					if (module_blocks == null) {
						nav_bar.dropdownOpen("Modules", false);
						module_blocks = new TreeMap<>();
					}
					module_blocks.put(block.getName(), block);
				}
			if (module_blocks != null) {
				for (Block block : module_blocks.values())
					nav_bar.item(block.getName(), "blocks/" + block.getName() + "/accounting module");
				nav_bar.dropdownClose();
			}
		}

		boolean is_in_group = ((mosaic.Person)r.getUser()).isInGroup("Finance Committee", r.db);
		TreeMap<String,Object> reports = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
		if (user_has_accounting_role || r.userHasRole("hoa history") || is_in_group)
			reports.put(m_label + (m_label.endsWith("Account") ? "s" : " Accounts"), "Household Accounts");
		if (m_show_arrears_report)
			reports.put("Arrears Report", null);
		if (m_show_balance_sheet_report)
			reports.put(m_balance_sheet_title, "BalanceSheet");
		if (m_support_budgets && r.db.countRows("accounts_budgets", "year>" + Time.newDate().getYear()) != 0)
			reports.put("Budget for Next Year", null);
		if (m_support_banking) {
			reports.put("Checks by Account", null);
			reports.put("Checks by Payee", null);
		}
		if (m_show_income_statement_report)
			reports.put(m_income_statement_title, "IncomeStatement");
		if (blocks != null)
			for (Block block : blocks.values())
				if (block.handlesContext("accounting report", r))
					reports.put(block.getName(), "blocks/" + block.getName() + "/accounting report");
		if (reports.size() > 0) {
			nav_bar.dropdownOpen("Reports", false);
			nav_bar.items(reports);
			nav_bar.dropdownClose();
		}


		if (user_has_accounting_role) {
			TreeMap<String,Object> actions = new TreeMap<>();
			nav_bar.dropdownOpen("Actions", false);
			actions.put("Export Transactions", null);
			actions.put("Import Accounts", null);
			actions.put("Import Transactions", null);
			if (m_create_hoa_invoices)
				actions.put("Create " + m_label + " Invoices", "js:new Dialog({cancel:true,modal:true,ok:true,title:'Create " + m_label + " Invoices',url:'" + apiURL("method form dialog", "addHOAInvoices") + "'})");
			if (m_support_email_statements)
				actions.put("Send/View Statements", "SendView Statements");
			if (blocks != null)
				for (Block block : blocks.values())
					if (block.handlesContext("accounting task", r))
						actions.put(block.getName(), "blocks/" + block.getName() + "/accounting task");
			nav_bar.items(actions);
			nav_bar.dropdownClose();
		}

		if (user_has_accounting_role || is_in_group) {
			nav_bar.dropdownOpen("Settings", false);
			nav_bar.item("Account List");
			if (m_support_banking)
				nav_bar.item("Bank Accounts");
			nav_bar.aOnClick("Other Settings...", "new Dialog({cancel:true,ok:true,title:'Accounting Settings',url:context+'/" + apiURL("Other Settings") + "'});return false;");
			nav_bar.dropdownClose();
		}
		nav_bar.close();
		r.w.js("JS.get('accounting-min')");
		return nav_bar;
	}

	// --------------------------------------------------------------------------

	private void
	writeReconciliation(Request r) {
		int id = r.getPathSegmentInt(3);
		HTMLWriter w = r.w;
		if (id != 0) {
			ViewState.setBaseFilter("reconciliation", "bank_accounts_id=" + id, r);
			ViewState.setFilter("reconciliation", null, r);
			Site.site.newView("reconciliation", r).writeComponent();
			w.write("<table class=\"table table-sm\" style=\"float:right;width:auto\"><tbody><tr><td><b>Statement Balance</b></td><td>")
				.setAttribute("onchange", "accounting.reconciliation_calc_diff()")
				.setId("statement_balance")
				.numberInput(null, null, null, "0.1", null, true)
				.write("</td></tr><tr><td><b>Difference</b></td><td id=\"reconciliation_difference\"></td></tr></table>");
		} else {
			w.h3("Reconciliation");
			RowsSelect bank_accounts = new RowsSelect(null, new Select("id,name").from("bank_accounts").where("reconcile").orderBy("name"), "name", "id", r);
			if (bank_accounts.size() > 1) {
				w.write("Bank Account: ");
				bank_accounts.setAllowNoSelection(true);
				bank_accounts.setOnChange("var e=_.$('#reconciliation');e.innerHTML='';var id=this.options[this.selectedIndex].value;if(id)net.replace(e, context+'/" + apiURL("Reconciliation") + "/'+id)");
				bank_accounts.write(w);
				w.write("<div id=\"reconciliation\" style=\"margin-top:10px;\"></div>");
			} else if (bank_accounts.size() == 1) {
				SelectOption bank_account = bank_accounts.iterator().next();
				w.write(bank_account.getText())
					.write("<div id=\"reconciliation\"></div>")
					.js("net.replace(_.$('#reconciliation'), context+'/" + apiURL("Reconciliation", bank_account.getValue()) + ");");
			} else
				w.write("There are no Bank Accounts configured for reconciliation.");
		}
	}

	// --------------------------------------------------------------------------

	private void
	writeSendViewStatements(Request r) {
		HTMLWriter w = r.w;
		Form f = new Form(null, null, w).setButtonsLocation(FormBase.Location.NONE);
		f.rowOpen(null);
		w.ui.buttonOnClick(Text.all, "this.parentNode.querySelectorAll('input[type=checkbox]').forEach(function(e){e.checked=true})")
			.space()
			.ui.buttonOnClick(Text.none, "this.parentNode.querySelectorAll('input[type=checkbox]').forEach(function(e){e.checked=false})");
		w.write("<div style=\"display:flex;flex-wrap:wrap\">");
		try (Rows rows = new Rows(new Select("id,name").from("households").where("active AND contact IS NOT NULL").orderBy("name"), r.db)) {
			while (rows.next()) {
				w.addStyle("width:200px")
					.tagOpen("span");
				w.ui.checkbox("h" + rows.getInt(1), rows.getString(2), null, false, true, false)
					.tagClose();
			}
		}
		w.write("</div>");
		f.rowOpen("List transactions starting from ");
		w.dateInput("from", Time.newDate().withDayOfMonth(1).minusMonths(1));
		f.rowOpen("Due date");
		w.dateInput("due", Time.newDate().withDayOfMonth(1).plusMonths(1));
		f.rowOpen(null);
		w.js("""
			function send_view(b,f) {
				let ids=[]
				b.form.querySelectorAll('input[type=checkbox]').forEach(function(e){if(e.checked)ids.push(e.name.substring(1))})
				if(ids.length==0){
					Dialog.alert(null,'Please select one or more households')
					return
				}
				f(ids, b.form.elements.from.value, b.form.elements.due.value)
			}""");
		w.ui.buttonOnClick(Text.send, "send_view(this,function(ids,from,due){net.post(context+'/" + apiURL("Send Statements") + "','ids='+ids.join(',')+'&from='+from+'&due='+due,function(){Dialog.alert(null,'statements sent')})})")
			.space()
			.ui.buttonOnClick(Text.view, "send_view(this,function(ids,from,due){new Dialog({url:context+'/" + apiURL("View Statements") + "?ids='+ids.join(',')+'&from='+from+'&due='+due}).open()})");
		f.close();
	}

	// --------------------------------------------------------------------------

	@Override
	protected void
	writeSettingsInput(Field field, Request r) {
		if ("m_default_bank_account".equals(field.getName()))
			new RowsSelect("default_bank_account", new Select("name,id").from("bank_accounts").orderBy("name"), "name", "id", r).setSelectedOption(null, Integer.toString(m_default_bank_account)).write(r.w);
		else
			super.writeSettingsInput(field, r);
	}
}
