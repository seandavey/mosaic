package accounting;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import app.Request;
import db.DBConnection;
import ui.InlineForm;
import util.Time;
import web.HTMLWriter;

public abstract class AccountReport {
	protected final List<Account>	m_accounts = new ArrayList<>();
	protected final int				m_num_periods; // 4 quarters or 12 months
	protected final int				m_period_size; // 1 or 3 months
	protected final LocalDate[]		m_periods;
	protected final String			m_report_title;
	protected final Request			m_r;
	protected int					m_year;

	//--------------------------------------------------------------------------

	protected
	AccountReport(String report_title, Request r) {
		m_report_title = report_title;
		m_r = r;
		m_year = m_r.getInt("year", 0);
		if (m_year == 0)
			m_year = Time.newDate().getYear();
		if (r.getParameter("period") == null || r.getParameter("period").equals("monthly")) {
			m_num_periods = 12;
			m_period_size = 1;
		} else {
			m_num_periods = 4;
			m_period_size = 3;
		}
		m_periods = new LocalDate[m_num_periods - 1];
		for (int i=0,month=m_period_size; i<m_num_periods-1; i++,month+=m_period_size)
			m_periods[i] = LocalDate.of(m_year, month + 1, 1).minusDays(1);
	}

	//--------------------------------------------------------------------------

	protected final Account
	getAccount(String name, int type, DBConnection db) {
		for (Account a : m_accounts)
			if (a.getType() == type && a.getName().equals(name))
				return a;
		Account account = new Account(name, m_year, db);
		m_accounts.add(account);
		return account;
	}

	//--------------------------------------------------------------------------

	protected static double
	total(double[] values) {
		float total = 0;
		for (double value : values)
			total += value;
		return total;
	}

	//--------------------------------------------------------------------------

	final void
	write() {
		if (writeTransactions())
			return;
		if (m_r.getParameter("year") != null) {
			writeReport();
			return;
		}
		HTMLWriter w = m_r.w;
		w.h2(m_report_title);
		w.js("function replace_report(f) {" +
				"var s=_.$('#year_select');" +
				"net.replace('#annual_report',context+'/api/Accounting/" + this.getClass().getSimpleName() + "?year='+s.options[s.selectedIndex].value+'&period='+(_.$('#period0').checked?'monthly':'quarterly'))" +
			"}");
		w.write("<div class=\"annual_report\">")
			.setAttribute("onchange", "replace_report(this)");
		InlineForm f = w.ui.inlineForm(null).open().formGroup();
		w.setId("year_select")
			.ui.selectOpen(null, true);
		int this_year = Time.newDate().getYear();
		for (int i=m_r.db.lookupInt("extract(year from MIN(date))", "transactions", null, this_year); i<=this_year+1; i++) {
			w.write("<option");
			if (i == m_year)
				w.write(" selected=\"selected\"");
			w.write(">").write(i).write("</option>");
		}
		w.tagClose();
		f.formGroup();
		String[] options = new String[] {"monthly", "quarterly"};
		w.ui.radioButtons("period", options, options, "monthly");
		f.close();
		w.write("<br/><div id=\"annual_report\">");
		writeReport();
		w.write("</div></div>");
	}

	//--------------------------------------------------------------------------

	protected void
	writeReport() {
	}

	//--------------------------------------------------------------------------

	abstract protected boolean writeTransactions();
}
