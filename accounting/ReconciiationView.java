package accounting;

import java.time.LocalDate;

import app.Request;
import db.View;
import db.ViewDef;

public class ReconciiationView extends View {
	public
	ReconciiationView(ViewDef view_def, Request r) {
		super(view_def, r);
	}

	// --------------------------------------------------------------------------

	@Override
	protected void
	writeRows(String[] columns) {
		m_w.write("<style>table[data-view] td:nth-child(8){text-wrap:nowrap}</style>");
		String bf = m_state.getBaseFilter();
		int bank_id = Integer.parseInt(bf.substring(bf.lastIndexOf('=') + 1));
		String f = m_state.getFilter();
		LocalDate d;
		if (f.startsWith("extract(year")) {
			int i = f.lastIndexOf('\'');
			d = LocalDate.of(Integer.parseInt(f.substring(f.lastIndexOf('\'',  i-1)+1, i)), 1, 1).minusDays(1);			
		} else {
			int i = f.indexOf("month") + 18;
			String m = f.substring(i, f.indexOf('\'', i));
			if (m.length() == 1)
				m = '0' + m;
			i = f.indexOf("year") + 17;
			String y = f.substring(i, f.indexOf('\'', i));
			d = LocalDate.of(Integer.parseInt(y), Integer.parseInt(m), 1).minusDays(1);
		}
		double balance = Accounting.calcBalanceBank(bank_id, d, m_r.db);
		m_w.tagOpen("tr");
		m_w.setAttribute("colspan", columns.length - 1)
			.tag("td")
			.tagOpen("td");
		m_w.writeCurrency(balance)
			.tagClose()
			.tagClose();
		getColumnData().put("balance", new MutableDouble(balance));
		super.writeRows(columns);
	}
}
