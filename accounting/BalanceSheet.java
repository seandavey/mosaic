package accounting;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import app.Request;
import app.Site;
import db.AnnualReport;
import db.AnnualReport.Sums;
import db.DBConnection;
import db.Rows;
import db.Select;
import ui.Table;
import util.Time;
import web.HTMLWriter;
import web.JS;

public class BalanceSheet extends AccountReport {
	private final Map<String, OngoingAccount>	m_ongoing_accounts = new TreeMap<>();
	private final boolean						m_support_budgets;

	//--------------------------------------------------------------------------

	BalanceSheet(String report_title,  boolean support_budgets, Request r) {
		super(report_title, r);
		m_support_budgets = support_budgets;
	}

	//--------------------------------------------------------------------------

	private OngoingAccount
	getOngoingAccount(String name, DBConnection db) {
		int index = name.lastIndexOf(' ');
		if (index == -1)
			return null;
		String account_name = name.substring(0, index);
		OngoingAccount ongoing_account = m_ongoing_accounts.get(account_name);
		if (ongoing_account == null) {
			ongoing_account = new OngoingAccount(account_name, m_year, db);
			m_ongoing_accounts.put(account_name, ongoing_account);
		}
		return ongoing_account;
	}

	//--------------------------------------------------------------------------

	private int
	readOngoingTransactions() {
		LocalDate start_date = LocalDate.of(m_year, 1, 1);
		int period = 0;
		try (Rows rows = new Rows(new Select("a.name,a.type,t.amount,t.date,t.type")
				.from("transactions t JOIN accounts a ON t.accounts_id=a.id")
				.where("(a.type=4 OR a.type=5) AND a.active AND date<='12/31/" + m_year + "'")
				.orderBy("date"), m_r.db)) {
			if (!rows.next())
				return period;
			do {
				String name = rows.getString(1);
				int account_type = rows.getInt(2);
				double amount = rows.getDouble(3);
				LocalDate d = rows.getDate(4);
				while (!d.isBefore(start_date) && period < m_num_periods - 1 && d.compareTo(m_periods[period]) > 0)
					++period;
				if (account_type == 4) {
					OngoingAccount a = getOngoingAccount(name, m_r.db);
					if (a != null)
						a.addIncome(d, start_date, period, amount);
				} else { // if (account_type == 5) {
					OngoingAccount a = getOngoingAccount(name, m_r.db);
					if (a != null)
						a.addExpense(d, start_date, period, amount);
				}
			} while (rows.next());
		}
		return period;
	}

	//--------------------------------------------------------------------------

	private double[]
	total(int type) {
		double[] totals = new double[m_num_periods];
		for (Account a : m_accounts)
			if (a.getType() == type) {
				double[] sums = a.getSums();
				for (int i=0; i<m_num_periods; i++)
					totals[i] += sums[i];
			}

		return totals;
	}

	//--------------------------------------------------------------------------

	final double
	writeAssets() {
		int y = m_r.getInt("year", 0);
		int year = y != 0 ? y : Time.newDate().getYear();
		LocalDate d = LocalDate.of(year, 1, 1).minusDays(1);
		Map<Object,Sums> sums = new TreeMap<>();
		try (Rows rows = new Rows(new Select("id,name").from("bank_accounts").where("reconcile"), m_r.db)) {
			while (rows.next())
				sums.put(rows.getString(2), new Sums(rows.getString(1), Accounting.calcBalanceBank(rows.getInt(1), d, m_r.db)));
		}
		return new AnnualReport(new Select("name,bank_accounts_id,date,CASE WHEN type='Deposit' THEN coalesce(amount,0.0)+coalesce((SELECT sum(amount) FROM transactions t2 WHERE transactions.id=t2.transactions_id),0.0) WHEN type='Interest' OR type='Transfer In' THEN amount ELSE -amount END")
				.from("transactions")
				.joinOn("bank_accounts", "bank_accounts_id=bank_accounts.id")
				.where("reconciled"),
				"Bank Account", 2, 1, 4)
			.initSums(sums)
			.setDetail("date,num,(SELECT name FROM bank_accounts WHERE bank_accounts.id=transactions.bank_accounts_id) AS bank_account,description,amount", "transactions", "bank_accounts_id")
			.setDetailFilter("reconciled")
			.setDisplayAsCurrency(true)
			.setIsBalanceReport(true)
			.setShowControls(false)
			.setUrl(Site.context + "/api/Accounting/BalanceSheet/assets")
			.write("Assets", m_r);		
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	writeReport() {
		double operating_funds = writeAssets();
		HTMLWriter w = m_r.w;
		w.br()
			.h3("Liabilities");
		double total = 0;
		Table table = w.ui.table().addClass("table table-condensed table-bordered");
		table.th("account").th("balance");
		try (Rows balances = new Rows(new Select("id,name,(SELECT SUM(CASE WHEN type='Invoice' THEN amount ELSE -amount END) FROM transactions WHERE transactions.accounts_id=accounts.id AND date<='12/31/" + m_year + "') a")
			.from("accounts")
			.where("accounts.type=6")
			.groupBy("id")
			.orderBy("name"), m_r.db)) {
			while (balances.next()) {
				String name = balances.getString(2);
				double balance = balances.getDouble(3);
				if (balance != 0.0) {
					total += balance;
					table.tr().td();
					w.aOnClick(name, "new Dialog({title:" + JS.string(name) + ",url:context+'/api/Accounting/BalanceSheet?account=" + balances.getInt(1) + "&year=" + m_year + "'}).open()");
					table.tdCurrency(balance);
				}
			}
		}
		table.tr().td("Total").tdCurrency(total)
			.close();
		operating_funds -= total;

		w.br()
			.h3("Equities");
		int period = readOngoingTransactions();
		m_accounts.sort(new Comparator<Account>() {
			@Override
			public int compare(Account a1, Account a2) {
				return a1.getName().compareToIgnoreCase(a2.getName());
			}
		});
		if (m_ongoing_accounts.size() > 0) {
			total = 0;
			table = w.ui.table().addClass("table table-condensed table-bordered");
			table.th("account").th("starting balance").th("type");
			for (int p=0; p<=period; p++) {
				table.th();
				if (m_period_size == 3) {
					w.write('Q');
					w.write(p + 1);
				} else
					w.write(Time.formatter.getMonthNameShort(p + 1));
			}
			if (period > 0)
				table.th("total");
			table.th("net").th("balance");
			for (OngoingAccount ongoing_account : m_ongoing_accounts.values())
				total += ongoing_account.writeRow(period, table, w);
//				if ("Capital Improvements".equals(ongoing_account.getName()))
//					ci_balance = balance;
			table.tr().td(table.numColumns() - 1, "Total").td();
			w.writeCurrency(total);
			table.close();
			operating_funds -= total;
		}

		boolean first = true;
		for (Account a : m_accounts)
			if (a.getType() == 3 && (a.getBudget() > 0 || total(a.getSums()) > 0)) {
				if (first) {
					first = false;
					w.h3("Capital Improvements")
						.addClass("table table-condensed table-bordered")
						.tagOpen("table");
					w.write("<thead><tr><th>Account</th><th>Budget</th><th>Spent</th><th>Remaining</th><th>Percent</th></tr></thead>");
				}
				a.writeRow(period, m_year, false, m_support_budgets, w);
			}
		if (!first) {
			/*total_expense +=*/ writeTotals(3, period, false, w);
			w.tagClose();
//			if (m_year == this_year) {
//				w.write("<p>Amount unallocated for Capital Improvements: ");
//				double total_budget_capital_improvements = 0;
//				for (Account a : m_accounts)
//					if (a.getType() == 3 && a.getBudget() > 0)
//						total_budget_capital_improvements += a.getBudget();
//				w.writeCurrency(ci_balance - (total_budget_capital_improvements - total(total(3))))
//					.write("</p>");
//			}
		}

		w.br()
			.h3("Operating Funds")
			.writeCurrency(operating_funds);
	}

	//--------------------------------------------------------------------------

	private double
	writeTotals(int type, int period, boolean show_periods, HTMLWriter w) {
		double[] sums = total(type);
		double sums_sum = total(sums);
		double total_budget = 0;
		w.write("<tr><td>Total");
		if (m_support_budgets) {
			for (Account a : m_accounts)
				if (a.getType() == type && a.getBudget() > 0)
					total_budget += a.getBudget();
			w.write("</td><td style=\"text-align:right\">");
			w.writeCurrency(total_budget);
		}
		if (show_periods)
			for (int p=0; p<=period; p++) {
				w.write("</td><td style=\"text-align:right\">");
				w.writeCurrency(sums[p]);
			}
		if (period > 0 || !show_periods) {
			w.write("</td><td style=\"text-align:right\">");
			w.writeCurrency(sums_sum);
		}
		if (m_support_budgets) {
			w.write("</td><td style=\"text-align:right\">");
			w.writeCurrency(total_budget - sums_sum);
			w.write("</td><td style=\"text-align:right\">");
			if (total_budget > 0)
				w.writePercent(1.0 - sums_sum / total_budget);
		}
		w.write("</td></tr>");
		return sums_sum;
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	writeTransactions() {
		int account_id = m_r.getInt("account", 0);
		if (account_id == 0)
			return false;
		HTMLWriter w = m_r.w;
		Table table = w.ui.table().addDefaultClasses().addClass("table-bordered").heads("Date", "Type", "Description", "Payee", "Amount", "Balance");
		double balance = 0;
		Rows rows = new Rows(new Select("date,type,description,(SELECT text FROM payees WHERE payees.id=transactions.payees_id) AS payee,CASE WHEN type='Invoice' THEN amount ELSE -amount END").from("transactions").where("accounts_id=" + account_id).orderBy("date"), m_r.db);
		while (rows.next()) {
			table.tr().td();
			LocalDate date = rows.getDate(1);
			double amount = rows.getDouble(5);
			balance += amount;
			if (date != null)
				w.writeDate(date);
			table.td(rows.getString(2))
				.td(rows.getString(3))
				.td(rows.getString(4))
				.addStyle(m_report_title, m_report_title)
				.tdRight();
			w.writeCurrency(amount);
			table.tdRight();
			w.writeCurrency(balance);
		}
		rows.close();
		table.close();
		return true;
	}
}
