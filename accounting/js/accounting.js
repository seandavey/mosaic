var accounting = {
	add_transfer(el, viewdef) {
		var d = new Dialog({title:'Add Transfer'})
		function a(text, kind, title) {
			d.body.append(_.ui.button(text, function() {
				Dialog.top().close()
				_.table.dialog_add(el, viewdef, context + '/Views/' + viewdef + '/component?db_mode=ADD_FORM&kind='+kind, title)
			}, {styles:{display:'block', marginBottom:5, width:'100%'}}))
		}
		a('add transfer between bank accounts', 'bank', 'Add Transfer')
		a('add other transfer', 'regular', 'Add Transfer')
		d.open()
	},
	reconciliation_calc_diff() {
		let sb = _.$('#statement_balance').value
		if (sb === '')
			return
		let row = _.$('#reconciliation').querySelector('table[data-view]').tBodies[0].lastElementChild
		while (row) {
			let t = row.lastChild.innerText
			if (t.indexOf('$') != -1) {
				let c = new Currency(sb).subtract(t)
				_.$('#reconciliation_difference').innerText = c.format()
				break
			}
			row = row.previousSibling
		}
	}
}
class Currency {
	constructor(value, opts) {
		let that = this

		if (!(that instanceof Currency)) {
			return new Currency(value, opts)
		}

		let settings = Object.assign({}, {
		  symbol: '$',
		  separator: ',',
		  decimal: '.',
		  errorOnInvalid: false,
		  precision: 2,
		  pattern: '!#',
		  negativePattern: '-!#',
		  fromCents: false
		}, opts), precision = Math.pow(10, settings.precision), v = Currency.parse(value, settings)

		that.intValue = v
		that.value = v / precision

		settings.increment = settings.increment || (1 / precision)
		settings.groups = /(\d)(?=(\d{3})+\b)/g

		this._settings = settings
		this._precision = precision
	}
	format() {
	  let { pattern, negativePattern, symbol, separator, decimal, groups } = this._settings
	    , split = ('' + this.value).replace(/^-/, '').split('.')
	    , dollars = split[0]
	    , cents = split[1];

	  return (this.value >= 0 ? pattern : negativePattern)
	    .replace('!', symbol)
	    .replace('#', dollars.replace(groups, '$1' + separator) + (cents ? decimal + cents : ''));
	}
	static parse(value, opts) {
		let v = 0
			, { decimal, errorOnInvalid, precision: decimals, fromCents } = opts
		, precision = Math.pow(10, decimals)
		, isNumber = typeof value === 'number'
		, isCurrency = value instanceof Currency

		if (isCurrency && fromCents)
			return value.intValue

		if (isNumber || isCurrency)
			v = isCurrency ? value.value : value
		else if (typeof value === 'string') {
			let regex = new RegExp('[^-\\d' + decimal + ']', 'g'), decimalString = new RegExp('\\' + decimal, 'g')
			v = value
				.replace(/\((.*)\)/, '-$1')
				.replace(regex, '')
				.replace(decimalString, '.')
			v = v || 0;
		} else {
			if(errorOnInvalid)
				throw Error('Invalid Input')
			v = 0;
		}

		if (!fromCents) {
			v *= precision
			v = v.toFixed(4)
		}

		return Math.round(v)
	}
	subtract(number) {
		let { intValue, _settings, _precision } = this
		return new Currency((intValue -= Currency.parse(number, _settings)) / (_settings.fromCents ? 1 : _precision), _settings)
	}
}
