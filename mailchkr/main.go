package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strings"

	_ "github.com/lib/pq"

	"github.com/emersion/go-imap"
	idle "github.com/emersion/go-imap-idle"
	"github.com/emersion/go-imap/client"
)

type account struct {
	*client.Client
	*idle.IdleClient
	site   string
	name   string
	domain string
}

func asMailboxUpdate(upd client.Update) *client.MailboxUpdate {
	if v, ok := upd.(*client.MailboxUpdate); ok {
		return v
	}
	return nil
}

func (a *account) deleteMessages(seqset *imap.SeqSet) error {
	item := imap.FormatFlagsOp(imap.AddFlags, true)
	flags := []interface{}{imap.DeletedFlag}
	if err := a.Store(seqset, item, flags, nil); err != nil {
		fmt.Println("IMAP Message Flag Update Failed")
		return err
	}
	if err := a.Expunge(nil); err != nil {
		return err
	}
	return nil
}

func (a *account) fetchMessages(mbox *imap.MailboxStatus) {
	if mbox.Messages == 0 {
		return
	}
	fmt.Println(mbox.Messages)
	seqset := new(imap.SeqSet)
	seqset.AddRange(1, mbox.Messages)
	messages := make(chan *imap.Message, 10)
	done := make(chan error, 1)
	go func() {
		done <- a.Fetch(seqset, []imap.FetchItem{imap.FetchRFC822}, messages)
	}()
	for msg := range messages {
		for _, value := range msg.Body {
			buf := make([]byte, value.Len())
			if _, err := value.Read(buf); err != nil {
				a.printError(err)
				return
			}
			if err := a.writeMessage(buf); err != nil {
				a.printError(err)
				return
			}
		}
	}
	if err := <-done; err != nil {
		a.printError(err)
		return
	}
	if err := a.deleteMessages(seqset); err != nil {
		a.printError(err)
		return
	}
	a.pingSite()
}

func launch(site, list, domain string) {
	c, err := login(site, list)
	if err != nil {
		return
	}
	if _, err := c.Select("INBOX", true); err != nil {
		log.Fatal(err)
	}
	idleClient := idle.NewClient(c)
	a := &account{c, idleClient, site, list, domain}
	a.run()
}

func loadLists(site string) []string {
	fmt.Println(site)
	connStr := "host=localhost port=5432 user=mosaic password=Postgres1! dbname=" + site + " sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		fmt.Println(err)
		return []string{}
	}
	defer db.Close()
	rows, err := db.Query(`SELECT name FROM mail_lists WHERE active ORDER BY name`)
	if err != nil {
		fmt.Println(err)
		return []string{}
	}
	defer rows.Close()
	var lists []string
	var name string
	for rows.Next() {
		err := rows.Scan(&name)
		if err != nil {
			fmt.Println(err)
			return []string{}
		}
		lists = append(lists, name)
	}
	err = rows.Err()
	if err != nil {
		fmt.Println(err)
		return []string{}
	}
	return lists
}

func loadSites() [][]string {
	text, err := ioutil.ReadFile("sites")
	if err != nil {
		fmt.Println(err)
		return [][]string{}
	}
	lines := strings.Split(string(text), "\n")
	sites := make([][]string, len(lines))
	for i, l := range lines {
		if len(l) > 0 {
			sites[i] = strings.Split(l, "\t")
		}
	}
	sites[len(lines)-1] = []string{"demo", "demo.cohousing.site"}
	return sites
}

func login(site, list string) (*client.Client, error) {
	c, err := client.DialTLS("mail.us.opalstack.com:993", nil)
	if err != nil {
		fmt.Printf("%s - %v\n", list, err)
		return nil, err
	}

	m := mailuser(site, list)
	if err := c.Login(m, "P"+reverse(m)+"1!"); err != nil {
		fmt.Printf("%s - %s - %s - %v\n", list, m, "P"+reverse(m)+"1!", err)
		return nil, err
	}
	return c, nil
}

func mailuser(site, list string) string {
	m := list + "_" + site
	m = strings.ToLower(m)
	m = strings.ReplaceAll(m, ".", "_")
	m = strings.ReplaceAll(m, "&", "_")
	m = strings.ReplaceAll(m, "/", "_")
	if len(m) > 32 {
		m = m[:32]
	}
	return m
}

func main() {
	sites := loadSites()
	for _, site := range sites {
		lists := loadLists(site[0])
		for _, list := range lists {
			s := strings.Split(site[1], ".")
			go launch(s[0], list, site[1])
		}
	}
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
}

func (a *account) pingSite() {
	resp, err := http.Get("https://" + a.domain + "/MailLists/" + a.name + "/receive_mail")
	if err != nil {
		a.printError(err)
		return
	}
	resp.Body.Close()
}

func (a *account) printError(err error) {
	fmt.Printf("%s - %s - %v\n", a.site, a.name, err)
}

func reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func (a *account) run() {
	chanupd := make(chan client.Update)
	// ...
	for {
		upd, err := a.waitForUpdate(chanupd)
		if err != nil {
			a.printError(err)
			return
		}
		a.fetchMessages(upd.Mailbox)
	}
}

func (a *account) waitForUpdate(chupd chan client.Update) (*client.MailboxUpdate, error) {
	done := make(chan error, 1)
	stop := make(chan struct{})
	go func() {
		done <- a.Idle(stop)
	}()

	var mboxupd *client.MailboxUpdate
waitLoop:
	for {
		select {
		case upd := <-chupd:
			if mboxupd = asMailboxUpdate(upd); mboxupd != nil {
				break waitLoop
			}
		case err := <-done:
			if err != nil {
				return nil, fmt.Errorf("error while idling: %s", err.Error())
			}
			return nil, nil
		}
	}

	close(stop)
	<-done

	return mboxupd, nil
}

func (a *account) writeMessage(buf []byte) error {
	dir := a.site
	if a.site == "demo" {
		dir = "webapps"
	}
	dir = "/home/mosaic/tomcat/" + dir + "/ROOT/inbox/" + a.name
	f, err := ioutil.TempFile(dir, "msg")
	if err != nil {
		return err
	}
	filename := filepath.Base(f.Name())
	if _, err := f.Write(buf); err != nil {
		return err
	}
	f.Close()
	if err := os.Rename(dir+"/"+filename, dir+"/"+filename+".txt"); err != nil {
		return err
	}
	if err := ioutil.WriteFile(dir+"/"+filename, buf, 0600); err != nil {
		return err
	}
	return nil
}
