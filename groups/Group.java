package groups;

import java.util.List;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;

import app.Site;
import db.DBConnection;
import db.NameValuePairs;
import db.Select;
import util.Array;
import web.JSON;

class Group {
	private final int	m_id;
	private String[]	m_items;
	private String		m_name;

	//--------------------------------------------------------------------------

	Group(int id) {
		m_id = id;
	}

	//--------------------------------------------------------------------------
	// returns [id, full address] of mail list for group if there is one

	final String[]
	getMailList(DBConnection db) {
		if (!Site.site.moduleIsActive("MailLists"))
			return null;
		return db.readRow(new Select("id,name").from("mail_lists").where("send_to='Group:" + m_id + "' AND active"));
	}

	//--------------------------------------------------------------------------

	final String[]
	getMailListRecipients(DBConnection db) {
		List<String> recipients = db.readValues(new Select("email").from("contacts").whereEquals("groups_id", m_id).andWhere("email IS NOT NULL").orderBy("email"));
		return recipients.toArray(new String[recipients.size()]);
	}

	//--------------------------------------------------------------------------

	public final String
	getName() {
		return m_name;
	}

	//--------------------------------------------------------------------------

	final void
	hideItem(String item, DBConnection db) {
		if (itemIsShown(item)) {
			m_items = m_items.clone();
			m_items = Array.remove(m_items, item);
			store(db);
		}
	}

	//--------------------------------------------------------------------------

	final boolean
	itemIsShown(String item) {
		return Array.indexOf(m_items, item) != -1;
	}

	//--------------------------------------------------------------------------

	final boolean
	load(String[] default_items, DBConnection db) {
		String[] s = db.readRow(new Select("name,items").from("groups").whereIdEquals(m_id));
		if (s == null)
			return false;
		m_name = s[0];
		if (s[1] != null)
			m_items = JSON.toArray(Json.parse(s[1]).asArray());
		else
			m_items = default_items;
		return true;
	}

	//--------------------------------------------------------------------------

	final void
	showItem(String item, DBConnection db) {
		if (!itemIsShown(item)) {
			m_items = m_items.clone();
			m_items = Array.concat(m_items, item);
			store(db);
		}
	}

	//--------------------------------------------------------------------------

	final void
	store(DBConnection db) {
		NameValuePairs nvp = new NameValuePairs();
		if (m_items != null) {
			JsonArray a = Json.array(m_items);
			nvp.set("items", a.toString());
		}
		nvp.set("name", m_name);
		db.update("groups", nvp, m_id);
	}
}