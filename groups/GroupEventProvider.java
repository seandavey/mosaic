package groups;

import java.util.ArrayList;

import app.Person;
import app.Request;
import app.Site;
import calendar.MosaicEventProvider;
import db.DBConnection;
import db.Select;
import db.ViewDef;
import db.ViewState;
import db.column.EmailColumn;

public class GroupEventProvider extends MosaicEventProvider {
	private final String	m_calendar_access_policy;
	private boolean 		m_calendar_public;
	final int				m_group_id;

	//--------------------------------------------------------------------------

	public
	GroupEventProvider(String name, int group_id, boolean calendar_public, String calendar_access_policy, DBConnection db) {
		super("Group" + group_id + "Calendar");
		setDisplayName(name);
		m_group_id = group_id;
		m_calendar_public = calendar_public;
		m_calendar_access_policy = calendar_access_policy;
		if ("group members".equals(m_calendar_access_policy))
			setAccessPolicy(new GroupAccessPolicy(group_id).add().delete().edit());
		setEventsCanRepeat(true);
		setEventsHaveColor(true);
		setEventsHaveTime(true);
		setEventsTable("group_" + group_id + "_events");
		setSupportReminders();
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	addAddresses(ArrayList<String> a) {
		a.add("group");
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	adjustTables(DBConnection db) {
		super.adjustTables(db);
		if ("group members".equals(m_calendar_access_policy))
			setAccessPolicy(new GroupAccessPolicy(m_group_id).add().delete().edit());
		else
			setAccessPolicy(null);
	}

	//--------------------------------------------------------------------------

	public final int
	getGroupID() {
		return m_group_id;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	isValidAddress(String address) {
		return "group".equals(address) || super.isValidAddress(address);
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		ViewDef view_def = super._newViewDef(name);
		if (name.equals(m_name + "_reminders"))
			view_def.setColumn(new EmailColumn("email"){
				@Override
				public String
				getDefaultValue(ViewState view_state, Request r) {
					String list = r.db.lookupString(new Select("name").from("mail_lists").where("send_to='Group:" + m_group_id + "'"));
					if (list != null)
						return Site.site.getEmailAddress(list);
					return r.getUser().getEmail();
				}
			}.setAddressProvider(this).setDisplayName("send email to").setIsRequired(true));
		return view_def;
	}

	//--------------------------------------------------------------------------

	public final void
	setCalendarPublic(boolean calendar_public) {
		m_calendar_public = calendar_public;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showOnMenu(Request r) {
		Person user = r.getUser();
		return m_calendar_public || user != null && Groups.isMember(m_group_id, user.getId(), r.db);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	writeSettingsForm(String[] column_names, boolean in_dialog, boolean hide_active, Request r) {
		if (column_names == null)
			column_names = new String[] { "m_color", "m_display_item_labels", "m_display_items", "m_display_name", "m_events_can_repeat", "m_events_have_category", "m_events_have_color", "m_events_have_event", "m_events_have_location", "m_events_have_start_time", "m_ical_items", "m_role", "m_support_attachments", "m_support_registrations", "m_support_reminders", "m_tooltip_item_labels", "m_tooltip_items", "m_uuid" };
		super.writeSettingsForm(column_names, in_dialog, false, r);
	}
}
