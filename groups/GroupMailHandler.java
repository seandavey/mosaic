package groups;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import app.Request;
import db.DBConnection;
import db.Rows;
import db.Select;
import email.MailHandler;
import mail.Addresses;
import util.MultiMap;
import util.Text;

class GroupMailHandler extends MailHandler {
	private final int		m_id;

	//--------------------------------------------------------------------------

	public
	GroupMailHandler(int id, DBConnection db) {
		super((db.lookupBoolean(new Select("active").from("groups").whereIdEquals(id)) ? "Group:" : "Inactive Group:") + db.lookupString(new Select("name").from("groups").whereIdEquals(id)));
		m_id = id;
	}

	//--------------------------------------------------------------------------

	@Override
	protected String
	getDisplayName() {
		return m_display_name;
	}

	//--------------------------------------------------------------------------

	@Override
	protected Addresses
	getAddresses(int list_id, DBConnection db) {
		return Groups.getEmailAddresses(m_id, "NOT EXISTS(SELECT 1 FROM mail_lists_digest WHERE mail_lists_digest.people_id=people.id AND mail_lists_digest.mail_lists_id=" + list_id + ")", db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	isSubscribed(int person_id, DBConnection db) {
		if (person_id == 0)
			return false;
		return Groups.isMember(m_id, person_id, db);
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	writeSubscribers(Request r) {
		Set<String> subscribers = new TreeSet<>();
		MultiMap<Integer> jobs_people = Groups.getJobsPeople(m_id, r.db);
		for (String job : jobs_people.keySet()) {
			List<Integer> people = jobs_people.get(job);
			for (Integer people_id : people) {
				String name = r.db.lookupString(new Select("first,last").from("people").whereIdEquals(people_id));
				if (name != null)
					subscribers.add(name);
			}
		}
		Rows rows = new Rows(new Select("first,last").from("people").joinOn("people_groups", "people.id=people_groups.people_id").whereEquals("groups_id", m_id).orderBy("first,last"), r.db);
		while (rows.next())
			subscribers.add(Text.join(" ", rows.getString(1), rows.getString(2)));
		rows.close();
		boolean first = true;
		for (String subscriber : subscribers) {
			if (first)
				first = false;
			else
				r.w.br();
			r.w.write(subscriber);
		}
		return !first;
	}
}
