package groups;

import app.Request;
import db.Rows;
import db.Select;
import db.View;
import db.access.AccessPolicy;
import mosaic.Person;

public class GroupAccessPolicy extends AccessPolicy {
	protected final int	m_group_id;

	//--------------------------------------------------------------------------

	public
	GroupAccessPolicy(int group_id) {
		m_group_id = group_id;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canDeleteRow(String from, int id, Request r) {
		if (!m_delete)
			return false;
		if (m_group_id == 0) {
			String role = getRole(r);
			if (role != null)
				return r.userHasRole(role);
		}
		return isMember(r) || r.userHasRole("groups");
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	canUpdateRow(String from, int id, Request r) {
		if (!m_edit)
			return false;
		if (m_group_id == 0) {
			String role = getRole(r);
			if (role != null)
				return r.userHasRole(role);
		}
		return isMember(r) || r.userHasRole("groups");
	}

	//--------------------------------------------------------------------------

	private String
	getRole(Request r) {
		return r.db.lookupString(new Select("role").from("groups").whereIdEquals(r.getSessionInt("groups_id", 0)));
	}

	//--------------------------------------------------------------------------

	boolean
	isMember(Request r) {
		int group_id = m_group_id;
		if (group_id == 0)
			group_id = r.getSessionInt("groups_id", 0);
		if (group_id == 0)
			return false;
		Person user = (Person)r.getUser();
		if (user == null)
			return false;
		if (Groups.isMember(group_id, user.getId(), r.db))
			return true;
		return Groups.isMemberOfSubgroup(group_id, user.getId(), r.db);
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showAddButton(View v, Request r) {
		if (!m_add)
			return false;
		if (m_group_id == 0) {
			String role = getRole(r);
			if (role != null)
				return r.userHasRole(role);
		}
		return isMember(r) || r.userHasRole("groups");
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showDeleteButtons(Rows data, Request r) {
		if (!m_delete)
			return false;
		if (m_group_id == 0) {
			String role = getRole(r);
			if (role != null)
				return r.userHasRole(role);
		}
		return isMember(r) || r.userHasRole("groups");
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	showEditButtons(Rows data, Request r) {
		if (!m_edit)
			return false;
		if (m_group_id == 0) {
			String role = getRole(r);
			if (role != null)
				return r.userHasRole(role);
		}
		return isMember(r) || r.userHasRole("groups");
	}
}
