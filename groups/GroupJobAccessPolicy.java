package groups;

import app.Request;
import mosaic.Person;

class GroupJobAccessPolicy extends GroupAccessPolicy {
	public
	GroupJobAccessPolicy(int group_id) {
		super(group_id);
	}

	//--------------------------------------------------------------------------

	@Override
	protected boolean
	isMember(Request r) {
		int group_id = m_group_id;
		if (group_id == 0)
			group_id = r.getSessionInt("groups_id", 0);
		if (group_id == 0)
			return false;
		Person user = (Person)r.getUser();
		if (user == null)
			return false;
		if (Groups.hasJob(group_id, user.getId(), r.db))
			return true;
		return false;
	}
}
