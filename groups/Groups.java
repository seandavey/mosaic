package groups;

import java.lang.reflect.Field;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.eclipsesource.json.Json;

import app.ClassTask;
import app.Request;
import app.Roles;
import app.Site;
import app.Site.Message;
import app.SiteModule;
import app.Subscriber;
import bootstrap5.ButtonNav;
import bootstrap5.ButtonNav.Panel;
import calendar.EventProvider;
import calendar.MonthView;
import db.DBConnection;
import db.Filter;
import db.LinkValueRenderer;
import db.ManyToMany;
import db.NameValuePairs;
import db.OneToMany;
import db.Rows;
import db.RowsSelect;
import db.SQL;
import db.Select;
import db.View;
import db.View.Mode;
import db.ViewDef;
import db.ViewState;
import db.access.AccessPolicy;
import db.access.And;
import db.access.Or;
import db.access.RecordOwnerAccessPolicy;
import db.column.Column;
import db.column.FolderColumn;
import db.column.LookupColumn;
import db.column.MultiColumnRenderer;
import db.column.OptionsColumn;
import db.column.PersonColumn;
import db.column.RolesColumn;
import db.column.TagsColumn;
import db.column.TextAreaColumn;
import db.feature.Button;
import db.feature.ColumnFilter;
import db.feature.Feature.Location;
import db.feature.TSVectorSearch;
import db.jdbc.JDBCColumn;
import db.jdbc.JDBCTable;
import db.object.JSONField;
import decisions.Decisions;
import lists.Lists;
import mail.Addresses;
import mail.Mail;
import minutes.LockedAccessPolicy;
import mosaic.Documents;
import mosaic.Minutes;
import mosaic.Person;
import pages.Page;
import social.Comments;
import ui.Form;
import ui.NavBar;
import ui.RichText;
import ui.Table;
import ui.Tags;
import util.Array;
import util.MultiMap;
import util.Text;
import util.Time;
import web.HTMLWriter;
import web.JSON;
import web.JSONBuilder;

public class Groups extends SiteModule implements Subscriber {
	private static final String[]	s_items = new String[] { "calendar", "contacts", "decisions", "documents", "email", "links", "lists", "mandate", "minutes", "people", "requests" };
	private static Groups			s_module;

	@JSONField(fields={"m_start_collapsed"})
	private boolean		m_allow_subgroups;
	@JSONField(after="items shown for groups that haven't overriden them on the group page", choices={ "calendar", "contacts", "decisions", "documents", "email", "links", "lists", "mandate", "minutes", "people", "requests" })
	private String[]	m_default_items = new String[] { "calendar", "contacts", "decisions", "documents", "email", "links", "mandate", "minutes", "people", "requests" };
	private Documents	m_documents_module;
	@JSONField
	private boolean		m_job_holders_can_sign_up_others;
	@JSONField(after="double-click to rename", on_edit_updated="e => {net.post(context+'/Groups/rename_job','from='+e.detail.previousData.__originalData.value+'&to='+e.detail.data.value)}")
	private String[]	m_jobs = new String[] { "convener" };
	@JSONField(required=true)
	private String		m_mandate_label = "mandate";
	@JSONField
	private boolean		m_show_email_link = true;
	@JSONField(label = "show reports on bottom of Groups menu")
	private boolean		m_show_reports = true;
	@JSONField
	private boolean		m_start_collapsed;
	@JSONField
	private boolean		m_use_date_fields_for_people = true;

	//--------------------------------------------------------------------------

	public
	Groups() {
		m_renameable = true;
		m_description = "Provides pages for teams and committees which can have their own calendars, contacts, decisions, documents, email list, minutes, etc.";
		s_module = this;
	}

	//--------------------------------------------------------------------------

	private String
	actionURL(int group_id, String action) {
		return "net.post(context+'/Groups/" + group_id + "/" + action + "',null,function(){_.$('#group_items').button_nav.refresh()})";
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	addPages(DBConnection db) {
		Site.pages.add(new Page("Groups", false) {
			@Override
			public void
			a(NavBar nav_bar, Request r) {
				List<String[]> g = load(r);
				HTMLWriter w = r.w;
				nav_bar.dropdownOpen(getDisplayName(r), false);
				if (m_allow_subgroups) {
					String uuid = Text.uuid();
					w.write("<div id=\"").write(uuid).write("\" onmouseup=\"event.stopPropagation()\" style=\"border:none;display:block;padding:0 20px 0 10px;position:relative\"></div>")
						.write("<script>new _.NavList([");
					boolean first = true;
					for (String[] group : g)
						if (group[2] == null) {
							if (first)
								first = false;
							else
								w.comma();
							writeGroup(group, g, w);
						}
					w.write("],{has_subs:true,a_class:'dropdown-item'");
					if (m_start_collapsed)
						w.write(",start_collapsed:true");
					w.write("}).render('#").write(uuid).write("')</script>");
				} else
					for (String[] group : g)
						nav_bar.aOnClick(group[1], "app.go('Groups/" + group[0] + "')");
				String label = Text.singularize(getDisplayName(r));
				if (m_show_reports) {
					nav_bar.divider();
					if (m_jobs != null && m_jobs.length > 0) {
						nav_bar.aOnClick("Email People With " + label + " Jobs", "new Dialog({title:'Email People With Group Jobs',url:context+'/" + apiURL("email people with jobs") + "'}).open()");
						nav_bar.aOnClick("View Jobs By " + label, "app.go('Groups/View Jobs By Group')");
					}
					nav_bar.aOnClick("View Membership By " + label, "app.go('Groups/View Membership By Group')");
					nav_bar.aOnClick("View Membership By Person", "app.go('Groups/View Membership By Person')");
				}
				if (r.db.exists(new Select(null).from("groups").where("NOT active"))) {
					nav_bar.divider();
					nav_bar.aOnClick("View Inactive " + label, "new Dialog({title:'Inactive " + getDisplayName(r) + "',url:context+'/" + apiURL("inactive groups") + "'}).open()");
				}
				nav_bar.dropdownClose();
			}
			private List<String[]>
			load(Request r) {
				Select select = new Select(m_allow_subgroups ? "id,name,parent_group" : "id,name").from("groups").where("active").orderBy("name");
				int people_id = r.getUser().getId();
				String where = "NOT private OR EXISTS(SELECT 1 FROM people_groups WHERE groups.id=groups_id AND people_id=" + people_id + ") OR EXISTS(SELECT 1 FROM groups_jobs WHERE groups.id=groups_id AND people_id=" + people_id + ")";
				return r.db.readRows(select.andWhere(where));
			}
		}, db);
		Site.site.addUserDropdownItem(new Page("Edit " + getDisplayName(null), this, false){
			@Override
			public void
			writeContent(Request r) {
				Site.site.newView("edit groups", r).writeComponent();
			}
		}.addRole("groups"));
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	adjustTables(DBConnection db) {
		JDBCTable table_def = new JDBCTable("groups")
			.add(new JDBCColumn("active", Types.BOOLEAN).setDefaultValue(true))
			.add(new JDBCColumn("calendar", Types.BOOLEAN))
			.add(new JDBCColumn("calendar_access_policy", Types.VARCHAR).setDefaultValue("group members"))
			.add(new JDBCColumn("calendar_public", Types.BOOLEAN).setDefaultValue(false))
			.add(new JDBCColumn("items", Types.VARCHAR))
			.add(new JDBCColumn("mandate", Types.VARCHAR))
			.add(new JDBCColumn("name", Types.VARCHAR))
			.add(new JDBCColumn("private", Types.BOOLEAN).setDefaultValue(false))
			.add(new JDBCColumn("role", Types.VARCHAR))
			.add(new JDBCColumn("self_joining", Types.BOOLEAN).setDefaultValue(true));
		if (m_allow_subgroups)
			table_def.add(new JDBCColumn("parent_group", Types.INTEGER));
		addJDBCColumns(table_def);
		JDBCTable.adjustTable(table_def, true, true, db);

		table_def = new JDBCTable("people_groups")
			.add(new JDBCColumn("groups"))
			.add(new JDBCColumn("people"));
		if (m_use_date_fields_for_people)
			table_def.add(new JDBCColumn("start", Types.DATE))
				.add(new JDBCColumn("end", Types.DATE))
				.add(new JDBCColumn("notes", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);

		table_def = new JDBCTable("groups_jobs")
			.add(new JDBCColumn("groups"))
			.add(new JDBCColumn("people"))
			.add(new JDBCColumn("job", Types.VARCHAR));
		if (m_use_date_fields_for_people)
			table_def.add(new JDBCColumn("start", Types.DATE))
				.add(new JDBCColumn("end", Types.DATE))
				.add(new JDBCColumn("notes", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);

		table_def = new JDBCTable("group_links")
			.add(new JDBCColumn("groups"))
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("url", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);

		table_def = new JDBCTable("group_requests")
			.add(new JDBCColumn("groups"))
			.add(new JDBCColumn("people"))
			.add(new JDBCColumn("_timestamp_", Types.TIMESTAMP))
			.add(new JDBCColumn("title", Types.VARCHAR))
			.add(new JDBCColumn("description", Types.VARCHAR));
		JDBCTable.adjustTable(table_def, true, false, db);
	}

	//--------------------------------------------------------------------------

	@ClassTask
	public final void
	cleanup(DBConnection db) {
		List<String> tables = db.getTables("group_%_events");
		for (String table : tables) {
			String group_id = table.substring(6, table.lastIndexOf('_'));
			if (!db.exists("groups", "id=" + group_id) || !db.lookupBoolean(new Select("calendar").from("groups").whereIdEquals(group_id)))
				db.dropTable(table);
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public final boolean
	doAPIGet(String[] path_segments, Request r) {
		if (super.doAPIGet(path_segments, r))
			return true;
		switch(path_segments[0]) {
		case "email people with jobs":
			emailPeopleWithJobs(r);
			return true;
		case "inactive groups":
			try (Rows rows = new Rows(new Select("id,name").from("groups").where("NOT active").orderBy("lower(name)"), r.db)) {
				while (rows.next())
					r.w.aOnClick(rows.getString(2), "app.go('Groups/" + rows.getInt(1) + "');Dialog.top().close()").br();
			}
			return true;
		}
		int group_id = Integer.parseInt(path_segments[0]);
		switch(path_segments[1]) {
		case "calendar":
			EventProvider calendar = (EventProvider)Site.site.getModule("Group" + group_id + "Calendar");
			if (calendar != null)
				new MonthView(calendar, r).writeComponent(r);
			return true;
		case "contacts":
			ViewState.setBaseFilter("group_contacts", "groups_id=" + group_id, r);
			Site.site.newView("group_contacts", r).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
			return true;
		case "decisions":
			r.setParameter("groups_id", group_id);
			Decisions d = (Decisions)Site.site.getModule("Decisions");
			NavBar nav_bar = d.writePageMenu(r);
			r.w.write("<div id=\"decisions_pane\" style=\"margin-top:10px\">");
			if (nav_bar == null || !nav_bar.checkClick(r))
				d.writePageContent(r);
			r.w.write("</div>");
			return true;
		case "documents":
			boolean allow_folders = ((Documents)Site.site.getModule("documents")).allowFolders();
			ViewState.setBaseFilter("group_documents", allow_folders ? "groups_id=" + group_id + " AND folder IS NULL" : "groups_id=" + group_id, r);
			ViewState.setQuicksearch("group_documents", null, null, r);
			Site.site.newView("group_documents", r).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
			return true;
		case "email contacts form":
			writeEmailContactsForm(r, r.w, group_id);
			return true;
		case "links":
			ViewState.setBaseFilter("group_links", "groups_id=" + group_id, r);
			ViewState.setQuicksearch("group_links", null, null, r);
			Site.site.newView("group_links", r).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
			return true;
		case "lists":
			((Lists)Site.site.getModule("Lists")).write(group_id, r);
			return true;
		case "mandate":
			r.w.write(r.db.lookupString(new Select("mandate").from("groups").whereIdEquals(group_id)));
			return true;
		case "minutes":
			ViewState.setBaseFilter("group_minutes", "groups_id=" + group_id, r);
			ViewState.setQuicksearch("group_minutes", null, null, r);
			Site.site.newView("group_minutes", r).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
			return true;
		case "people":
			Rows group = new Rows(new Select("*").from("groups").whereIdEquals(group_id), r.db);
			group.next();
			writeJobs(group_id, group, r);
			r.w.br();
			writeMembers(group_id, group, r);
			group.close();
			return true;
		case "requests":
			ViewState.setBaseFilter("group_requests", "groups_id=" + group_id, r);
			Site.site.newView("group_requests", r).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public final boolean
	doGet(boolean full_page, Request r) {
		if (super.doGet(full_page, r))
			return true;
		String segment_one = r.getPathSegment(1);
		if (segment_one == null)
			return false;
		switch (segment_one) {
		case "View Jobs By Group":
			viewJobsByGroup(full_page, r);
			return true;
		case "View Membership By Group":
			viewMembershipByGroup(full_page, r);
			return true;
		case "View Membership By Person":
			viewMembershipByPerson(full_page, r);
			return true;
		}
		int group_id;
		try {
			group_id = r.getPathSegmentInt(segment_one.equals("p") ? 2 : 1);
		} catch (NumberFormatException e) {
			return false;
		}
		if (group_id == 0)
			return false;
//		if (r.getPathSegment(2) == null) {
			writeGroupPage(group_id, full_page, r);
			return true;
//		}
//		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public boolean
	doPost(Request r) {
		if (super.doPost(r))
			return true;
		String segment_one = r.getPathSegment(1);
		switch(segment_one) {
		case "delete people":
			r.db.truncateTable("people_groups");
			r.db.truncateTable("groups_jobs");
			return true;
		case "rename_job":
			String from = r.getParameter("from");
			String to = r.getParameter("to");
			r.db.update("groups_jobs", "job=" + SQL.string(to), "job=" + SQL.string(from));
			int i = Array.indexOf(m_jobs, from);
			if (i != -1)
				m_jobs[i] = to;
			return true;
		case "reset items":
			NameValuePairs nvp = new NameValuePairs();
			nvp.set("items", new JSONBuilder().array(m_default_items).toString());
			r.db.update("groups", nvp, null);
			return true;
		}
		int group_id = r.getPathSegmentInt(1);
		String segment_two = r.getPathSegment(2);
		if (segment_two == null)
			return false;
		switch (segment_two) {
		case "email contacts":
			new Mail(r.getParameter("subject"), r.getParameter("message"))
				.from(new Group(group_id).getMailList(r.db)[1])
				.to(JSON.toArray(Json.parse(r.getParameter("to")).asArray()))
				.send();
			return true;
		case "job":
			int people_id = r.getInt("p", 0);
			if (people_id == 0) {
				r.db.delete("groups_jobs", "groups_id=" + group_id + " AND job=" + SQL.string(r.getParameter("j")), false);
				return true;
			}
			NameValuePairs nvp = new NameValuePairs();
			nvp.set("groups_id", group_id);
			nvp.set("job", r.getParameter("j"));
			String where = nvp.getWhereString("groups_jobs", r.db);
			nvp.setId("people_id", people_id);
			if (m_use_date_fields_for_people) {
				nvp.setIfNotNull("start", r.getParameter("s"));
				nvp.setIfNotNull("end", r.getParameter("e"));
				nvp.setIfNotNull("notes", r.getParameter("n"));
			}
			r.db.updateOrInsert("groups_jobs", nvp, where);
			return true;
		case "join":
			people_id = r.getPathSegmentInt(4);
			if (people_id == 0)
				people_id = r.getUser().getId();
			String job = r.getPathSegment(3);
			if (job != null) {
				nvp = new NameValuePairs();
				nvp.set("groups_id", group_id);
				nvp.set("people_id", people_id);
				nvp.set("job", job);
				if (m_use_date_fields_for_people)
					nvp.setDate("start", Time.newDate());
				r.db.insert("groups_jobs", nvp);
			} else if (!r.db.rowExists("people_groups", "people_id=" + people_id + " AND groups_id=" + group_id))
				r.db.insert("people_groups", "people_id,groups_id", people_id + "," + group_id);
			return true;
		case "leave":
			job = r.getPathSegment(3);
			if (job != null)
				r.db.delete("groups_jobs", "groups_id=" + group_id + " AND job=" + SQL.string(job), false);
			else
				r.db.delete("people_groups", "people_id=" + r.getUser().getId() + " AND groups_id=" + group_id, false);
			return true;
		default:
			if (Array.indexOf(s_items, segment_two) == -1)
				return false;
			Group group = new Group(group_id);
			if (!group.load(m_default_items, r.db))
				return false;
			String segment_three = r.getPathSegment(3);
			if (segment_three == null)
				return false;
			switch (segment_three) {
			case "hide":
				group.hideItem(segment_two, r.db);
				return true;
			case "show":
				group.showItem(segment_two, r.db);
				return true;
			}
		}
		return false;
	}

	//--------------------------------------------------------------------------

	private void
	emailPeopleWithJobs(Request r) {
		Rows rows;
		MultiMap<String> job_emails = new MultiMap<>();
		rows = new Rows(new Select("job,email").from("groups").joinOn("groups_jobs", "groups.id=groups_id").joinOn("people", "people_id=people.id").where("groups.active AND email IS NOT NULL"), r.db);
		while (rows.next())
			job_emails.put(rows.getString(1), rows.getString(2));
		rows.close();
		for (String job : job_emails.keySet()) {
			List<String> emails = job_emails.get(job);
			r.w.tagOpen("p");
			r.w.setAttribute("emails", Text.join(",", emails))
				.setAttribute("onclick", "clicked()")
				.ui.checkbox(job, job + " (" + emails.size() + " " + Text.pluralize("person", emails.size()) + ")", null, false, false, false)
				.tagClose();
		}
		r.w.setId("compose").ui.aButton("Compose Message", "javascript:Dialog.alert(null,'Please select one or more jobs.')");
		r.w.js("""
			function clicked() {
				var emails = new Set()
				_.$('input[type="checkbox"]').forEach(function(cb) {
					if (cb.checked)
						cb.getAttribute('emails').split(',').forEach(function(e){emails.add(e)})
				})
				var addresses = Array.from(emails.values()).join(',')
				_.$('#compose').href = addresses ? 'mailto:' + addresses : "javascript:Dialog.alert(null,'Please select one or more jobs.')"
			}""");
	}

	//--------------------------------------------------------------------------

	static Addresses
	getEmailAddresses(int group_id, String and, DBConnection db) {
		Addresses addresses = new Addresses();
		MultiMap<Integer> jobs_people = getJobsPeople(group_id, db);
		for (String job : jobs_people.keySet()) {
			List<Integer> people = jobs_people.get(job);
			for (Integer people_id : people) {
				String email_address = db.lookupString(new Select("email").from("people").whereIdEquals(people_id));
				if (email_address != null)
					addresses.add(email_address);
			}
		}
		Select query = new Select("email").from("people").joinOn("people_groups", "people.id=people_groups.people_id").where("email IS NOT NULL AND groups_id=" + group_id);
		if (and != null)
			query.andWhere(and);
		return addresses.add(db.readValues(query));
	}

	//--------------------------------------------------------------------------

	public static Set<String>
	getGroupEmails(int id, DBConnection db) {
		Set<String> emails = new HashSet<>();
		Rows rows = new Rows(new Select("email").from("people").join("people", "people_groups").whereEquals("groups_id", id).andWhere("email IS NOT NULL"), db);
		while (rows.next())
			emails.add(rows.getString(1));
		rows.close();
		rows = new Rows(new Select("email").from("groups_jobs").joinOn("people", "people_id=people.id").whereEquals("groups_id", id).andWhere("email IS NOT NULL"), db);
		while (rows.next())
			emails.add(rows.getString(1));
		rows.close();
		return emails;
	}

	//--------------------------------------------------------------------------

	public final List<String>
	getGroups(int people_id, DBConnection db) {
		return db.readValues(new Select("name").from("groups").join("groups", "people_groups").whereEquals("people_id", people_id).andWhere("active"));
	}

	//--------------------------------------------------------------------------
	// return list of group names and jobs for a person

	public final List<String[]>
	getGroupsJobs(int people_id, DBConnection db) {
		ArrayList<String[]> group_jobs = new ArrayList<>();
		Rows rows = new Rows(new Select("name,job").from("groups").joinOn("groups_jobs", "groups.id=groups_id").where("people_id=" + people_id), db);
		while (rows.next())
			group_jobs.add(new String[] { rows.getString(1), rows.getString(2) });
		rows.close();
		return group_jobs;
	}

	//--------------------------------------------------------------------------

	static MultiMap<Integer>
	getJobsPeople(int group_id, DBConnection db) {
		MultiMap<Integer> jobs_people = new MultiMap<>();
		Rows rows = new Rows(new Select("job,people_id").from("groups_jobs").whereEquals("groups_id", group_id), db);
		while (rows.next())
			jobs_people.put(rows.getString(1), rows.getInt(2));
		rows.close();
		return jobs_people;
	}

	//--------------------------------------------------------------------------

	static boolean
	hasJob(int group_id, int people_id, DBConnection db) {
		return db.exists(new Select().from("groups_jobs").whereEquals("groups_id", group_id).andWhere("people_id=" + people_id));
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	init(boolean was_added, DBConnection db) {
		super.init(was_added, db);
		db.createManyToManyLinkTable("people", "groups");
		Rows groups = new Rows(new Select("id,name,calendar_public,calendar_access_policy").from("groups").where("active AND calendar"), db);
		String[] colors = new String[] { "#3D9970", "#2ECC40", "#01FF70", "#AAAAAA", "#DDDDDD" };
		int num = 0;
		while (groups.next()) {
			EventProvider event_provider = new GroupEventProvider(groups.getString(2), groups.getInt(1), groups.getBoolean(3), groups.getString(4), db);
			Site.site.addModule(event_provider, db);
			if (event_provider.getColor() == null)
				event_provider.setColor(colors[num++ % colors.length]);
		}
		groups.close();
		Site.site.addModule(new Comments("group_requests"), db);
		Roles.add("groups", "can edit the groups with this role");
	}

	//--------------------------------------------------------------------------

	public static boolean
	isActive(int group_id, DBConnection db) {
		return db.lookupBoolean(new Select("active").from("groups").whereIdEquals(group_id));
	}

	//--------------------------------------------------------------------------

	public static boolean
	isMember(int group_id, int people_id, DBConnection db) {
		if (hasJob(group_id, people_id, db))
			return true;
		return db.rowExists("people_groups", "people_id=" + people_id + " AND groups_id=" + group_id);
	}

	//--------------------------------------------------------------------------

	static boolean
	isMemberOfSubgroup(int group_id, int people_id, DBConnection db) {
		if (!s_module.m_allow_subgroups)
			return false;
		return s_module.isMemberOfSubgroup(group_id, people_id, db, new HashSet<>());
	}

	//--------------------------------------------------------------------------

	private boolean
	isMemberOfSubgroup(int group_id, int people_id, DBConnection db, Set<String> seen) {
		List<String> ids = db.readValues(new Select("id").from("groups").where("parent_group=" + group_id));
		for (String id : ids)
			if (!seen.contains(id)) {
				seen.add(id);
				int parent_group_id = Integer.parseInt(id);
				if (isMember(parent_group_id, people_id, db))
					return true;
				if (isMemberOfSubgroup(parent_group_id, people_id, db, seen))
					return true;
			} else
				Site.site.log("parent group id " + id + " already seen", false);

		return false;
	}

	//--------------------------------------------------------------------------

	@Override
	public ViewDef
	_newViewDef(String name) {
		switch (name) {
		case "edit groups":
			ViewDef view_def = new ViewDef(name) {
				private void
				addEventProvider(String group_name, int group_id, boolean calendar_public, String access_policy, Request r) {
					EventProvider ep = new GroupEventProvider(group_name, group_id, calendar_public, access_policy, r.db);
					Site.site.addModule(ep, r.db);
					ep.store(r.db);
				}
				@Override
				public void
				afterInsert(int id, NameValuePairs nvp, Request r) {
					if (nvp.getBoolean("active") && nvp.getBoolean("calendar"))
						addEventProvider(nvp.getString("name"), id, nvp.getBoolean("calendar_public"), nvp.getString("calendar_access_policy"), r);
					super.afterInsert(id, nvp, r);
				}
				@Override
				public String
				beforeDelete(String where, Request r) {
					String calendar_name = "Group" + where.substring(3) + "Calendar"; // assuming where starts with id=
					app.Module ep = Site.site.getModule(calendar_name);
					if (ep != null)
						ep.remove(r.db);
					return super.beforeDelete(where, r);
				}
				@Override
				public String
				beforeUpdate(int id, NameValuePairs nvp, Map<String, Object> previous_values, Request r) {
					if (!nvp.getBoolean("active") || !nvp.getBoolean("calendar")) {
						app.Module ep = Site.site.getModule("Group" + id + "Calendar");
						if (ep != null)
							ep.remove(r.db);
					}
					if (!nvp.getBoolean("active") && r.db.lookupBoolean(new Select("active").from("groups").whereIdEquals(id))) {
						r.db.delete("people_groups", "groups_id", id, false);
						r.db.delete("groups_jobs", "groups_id=" + id, false);
						List<String> mail_list_ids = r.db.readValues(new Select("id").from("mail_lists").where("send_to='Group:" + id + "' AND active"));
						for (String mail_list_id : mail_list_ids)
							r.db.update("mail_lists", "active=false", "id=" + mail_list_id);
					}
					if (nvp.getBoolean("active") && nvp.getBoolean("calendar")) {
						GroupEventProvider ep = (GroupEventProvider)Site.site.getModule("Group" + id + "Calendar");
						if (ep == null)
							addEventProvider(nvp.getString("name"), id, nvp.getBoolean("calendar_public"), nvp.getString("calendar_access_policy"), r);
						else {
							ep.setAccessPolicy("group members".equals(nvp.getString("calendar_access_policy")) ? new GroupAccessPolicy(id).add().delete().edit() : null);
							ep.setCalendarPublic(nvp.getBoolean("calendar_public"));
							ep.setDisplayName(nvp.getString("name"));
							ep.store(r.db);
						}
					}
					return super.beforeUpdate(id, nvp, previous_values, r);
				}
			}	.setDefaultOrderBy("lower(name)")
				.setFrom("groups")
				.setRecordName(Text.singularize(getDisplayName(null)), true)
				.setColumn(new Column("calendar").setSubColumns(true, "calendar_public", "calendar_access_policy"))
				.setColumn(new OptionsColumn("calendar_access_policy", "group members", "event poster").setDisplayName("who can edit events"))
				.setColumn(new Column("calendar_public").setDisplayName("show calendar to community").setHelpText("Otherwise only group members can view the calendar"))
				.setColumn(new TagsColumn("items", s_items).setDefaultValue(m_default_items).setHelpText("calendar, email, list, and mandate will only be displayed when available"))
				.setColumn(new TextAreaColumn("mandate").setDisplayName(m_mandate_label).setIsRichText(true, false))
				.setColumn(new Column("name").setIsRequired(true))
				.setColumn(new RolesColumn("role"))
				.setColumn(new Column("self_joining").setDisplayName("allow people to join/leave the group themselves"))
				.addRelationshipDef(new ManyToMany("edit groups people", "people_groups", "first,last"))
				.addRelationshipDef(new OneToMany("edit documents"))
				.addRelationshipDef(new OneToMany("groups_jobs"))
				.addRelationshipDef(new OneToMany("group_links"))
				.addRelationshipDef(new OneToMany("minutes"));
			if (m_allow_subgroups)
				view_def.setColumn(new LookupColumn("parent_group", "groups", "name").setAllowNoSelection(true));
			ArrayList<String> column_names = new ArrayList<>();
			column_names.add("name");
			column_names.add("active");
			if (m_allow_subgroups)
				column_names.add("parent_group");
			view_def.setColumnNamesTable(column_names);
			column_names.add(2, "private");
			column_names.add(3, "self_joining");
			column_names.add("mandate");
			column_names.add("calendar");
//			column_names.add("calendar_public");
//			column_names.add("calendar_access_policy");
			column_names.add("items");
			column_names.add("role");
			view_def.setColumnNamesForm(column_names);
			return view_def;
		case "edit groups people":
			return new ViewDef(name)
				.setAccessPolicy(new GroupAccessPolicy(0).add().delete())
				.setDefaultOrderBy("first,last")
				.setFrom("people")
				.setRecordName("Member", true)
				.setColumnNamesTable(new String[] { "person" })
				.setColumn(new Column("person").setValueRenderer(new MultiColumnRenderer(new String[] { "first", "last" }, true, false), false));
		case "edit people groups":
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy(){
					@Override
					public boolean
					showDeleteButtonForRow(Rows data, Request r) {
						return r.userIsAdmin() || data.getBoolean("self_joining");
					}
				}.add().delete())
				.setAddButtonText(Text.join)
				.setDefaultOrderBy("lower(name)")
				.setDeleteButtonText(Text.leave)
				.setFrom("groups")
				.setRecordName("Group", true)
//				.setShowColumnHeads(false)
				.setColumnNamesTable(new String[] { "name" });
		case "group_contacts":
			view_def = Site.site.getModule("Contacts")._newViewDef("contacts")
				.addFeature(new Button(Location.TOP_LEFT, null, null) {
					@Override
					public boolean
					write(Location location, View v, Request r) {
						int group_id = r.getPathSegmentInt(2);
						if (group_id == 0)
							return false;
						Group g = new Group(group_id);
						if (g.getMailList(r.db) == null || r.db.countRows("contacts", "groups_id=" + group_id + " AND email IS NOT NULL") == 0)
							return false;
						if (location == m_location) {
							r.w.ui.buttonOnClick("Email Contacts", "new Dialog({cancel:true,modal:true,title:'Send EMail to Contacts',url:context+'/" + apiURL(Integer.toString(group_id), "email contacts form") + "'}).open()");
							return true;
						}
						return false;
					}
				})
				.setName(name)
				.setShowDoneLink(true)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setDefaultToSessionAttribute().setDisplayName("group").setViewRole("admin"));
			view_def.setColumnNamesForm(Array.concat(view_def.getColumnNamesForm(), "groups_id"));
			return view_def;
		case "group_documents":
			if (m_documents_module == null) {
				m_documents_module = (Documents)Site.site.getModule("Documents");
				Site.site.subscribe(m_documents_module, this);
			}
			view_def = m_documents_module._newViewDef("documents")
				.addFeature(new ColumnFilter("type", "Type", Location.TOP_LEFT).setAllowNone(true).setFilter("kind='f' OR type"))
				.addFeature(new TSVectorSearch(null, Location.TOP_RIGHT))
				.setAccessPolicy(new GroupAccessPolicy(0).add().delete().edit())
				.setName(name)
				.setShowDoneLink(true)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDefaultToSessionAttribute().setDisplayName("Group").setViewRole("admin"))
				.setColumn(new Column("show_on_main_docs_page") {
						@Override
						public boolean
						userCanView(View.Mode mode, Rows data, Request r) {
							return mode == Mode.ADD_FORM ? !"folder".equals(r.getParameter("kind")) : !"f".equals(data.getString("kind"));
						}
					});
			if (m_documents_module.allowFolders())
				((FolderColumn)view_def.getColumn("folder")).setAndWhereColumn("groups_id");
			return view_def;
		case "groups_jobs":
			return new ViewDef(name)
				.setRecordName("job", true)
				.setColumn(new PersonColumn("people_id", false));
		case "group_links":
			return new ViewDef(name)
				.setAccessPolicy(new GroupAccessPolicy(0).add().delete().edit())
				.setDefaultOrderBy("title")
				.setRecordName("Link", true)
				.setShowDoneLink(true)
				.setTimestampRecords(true)
				.setColumnNamesTable(new String[] { "link" })
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDefaultToSessionAttribute().setDisplayName("Group").setIsHidden(true).setViewRole("admin"))
				.setColumn(new Column("link").setValueRenderer(new LinkValueRenderer("$(url)", "$(title)").setTarget("_blank"), false));
		case "group_lists":
			return Site.site.getModule("Lists")._newViewDef("lists")
				.setAccessPolicy(new AccessPolicy().view())
				.setName(name)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDefaultToSessionAttribute().setDisplayName("Group").setViewRole("admin"));
		case "group_minutes":
			AccessPolicy access_policy = new GroupAccessPolicy(0).add().delete().edit();
			if (((Minutes)Site.site.getModule("Minutes")).supportsLocking())
				access_policy = new And(new LockedAccessPolicy().add().delete().edit().view(), access_policy);
			return Site.site.getModule("Minutes")._newViewDef("minutes")
				.setAccessPolicy(access_policy)
				.setName(name)
				.setShowDoneLink(true)
				.setViewClass(GroupMinutesView.class)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setAllowNoSelection(true).setDefaultToSessionAttribute().setDisplayName("Group").setIsHidden(true).setViewRole("admin"));
		case "group_requests":
			return Comments.addFormHook(new ViewDef(name)
				.setAccessPolicy(new Or(new AccessPolicy().view(), new RecordOwnerAccessPolicy().add().delete().edit()))
				.setDefaultOrderBy("title")
				.setRecordName("Request", true)
				.setViewLinkColumn("title")
				.setColumnNamesForm("groups_id", "title", "description", "_timestamp_", "people_id")
				.setColumnNamesTable("title", "people_id", "_timestamp_")
				.setColumn(new TextAreaColumn("description").setIsRichText(true, false))
				.setColumn(new LookupColumn("groups_id", "groups", "name").setDefaultToSessionAttribute().setDisplayName("group").setViewRole("admin"))
				.setColumn(new Column("_timestamp_").setDisplayName("added").setHideOnForms(Mode.ADD_FORM).setIsReadOnly(true))
				.setColumn(new Column("title").setIsRequired(true))
				.setColumn(new PersonColumn("people_id", false).setDefaultToUserId().setDisplayName("posted by").setIsReadOnly(true))
			);
		case "members":
			return new ViewDef(name)
				.setAccessPolicy(new GroupAccessPolicy(0).add().edit().delete())
				.setDefaultOrderBy("first,last")
				.setDeleteButtonText("remove")
				.setFrom("people_groups")
				.setRecordName("Member", true)
				.setSelectFrom("people_groups JOIN people ON people.id=people_id")
//				.setShowColumnHeads(false)
				.setShowDoneLink(true)
				.setColumnNamesForm(m_use_date_fields_for_people ? new String[] { "groups_id", "people_id", "start", "end", "notes" } : new String[] { "groups_id", "people_id" })
				.setColumnNamesTable(m_use_date_fields_for_people ? new String[] { "people_id", "start", "end", "notes" } : new String[] { "people_id" })
				.setColumn(new LookupColumn("groups_id", "groups", "name").setDefaultToSessionAttribute().setDisplayName("Group").setIsHidden(true).setViewRole("admin"))
				.setColumn(new PersonColumn("people_id", true).setDisplayName("person"));
		case "people_groups":
			return new ViewDef(name)
				.setColumn(new LookupColumn("groups_id", "groups", "name").setFilter(new Filter(){
					@Override
					public boolean
					accept(Rows rows, Request r) {
						return rows.getBoolean("active") && rows.getBoolean("self_joining");
					}
				}).setDisplayName("group"))
				.setColumnNamesTableAndForm("people_id")
				.setColumn(new PersonColumn("people_id", false));
		case "report groups":
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy())
				.setAllowSorting(false)
				.setBaseFilter("active")
				.setDefaultOrderBy("lower(name)")
				.setFrom("groups")
				.setShowHead(false)
				.setColumnNamesTable(new String[] { "name", "people" })
				.setColumn(new Column("name").setDisplayName("group"))
				.setColumn(new Column("people"){
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						Set<String> people = new TreeSet<>();
						List<String[]> members = r.db.readRows(new Select("first,last").from("people_groups JOIN people ON people.id=people_groups.people_id").whereEquals("groups_id", v.data.getInt("id")).orderBy("first,last"));
						for (String[] person : members)
							people.add(Text.join(" ", person[0], person[1]));
						MultiMap<Integer> jobs_people = getJobsPeople(v.data.getInt("id"), r.db);
						for (String job : jobs_people.keySet()) {
							ArrayList<Integer> people_ids = jobs_people.get(job);
							for (Integer people_id : people_ids) {
								String person = Site.site.lookupName(people_id, r.db);
								if (person != null) {
									people.remove(person);
									people.add(person + " (" + job + ")");
								}
							}
						}
						r.w.writeList(people, ", ");
						return true;
					}
				});
		case "report people":
			return new ViewDef(name)
				.setAccessPolicy(new AccessPolicy())
				.setAllowSorting(false)
				.setBaseFilter("active")
				.setDefaultOrderBy("first,last")
				.setFrom("people")
				.setShowHead(false)
				.setColumnNamesTable(new String[] { "person", "groups" })
				.setColumn(new Column("groups"){
					@Override
					public boolean
					writeValue(View v, Map<String,Object> data, Request r) {
						String person_id = v.data.getString("id");
						String on = "groups.id=people_groups.groups_id AND people_id=" + person_id + " OR EXISTS(SELECT 1 FROM groups_jobs WHERE groups.id=groups_id AND groups_jobs.people_id=" + person_id + ")";
						List<String> groups = r.db.readValues(new Select("name").distinct().from("people_groups JOIN groups ON " + on).where("groups.active").orderBy("name"));
						boolean first = true;
						for (String group : groups) {
							if (first)
								first = false;
							else
								r.w.write(", ");
							r.w.write(group);
						}
						return true;
					}
				})
				.setColumn(new Column("person").setValueRenderer(new MultiColumnRenderer(new String[] { "first", "last" }, true, false), false));
		}
		return null;
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	publish(Object object, Message message, Object data, DBConnection db) {
		if (object == m_documents_module && message == Message.UPDATE)
			Site.site.removeViewDef("group_documents");
	}

	//--------------------------------------------------------------------------

	public final void
	removePerson(int id, DBConnection db) {
		db.delete("people_groups", "people_id", id, false);
		db.delete("groups_jobs", "people_id=" + id, false);
	}

	//--------------------------------------------------------------------------

	@ClassTask({"from", "to"})
	public final void
	renameJob(String from, String to, DBConnection db) {
		db.update("groups_jobs", "job=" + SQL.string(to), "job=" + SQL.string(from));
		for (int i=0; i<m_jobs.length; i++)
			if (m_jobs[i].equals(from)) {
				m_jobs[i] = to;
				break;
			}
		store(db);
	}

	//--------------------------------------------------------------------------

	private void
	viewJobsByGroup(boolean full_page, Request r) {
		Rows rows;
		if (full_page)
			Site.site.pageOpen(getDisplayName(r), true, r);
		else
			r.w.ui.containerOpen();
		r.w.h2("View Jobs By " + Text.singularize(getDisplayName(r)));
		Table table = r.w.ui.table().addDefaultClasses().th("Group");
		Map<String,MultiMap<Integer>> gjp = new TreeMap<>();
		Set<String> all_jobs = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
		rows = new Rows(new Select("name,job,people_id").from("groups").joinOn("groups_jobs", "groups.id=groups_id").where("active"), r.db);
		while (rows.next()) {
			String group = rows.getString(1);
			MultiMap<Integer> jobs = gjp.get(group);
			if (jobs == null) {
				jobs = new MultiMap<>();
				gjp.put(group, jobs);
			}
			String job = rows.getString(2);
			all_jobs.add(job);
			jobs.put(job, rows.getInt(3));
		}
		rows.close();
		String[] columns = all_jobs.toArray(new String[all_jobs.size()]);
		for (String job : columns)
			table.th(job);
		for (String group : gjp.keySet()) {
			table.tr().td(group);
			MultiMap<Integer> jobs = gjp.get(group);
			for (String column : columns) {
				table.td();
				List<Integer> people = jobs.get(column);
				if (people != null) {
					boolean first = true;
					for (Integer people_id : people) {
						Person person = (Person)Site.site.newPerson(people_id, r.db);
						if (person != null) {
							if (first)
								first = false;
							else
								r.w.write(", ");
							person.write(false, 32, false, r);
						}
					}
				} else
					r.w.nbsp();
			}
		}
		table.close();
		if (full_page)
			r.close();
	}

	//--------------------------------------------------------------------------

	private void
	viewMembershipByGroup(boolean full_page, Request r) {
		if (full_page)
			Site.site.pageOpen(getDisplayName(r), true, r);
		else
			r.w.ui.containerOpen();
		r.w.h2("View Membership By " + Text.singularize(getDisplayName(r)));
		Site.site.newView("report groups", r).write();
		if (full_page)
			r.close();
	}

	//--------------------------------------------------------------------------

	private void
	viewMembershipByPerson(boolean full_page, Request r) {
		Rows rows;
		if (full_page)
			Site.site.pageOpen(getDisplayName(r), true, r);
		else
			r.w.ui.containerOpen();
		r.w.h2("View Membership By Person");
		Site.site.newView("report people", r).write();
		rows = new Rows(new Select("c,COUNT(c)").from(
			"(SELECT COUNT(*) AS c FROM "
			+ "(SELECT DISTINCT people_id,groups.id FROM people_groups JOIN groups ON groups.id=people_groups.groups_id OR EXISTS(SELECT 1 FROM groups_jobs WHERE groups_jobs.groups_id=groups.id AND groups_jobs.people_id=people_groups.people_id) WHERE groups.active ORDER BY people_id,groups.id) a "
			+ "GROUP BY people_id ORDER BY people_id) b")
			.groupBy("c").orderBy("c"), r.db);
		while (rows.next()) {
			int num_groups = rows.getInt(1);
			int num_people = rows.getInt(2);
			r.w.write("<div>" + num_people + " " + (num_people == 1 ? "person is a member of " : "people are members of ") + num_groups + (num_groups == 1 ? " group" : " groups") + "</div>");
		}
		rows.close();
		if (full_page)
			r.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeEmailContactsForm(Request r, HTMLWriter w, int group_id) {
		Form f = new Form(null, Site.context + "/Groups/" + group_id + "/email contacts", w).setButtonsLocation(Form.Location.NONE).open();
		f.rowOpen("from");
		Group g = new Group(group_id);
		w.write("<div>" + Site.site.getEmailAddress(g.getMailList(r.db)[1]) + "</div>");
		f.rowOpen("to");
		String[] recipients = g.getMailListRecipients(r.db);
		w.setAttribute("required", "required");
		new Tags("to", recipients, recipients).setAllowDragging(true).setRestrictToChoices(true).write(w);
		f.rowOpen("subject");
		w.setAttribute("required", "required").textInput("subject", 0, null);
		f.rowOpen("message");
		w.setAttribute("required", "required");
		new RichText().write("message", null, "textarea", true, "full", w);
		f.close();
		w.js("Dialog.top().add_ok('send',function(){_.form.send(this.body.querySelector('form'),function(){Dialog.top().close();Dialog.alert(null,'Email sent!')})})");
	}

	//--------------------------------------------------------------------------

	final void
	writeGroup(String[] group, List<String[]> groups, HTMLWriter w) {
		w.write("[").jsString(group[1]).comma().write("function(){app.go('Groups/" + group[0] + "')}");
		if (m_allow_subgroups) {
			boolean first = true;
			for (String[] g : groups)
				if (group[0].equals(g[2])) {
					if (first) {
						first = false;
						w.write(",[");
					} else
						w.comma();
					writeGroup(g, groups, w);
				}
			if (!first)
				w.write("]");
		}
		w.write("]");
	}

	//--------------------------------------------------------------------------

	final void
	writeGroupPage(int group_id, boolean full_page, Request r) {
		HTMLWriter w = r.w;
		w.js("var _menus=[['#top_menu','Groups/" + group_id + "']]");
		if (full_page)
			Site.site.pageOpen(getDisplayName(r), true, r);
		else
			w.ui.containerOpen();
		Group group = new Group(group_id);
		if (!group.load(m_default_items, r.db))
			return;
		app.Person user = r.getUser();
		if (user != null)
			r.setSessionAttribute("current page", "Group " + group_id);
		w.js("JS.get('documents-min')");
		w.write("<div><h2 style=\"display:inline-block\">");
		w.write(group.getName());
		w.write("</h2>");
		Group g = new Group(group_id);
		String[] mail_list = g.getMailList(r.db);
		if (m_show_email_link)
			if (mail_list != null) {
				String domain = Site.settings.getString("mail list domain override");
				mail_list[1] = domain == null ? Site.site.getEmailAddress(mail_list[1]) : mail_list[1] + "@" + domain;
				w.addStyle("margin-left:20px")
					.aOpen("mailto:" + mail_list[1]);
				w.ui.icon("envelope").nbsp().write(mail_list[1]).tagClose();
			} else {
				Addresses emails = getEmailAddresses(group_id, null, r.db);
				if (emails.size() > 0) {
					StringBuilder s = new StringBuilder();
					for (String email : emails) {
						if (s.length() > 0)
							s.append(',');
						s.append(email);
					}
					w.addStyle("margin-left:20px")
						.aOpen("mailto:" + s.toString());
					w.ui.icon("envelope").nbsp().write("Email Group Members").tagClose();
				}
			}
		w.write("</div>");
		writeItems(group_id, group, mail_list, r);
		String type = r.getParameter("type");
		if (type != null)
			ViewState.setFilter("group_documents", "type='" + type + "'", r);
		String item = r.getParameter("item");
		if (item != null) {
			w.write("<script>_.$('#group_items').nav.click('").write(item).write("'");
			String tab = r.getParameter("tab");
			if (tab != null)
				w.write(",function(){_.$('#group_documents-tabs').tabs.show('" + tab + "')}");
			w.write(")</script>");
		}
		r.setSessionInt("groups_id", group_id); // used by GroupAccessPolicy and GroupJobAccessPolicy
		if (full_page)
			r.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeItems(int group_id, Group group, String[] mail_list, Request r) {
		ButtonNav bn = new ButtonNav("group_items", 2, r);
		bn.setOneDiv(true);
		if (r.userIsAdmin() || isMember(group_id, r.getUser().getId(), r.db))
			bn.allowToggling(group.getName(), "/Groups/" + group_id);
		for (String item : s_items) {
			String text = item.equals("mandate") ? m_mandate_label.toLowerCase() : item;
			String url;
			if (item.equals("email") && mail_list != null)
				url = "api/MailLists/" + mail_list[0] + "/archive";
			else if (item.equals("mandate"))
				url = "api/Groups/" + group_id + "/mandate";
			else
				url = apiURL(Integer.toString(group_id), text);
			Panel panel = new Panel(text, null, url);
			if (!group.itemIsShown(item))
				panel.hidden = true;
			else
				switch(item) {
				case "calendar":
					if (!r.db.lookupBoolean(new Select("calendar AND active").from("groups").whereIdEquals(group_id)))
						panel.hidden = true;
					break;
				case "email":
					if (mail_list == null)
						panel.hidden = true;
					break;
				case "lists":
					if (!r.db.exists(new Select("1").from("lists").whereEquals("groups_id", group_id)))
						panel.hidden = true;
					break;
				case "mandate":
					String mandate = r.db.lookupString(new Select("mandate").from("groups").whereIdEquals(group_id));
					if (mandate == null || mandate.length() == 0)
						panel.hidden = true;
					break;
				case "people":
					if (!r.db.lookupBoolean(new Select("active").from("groups").whereIdEquals(group_id)))
						panel.hidden = true;
				}
			bn.add(panel);
		}
		bn.close();
	}

	//--------------------------------------------------------------------------

	private void
	writeJob(String job, int group_id, ui.Select select, boolean self_joining, Table table, Request r) {
		Object[] row = r.db.readRowObjects(new Select(m_use_date_fields_for_people ? "people_id,start,\"end\",notes" : "people_id").from("groups_jobs").whereEquals("groups_id", group_id).andWhere("job=" + SQL.string(job)));
		int people_id = 0;
		if (row != null)
			people_id = row[0] == null ? 0 : ((Integer)row[0]).intValue();
		String job_label = job.replace('_', ' ');
		table.tr().td(job_label).td();
		if (select != null)
			select.setSelectedOption(null, people_id != 0 ? Integer.toString(people_id) : null)
				.write(r.w);
		else if (people_id == 0) {
			if (self_joining || r.userIsAdmin())
				r.w.ui.buttonOnClick("Sign up as " + job_label, actionURL(group_id, "join/" + HTMLWriter.URIEncode(job)));
		} else {
			app.Person person = Site.site.newPerson(people_id, r.db);
			if (person != null)
				person.write(false, 32, false, r);
			if (r.getUser() != null && people_id == r.getUser().getId() && (self_joining || r.userIsAdmin()))
				r.w.nbsp().nbsp().nbsp().ui.buttonOnClick("Remove me as " + job_label, actionURL(group_id, "leave/" + HTMLWriter.URIEncode(job)));
		}
		if (m_use_date_fields_for_people) {
			LocalDate start = null;
			LocalDate end = null;
			String notes = null;
			if (row != null) {
				start = (LocalDate)row[1];
				end = (LocalDate)row[2];
				notes = (String)row[3];
			}
			if (select != null && people_id != 0 || r.getUser().getId() == people_id) {
				table.td();
				r.w.dateInput(null, start, "function(d,s,i){set_job(i.input," + group_id + "," + people_id + ")}", false, true);
				table.td();
				r.w.dateInput(null, end, "function(d,s,i){set_job(i.input," + group_id + "," + people_id + ")}", false, true);
				table.td();
				r.w.setAttribute("onfocus", "_.ui.ok_cancel(this.parentNode, set_job.bind(this,this," + group_id + "," + people_id + "))")
					.textInput(null, 0, notes);
			} else
				table.td(start != null ? Time.formatter.getDateShort(start) : "&nbsp;")
					.td(end != null ? Time.formatter.getDateShort(end) : "&nbsp;")
					.td(notes != null ? notes : "&nbsp;");
		}
	}

	//--------------------------------------------------------------------------

	private void
	writeJobs(int group_id, Rows group, Request r) {
		if (m_jobs == null)
			return;
		ui.Select select = null;
		if (m_job_holders_can_sign_up_others && hasJob(group_id, r.getUser().getId(), r.db) || r.userIsAdmin())
			select = new RowsSelect(null, new Select("id,first,last").from("people").where("active").orderBy("first,last"), "first,last", "id", r)
				.setAllowNoSelection(true)
				.setOnChange("set_job(this," + group_id + ",this.options[this.selectedIndex].value)");
		Table table = r.w.ui.table().addDefaultClasses().th("Job").th("Person");
		if (m_use_date_fields_for_people)
			table.th("Start").th("End").th("Notes");
		for (String job : m_jobs)
			writeJob(job, group_id, select, group.getBoolean("self_joining"), table, r);
		table.close();
		r.w.js("""
			function set_job(e,g,p){
				let c=e.closest('tr').cells
				let u='j='+encodeURIComponent(c[0].innerText)+'&p='+p
				if(c.length>2){
					let s=c[2].querySelector('input[type="hidden"]')
					if(s){
						e=c[3].querySelector('input[type="hidden"]')
						u+='&s='+s.value+'&e='+e.value+'&n='+encodeURIComponent(c[4].firstChild.value)
					}
				}
				net.post(context+'/Groups/'+g+'/job',u,function(){_.ui.ok_cancel_remove();_.$('#group_items').button_nav.refresh()})
			}
			""");
	}

	//--------------------------------------------------------------------------

	private void
	writeMembers(int group_id, Rows groups, Request r) {
		boolean self_joining = groups.getBoolean("self_joining") && !r.userIsGuest();
		HTMLWriter w = r.w;
		if (self_joining)
			w.write("<div style=\"display:table;\">");
		ViewState.setBaseFilter("members", "groups_id=" + group_id, r);
		Site.site.newView("members", r).setMode(View.Mode.READ_ONLY_LIST).writeComponent();
		if (self_joining) {
			w.write("<hr style=\"margin:2px 0;\" />");
			if (r.db.rowExists("people_groups", "people_id=" + r.getUser().getId() + " AND groups_id=" + group_id))
				w.ui.buttonOnClick("Leave Group", actionURL(group_id, "leave"));
			else if (!isMember(group_id, r.getUser().getId(), r.db))
				w.ui.buttonOnClick("Join Group", actionURL(group_id, "join"));
			w.write("</div>");
		}
	}

	//--------------------------------------------------------------------------

	@Override
	public final void
	writeSettingsForm(String[] field_names, boolean in_dialog, boolean hide_active, Request r) {
		super.writeSettingsForm(field_names, in_dialog, hide_active, r);
		r.w.ui.buttonOnClick("Clear members from all groups", "net.post(context+'/Groups/delete people',null,function(){Dialog.alert(null,'members cleared')})");
	}

	//--------------------------------------------------------------------------

	@Override
	protected final void
	writeSettingsInput(Field field, Request r) {
		super.writeSettingsInput(field, r);
		if ("m_default_items".equals(field.getName()))
			r.w.ui.buttonOnClick("Reset all groups to default items",
				"net.post(context+'/Groups/reset items',null,function(){Dialog.alert('Reset Group Items','done')})");
	}
}