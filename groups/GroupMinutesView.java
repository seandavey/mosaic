package groups;

import app.Request;
import db.Select;
import db.ViewDef;
import db.ViewState;
import db.feature.TSVectorSearch;
import minutes.MinutesView;
import util.Text;

public class GroupMinutesView extends MinutesView {
	private int				m_current_group = -1;
	private final boolean	m_show_group_headers;

	//--------------------------------------------------------------------------

	public
	GroupMinutesView(ViewDef view_def, Request r) {
		super(view_def, r);
		ViewState state = (ViewState)r.getOrCreateState(ViewState.class, view_def.getName());
		String base_filter = state.getBaseFilter();
		if (base_filter != null)
			m_show_group_headers = base_filter.startsWith("groups_id IS NULL");
		else
			m_show_group_headers = false;
	}

	//--------------------------------------------------------------------------

	@Override
	protected void
	writeRow(String[] columns) {
		if (m_mode != Mode.READ_ONLY_LIST) {
			super.writeRow(columns);
			return;
		}
		if (m_show_group_headers) {
			int groups_id = data.getInt("groups_id");
			if (groups_id != m_current_group) {
				m_w.write("<div class=\"").write(m_w.ui.section_head).write("\" style=\"margin-bottom:0;padding:5px 10px;\">");
				if (groups_id == 0)
					m_w.write(Text.pluralize(m_minutes.getMainMeetingLabel()));
				else
					m_w.write(m_r.db.lookupString(new Select("name").from("groups").whereIdEquals(groups_id)));
				m_w.write("</div>");
				m_current_group = groups_id;
			}
		}
		super.writeRow(columns);
	}

	//--------------------------------------------------------------------------

	@Override
	public void
	write() {
		if (!m_request_processed) {
			processRequest(this, m_r).write();
			return;
		}
		if (getMode() != Mode.READ_ONLY_LIST) {
			m_w.style("div [data-view=\"group_minutes\"] table.table td:first-child{white-space:nowrap}");
			super.write();
			return;
		}
		m_w.write("<div style=\"text-align:right;\">");
		TSVectorSearch search = new TSVectorSearch(null, null);
		search.init(m_view_def);
		search.write(null, this, m_r);
		m_w.write("</div>");
		super.write();
	}
}
